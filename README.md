RISE - OnRising Recruitment Platform

To RUN (Local):

- Install Docker
- Clone this Repository
- Open terminal and run:
	- docker-compose up -d --build	

WebApp Url: http://localhost:8080 
API Url: http://localhost:8181/swagger