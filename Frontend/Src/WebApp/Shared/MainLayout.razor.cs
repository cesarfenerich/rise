﻿using Common.Exceptions;
using Common.Extensions;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Services;
using WebApp.Services.Interfaces;

namespace WebApp.Shared
{
    public partial class MainLayoutModel : LayoutComponentBase, IDisposable
    {
        [Inject] protected AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }
        [Inject] protected IOwnerService OwnerService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }


        protected SfContextMenu<MenuItem> cxtUserMenu = new SfContextMenu<MenuItem>();

        protected SfSwitch<bool> btnAdmin = new SfSwitch<bool>();
        protected SfSwitch<bool> btnActive = new SfSwitch<bool>();

        protected List<MenuItem> UserActions = new List<MenuItem> { new MenuItem { Text= "Perfil", IconCss="e-icons rise-icon rise-profile-icon", Hidden = false },
                                                                    new MenuItem { Text = "Job Roles", IconCss = "e-icons rise-icon rise-logs-icon", Hidden = true},
                                                                    new MenuItem { Text = "Tech Skills", IconCss = "e-icons rise-icon rise-logs-icon", Hidden = true},
                                                                    new MenuItem { Text = "Gerir Utilizadores", IconCss = "e-icons rise-icon rise-users-icon", Hidden = true},
                                                                    new MenuItem { Text = "Logs", IconCss = "e-icons rise-icon rise-logs-icon", Hidden = true}};      

        protected DialogButton btnInfoDialogOk = new DialogButton();

        public OwnerModel OwnerInfoModel { get; set; } = new OwnerModel();

        public bool ShowOwnerUpdateModal { get; set; }

        public string OwnerUpdateHeader { get; set; } = "Perfil";

        public string OwnerNewPassword { get; set; }

        public string OwnerNewPasswordConfirm { get; set; }

        protected bool ShowInfoDialog { get; set; } = false;
        protected bool IsErrorDialog { get; set; } = false;
        protected bool IsWarningDialog { get; set; } = false;
        protected bool IsSuccessDialog { get; set; } = false;

        protected string InfoDialogHeaderText { get; set; }
        protected string InfoDialogContentText { get; set; }

        protected bool IsAdmin { get; set; } = false;
        protected bool IsActive { get; set; } = false;
        public bool SpinnerVisible { get; set; } = false;
        public string SpinnerLabel { get; set; }

        public async Task SetSpinner(bool show, string label = "A Carregar...")
        {
            this.SpinnerVisible = show;
            this.SpinnerLabel = label;
            await Task.Delay(300);
        }      

        protected async Task GoToHome()
        {          
            _appState.EnableHeaderForHome();
            _navMan.NavigateTo("/home");           
        }

        protected async Task AdvancedSearch()
        {
            await SetSpinner(true, "A Carregar Pesquisa...");
            await Task.Delay(750);
            _appState.EnableHeaderForSearch();
            _navMan.NavigateTo("/interview/search");            
            await SetSpinner(false);
        }

        protected async Task CreateInterview()
        {
            await SetSpinner(true, "A Preparar Criação de Entrevista...");
            _appState.EnableHeaderForCreateInterview();
            _navMan.NavigateTo("/interview/new");
            await Task.Delay(400);
            await SetSpinner(false);
        }

        protected async Task Logout()
        {
            await SetSpinner(true, "A Encerrar...");
            await JsRuntime.InvokeVoidAsync("clearContent");
            _appState.Logout();
            _navMan.NavigateTo("/");
            await SetSpinner(false);
        }

        protected async Task JobRoles()
        {
            await SetSpinner(true, "A Carregar JobRoles...");
            _appState.EnableHeaderForUsers();
            _navMan.NavigateTo("/admin/job_roles");
            await Task.Delay(400);
            await SetSpinner(false);
        }

        protected async Task TechSkills()
        {
            await SetSpinner(true, "A Carregar TechSkills...");
            _appState.EnableHeaderForUsers();
            _navMan.NavigateTo("/admin/tech_skills");
            await Task.Delay(400);
            await SetSpinner(false);
        }

        protected async Task Users()
        {
            await SetSpinner(true, "A Carregar Utilizadores...");
            _appState.EnableHeaderForUsers();
            _navMan.NavigateTo("/admin/users");
            await Task.Delay(400);
            await SetSpinner(false);
        }

        protected async Task Logs()
        {
            await SetSpinner(true, "A Carregar Logs...");
            _appState.EnableHeaderForLogs();
            _navMan.NavigateTo("/admin/logs");
            await Task.Delay(400);
            await SetSpinner(false);
        }

        public async Task PerformUserAction(MenuEventArgs<MenuItem> args)
        {
            if (args.Item != null)
            {
                switch (args.Item.Text)
                {
                    case "Perfil":
                        OpenUserProfile(); break;
                    case "Job Roles":
                        await JobRoles(); break;
                    case "Tech Skills":
                        await TechSkills(); break;
                    case "Gerir Utilizadores": 
                        await Users(); break;
                    case "Logs":
                        await Logs(); break;
                    default:
                        return;
                }               
            }            
        }

        public void OpenUserProfile()
        {
            OwnerInfoModel = OwnerService.GetOwnerById(_appState.OwnerLogged.Id.Value, _appState.AccessToken);

            IsAdmin = OwnerInfoModel.Profile.Equals("OP_0001");

            IsActive = OwnerInfoModel.Active ?? false;

            ShowOwnerUpdateModal = true;
        }

        public void SetupUserActions()
        {
            var isAdmin = _appState.OwnerLogged.Profile != null && 
                          _appState.OwnerLogged.Profile.Equals("OP_0001");

            for (int i = 1; i <= UserActions.Count -1; i++)
            {
                cxtUserMenu.Items[i].Hidden = !isAdmin;
            }                  
        }

        public async Task OpenUserMenu(MouseEventArgs e)
        {
            SetupUserActions();

            this.cxtUserMenu.Open(e.ClientX - 150, e.ClientY + 20);

            StateHasChanged();
        }

        public void ModalOwnerUpdateClose()
        {
            ShowOwnerUpdateModal = false;
            OwnerInfoModel = new OwnerModel();
            OwnerNewPassword = null;
            OwnerNewPasswordConfirm = null;
        }

        public void UpdateOwnerPassword()
        {
            try
            {
                if (!OwnerNewPassword.IsNullOrWhitespaceOrEmpty())
                {
                    if (ValidatePasswordChange(out string validationMessage))
                    {
                        OwnerInfoModel.Password = OwnerNewPassword;
                        OwnerInfoModel.Profile = IsAdmin ? "OP_0001" : "OP_0002";
                        OwnerInfoModel.Active = IsActive;

                        OwnerService.UpdateOwner(OwnerInfoModel, _appState.AccessToken);

                        ShowSuccessDialog("Informações alteradas com Sucesso!");

                        ModalOwnerUpdateClose();

                        OwnerInfoModel.Password = SecurityHelper.Encript(OwnerInfoModel.Password);
                        LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                    }
                    else
                    {
                        ShowWarningDialog(validationMessage);
                    }
                }
                else
                {
                    OwnerInfoModel.Profile = IsAdmin ? "OP_0001" : "OP_0002";
                    OwnerInfoModel.Active = IsActive;

                    OwnerService.UpdateOwner(OwnerInfoModel, _appState.AccessToken);

                    ShowSuccessDialog("Informações alteradas com Sucesso!");

                    ModalOwnerUpdateClose();

                    LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);
                }
            }
            catch (AppException ex)
            {
                if (!string.IsNullOrEmpty(OwnerNewPassword))
                    OwnerInfoModel.Password = SecurityHelper.Encript(OwnerNewPassword);
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(OwnerNewPassword))
                    OwnerInfoModel.Password = SecurityHelper.Encript(OwnerNewPassword);
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        private bool ValidatePasswordChange(out string validationMessage)
        {
            if (OwnerNewPassword.IsNullOrWhitespaceOrEmpty())
            {
                validationMessage = "Password inválida.";
                return false;
            }

            if (OwnerNewPasswordConfirm.IsNullOrWhitespaceOrEmpty())
            {
                validationMessage = "Confirmação de password inválida.";
                return false;
            }

            if (!OwnerNewPasswordConfirm.Equals(OwnerNewPassword))
            {
                validationMessage = "As passwords não conferem.";
                return false;
            }

            if (OwnerNewPassword.Length < 10 || !OwnerNewPassword.Any(char.IsDigit) || !OwnerNewPassword.Any(char.IsUpper) || !CheckSpecialCharacters(OwnerNewPassword))
            {
                validationMessage = "A password não respeita os requisitos. Necessita ter pelo menos 1 maiúscula, 1 número 2 símbolos, e tem de ter um comprimento de 10 algarismos.";
                return false;
            }

            validationMessage = null;
            return true;
        }

        private bool CheckSpecialCharacters(string word)
        {
            char[] original = word.ToCharArray();
            char[] copy = new char[original.Length];
            int c = 0;
            for (int i = 0; i < original.Length; i++)
            {
                if (!char.IsLetterOrDigit(original[i]))
                {
                    copy[c] = original[i];
                    c++;
                }
            }
            Array.Resize(ref copy, c);
            if (copy.Length == 0) return false;
            return true;
        }

        #region InfoDialog 
        public void ShowSuccessDialog(string message)
        {
            ShowInfoDialog = true;
            IsSuccessDialog = true;
            InfoDialogHeaderText = "Sucesso!";
            InfoDialogContentText = message;
        }

        public void ShowWarningDialog(string message)
        {
            ShowInfoDialog = true;
            IsWarningDialog = true;
            InfoDialogHeaderText = "Atenção!";
            InfoDialogContentText = message;
        }

        public void ShowErrorDialog(AppException ex)
        {
            ShowInfoDialog = true;
            IsErrorDialog = true;
            InfoDialogHeaderText = "Erro!";
            InfoDialogContentText = $"{ex.Message} - ({ex.Code})";
        }


        public void HideInfoDialog()
        {
            ShowInfoDialog = false;
            InfoDialogHeaderText = null;
            InfoDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
        }

        #endregion

        public void Dispose()
        {            
            //cxtUserMenu = null;
            btnAdmin = null;
            btnActive = null;
            UserActions = null;
            btnInfoDialogOk = null;
            OwnerInfoModel = null;
            OwnerUpdateHeader = null;
            OwnerNewPassword = null;
            OwnerNewPasswordConfirm = null;
            InfoDialogHeaderText = null;
            InfoDialogContentText = null;
            GC.Collect();
        }
    }
}
