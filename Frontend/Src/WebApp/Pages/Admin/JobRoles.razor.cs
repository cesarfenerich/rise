﻿using Common.Exceptions;
using Common.Models;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Services;
using WebApp.Services.Interfaces;
using WebApp.Shared;

namespace WebApp.Pages.Admin
{
    [Authorize]
    public class JobRolesModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private ICandidateService CandidateService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }
      
        

        protected SfGrid<SelectionModel> JobRolesGrid;
        protected List<SelectionModel> JobRoles { get; set; } = new List<SelectionModel>();
        protected List<string> Actions { get; set; } = new List<string>() { "Add", "Delete", "Cancel", "Update"};
            

        protected override Task OnInitializedAsync()
        {
            try
            {
                LoadJobRoles();

                return base.OnInitializedAsync();
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            try
            {
                if (firstRender)
                    StateHasChanged();
                else
                {                  

                    _appState.EnableHeaderForHome();
                    
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }        

        public void LoadJobRoles()
        {
            JobRoles = CandidateService.GetJobRoles(_appState.AccessToken).ToList();
        }

        public async Task ActionBeginHandler(ActionEventArgs<SelectionModel> arg)
        {
            if (arg.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Save))
            {
                if (arg.Action == "Add")
                {
                    arg.Data.Id = 0;
                    CandidateService.CreateJobRole(arg.Data, _appState.AccessToken);
                }
                else if (arg.Action == "Edit")
                    CandidateService.UpdateJobRole(arg.Data, _appState.AccessToken);        
            }

            if (arg.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Delete))           
                CandidateService.DeleteJobRole(arg.Data.Id.GetValueOrDefault(), _appState.AccessToken);            
        }

        public async Task ActionCompleteHandler(ActionEventArgs<SelectionModel> args)
        {
            if (args.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Save))
                LoadJobRoles();
        }

        public void Dispose()
        {           
            JobRolesGrid = null;
            JobRoles = null;
            Actions = null;

            GC.Collect();
        }
    }
}
