﻿using Common.Exceptions;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Spinner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Services;
using WebApp.Services.Interfaces;

namespace WebApp.Pages.Admin
{
    [Authorize]
    public class UsersModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private IOwnerService OwnerService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }

        protected SfSpinner Spinner;
        string target { get; set; } = "#page";

        protected SfGrid<OwnerModel> OwnersGrid;
        public List<OwnerModel> Owners { get; set; } = new List<OwnerModel>();
        public OwnerModel OwnerInfoModel { get; set; } = new OwnerModel();
        public string OwnerCountInfo { get; set; } = string.Empty;
        protected bool ShowModalEditOwner { get; set; } = false;
        protected bool ShowModalNewOwner { get; set; } = false;
        protected bool ShowModalCreateOwner { get; set; } = false;
        protected SfDialog ModalEditOwner = new SfDialog();
        protected SfDialog ModalNewOwner = new SfDialog();
        public SfDialog ownerDialog = new SfDialog();
        protected SfSwitch<bool> btnAdmin = new SfSwitch<bool>();
        protected SfSwitch<bool> btnActive = new SfSwitch<bool>();
        protected SfSwitch<bool> btnBlock = new SfSwitch<bool>();
        protected SfSwitch<bool> btnNewAdmin = new SfSwitch<bool>();
        protected SfSwitch<bool> btnNewActive = new SfSwitch<bool>();
        public DialogButton btnEdit = new DialogButton();
        public DialogButton btnCancelEdit = new DialogButton();
        public DialogButton btnCreate = new DialogButton();
        public DialogButton btnCancelCreate = new DialogButton();
        public DialogButton btnOwnerDialogOk = new DialogButton();
        protected bool IsAdmin { get; set; } = false;
        protected bool IsActive { get; set; } = false;
        protected bool IsBlock { get; set; } = false;
        protected bool IsNewAdmin { get; set; } = false;
        protected bool IsNewActive { get; set; } = false;
        public bool ShowOwnerDialog { get; set; } = false;
        public string OwnerDialogHeaderText { get; set; } = string.Empty;
        public string OwnerDialogContentText { get; set; } = string.Empty;

        public bool IsErrorDialog { get; set; } = false;
        public bool IsWarningDialog { get; set; } = false;
        public bool IsSuccessDialog { get; set; } = false;

        protected string editCheckPassword { get; set; } = string.Empty;
        protected string editConfirmPassword { get; set; } = string.Empty;
        protected string editNewCheckPassword { get; set; } = string.Empty;
        protected string editNewConfirmPassword { get; set; } = string.Empty;
        protected string createName { get; set; } = string.Empty;
        protected string createEmail { get; set; } = string.Empty;

        public void ShowSpinner()
        {
            Spinner.Show();
        }

        public void HideSpinner()
        {
            Spinner.Hide();
        }

        protected override Task OnInitializedAsync()
        {
            try
            {
                LoadOwners();

                return base.OnInitializedAsync();
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            try
            {
                if (firstRender)
                    StateHasChanged();
                else
                {
                    ShowSpinner();

                    _appState.EnableHeaderForHome();

                    HideSpinner();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        public void CreateOwner()
        {
            ShowSpinner();

            OwnerInfoModel = new OwnerModel();

            ShowModalNewOwner = true;

            HideSpinner();
        }

        public void EditOwner(long ownerId)
        {
            ShowSpinner();

            OwnerInfoModel = OwnerService.GetOwnerById(ownerId, _appState.AccessToken);

            IsAdmin = OwnerInfoModel.Profile.Equals("OP_0001") ? true : false;

            IsActive = OwnerInfoModel.Active.Value ? true : false;

            IsBlock = OwnerService.IsBlock(ownerId, _appState.AccessToken);

            ShowModalEditOwner = true;

            HideSpinner();

        }

        public void ShowSuccessDialog(string message)
        {
            ShowOwnerDialog = true;
            IsSuccessDialog = true;
            OwnerDialogHeaderText = "Sucesso!";
            OwnerDialogContentText = message;
        }

        public void ShowWarningDialog(string message)
        {
            ShowOwnerDialog = true;
            IsWarningDialog = true;
            OwnerDialogHeaderText = "Atenção!";
            OwnerDialogContentText = message;
        }

        public void ShowErrorDialog(AppException ex)
        {
            ShowOwnerDialog = true;
            IsErrorDialog = true;
            OwnerDialogHeaderText = "Erro!";
            OwnerDialogContentText = $"{ex.Message} - ({ex.Code})";
        }

        private bool ValidatePasswordChange(string password, string confirmPassword, out string validationMessage)
        {
            if (string.IsNullOrEmpty(password))
            {
                validationMessage = "Password inválida.";
                return false;
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                validationMessage = "Confirmação de password inválida.";
                return false;
            }

            if (!password.Equals(confirmPassword))
            {
                validationMessage = "As passwords não conferem.";
                return false;
            }

            if (password.Length < 10 || !password.Any(char.IsDigit) || !password.Any(char.IsUpper) || !CheckSpecialCharacters(password))
            {
                validationMessage = "A password não respeita os requisitos. Necessita ter pelo menos 1 maiúscula, 1 número 2 símbolos, e tem de ter um comprimento de 10 algarismos.";
                return false;
            }

            validationMessage = null;
            return true;
        }

        private bool CheckSpecialCharacters(string word)
        {
            char[] original = word.ToCharArray();
            char[] copy = new char[original.Length];
            int c = 0;
            for (int i = 0; i < original.Length; i++)
            {
                if (!char.IsLetterOrDigit(original[i]))
                {
                    copy[c] = original[i];
                    c++;
                }
            }
            Array.Resize(ref copy, c);
            if (copy.Length == 0) return false;
            return true;
        }

        private bool ValidateMinFieldsToCreateOwner()
        {
            var hasName = !string.IsNullOrEmpty(createName);
            var hasEmail = !string.IsNullOrEmpty(createEmail);

            if (hasName && hasEmail)
                return true;
            else
            {
                StringBuilder builder = new StringBuilder("Não é possível criar Utilizador sem os dados mínimos para:");

                if (!hasName)
                    builder.AppendLine("Nome, ");

                if (!hasEmail)
                    builder.AppendLine("Email, ");

                builder.Remove(builder.Length - 4, 2);

                ShowWarningDialog(builder.ToString());
            }

            return false;
        }

        public void HideDialog()
        {
            ShowOwnerDialog = false;
            OwnerDialogHeaderText = null;
            OwnerDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
        }

        public void LoadOwners()
        {
            Owners = OwnerService.GetAllOwners(_appState.AccessToken).ToList();
        }

        protected void Edit()
        {
            ShowSpinner();
            try
            {
                if (!string.IsNullOrEmpty(editConfirmPassword))
                {
                    if (ValidatePasswordChange(editConfirmPassword, editCheckPassword, out string validationMessage))
                    {
                        OwnerInfoModel.Password = editConfirmPassword;
                        OwnerInfoModel.Profile = IsAdmin ? "OP_0001" : "OP_0002";
                        OwnerInfoModel.Active = IsActive;

                        OwnerInfoModel = OwnerService.UpdateOwner(OwnerInfoModel, _appState.AccessToken);

                        LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0008, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                        ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0037"));

                        ShowModalEditOwner = false;
                    }
                    else
                    {
                        ShowWarningDialog(validationMessage);
                    }
                }
                else
                {
                    OwnerInfoModel.Profile = IsAdmin ? "OP_0001" : "OP_0002";
                    OwnerInfoModel.Active = IsActive;

                    OwnerInfoModel = OwnerService.UpdateOwner(OwnerInfoModel, _appState.AccessToken);

                    LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0008, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                    ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0037"));

                    ShowModalEditOwner = false;
                }

            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0008, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0008, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                HideSpinner();
                LoadOwners();
                OwnerInfoModel = new OwnerModel();
                StateHasChanged();

            }
        }

        protected void CancelEdit()
        {
            ShowModalEditOwner = false;
            OwnerInfoModel = new OwnerModel();
        }

        protected void Create()
        {
            ShowSpinner();
            try
            {
                if (ValidateMinFieldsToCreateOwner())
                {
                    if (ValidatePasswordChange(editNewConfirmPassword, editNewCheckPassword, out string validationMessage))
                    {
                        OwnerInfoModel.Name = createName;
                        OwnerInfoModel.Email = createEmail;
                        OwnerInfoModel.Password = editNewConfirmPassword;
                        OwnerInfoModel.Profile = IsNewAdmin ? "OP_0001" : "OP_0002";
                        OwnerInfoModel.Active = IsNewActive;

                        OwnerInfoModel = OwnerService.CreateOwner(OwnerInfoModel, _appState.AccessToken);

                        LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0004, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                        ShowModalNewOwner = false;
                        ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0038"));
                    }
                    else
                    {
                        ShowWarningDialog(validationMessage);
                    }
                }

            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0004, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0004, JsonRequest = JsonConvert.SerializeObject(OwnerInfoModel), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                HideSpinner();
                LoadOwners();
                OwnerInfoModel = new OwnerModel();
                StateHasChanged();

            }
        }

        protected void CancelCreate()
        {
            ShowModalNewOwner = false;
            OwnerInfoModel = new OwnerModel();
        }

        public void Dispose()
        {
            OwnersGrid = null;
            Owners = null;
            OwnerInfoModel = null;
            OwnerCountInfo = null;
            ModalEditOwner = null;
            ModalNewOwner = null;
            ownerDialog = null;
            btnAdmin = null;
            btnActive = null;
            btnBlock = null;
            btnNewAdmin = null;
            btnNewActive = null;
            btnEdit = null;
            btnCancelEdit = null;
            btnCreate = null;
            btnCancelCreate = null;
            btnOwnerDialogOk = null;
            OwnerDialogHeaderText = null;
            OwnerDialogContentText = null;
            editCheckPassword = null;
            editConfirmPassword = null;
            editNewCheckPassword = null;
            editNewConfirmPassword = null;
            createName = null;
            createEmail = null;
            GC.Collect();
        }
    }
}
