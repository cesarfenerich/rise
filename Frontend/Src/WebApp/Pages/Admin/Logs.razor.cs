﻿using Common.Exceptions;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Services.Interfaces;

namespace WebApp.Pages.Admin
{
    [Authorize]
    public class LogsModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private ILogService LogService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }        
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }     

        protected SfGrid<LogModel> LogsGrid;

        protected SfDialog ModalLog = new SfDialog();
        public List<LogModel> Logs { get; set; } = new List<LogModel>();
        protected bool ShowModalLog { get; set; } = false;
        public LogModel LogInfoModel { get; set; } = new LogModel();
        protected string LogDate { get; set; }
        protected string LogEstado { get; set; }

        public void ShowSpinner()
        {
            _appState.IsLoading = true;
        }

        public void HideSpinner()
        {
            _appState.IsLoading = false;
        }

        protected override Task OnInitializedAsync()
        {
            try
            {
                LoadLogs();

                return base.OnInitializedAsync();
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                return base.OnInitializedAsync();
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            try
            {
                if (firstRender)
                    StateHasChanged();
                else
                {
                    ShowSpinner();

                    _appState.EnableHeaderForHome();

                    HideSpinner();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        public void LoadLogs()
        {
            Logs = LogService.GetAllLogs(_appState.AccessToken).ToList();
        }

        public void GetLogSelected(RowSelectEventArgs<LogModel> args)
        {
            if (args != null && args.Data != null)
            {
                ShowSpinner();

                LogInfoModel = LogService.GetLogById(args.Data.Id.Value, _appState.AccessToken);

                LogDate = LogInfoModel.CreatedAt.ToString("dd/MM/yyyy hh:mm");

                LogEstado = LogInfoModel.Status.Equals("Error") ? "Erro" : "Informação";

                ShowModalLog = true;

                HideSpinner();
            }
        }

        public void Dispose()
        {
            LogsGrid = null;
            ModalLog = null;
            Logs = null;
            LogInfoModel = null;
            LogDate = null;
            LogEstado = null;
            GC.Collect();
        }

    }
}
