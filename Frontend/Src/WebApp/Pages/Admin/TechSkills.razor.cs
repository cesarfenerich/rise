﻿using Common.Extensions;
using Common.Models;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Services;
using WebApp.Services.Interfaces;

namespace WebApp.Pages.Admin
{
    [Authorize]
    public class TechSkillsModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private ICandidateService CandidateService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }          

        protected SfGrid<TechSkillModel> TechSkillsGrid;
        protected List<TechSkillModel> TechSkills { get; set; } = new List<TechSkillModel>();
        protected List<string> Actions { get; set; } = new List<string>() { "Add", "Delete", "Cancel", "Update"};
        protected SelectionModel[] TechSkillsCategories { get; set; }
        protected List<string> TechSkillsCategoriesOptions { get; set; } = new List<string>();
        protected string SelectedTechSkillCategory { get; set; }              

        protected override Task OnInitializedAsync()
        {
            try
            {
                LoadTechSkills();

                return base.OnInitializedAsync();
            }           
            catch (Exception ex)
            {
                LogTechSkillsAction(true, null, ex.Message);
                return base.OnInitializedAsync();
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            try
            {
                if (firstRender)
                    StateHasChanged();
                else                               
                    _appState.EnableHeaderForHome();               
            }           
            catch (Exception ex)
            {
                LogTechSkillsAction(true, null, ex.Message);
            }
        }        

        public void LoadTechSkills()
        {
            TechSkills = CandidateService.GetTechSkills(_appState.AccessToken).ToList();

            this.TechSkillsCategories = CandidateService.GetAllTechSkillCategories(_appState.AccessToken);
            TechSkillsCategoriesOptions = TechSkillsCategories.Select(x => x.Value).ToList();

            foreach (var techSkill in TechSkills)            
                techSkill.Category = this.TechSkillsCategories.FirstOrDefault(x => x.ConstantId.Equals(techSkill.Category) || 
                                                                                   x.Value.Equals(techSkill.Category)).Value;            
        }        

        public async Task ActionBeginHandler(ActionEventArgs<TechSkillModel> arg)
        {
            if (arg.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Save))
            {
                if (arg.Action == "Add")
                {
                    arg.Data.Id = 0;                                                                                 
                    arg.Data.Category = SelectedTechSkillCategory;
                    CandidateService.CreateTechSkill(arg.Data, _appState.AccessToken);
                }
                else if (arg.Action == "Edit")
                {                    
                    arg.Data.Category = SelectedTechSkillCategory;
                    CandidateService.UpdateTechSkill(arg.Data, _appState.AccessToken);
                }
            }

            if (arg.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Delete))           
                CandidateService.DeleteJobRole(arg.Data.Id, _appState.AccessToken);            
        }

        public void ChangeCategory(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            SelectedTechSkillCategory = null;

            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {               
                var category = TechSkillsCategories.FirstOrDefault(x => x.Value.Equals(args.Value));               
                SelectedTechSkillCategory = category.ConstantId;                
            } 
        }

        public async Task ActionCompleteHandler(ActionEventArgs<TechSkillModel> args)
        {
            if (args.RequestType.Equals(Syncfusion.Blazor.Grids.Action.Save))
                LoadTechSkills();
        }

        public void LogTechSkillsAction(bool isAnError, string request, string message)
        {
            LogService.CreateLog(httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(),
                                 _appState.OwnerLogged.Name,
                                 null,
                                 LogOperations.LO_0015,
                                 isAnError ? string.Empty : request ?? string.Empty,
                                 isAnError ? LogStatus.LS_0001 : LogStatus.LS_0002,
                                 message, _appState.AccessToken);        
        }        

        public void Dispose()
        {            
            TechSkillsGrid = null;
            TechSkills = null;
            Actions = null;
            TechSkillsCategories = null;
            TechSkillsCategoriesOptions = null;

            GC.Collect();
        }
    }
}
