﻿using Common.Exceptions;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Components;
using WebApp.Services;
using WebApp.Services.Interfaces;

namespace WebApp.Pages.Home
{
    [Authorize]
    public partial class HomeModel : ComponentBase, IDisposable
    {
        [Inject] private NavigationManager _navMan { get; set; }
        [Inject] protected AppState _appState { get; set; }
        [Inject] private IInterviewService InterviewService { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }

        protected SfGrid<InterviewGridModel> InterviewsGrid;
        protected InterviewDialog interviewDialog { get; set; } = new InterviewDialog();
        protected ArchiveInterviewDialog archiveInterviewDialog { get; set; } = new ArchiveInterviewDialog();
        public List<InterviewGridModel> Interviews { get; set; } = new List<InterviewGridModel>();
        public List<string> InterviewStates { get; set; } = new List<string>();

        public string InterviewCountInfo { get; set; } = string.Empty;

        #region Spinner 

        public bool SpinnerVisible { get; set; } = false;
        public string SpinnerLabel { get; set; }

        public async Task SetSpinner(bool show, string label = "A Carregar...")
        {
            this.SpinnerVisible = show;
            this.SpinnerLabel = label;
            await Task.Delay(300);
        }

        #endregion

        protected override async Task OnInitializedAsync()
        {
            try
            {
                await SetSpinner(true, "Carregando Entrevistas..");

                Interviews = InterviewService.GetLatestInterviews(_appState.AccessToken).ToList();
                InterviewStates = Interviews.Select(x => x.State).Distinct().ToList();

                InterviewCountInfo = $"{InterviewService.GetInterviewCount(_appState.AccessToken).Value} Entrevistas Registadas";

                await SetSpinner(false);
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);                
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                await base.OnInitializedAsync();
            }
        }

        protected override async void OnAfterRender(bool firstRender)
        {
            try
            {   
                if (firstRender)
                {
                    StateHasChanged();
                    await JsRuntime.InvokeVoidAsync("setAppHeightToAuto");
                }
                else
                    _appState.EnableHeaderForHome();              

            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        public async Task GetInterviewDoubleSelected(RecordDoubleClickEventArgs<InterviewGridModel> args)
        {
            if (args != null && args.RowData != null)
            {
                await SetSpinner(true);

                var interviewInfo = InterviewService.GetInterviewInfoById(args.RowData.Id, _appState.AccessToken);

                if (interviewInfo != null)
                    interviewDialog.Show(interviewInfo);

                await InterviewsGrid.ClearSelection();

                await SetSpinner(false);
            }
        }

        public async Task CreateInterview()
        {
            await SetSpinner(true);
            _navMan.NavigateTo("/interview/new");
        }

        public void EditArchivedInterview(long id)
        {
            var interview = InterviewService.GetInterviewById(id, _appState.AccessToken);
            archiveInterviewDialog.Show(interview, RemoveArchivedInterview);            
        }

        public async Task RemoveArchivedInterview()
        {
            var records = await this.InterviewsGrid.GetSelectedRecords();
           
            this.Interviews.Remove(records[0]);
            await this.InterviewsGrid.DeleteRecord("Id", records[0]);            
            this.InterviewsGrid.Refresh();
            this.InterviewsGrid.ForceUpdate = true;          
        }

        public void Dispose()
        {
            InterviewsGrid = null;
            Interviews = null;
            InterviewCountInfo = null;
            interviewDialog = null;
            archiveInterviewDialog = null;
            GC.Collect();
        }
    }
}
