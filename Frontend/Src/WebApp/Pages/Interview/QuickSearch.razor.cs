﻿using Common.Exceptions;
using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Newtonsoft.Json.Linq;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Spinner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Components;
using WebApp.Helpers;
using WebApp.Services;
using WebApp.Services.Interfaces;

namespace WebApp.Pages.Interview
{
    [Authorize]
    public partial class QuickSearchModel : ComponentBase, IDisposable
    {
        [Inject] protected AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }

        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected IInterviewService InterviewService { get; set; }
        [Inject] protected ICandidateService CandidateService { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }


        #region Screen Components and Behaviour Components Reference Objects 

        protected SfSpinner Spinner = new SfSpinner();
        protected SfAutoComplete<string, string> txtSearchBox { get; set; } = new SfAutoComplete<string, string>();
        public SfGrid<InterviewGridModel> interviewsGrid { get; set; } = new SfGrid<InterviewGridModel>();
        protected InterviewDialog interviewDialog { get; set; } = new InterviewDialog();

        #endregion


        #region Page Models Setup

        public List<string> SearchResultOptions { get; set; } = new List<string>();
        public List<BaseSearchModel> SearchResults { get; set; } = new List<BaseSearchModel> { new BaseSearchModel("Nome", string.Empty),
                                                                                               new BaseSearchModel("Email", string.Empty),
                                                                                               new BaseSearchModel("JobRole", string.Empty)};
        public List<InterviewGridModel> Interviews { get; set; } = new List<InterviewGridModel>();
        public string SearchTextField { get; set; } = null;
        public string SearchText { get; set; } = null;
        public bool ShowSearchResults { get; set; } = false;
        public BaseSearchModel SelectedResult { get; set; } = new BaseSearchModel();

        #endregion

        public void ShowSpinner()
        {
            Spinner.Show();
        }

        public void HideSpinner()
        {
            Spinner.Hide();
        }

        protected override async void OnAfterRender(bool firstRender)
        {
            try
            {
                if (firstRender)
                    StateHasChanged();
                else
                {
                    if (!ShowSearchResults)
                        await txtSearchBox.FocusIn();

                    _appState.EnableHeaderForQuickSearch();

                    HideSpinner();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }


        public void AbortSearch()
        {
            _appState.EnableHeaderForHome();
            StateHasChanged();
            ShowSpinner();
            _navMan.NavigateTo("/home");
        }

        public void InitSuggestion()
        {
            ShowSearchResults = false;
            SearchText = null;
            txtSearchBox.Clear();
            SearchResultOptions = new List<string>();
        }

        public void GetSuggestions(FilteringEventArgs args)
        {
            if (!args.Text.IsNullOrWhitespaceOrEmpty() && args.Text != SearchText)
            {
                SearchText = args.Text;
                ShowSearchResults = false;
                SetSearchResults();
            }
        }

        private void SetSearchResults()
        {
            SearchResultOptions = new List<string>();

            foreach (var result in SearchResults)
            {
                SearchResultOptions.Add($"Pequisar por {result.Field}: {SearchText}");
            }
        }

        public async void SearchByTerm(Syncfusion.Blazor.DropDowns.SelectEventArgs<string> args)
        {
            try
            {
                if (args.ItemData != null)
                {
                    ShowSpinner();

                    ShowSearchResults = false;

                    var item = (JObject)args.ItemData;

                    var model = SearchResults.FirstOrDefault(x => item["value"].ToString().Contains(x.Field));

                    if (model != null)
                    {
                        Interviews = InterviewService.DoQuickSearch(model.Field ?? string.Empty, SearchText ?? string.Empty, _appState.AccessToken)?.ToList() ?? new List<InterviewGridModel>();

                        ShowSearchResults = true;

                        await txtSearchBox.Clear();

                        await txtSearchBox.FocusOut();
                    }

                    HideSpinner();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        public void GetInterviewSelected(RowSelectEventArgs<InterviewGridModel> args)
        {
            if (args != null && args.Data != null)
            {
                ShowSpinner();

                var interviewInfo = InterviewService.GetInterviewInfoById(args.Data.Id, _appState.AccessToken);

                if (interviewInfo != null)
                    interviewDialog.Show(interviewInfo);

                HideSpinner();

                interviewsGrid.ClearSelection();
            }
        }

        public void ClearMessages()
        {
            ShowSearchResults = false;
            interviewDialog.Hide();        
        }

        public string GetEmptyMessage()
        {
            return ErrorHelper.GetErrorMessage("APP_0026");
        }

        public void Dispose()
        {
            txtSearchBox = null;
            interviewsGrid = null;
            SearchResultOptions = null;
            SearchResults = null;
            Interviews = null;
            SearchTextField = null;
            SearchText = null;
            SelectedResult = null;
            interviewDialog = null;
            GC.Collect();
        }
    }
}
