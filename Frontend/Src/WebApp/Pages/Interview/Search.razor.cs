﻿using Common.Exceptions;
using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Inputs;
using Syncfusion.Blazor.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Components;
using WebApp.Services;
using WebApp.Services.Interfaces;
using WebApp.Shared;

namespace WebApp.Pages.Interview
{
    [Authorize]
    public partial class SearchModel : ComponentBase, IDisposable
    {
        #region Injection

        [Inject] protected AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }

        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected IInterviewService InterviewService { get; set; }
        [Inject] protected ICandidateService CandidateService { get; set; }
        [Inject] protected IOwnerService OwnerService { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }
        [CascadingParameter] protected MainLayout _mainLayout { get; set; }     

        #endregion

        #region Screen Components and Behaviour Components Reference Objects 

        protected SfTextBox txtName = new SfTextBox();

        protected SfDropDownList<string, string> ddlAvailability = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlSalaryExpected = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlContractType = new SfDropDownList<string, string>();

        protected SfDropDownList<string, string> ddlOwner = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlBusinessArea = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlYearsOfExperience = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlLanguages = new SfDropDownList<string, string>();

        protected SfDropDownList<string, string> ddlProfile = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlProgramming = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlSystems = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlDatabases = new SfDropDownList<string, string>();

        protected SfDropDownList<string, string> ddlTests = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlBusinessIntelligence = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlFrameworks = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlCMS = new SfDropDownList<string, string>();

        protected SfDropDownList<string, string> ddlSAP = new SfDropDownList<string, string>();
        protected SfDropDownList<string, string> ddlMethodologies = new SfDropDownList<string, string>();

        protected SfButton btnApplyFilters = new SfButton();
        protected SfButton btnClearFilters = new SfButton();
        public DialogButton btnModalAlertOk = new DialogButton();

        protected SfGrid<InterviewGridModel> interviewsGrid = new SfGrid<InterviewGridModel>(); 

        protected SfChip clsTechnologies = new SfChip();
        protected SfChip clsLanguages = new SfChip();

        protected InterviewDialog interviewDialog { get; set; } = new InterviewDialog();

        #endregion

        #region PageModel Setup

        protected SelectionModel[] Availabilities;
        protected SelectionModel[] SalaryRange;
        protected SelectionModel[] ContractTypes;
        protected OwnerModel[] Owners;
        protected SelectionModel[] YearsOfExperienceRange;
        protected SelectionModel[] JobRoles;
        protected SelectionModel[] Categories;
        protected TechSkillModel[] TechSkills;
        protected SelectionModel[] Languages;

        public bool MatchFieldsOfSearch { get; set; } = true;
        public string CandidateName { get; set; } = string.Empty;
        public string SelectedAvailability { get; set; } = string.Empty;
        public string SelectedSalaryExpected { get; set; } = string.Empty;
        public string SelectedContractType { get; set; } = string.Empty;
        public string SelectedOwner { get; set; } = string.Empty;
        public string SelectedYearOfExperience { get; set; } = string.Empty;
        public string CandidateLinkdin { get; set; } = string.Empty;
        public string Location { get; set; } = string.Empty;

        protected List<string> CandidateAvailabilityOptions { get; set; } = new List<string>();
        protected List<string> SalaryExpectedOptions { get; set; } = new List<string>();
        protected List<string> ContractTypeOptions { get; set; } = new List<string>();
        protected List<string> OwnerOptions { get; set; } = new List<string>();
        protected List<string> BusinessAreaTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> YearsOfExperienceOptions { get; set; } = new List<string>();
        protected List<string> ProfileTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> ProgrammingTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> SystemsTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> DatabasesTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> TestsTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> BITechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> FrameworksTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> CMSTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> SAPTechSkillCategoryOptions { get; set; } = new List<string>();
        protected List<string> MethodologiesTechSkillCategoryOptions { get; set; } = new List<string>();
        protected IEnumerable<string> LanguageOptions { get; set; } = new List<string>();

        public List<InterviewGridModel> Interviews { get; set; } = new List<InterviewGridModel>();
        protected List<TechSkillModel> SelectedTechnologies { get; set; } = new List<TechSkillModel>();
        protected List<SelectionModel> SelectedLanguages { get; set; } = new List<SelectionModel>();

        public bool ShowSearchResults { get; set; } = false;
        public bool ShowModalAlert { get; set; } = false;

        public bool IsErrorModal { get; set; } = false;
        public bool IsWarningModal { get; set; } = false;
        public bool IsSuccessModal { get; set; } = false;

        public string ModalAlertHeaderText { get; set; } = string.Empty;
        public string ModalAlertContentText { get; set; } = string.Empty;

        #endregion

        #region Init and Load

        protected override void OnInitialized()
        {
            try
            {
                base.OnInitialized();

                LoadInitialData();
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorAlert(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
                StateHasChanged();
        }

        private void LoadInitialData()
        {
            this.Availabilities = CandidateService.GetCandidateAvailabilities(_appState.AccessToken);
            this.CandidateAvailabilityOptions = Availabilities.Select(x => x.Value).ToList();

            this.SalaryRange = InterviewService.GetSalaryExpectationRange(_appState.AccessToken);
            this.SalaryExpectedOptions = SalaryRange.Select(x => x.Value).ToList();

            this.ContractTypes = InterviewService.GetContractTypes(_appState.AccessToken);
            this.ContractTypeOptions = ContractTypes.Select(x => x.Value).ToList();

            this.Owners = OwnerService.GetAllOwners(_appState.AccessToken);
            this.OwnerOptions = Owners.Select(x => x.Name).ToList();

            this.YearsOfExperienceRange = InterviewService.GetYearsOfExperienceRange(_appState.AccessToken);
            this.YearsOfExperienceOptions = YearsOfExperienceRange.Select(x => x.Value).ToList();

            this.Categories = CandidateService.GetAllTechSkillCategories(_appState.AccessToken);
            this.TechSkills = CandidateService.GetTechSkills(_appState.AccessToken);

            this.Languages = CandidateService.GetLanguages(_appState.AccessToken);
            this.LanguageOptions = this.Languages.Select(language => language.Value)
                                                 .Where(language=> !language.Contains("Outro"));

            var profileId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0001)).ConstantId;
            this.ProfileTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(profileId)).Select(y => y.Description).ToList();

            var businessAreaId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0002)).ConstantId;
            this.BusinessAreaTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(businessAreaId)).Select(y => y.Description).ToList();

            var programmingId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0003)).ConstantId;
            this.ProgrammingTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(programmingId)).Select(y => y.Description).ToList();

            var systemsId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0004)).ConstantId;
            this.SystemsTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(systemsId)).Select(y => y.Description).ToList();

            var databasesId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0005)).ConstantId;
            this.DatabasesTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(databasesId)).Select(y => y.Description).ToList();

            var testsId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0006)).ConstantId;
            this.TestsTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(testsId)).Select(y => y.Description).ToList();

            var businessIntelligenceId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0007)).ConstantId;
            this.BITechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(businessIntelligenceId)).Select(y => y.Description).ToList();

            var frameworksId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0008)).ConstantId;
            this.FrameworksTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(frameworksId)).Select(y => y.Description).ToList();

            var cmsId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0009)).ConstantId;
            this.CMSTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(cmsId)).Select(y => y.Description).ToList();

            var sapId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0010)).ConstantId;
            this.SAPTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(sapId)).Select(y => y.Description).ToList();

            var methodologiesId = Categories.FirstOrDefault(x => x.Value.Equals(TechSkillCategories.TSC_0011)).ConstantId;
            this.MethodologiesTechSkillCategoryOptions = TechSkills.Where(x => x.Category.Equals(methodologiesId)).Select(y => y.Description).ToList();

        }

        #endregion

        #region Behaviour

        #region Spinner 
        public bool SpinnerVisible { get; set; } = false;
        public string SpinnerLabel { get; set; }

        public async Task SetSpinner(bool show, string label = "A Carregar...")
        {
            this.SpinnerVisible = show;
            this.SpinnerLabel = label;
            await Task.Delay(300);
        }

        public async Task ShowSpinner()
        {
            await SetSpinner(true);
        }

        public async Task HideSpinner()
        {
            await SetSpinner(false);
        }

        #endregion

        #region Actions

        public async Task ApplyFilters()
        {
            try
            {
                if (!ValidateSearch())
                    ShowWarningAlert(ErrorHelper.GetErrorMessage("APP_0025"));
                else
                {
                    await ShowSpinner();

                    var owner = Owners.FirstOrDefault(x => x.Name.Equals(SelectedOwner));

                    Interviews = InterviewService.DoAdvancedSearch(new AdvancedSearchModel(MatchFieldsOfSearch,
                                                                                           CandidateName,
                                                                                           Availabilities.FirstOrDefault(x => x.Value.Equals(SelectedAvailability))?.ConstantId,
                                                                                           SalaryRange.FirstOrDefault(x => x.Value.Equals(SelectedSalaryExpected))?.ConstantId,
                                                                                           YearsOfExperienceRange.FirstOrDefault(x => x.Value.Equals(SelectedYearOfExperience))?.ConstantId,
                                                                                           ContractTypes.FirstOrDefault(x => x.Value.Equals(SelectedContractType))?.ConstantId,
                                                                                           owner?.Id,
                                                                                           SelectedTechnologies.Select(x => x.Id),
                                                                                           CandidateLinkdin,
                                                                                           Location,
                                                                                           SelectedLanguages.Select(x => (long)x.Id)), _appState.AccessToken).ToList();
                    StateHasChanged();
                    this.ShowSearchResults = true;

                    await HideSpinner();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorAlert(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0006, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        private bool ValidateSearch()
        {
            if (CandidateName.IsNullOrWhitespaceOrEmpty() &&
                SelectedAvailability.IsNullOrWhitespaceOrEmpty() &&
                SelectedSalaryExpected.IsNullOrWhitespaceOrEmpty() &&
                SelectedYearOfExperience.IsNullOrWhitespaceOrEmpty() &&
                SelectedContractType.IsNullOrWhitespaceOrEmpty() &&
                SelectedOwner.IsNullOrWhitespaceOrEmpty() &&
                SelectedTechnologies.Count == 0 &&
                CandidateLinkdin.IsNullOrWhitespaceOrEmpty() &&
                Location.IsNullOrWhitespaceOrEmpty() &&
                !SelectedLanguages.Any())
                return false;

            return true;
        }

        public async Task ClearFilters()
        {
            await ShowSpinner();

            ShowSearchResults = false;            
            CandidateName = null;
            SelectedAvailability = null;
            SelectedSalaryExpected = null;
            SelectedYearOfExperience = null;
            SelectedContractType = null;
            SelectedOwner = null;
            SelectedTechnologies = new List<TechSkillModel>();
            Interviews = new List<InterviewGridModel>();
            SelectedLanguages = new List<SelectionModel>();
            CandidateLinkdin = null;
            Location = null;

            clsTechnologies.Chips.Clear();
            clsLanguages.Chips.Clear();

            if (ShowSearchResults)
                interviewsGrid.Refresh();        
            
            interviewDialog.Hide();
            await HideSpinner();
        }

        public async Task AbortSearch()
        {
            Focus("page");

            await ShowSpinner();

            _appState.EnableHeaderForHome();
            
            _navMan.NavigateTo("/home");            

            await HideSpinner();
        }

        #region DropDownLists Add Methods

        protected void AddLanguage(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string,string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                var value = args.Value;
                bool exists = clsLanguages.Chips.Any(x => x.Value.Equals(value));
                
                if (!exists)
                {
                    var languageToAdd = Languages.FirstOrDefault(x => x.Value.Equals(value));
                    if(languageToAdd?.Id != null)
                    {
                        var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = languageToAdd.Id.ToString(), Text = value };
                        clsLanguages.AddChip(model);
                        SelectedLanguages.Add(languageToAdd);
                    }
                }

                ddlLanguages.Clear();
                StateHasChanged();
            }
        }

        protected void AddBusinessArea(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlBusinessArea.Clear();
                StateHasChanged();
            }
        }

        protected void AddProfile(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlProfile.Clear();
                StateHasChanged();
            }
        }

        protected void AddProgramming(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlProgramming.Clear();
                StateHasChanged();
            }
        }

        protected void AddSystem(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlSystems.Clear();
                StateHasChanged();
            }
        }

        protected void AddDatabase(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlDatabases.Clear();
                StateHasChanged();
            }
        }

        protected void AddTest(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlTests.Clear();
                StateHasChanged();
            }
        }

        protected void AddBusinessIntelligence(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlBusinessIntelligence.Clear();
                StateHasChanged();
            }
        }

        protected void AddFramework(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlFrameworks.Clear();
                StateHasChanged();
            }
        }

        protected void AddCMS(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlCMS.Clear();
                StateHasChanged();
            }
        }

        protected void AddSAP(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlSAP.Clear();
                StateHasChanged();
            }
        }

        protected void AddMethodology(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                AddTech(args.Value);
                ddlMethodologies.Clear();
                StateHasChanged();
            }
        }

        private void AddTech(string value)
        {
            bool exists = clsTechnologies.Chips.Any(x => x.Value.Equals(value));

            if (!exists)
            {
                var skillToAdd = TechSkills.FirstOrDefault(x => x.Description.Equals(value));
                var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = skillToAdd.Description, Text = skillToAdd.Description };

                clsTechnologies.AddChip(model);
                SelectedTechnologies.Add(skillToAdd);
            }
        }

        #endregion        

        #endregion

        #region Events

        protected void RemoveSelectedTechnology(Syncfusion.Blazor.Buttons.ChipEventArgs args)
        {
            var techToRemove = SelectedTechnologies.FirstOrDefault(x => x.Description.Equals(args.Text));

            if (techToRemove != null)
                SelectedTechnologies.Remove(techToRemove);
        }

        protected void RemoveSelectedLanguages(Syncfusion.Blazor.Buttons.ChipEventArgs args)
        {
            var languageToRemove = SelectedLanguages.FirstOrDefault(x => x.Equals(long.Parse(args.Value)));

            if (languageToRemove != null)
                SelectedLanguages.Remove(languageToRemove);
        }


        #endregion        

        #endregion

        #region InterviewInfo Dialog


        //public async Task GetInterviewSelected(RowSelectEventArgs<InterviewGridModel> args)
        //{
        //    if (args != null && args.Data != null)
        //    {
        //        await ShowSpinner();

        //        var interviewInfo = InterviewService.GetInterviewInfoById(args.Data.Id, _appState.AccessToken);

        //        if (interviewInfo != null)
        //            interviewDialog.Show(interviewInfo);

        //        interviewsGrid.ClearSelection();                

        //        await HideSpinner();
        //    }
        //}


        public async Task GetInterviewDoubleSelected(RecordDoubleClickEventArgs<InterviewGridModel> args)
        {
            if (args != null && args.RowData != null)
            {
                await SetSpinner(true);

                var interviewInfo = InterviewService.GetInterviewInfoById(args.RowData.Id, _appState.AccessToken);

                if (interviewInfo != null)
                    interviewDialog.Show(interviewInfo);

                await interviewsGrid.ClearSelection();

                await HideSpinner();
            }
        }

        #endregion

        #region AlertDialog

        public void HideModalAlert()
        {
            ShowModalAlert = false;
            ModalAlertHeaderText = null;
            ModalAlertContentText = null;
            IsSuccessModal = false;
            IsWarningModal = false;
            IsErrorModal = false;
        }

        public void ShowErrorAlert(AppException ex)
        {
            ShowModalAlert = true;
            IsErrorModal = true;
            ModalAlertHeaderText = "Erro!";
            ModalAlertContentText = $"{ex.Message} - ({ex.Code})";
        }

        public void ShowWarningAlert(string message)
        {
            ShowModalAlert = true;
            IsWarningModal = true;
            ModalAlertHeaderText = "Atenção!";
            ModalAlertContentText = message;
        }

        #endregion        

        public void Focus(string elementId)
        {
            JsRuntime.InvokeVoidAsync("jsfunction.focusElement", elementId);
        }

        public string GetEmptyMessage()
        {
            return ErrorHelper.GetErrorMessage("APP_0026");
        }

        public void Dispose()
        {
            txtName = null;
            ddlAvailability = null;
            ddlSalaryExpected = null;
            ddlContractType = null;
            ddlOwner = null;
            ddlBusinessArea = null;
            ddlYearsOfExperience = null;
            ddlProfile = null;
            ddlProgramming = null;
            ddlSystems = null;
            ddlDatabases = null;
            ddlTests = null;
            ddlBusinessIntelligence = null;
            ddlFrameworks = null;
            ddlCMS = null;
            ddlSAP = null;
            ddlMethodologies = null;
            btnApplyFilters = null;
            btnClearFilters = null;
            btnModalAlertOk = null;
            interviewsGrid = null;
            clsTechnologies = null;
            Availabilities = null;
            SalaryRange = null;
            ContractTypes = null;
            Owners = null;
            YearsOfExperienceRange = null;
            JobRoles = null;
            Categories = null;
            TechSkills = null;
            CandidateName = null;
            SelectedAvailability = null;
            SelectedSalaryExpected = null;
            SelectedContractType = null;
            SelectedOwner = null;
            SelectedYearOfExperience = null;
            CandidateLinkdin = null;
            CandidateAvailabilityOptions = null;
            SalaryExpectedOptions = null;
            ContractTypeOptions = null;
            OwnerOptions = null;
            BusinessAreaTechSkillCategoryOptions = null;
            YearsOfExperienceOptions = null;
            ProfileTechSkillCategoryOptions = null;
            ProgrammingTechSkillCategoryOptions = null;
            SystemsTechSkillCategoryOptions = null;
            DatabasesTechSkillCategoryOptions = null;
            TestsTechSkillCategoryOptions = null;
            BITechSkillCategoryOptions = null;
            FrameworksTechSkillCategoryOptions = null;
            CMSTechSkillCategoryOptions = null;
            SAPTechSkillCategoryOptions = null;
            MethodologiesTechSkillCategoryOptions = null;
            Interviews = null;
            SelectedTechnologies = null;
            ModalAlertHeaderText = null;
            ModalAlertContentText = null;
            Location = null;
            interviewDialog = null;
            ddlLanguages = null;          
            SelectedLanguages = null;
            GC.Collect();
        }
    }
}
