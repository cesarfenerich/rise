﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Buttons;
using System;
using Microsoft.JSInterop;
using Common.Models;
using System.Collections.Generic;
using Domain.Models;
using System.Linq;
using WebApp.Services;
using Common.Extensions;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Calendars;
using Syncfusion.Blazor.Data;
using Common.Exceptions;
using Common.Helpers;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Constants;
using Syncfusion.Blazor.Inputs;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Syncfusion.Blazor.Charts;
using DinkToPdf;
using DinkToPdf.Contracts;
using WebApp.Services.Interfaces;
using WebApp.Helpers;
using WebApp.Models.Components;
using System.Text.RegularExpressions;

namespace WebApp.Pages.Interview    
{
    [Authorize]
    public partial class NewModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected IInterviewService InterviewService { get; set; }
        [Inject] protected ICandidateService CandidateService { get; set; }
        [Inject] protected ILogService LogService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }
        [Inject] protected IConfiguration Configuration { get; set; }
        [Inject] protected IConverter _converter { get; set; }      

        [Parameter]
        public string InterviewId { get; set; }

        #region Spinner 

        public bool SpinnerVisible { get; set; } = false;
        public string SpinnerLabel { get; set; }

        public async Task SetSpinner(bool show, string label = "A Carregar...")
        {
            this.SpinnerVisible = show;
            this.SpinnerLabel = label;
            await Task.Delay(300);
        }
        public async Task ShowSpinner()
        {
            await this.SetSpinner(true);
        }

        public async Task HideSpinner()
        {
            await this.SetSpinner(false);
        }


        #endregion

        #region Screen Components and Behaviour Components Reference Objects

        public SfTab tabsInterview = new SfTab();
        protected SfDatePicker<DateTime?> dtpBornDate = new SfDatePicker<DateTime?>();
        public SfDropDownList<string, string> ddlLocation = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlMaritalStatus = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlLiteraryAbilities = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlLanguages = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlJobRoles = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlTechSkillCategory = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlTechSkill = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlContractType = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlAvailability = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlInterval = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlMobility = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlContractFrequency = new SfDropDownList<string, string>();
        protected SfDatePicker<DateTime?> dtpAvailableAt = new SfDatePicker<DateTime?>();
        public SfDropDownList<string, string> ddlCandidateType = new SfDropDownList<string, string>();
        public SfDropDownList<string, string> ddlCandidateExperience = new SfDropDownList<string, string>();
        protected SfUploader upPersonalFiles = new SfUploader();
        protected SfButton btnCompleteInterview = new SfButton();
        protected SfButton btnSaveInterviewAsDraft = new SfButton();
        public SfDialog candidateDialog = new SfDialog();
        public DialogButton btnInterviewDialogOk = new DialogButton();
        public DialogButton btnCandidateDialogYes = new DialogButton();
        public DialogButton btnCandidateDialogNo = new DialogButton();       
        protected SfChip clsLanguages = new SfChip();
        protected SfChip clsTechSkills = new SfChip();
        protected ChartSeriesType SeriesType = ChartSeriesType.Polar;
        protected SfChart CompetenceGraph = new SfChart();

        #endregion

        #region Page Models Setup        

        protected SelectionModel[] Locations;
        protected SelectionModel[] MaritalStatuses;
        protected SelectionModel[] JobRoles;
        protected SelectionModel[] LiteraryAbilities;
        protected SelectionModel[] Languages;
        protected SelectionModel[] TechSkillsCategories;
        protected TechSkillModel[] TechSkills;
        protected SelectionModel[] ContractTypes;
        protected SelectionModel[] CandidateAvailabilities;
        protected SelectionModel[] CandidateIntervals;
        protected SelectionModel[] CandidateMobility;
        protected SelectionModel[] CandidateTypes;
        protected SelectionModel[] CandidateExperience;
        protected SelectionModel[] ContractFrequency;

        protected List<string> LocationOptions = new List<string>();
        protected List<string> MaritalStatusesOptions = new List<string>();
        protected List<string> JobRolesOptions = new List<string>(); 
        protected List<string> LiteraryAbilitiesOptions = new List<string>();
        protected List<string> LanguagesOptions = new List<string>();
        protected List<string> TechSkillsCategoriesOptions = new List<string>();
        protected List<string> TechSkillsOptions = new List<string>();
        protected List<string> ContractTypesOptions = new List<string>();
        protected List<string> CandidateAvailabilitiesOptions = new List<string>();
        protected List<string> CandidateIntervalsOptions = new List<string>();
        protected List<string> CandidateMobilityOptions = new List<string>();
        protected List<string> CandidateTypesOptions = new List<string>();
        protected List<string> CandidateExperienceOptions = new List<string>();
        protected IEnumerable<string> ContractFrequencyOptions = new List<string>();

        protected string SelectedLocation = string.Empty;
        protected string SelectedMaritalStatus = string.Empty;
        protected string SelectedLiteraryAbility = string.Empty;
        protected string SelectedJobRole = string.Empty;
        protected string SelectedTechSkillCategory = string.Empty;
        protected string SelectedTechSkill = string.Empty;
        protected string SelectedContractType = string.Empty;
        protected string SelectedAvailability = string.Empty;
        protected string SelectedInterval = string.Empty;
        protected string SelectedMobility = string.Empty;
        protected string SelectedCandidateType = string.Empty;
        protected string SelectedCandidateExperience = string.Empty;
        protected string SelectedContractFrequencyOptions = string.Empty;
        protected string SelectedContractFrequency = string.Empty;
        protected List<Question> NaifasQuestions = new List<Question>();       
        
        protected bool TypeCurrentSalaryLiquido;
        protected bool TypeCurrentSalaryBruto;
        protected bool TypeExpectedSalaryLiquido;
        protected bool TypeExpectedSalaryBruto;
        protected bool TypeCurrentFrequencyPaymentDaily;
        protected bool TypeCurrentFrequencyPaymentMonthly;
        protected bool TypeExpectFrequencyPaymentDaily;
        protected bool TypeExpectFrequencyPaymentMonthly;

        protected InterviewModel Interview { get; set; } = new InterviewModel();
        protected CandidateModel Candidate { get; set; } = new CandidateModel();
        protected CandidateModel CandidateReference { get; set; } = new CandidateModel();

        protected List<SelectionModel> CandidateLanguages { get; set; } = new List<SelectionModel>();
        protected List<TechSkillModel> CandidateTechSkills { get; set; } = new List<TechSkillModel>();
        protected List<UploadModel> CandidateUploads { get; set; } = new List<UploadModel>();

        public string AddLocationIcon { get; set; } = "add";
        public bool IsAddingLocation { get; set; } = false;
        public string LocationToAdd { get; set; } = string.Empty;

        public string AddJobRoleIcon { get; set; } = "add";
        public bool IsAddingJobRole { get; set; } = false;
        public string JobRoleToAdd { get; set; } = string.Empty;

        public string AddLanguageIcon { get; set; } = "add";
        public bool IsAddingLanguage { get; set; } = false;
        public string LanguageToAdd { get; set; } = string.Empty;

        public string AddTechSkillIcon { get; set; } = "add";
        public bool IsAddingTechSkill { get; set; } = false;
        public string TechSkillToAdd { get; set; } = string.Empty;

        public bool EnableTechSkillDropDown { get; set; } = false;

        public bool ShowInterviewDialog { get; set; } = false;
        public string InterviewDialogHeaderText { get; set; } = string.Empty;
        public string InterviewDialogContentText { get; set; } = string.Empty;

        public bool IsErrorDialog { get; set; } = false;
        public bool IsWarningDialog { get; set; } = false;
        public bool IsSuccessDialog { get; set; } = false;

        public bool ShowCandidateDialog { get; set; } = false;

        public bool IsCandidateAvailable { get; set; } = false;

        public bool CustomInterval { get; set; } = false;

        #region Chart
        public class PolarAreaChartData
        {
            public string XValue { get; set; }
            public double YValue { get; set; }
        }
        public List<PolarAreaChartData> GraphDataSource = new List<PolarAreaChartData>();
        public string CompetenceGraphPath { get; set; } = string.Empty;
        #endregion
        #endregion

        #region Init and Data        
        protected override void OnInitialized()
        {
            try
            {
                base.OnInitialized();               

                LoadInitialData();

                if (!InterviewId.IsNullOrWhitespaceOrEmpty())
                {
                    if (InterviewId.Contains("new"))
                    {
                        var candidateEmail = InterviewId.Split('|')[1];
                        Candidate.Email = candidateEmail;
                        CandidateReference = CandidateService.GetCandidateByReference(new CandidateSearchModel(Candidate.Name, Candidate.Email), _appState.AccessToken);

                        if (CandidateReference != null)
                            ShowLoadCandidateDialog();
                    }
                    else
                        LoadInterviewById();
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0014, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch(Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0014, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }

        }

        protected override async void OnAfterRender(bool firstRender)
        {
            if (firstRender)
                StateHasChanged();
            else
            {
                _appState.EnableHeaderForCreateInterview();

                await HideSpinner();
            }
        }

        private void LoadInitialData()
        {
            this.Locations = CandidateService.GetLocations(_appState.AccessToken);
            this.LocationOptions = Locations.Select(x => x.Value).ToList();

            this.MaritalStatuses = CandidateService.GetMaritalStatuses(_appState.AccessToken);
            this.MaritalStatusesOptions = MaritalStatuses.Select(x => x.Value).ToList();

            this.LiteraryAbilities = CandidateService.GetLiteraryAbilities(_appState.AccessToken);
            this.LiteraryAbilitiesOptions = LiteraryAbilities.Select(x => x.Value).ToList();

            this.Languages = CandidateService.GetLanguages(_appState.AccessToken);
            this.LanguagesOptions = Languages.Select(x => x.Value).ToList();

            this.JobRoles = CandidateService.GetJobRoles(_appState.AccessToken);
            this.JobRolesOptions = JobRoles.Select(x => x.Value).ToList();

            this.TechSkillsCategories = CandidateService.GetAllTechSkillCategories(_appState.AccessToken);
            this.TechSkillsCategoriesOptions = TechSkillsCategories.Select(x => x.Value).ToList();

            this.TechSkills = CandidateService.GetTechSkills(_appState.AccessToken);
            this.TechSkillsOptions = TechSkills.Select(x => x.Description).ToList();

            this.ContractTypes = InterviewService.GetContractTypes(_appState.AccessToken);
            this.ContractTypesOptions = ContractTypes.Select(x => x.Value).ToList();

            this.CandidateAvailabilities = CandidateService.GetCandidateAvailabilities(_appState.AccessToken);
            this.CandidateAvailabilitiesOptions = CandidateAvailabilities.Select(x => x.Value).ToList();

            this.CandidateIntervals = CandidateService.GetCandidateIntervals(_appState.AccessToken);
            this.CandidateIntervalsOptions = CandidateIntervals.Select(x => x.Value).ToList();

            this.CandidateMobility = CandidateService.GetCandidateMobility(_appState.AccessToken);
            this.CandidateMobilityOptions = CandidateMobility.Select(x => x.Value).ToList();

            this.CandidateTypes = CandidateService.GetCandidateTypes(_appState.AccessToken);
            this.CandidateTypesOptions = CandidateTypes.Select(x => x.Value).ToList();

            this.CandidateExperience = InterviewService.GetYearsOfExperienceRange(_appState.AccessToken);
            this.CandidateExperienceOptions = CandidateExperience.Select(x => x.Value).ToList();          

            this.NaifasQuestions = new List<Question> { new Question(1, "Liderança", "Em que medida é importante para si existirem oportunidades para liderar pessoas?", 10),
                                                        new Question(2, "Liderança", "Em que medida considera ter as capacidades necessárias para supervisionar uma equipa?", 10),
                                                        new Question(3, "Trabalho em Equipa", "Em que medida está orientado para trabalhar em equipa?", 10),
                                                        new Question(4, "Trabalho em Equipa", "Em que medida considera um fator importante ter a opinião dos seus colegas de trabalho quando surge uma situação profissional de conflito ou uma situação nova?", 10),
                                                        new Question(5, "Relações Interpessoais", "Em que medida é importante para si dar-se com os seus colegas de trabalho?", 10),
                                                        new Question(6, "Relações Interpessoais", "Em que medida é fácil para si iniciar contatos sociais e construir redes de contatos?", 10),
                                                        new Question(7, "Resiliência", "Em que medida é capaz e está disponível para lidar com novos desafios profissionais, com o stress e com contrariedades a nível profissional?", 10),
                                                        new Question(8, "Resiliência", "Em que medida se sente confortável com a mudança?", 10),
                                                        new Question(9, "Meticulosidade", "Em que medida a precisão e o rigor são importantes para si?", 10),
                                                        new Question(10, "Orientação para Resultados", "Em que medida é exigente consigo próprio e com os objetivos que se propõem em alcançar?", 10)};

        }

        private void LoadNaifas()
        {
            var candidateNaifas = CandidateService.GetNaifasByCandidate(Interview.CandidateId.Value, _appState.AccessToken);

            foreach (var naifa in candidateNaifas)
            {
                Int32.TryParse(naifa.Question, out int order);
                var question = this.NaifasQuestions.FirstOrDefault(x => x.Order.Equals(order));
                question.SetCheckedValue(naifa.Answer);      
            }

            HandleNaifasGraph();
        }       

        private void LoadInterviewById()
        {
            Int64.TryParse(InterviewId, out long interviewId);

            Interview = InterviewService.GetInterviewById(interviewId, _appState.AccessToken);

            ValidateFinishedInterview();

            Candidate = CandidateService.GetCandidateById(Interview.CandidateId.Value, _appState.AccessToken);
            CandidateLanguages = CandidateService.GetLanguagesByCandidate(Candidate.Id.Value, _appState.AccessToken).ToList();
            CandidateTechSkills = CandidateService.GetTechSkillsByCandidate(Candidate.Id.Value, _appState.AccessToken).ToList();
            CandidateUploads = CandidateService.GetUploadsByCandidate(Candidate.Id.Value, _appState.AccessToken).ToList();

            SelectedLocation = Locations.FirstOrDefault(x => x.Id.Equals(Candidate.LocationId))?.Value ?? string.Empty;
            SelectedMaritalStatus = MaritalStatuses.FirstOrDefault(x => x.ConstantId.Equals(Candidate.MaritalStatus))?.Value ?? string.Empty;
            SelectedLiteraryAbility = LiteraryAbilities.FirstOrDefault(x => x.ConstantId.Equals(Candidate.LiteraryAbility))?.Value ?? string.Empty;
            SelectedJobRole = JobRoles.FirstOrDefault(x => x.Id.Equals(Candidate.JobRoleId))?.Value ?? string.Empty;
            SelectedContractType = ContractTypes.FirstOrDefault(x => x.ConstantId.Equals(Interview.ContractType))?.Value ?? string.Empty;
            SelectedAvailability = CandidateAvailabilities.FirstOrDefault(x => x.ConstantId.Equals(Candidate.Available))?.Value ?? string.Empty;
            if (SelectedAvailability.Equals(CandidateAvailability.CAV_0002))
                IsCandidateAvailable = true;
            SelectedInterval = CandidateIntervals.FirstOrDefault(x => x.ConstantId.Equals(Candidate.Interval))?.Value ?? string.Empty;
            if (SelectedInterval.Equals(CandidateInterval.CINT_0005))
                CustomInterval = true;
            SelectedMobility = CandidateMobility.FirstOrDefault(x => x.ConstantId.Equals(Candidate.Mobility))?.Value ?? string.Empty;
            SelectedCandidateType = CandidateTypes.FirstOrDefault(x => x.ConstantId.Equals(Candidate.Type))?.Value ?? string.Empty;
            var candidateExperience = InterviewHelper.ConvertToExperience(Candidate.YearsOfExperience);
            SelectedCandidateExperience = CandidateExperience.FirstOrDefault(x=>x.ConstantId.Equals(candidateExperience))?.Value ?? string.Empty;           

            if (Interview.IsCurrentSalaryNet.HasValue && Interview.IsCurrentSalaryNet.Value == true)
                TypeCurrentSalaryLiquido = true;
            else
                TypeCurrentSalaryBruto = true;

            if (Interview.IsExpectationSalaryNet.HasValue && Interview.IsExpectationSalaryNet.Value == true)
                TypeExpectedSalaryLiquido = true;
            else
                TypeExpectedSalaryBruto = true;

            TypeCurrentFrequencyPaymentDaily = !Interview?.IsCurrentMonthlyPayment ?? false;
            TypeCurrentFrequencyPaymentMonthly = Interview?.IsCurrentMonthlyPayment ?? false;

            TypeExpectFrequencyPaymentDaily = !Interview?.IsExpectationMonthlyPayment?? false;
            TypeExpectFrequencyPaymentMonthly = Interview?.IsExpectationMonthlyPayment??false;

            LoadNaifas();
        }

        private void ValidateFinishedInterview()
        {
            if (Interview.Status.Equals("IS_0002"))
            {
                if (Interview.OwnerId == _appState.OwnerLogged.Id.Value)
                {
                    _appState.EnableHeaderForCreateInterview();
                    _navMan.NavigateTo($"/interview/{Interview.Id}");
                }
                else
                {
                    _appState.EnableHeaderForCandidateView();
                    _navMan.NavigateTo($"/candidate/{Interview.CandidateId}");
                }
            }
        }

        private void LoadLastInterview(CandidateModel lastCandidateInfo)
        {
            foreach(var item in lastCandidateInfo.GetType().GetProperties())
            {
                var property = Candidate.GetType().GetProperty(item.Name);
                var candidateValue = property.GetValue(Candidate);
                var lastCandidateInfoValue = item.GetValue(lastCandidateInfo);

                if (lastCandidateInfoValue != null)
                {
                    property.SetValue(Candidate, lastCandidateInfoValue);
                }                
            }
        }

        #endregion

        #region Behaviours

        public async Task AbortCreation()
        {
            await ShowSpinner();

            _navMan.NavigateTo("/home");            

            await HideSpinner();
        }

        #endregion

        #region Tabs

        public void TabCreate()
        {
            tabsInterview.EnableTab(0, true);
            tabsInterview.EnableTab(1, true);
            tabsInterview.EnableTab(2, true);
            tabsInterview.EnableTab(3, true);
            tabsInterview.EnableTab(4, true);
        }

        public void Next()
        {
            var selectedTab = tabsInterview.SelectedItem;

            if (selectedTab == 4)
            {
                tabsInterview.Select(0);
                return;
            }

            tabsInterview.Select(selectedTab + 1);
        }

        public void Previous()
        {
            var selectedTab = tabsInterview.SelectedItem;

            if (selectedTab == 0)
            {
                tabsInterview.Select(3);
                return;
            }

            tabsInterview.Select(selectedTab - 1);
        }

        #endregion

        #region Buttons

        public void SearchCandidate(Microsoft.AspNetCore.Components.Web.FocusEventArgs args)
        {
            if(Candidate.Id == null)
            {
                CandidateReference = CandidateService.GetCandidateByReference(new CandidateSearchModel(Candidate.Name, Candidate.Email), _appState.AccessToken);

                if (CandidateReference != null)
                    ShowLoadCandidateDialog();
            }          
        }

        public void LoadCandidateByReference()
        {
            LoadLastInterview(CandidateReference);
            //Candidate = CandidateReference;
            HideCandidateDialog();
            StateHasChanged();
        }

        private void ShowLoadCandidateDialog()
        {
            this.ShowCandidateDialog = true;
        }

        public async Task CompleteInterview()
        {
            await ShowSpinner();

            try
            {
                ValidateInterviewAsDraft();

                if (ValidateMinFieldsToCompleteInterview())
                {
                    SaveCandidate();
                    SaveInterview();

                    Interview = InterviewService.UpdateInterview(Interview, _appState.AccessToken);                    

                    ExportCompetenceGraph();

                    LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0010, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                    ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0023"));
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0010, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0010, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                //ShowErrorDialog(ErrorHelper.Except("APP_0039"));
            }
            finally
            {
                await HideSpinner();
            }
        }       

        private void ValidateInterviewAsDraft()
        {
            if (!Interview.Id.HasValue)
                ErrorHelper.Except("APP_0024");
        }

        private bool ValidateMinFieldsToCompleteInterview()
        {
            var hasName = !Candidate.Name.IsNullOrWhitespaceOrEmpty();
            var hasEmail = !Candidate.Email.IsNullOrWhitespaceOrEmpty();
            var hasBornDate = Candidate.BornDate.HasValue;
            var hasPhoneNumber = !Candidate.PhoneNumber.IsNullOrWhitespaceOrEmpty();
            var hasLocation = !SelectedLocation.IsNullOrWhitespaceOrEmpty();
            var hasJobRole = !SelectedJobRole.IsNullOrWhitespaceOrEmpty();
            var hasAvailability = !SelectedAvailability.IsNullOrWhitespaceOrEmpty();
            var hasContractType = !SelectedContractType.IsNullOrWhitespaceOrEmpty();           

            if (hasName && hasEmail && hasBornDate &&
               hasPhoneNumber && hasLocation && hasJobRole &&
               hasAvailability && hasContractType)
                return true;
            else
            {
                StringBuilder builder = new StringBuilder("Não é possível Concluir a Entrevista sem os dados mínimos para:");

                if (!hasName)
                    builder.AppendLine("Nome, ");

                if (!hasEmail)
                    builder.AppendLine("Email, ");

                if (!hasBornDate)
                    builder.AppendLine("Data de Nascimento, ");

                if (!hasPhoneNumber)
                    builder.AppendLine("Telemóvel, ");

                if (!hasLocation)
                    builder.AppendLine("Localização, ");

                if (!hasJobRole)
                    builder.AppendLine("Cargo/Função do Candidato, ");

                if (!hasAvailability)
                    builder.AppendLine("Disponibilidade do Candidato, ");

                if (!hasContractType)
                    builder.AppendLine("Tipo de Contrato, ");              

                builder.Remove(builder.Length - 4, 2);

                ShowWarningDialog(builder.ToString());
            }

            return false;
        }

        public async Task SaveInterviewDraft()
        {
            if (Candidate.Name.IsNullOrWhitespaceOrEmpty() ||
                Candidate.Email.IsNullOrWhitespaceOrEmpty())
                ShowWarningDialog(ErrorHelper.GetErrorMessage("APP_0021"));
            else
            {
                if (!Interview.Id.HasValue)
                    await CreateInterviewDraft();
                else
                    await UpdateInterviewDraft();
            }
        }

        private void SaveInterview()
        {
            Interview.CandidateId = Candidate.Id;
            Interview.ContractType = ContractTypes.FirstOrDefault(x => x.Value.Equals(SelectedContractType))?.ConstantId ?? null;
            Interview.Status = "IS_0002";

            Interview = InterviewService.UpdateInterview(Interview, _appState.AccessToken);
        }


        private async Task CreateInterviewDraft()
        {
            try
            {
                await ShowSpinner();

                SaveCandidate();
                Interview.OwnerId = _appState.OwnerLogged.Id;
                Interview.CandidateId = Candidate.Id;
                Interview.ContractType = ContractTypes.FirstOrDefault(x => x.Value.Equals(SelectedContractType))?.ConstantId ?? null;                

                Interview = InterviewService.CreateInterview(Interview, _appState.AccessToken);

                ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0022"));

                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0002, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0002, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0002, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                await HideSpinner();
            }
        }

        private async Task UpdateInterviewDraft()
        {
            await ShowSpinner();

            try
            {
                SaveCandidate();
                Interview.CandidateId = Candidate.Id;
                Interview.ContractType = ContractTypes.FirstOrDefault(x => x.Value.Equals(SelectedContractType))?.ConstantId ?? null;
                                
                Interview = InterviewService.UpdateInterview(Interview, _appState.AccessToken);
                

                ShowSuccessDialog(ErrorHelper.GetErrorMessage("APP_0022"));

                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = string.Concat(JsonConvert.SerializeObject(Interview), JsonConvert.SerializeObject(Candidate)), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(new AppException("APP_0042", ex));
            }
            finally
            {
                await HideSpinner();
            }
        }

        private void SaveCandidate()
        {
            Candidate.LocationId = SelectedLocation is null ? null : Locations.FirstOrDefault(x => x.Value.Equals(SelectedLocation))?.Id ?? null;
            Candidate.MaritalStatus = SelectedMaritalStatus is null ? null : MaritalStatuses.FirstOrDefault(x => x.Value.Equals(SelectedMaritalStatus))?.ConstantId ?? null;
            Candidate.LiteraryAbility = SelectedAvailability is null ? null : LiteraryAbilities.FirstOrDefault(x => x.Value.Equals(SelectedLiteraryAbility))?.ConstantId ?? null;
            Candidate.JobRoleId = SelectedJobRole is null ? null : JobRoles.FirstOrDefault(x => x.Value.Equals(SelectedJobRole))?.Id ?? null;
            Candidate.Available = SelectedAvailability is null ? null : CandidateAvailabilities.FirstOrDefault(x => x.Value.Equals(SelectedAvailability))?.ConstantId ?? null;
            Candidate.Interval = SelectedInterval is null ? null : CandidateIntervals.FirstOrDefault(x => x.Value.Equals(SelectedInterval))?.ConstantId ?? null;
            Candidate.Mobility = SelectedMobility is null ? null : CandidateMobility.FirstOrDefault(x => x.Value.Equals(SelectedMobility))?.ConstantId ?? null;
            Candidate.Type = SelectedCandidateType is null ? null : CandidateTypes.FirstOrDefault(x => x.Value.Equals(SelectedCandidateType))?.ConstantId ?? null;
            
            if (SelectedCandidateExperience != null)
                Candidate.YearsOfExperience = InterviewHelper.ConvertFromExperience(CandidateExperience.FirstOrDefault(x => x.Value.Equals(SelectedCandidateExperience))?.ConstantId ?? null);

            if (!Candidate.Id.HasValue || Candidate.Id == 0)
                Candidate = CandidateService.CreateCandidate(Candidate, _appState.AccessToken);
            else
                Candidate = CandidateService.UpdateCandidate(Candidate, _appState.AccessToken);

            HandleCandidateLanguages();
            HandleCandidateTechSkills();
            HandleCandidateNaifas();            
            HandleCandidateUploads();            
        }

        private void HandleCandidateNaifas()
        {
            var candidateNaifasQuestions = CandidateService.GetNaifasByCandidate(Candidate.Id.GetValueOrDefault(0), _appState.AccessToken).ToList();

            foreach (var question in NaifasQuestions)
            {
                var candidateAnswer = candidateNaifasQuestions.Where(x => x.Question.Equals(question.Order.ToString())).SingleOrDefault();

                if (candidateAnswer is null)
                {                    
                    var createdNaifas = CandidateService.CreateNaifas(new NaifasModel(0, 
                                                                                      question.Order.ToString(), 
                                                                                      string.IsNullOrEmpty(question.ValueChecked) ? "0" : question.ValueChecked), _appState.AccessToken);

                    CandidateService.CreateCandidateNaifas(Candidate.Id.GetValueOrDefault(0), createdNaifas.Id, _appState.AccessToken);                  
                }
                else if (!string.IsNullOrEmpty(question.ValueChecked))
                {
                    candidateAnswer.Answer = question.ValueChecked;
                    CandidateService.UpdateNaifas(candidateAnswer, _appState.AccessToken);                    
                }

            }

            HandleNaifasGraph();
        }

        private void ExportCompetenceGraph()
        {            
            CompetenceGraph.Export(ExportType.PNG, $"Competence_Graph_{Candidate.Name.RemoveSpecialCharacters()}", null, false);
        }

        private void HandleCandidateLanguages()
        {
            var currentLanguages = CandidateService.GetLanguagesByCandidate(Candidate.Id.GetValueOrDefault(0), _appState.AccessToken);

            foreach (var currentLanguage in currentLanguages)
            {
                var language = CandidateLanguages.FirstOrDefault(x => x.Value.Equals(currentLanguage.Value));

                if (language is null)
                {
                    CandidateService.DeleteCandidateLanguage(Candidate.Id.GetValueOrDefault(0), currentLanguage.Id.GetValueOrDefault(0), _appState.AccessToken);
                }
            }

            foreach (var candidateLanguage in CandidateLanguages)
            {
                var language = currentLanguages.FirstOrDefault(x => x.Value.Equals(candidateLanguage.Value));

                if (language is null)
                {
                    CandidateService.CreateCandidateLanguage(Candidate.Id.GetValueOrDefault(0), candidateLanguage.Id.GetValueOrDefault(0), _appState.AccessToken);
                }
            }
        }

        private void HandleCandidateTechSkills()
        {
            var currentTechSkills = CandidateService.GetTechSkillsByCandidate(Candidate.Id.GetValueOrDefault(0), _appState.AccessToken);

            foreach (var currentTechSkill in currentTechSkills)
            {
                var techSkill = CandidateTechSkills.FirstOrDefault(x => x.Description.Equals(currentTechSkill.Description));

                if (techSkill is null)
                {
                    CandidateService.DeleteCandidateTechSkill(Candidate.Id.GetValueOrDefault(0), currentTechSkill.Id, _appState.AccessToken);
                }
            }

            foreach (var candidateTechSkill in CandidateTechSkills)
            {
                var techSkill = currentTechSkills.FirstOrDefault(x => x.Description.Equals(candidateTechSkill.Description));

                if (techSkill is null)
                {
                    CandidateService.CreateCandidateTechSkill(Candidate.Id.GetValueOrDefault(0), candidateTechSkill.Id, _appState.AccessToken);
                }
            }
        }        

        private void HandleNaifasGraph()
        {
            List<PolarAreaChartData> chartAreas = new List<PolarAreaChartData>();

            foreach (var question in this.NaifasQuestions)
            {
                string area = question.Area switch
                {
                    "Liderança" => "Leadership",
                    "Trabalho em Equipa" => "Teamwork",
                    "Relações Interpessoais" => "Interpersonal Relationships",
                    "Resiliência" => "Resilience",
                    "Meticulosidade" => "Meticulousness",
                    "Orientação para Resultados" => "Orientation for Results",
                    _ => string.Empty,
                };

                var chartArea = chartAreas.FirstOrDefault(x => x.XValue.Equals(area));
                var naifasAnswers = NaifasQuestions.Where(x => x.Area.Equals(question.Area));
                double averageValue = naifasAnswers.Average(x => Int32.Parse(x.ValueChecked.IsNullOrWhitespaceOrEmpty() ? "0" : x.ValueChecked));
                double chartAreaValue = Math.Round(averageValue, 2);

                if (chartArea is null)
                {
                    chartArea = new PolarAreaChartData { XValue = area, YValue = chartAreaValue };
                    chartAreas.Add(chartArea);
                }                    

                chartArea.YValue = chartAreaValue;                
            }          

            GraphDataSource = chartAreas;  
        }

        private void HandleCandidateUploads()
        {
            var currentUploads = CandidateService.GetUploadsByCandidate(Candidate.Id.GetValueOrDefault(0), _appState.AccessToken);

            foreach (var currentUpload in currentUploads)
            {
                var upload = CandidateUploads.FirstOrDefault(x => x.FileName.Equals(currentUpload.FileName));

                if (upload is null)
                    CandidateService.DeleteCandidateUpload(Candidate.Id.GetValueOrDefault(0), currentUpload.Id.GetValueOrDefault(0), _appState.AccessToken);                
            }

            foreach (var candidateUpload in CandidateUploads)
            {
                var upload = currentUploads.FirstOrDefault(x => x.FileName.Equals(candidateUpload.FileName));

                if (upload is null)
                    CandidateService.CreateCandidateUpload(Candidate.Id.GetValueOrDefault(0), candidateUpload.Id.GetValueOrDefault(0), _appState.AccessToken);
            }
        }

        #endregion

        #region Uploader
        public void UploadFile(UploadChangeEventArgs args)
        {
            foreach (var file in args.Files)
            {
                var path = Path.Combine(Path.GetTempPath(), file.FileInfo.Name);             
                FileStream filestream = new FileStream(path, FileMode.Create, FileAccess.Write);
                file.Stream.WriteTo(filestream);              
                filestream.Close();
                file.Stream.Close();

                UploadModel model = CandidateService.UploadFile(file.FileInfo.Name, 
                                                                File.ReadAllBytes(path), 
                                                                _appState.AccessToken);
                if (model != null)
                    CandidateUploads.Add(model);
            }
        }

        public void RemoveUpload(RemovingEventArgs args)
        {
            foreach (var removeFile in args.FilesData)
            {
                var path = Path.Combine(Path.GetTempPath(), removeFile.Name);

                if (File.Exists(path))               
                    File.Delete(path);
            }
        }

        #endregion

        #region Dialogs

        public void ShowSuccessDialog(string message)
        {
            ShowInterviewDialog = true;
            IsSuccessDialog = true;
            InterviewDialogHeaderText = "Sucesso!";
            InterviewDialogContentText = message;

            StateHasChanged();
        }

        public void ShowWarningDialog(string message)
        {
            ShowInterviewDialog = true;
            IsWarningDialog = true;
            InterviewDialogHeaderText = "Atenção!";
            InterviewDialogContentText = message;

            StateHasChanged();

        }

        public void ShowErrorDialog(AppException ex)
        {
            ShowInterviewDialog = true;
            IsErrorDialog = true;
            InterviewDialogHeaderText = "Erro!";
            InterviewDialogContentText = $"{ex.Message} - ({ex.Code})";
            StateHasChanged();

        }

        public void HideDialog()
        {
            ShowInterviewDialog = false;
            InterviewDialogHeaderText = null;
            InterviewDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
        }


        #region CandidateDialog

        public void HideCandidateDialog()
        {
            ShowCandidateDialog = false;
            CandidateReference = new CandidateModel();
        }

        #endregion

        #endregion

        #region Locations

        protected void SetLocation()
        {
            LocationToAdd = string.Empty;

            if (!IsAddingLocation)
            {
                IsAddingLocation = true;
                AddLocationIcon = "remove";
            }
            else
            {
                IsAddingLocation = false;
                AddLocationIcon = "add";
            }
        }

        protected void SetLocation(SelectionModel location)
        {
            ShowSpinner();

            //ddlLocation.Refresh();
            SetLocation();
            SelectedLocation = location.Value;

            HideSpinner();

            StateHasChanged();
        }

        protected void CreateLocation()
        {
            ShowSpinner();
            var locationAdded = CandidateService.CreateLocation(new SelectionModel(0, LocationToAdd), _appState.AccessToken);
            UpdateLocations(locationAdded);
            SetLocation(locationAdded);
            HideSpinner();
        }

        private void UpdateLocations(SelectionModel locationAdded)
        {
            if (!LocationOptions.Any(x => x.Contains(locationAdded.Value)))
            {
                Locations = Locations.Concat(Enumerable.Repeat(locationAdded, 1)).ToArray();
                LocationOptions = Locations.Select(x => x.Value).ToList();
            }

            //ddlLocation.Refresh();
            SelectedLocation = locationAdded.Value;
            StateHasChanged();
        }

        #endregion

        #region JobRoles

        protected void SetJobRole()
        {
            JobRoleToAdd = string.Empty;

            if (!IsAddingJobRole)
            {
                IsAddingJobRole = true;
                AddJobRoleIcon = "remove";
            }
            else
            {
                IsAddingJobRole = false;
                AddJobRoleIcon = "add";
            }
        }

        protected void SetJobRole(SelectionModel jobRole)
        {
            ShowSpinner();

            //ddlJobRoles.Refresh();
            SetJobRole();
            SelectedJobRole = jobRole.Value;

            HideSpinner();

            StateHasChanged();
        }

        protected void CreateJobRole()
        {
            ShowSpinner();
            var JobRoleAdded = CandidateService.CreateJobRole(new SelectionModel(0, JobRoleToAdd), _appState.AccessToken);
            UpdateJobRoles(JobRoleAdded);
            SetJobRole(JobRoleAdded);
            HideSpinner();
        }

        private void UpdateJobRoles(SelectionModel JobRoleAdded)
        {
            if (!JobRolesOptions.Any(x => x.Contains(JobRoleAdded.Value)))
            {
                JobRoles = JobRoles.Concat(Enumerable.Repeat(JobRoleAdded, 1)).ToArray();
                JobRolesOptions = JobRoles.Select(x => x.Value).ToList();
            }

            SelectedJobRole = JobRoleAdded.Value;
            //ddlJobRoles.Refresh();

            StateHasChanged();
        }

        #endregion

        #region Languages

        protected void SetLanguage()
        {
            LanguageToAdd = string.Empty;

            if (!IsAddingLanguage)
            {
                IsAddingLanguage = true;
                AddLanguageIcon = "remove";
            }
            else
            {
                IsAddingLanguage = false;
                AddLanguageIcon = "add";
            }
        }

        protected void SetLanguage(SelectionModel language)
        {
            ShowSpinner();

            //ddlLanguages.Refresh();
            SetLanguage();
            AddCandidateLanguage(new Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> { Value = language.Value });

            HideSpinner();

            StateHasChanged();
        }

        protected void CreateLanguage()
        {
            ShowSpinner();
            var LanguageAdded = CandidateService.CreateLanguage(new SelectionModel(0, LanguageToAdd), _appState.AccessToken);
            UpdateLanguages(LanguageAdded);
            SetLanguage(LanguageAdded);
            HideSpinner();
        }

        private void UpdateLanguages(SelectionModel LanguageAdded)
        {
            if (!LanguagesOptions.Any(x => x.Contains(LanguageAdded.Value)))
            {
                Languages = Languages.Concat(Enumerable.Repeat(LanguageAdded, 1)).ToArray();
                LanguagesOptions = Languages.Select(x => x.Value).ToList();
            }

            //ddlLanguages.Refresh();

            StateHasChanged();
        }

        protected void LoadCandidateLanguages()
        {
            foreach (var language in CandidateLanguages)
            {
                var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = language.Value, Text = language.Value };

                clsLanguages.AddChip(model);

                StateHasChanged();
            }
        }

        protected void AddCandidateLanguage(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                bool exists = clsLanguages.Chips.Any(x => x.Value.Equals(args.Value));

                if (!exists)
                {
                    var languageToAdd = Languages.FirstOrDefault(x => x.Value.Equals(args.Value));
                    var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = languageToAdd.Value, Text = languageToAdd.Value };

                    clsLanguages.AddChip(model);

                    CandidateLanguages.Add(languageToAdd);

                    StateHasChanged();

                    //ddlLanguages.Refresh();
                }
            }
        }

        protected void RemoveCandidateLanguage(Syncfusion.Blazor.Buttons.ChipEventArgs args)
        {
            var languageToRemove = CandidateLanguages.FirstOrDefault(x => x.Value.Equals(args.Text));

            CandidateLanguages.Remove(languageToRemove);
        }

        #endregion

        #region TechSkills       

        protected void SetTechSkill()
        {
            TechSkillToAdd = string.Empty;

            if (!IsAddingTechSkill)
            {
                IsAddingTechSkill = true;
                AddTechSkillIcon = "remove";
            }
            else
            {
                IsAddingTechSkill = false;
                AddTechSkillIcon = "add";
            }
        }

        protected void SetTechSkill(TechSkillModel TechSkill)
        {
            ShowSpinner();

            //ddlTechSkill.Refresh();
            SetTechSkill();
            AddCandidateTechSkill(new Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> { Value = TechSkill.Description });

            HideSpinner();

            StateHasChanged();
        }

        protected void CreateTechSkill()
        {
            ShowSpinner();
            var category = TechSkillsCategories.FirstOrDefault(x => x.Value.Equals(SelectedTechSkillCategory));
            var TechSkillAdded = CandidateService.CreateTechSkill(new TechSkillModel(0, category.ConstantId, TechSkillToAdd), _appState.AccessToken);
            UpdateTechSkills(TechSkillAdded);
            SetTechSkill(TechSkillAdded);
            HideSpinner();
        }

        private void UpdateTechSkills(TechSkillModel TechSkillAdded)
        {
            if (!TechSkillsOptions.Any(x => x.Contains(TechSkillAdded.Description)))
            {
                TechSkills = TechSkills.Concat(Enumerable.Repeat(TechSkillAdded, 1)).ToArray();
                TechSkillsOptions = TechSkills.Select(x => x.Description).ToList();
            }

            //ddlTechSkill.Refresh();

            StateHasChanged();
        }

        protected void LoadCandidateTechSkillsCategories()
        {
            EnableTechSkillDropDown = false;

            StateHasChanged();
        }

        protected void LoadCandidateTechSkills()
        {
            foreach (var TechSkill in CandidateTechSkills)
            {
                var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = TechSkill.Description, Text = TechSkill.Description };

                clsTechSkills.AddChip(model);

                StateHasChanged();
            }
        }

        protected void AddCandidateTechSkill(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value?.IsNullOrWhitespaceOrEmpty() ?? false)
            {
                bool exists = clsTechSkills.Chips.Any(x => x.Value.Equals(args.Value));

                if (!exists)
                {
                    var TechSkillToAdd = TechSkills.FirstOrDefault(x => x.Description.Equals(args.Value));
                    var model = new ChipItem { CssClass = "e-chip", Enabled = true, Value = TechSkillToAdd.Description, Text = TechSkillToAdd.Description };

                    clsTechSkills.AddChip(model);

                    CandidateTechSkills.Add(TechSkillToAdd);

                    StateHasChanged();

                    //ddlTechSkill.Refresh();
                }
            }
        }

        protected void RemoveCandidateTechSkill(Syncfusion.Blazor.Buttons.ChipEventArgs args)
        {
            var TechSkillToRemove = CandidateTechSkills.FirstOrDefault(x => x.Description.Equals(args.Text));

            CandidateTechSkills.Remove(TechSkillToRemove);
        }

        #endregion

        public void ChangeCategory(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                EnableTechSkillDropDown = true;
                var category = TechSkillsCategories.FirstOrDefault(x => x.Value.Equals(args.Value));
                TechSkillsOptions = TechSkills.Where(x => x.Category.Equals(category?.ConstantId ?? string.Empty)).Select(y => y.Description).ToList();
                SelectedTechSkillCategory = args.Value;
                SelectedTechSkill = null;                
            }
            else
            {
                EnableTechSkillDropDown = false;
                SelectedTechSkillCategory = null;
                SelectedTechSkill = null;              

                StateHasChanged();
            }
        }

        public void ChangeAvailability(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                var availability = CandidateAvailabilities.FirstOrDefault(x => x.Value.Equals(args.Value));

                if (availability.Value.Equals("Disponível"))
                {
                    IsCandidateAvailable = true;
                    StateHasChanged();
                }
                else
                {
                    IsCandidateAvailable = false;
                    StateHasChanged();
                }

                SelectedAvailability = args.Value;
            }
            else
            {
                Candidate.Interval = null;
                IsCandidateAvailable = false;
                SelectedAvailability = null;                

                StateHasChanged();
            }
        }

        public void ChangeInterval(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                var interval = CandidateIntervals.FirstOrDefault(x => x.Value.Equals(args.Value));

                switch (interval.Value)
                {
                    case CandidateInterval.CINT_0005:
                        CustomInterval = true;
                        StateHasChanged();
                        break;
                    default:
                        Candidate.Interval = interval.Value;
                        Candidate.AvailableAt = null;
                        CustomInterval = false;
                        StateHasChanged();
                        break;
                }

                SelectedInterval = args.Value;
            }
            else
            {
                CustomInterval = false;
                SelectedInterval = null;

                StateHasChanged();
            }
        }

        public void ChangeMobility(Syncfusion.Blazor.DropDowns.ChangeEventArgs<string, string> args)
        {
            if (!args.Value.IsNullOrWhitespaceOrEmpty())
            {
                var mobility = CandidateMobility.FirstOrDefault(x => x.Value.Equals(args.Value));

                Candidate.Mobility = mobility.Value;
                StateHasChanged();
            }
            else
            {
                //ddlMobility.Refresh();

                StateHasChanged();
            }
        }        

        public void SetCurrentPaymentType(string type)
        {
            Interview.IsCurrentMonthlyPayment = type == PaymentFrequency.PF_0002;

            TypeCurrentFrequencyPaymentDaily = type == PaymentFrequency.PF_0001;
            TypeCurrentFrequencyPaymentMonthly = type == PaymentFrequency.PF_0002;
        }

        public void SetExpectPaymentType(string type)
        {
            Interview.IsExpectationMonthlyPayment = type == PaymentFrequency.PF_0002;

            TypeExpectFrequencyPaymentDaily = type == PaymentFrequency.PF_0001;
            TypeExpectFrequencyPaymentMonthly = type == PaymentFrequency.PF_0002;
        }


        public void CurrentSalaryType(string type)
        {
            switch(type)
            {
                case "liquido":
                    Interview.IsCurrentSalaryNet = true;
                    break;
                default:
                    Interview.IsCurrentSalaryNet = false;
                    break;
            }
        }

        public void ExpectSalaryType(string type)
        {
            switch (type)
            {
                case "liquido":
                    Interview.IsExpectationSalaryNet = true;
                    break;
                default:
                    Interview.IsExpectationSalaryNet = false;
                    break;
            }
        }

        public async Task SaveChart(ExportEventArgs Arg)
        {
            try
            {
                if (!CandidateUploads.Any(x => x.FileName.Contains("Competence_Graph_")))
                {
                    string generatedHtml = GenerateHtml(Arg.DataUrl);
                    string tempPath = Path.GetTempPath();
                    string fileName = $"Competence_Graph_{Candidate.Name.RemoveSpecialCharacters() ?? string.Empty}.pdf";

                    var globalSettings = new GlobalSettings
                    {
                        ColorMode = DinkToPdf.ColorMode.Color,
                        Orientation = DinkToPdf.Orientation.Portrait,
                        PaperSize = PaperKind.A4,
                        Margins = new MarginSettings { Top = 10 },
                        DocumentTitle = fileName,
                        Out = Path.Combine(tempPath, fileName)
                    };

                    var objectSettings = new ObjectSettings
                    {
                        PagesCount = true,
                        HtmlContent = generatedHtml,
                        WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/css", "site.css") }
                    };

                    var pdf = new HtmlToPdfDocument()
                    {
                        GlobalSettings = globalSettings,
                        Objects = { objectSettings }
                    };

                    _converter.Convert(pdf);

                    var path = Path.Combine(tempPath, fileName);
                    var upload = CandidateService.UploadFile(fileName, File.ReadAllBytes(Path.Combine(tempPath, fileName)), _appState.AccessToken);

                    if (upload != null)
                        CandidateService.CreateCandidateUpload(Candidate.Id.Value, upload.Id.Value, _appState.AccessToken);
                }                
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0012, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }

        }

        private string GenerateHtml(string dataUrl)
        {
            var base64Data = Regex.Match(dataUrl, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var binData = Convert.FromBase64String(base64Data);    
            var sb = new StringBuilder();

            sb.Append("<html>");
            sb.Append("<div style='font-family:gilroy_semi_bold,sans-serif;font-weight:700;font-size:x-large;padding:30px 0 15px 0'>Competence Graph</div>");
            sb.Append($"<div style='font-family:gilroy_semi_bold,sans-serif;font-weight:700;font-size:large;padding:0 0 30px 0'>{Candidate.Name}</div>");
            sb.Append("<div style='width:100%;font-family:gilroy_light,sans-serif;font-weight:500;padding:0 0 30px 0'>The following Competence Graph is an assessment carried out by our team during the recruitment process, reflecting long-term perspectives and motivations, in line with our HR and career management policy. Considering it is sensitive information, we kindly ask you not to share it at any time with the consultant or any other source other than OnRising or within your company</div>");
            sb.Append($"<div style='padding-left:120px'><img src='data:image/png;base64,{Convert.ToBase64String(binData, 0, binData.Length)}'/></div>");
            sb.Append("<table style='width:100%;padding-top:30px'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Leadership</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Resilience</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to guide the actions of an individual towards the realization of a common vision</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to persist in an action despite the obstacles and difficult situations that arise</td>");
            sb.Append("</tr>");
            sb.Append("<tr style='height:40px'>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Teamwork</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Meticulousness</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to work collectively, cooperate and integrate into a working group in an active and receptive manner</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to demonstrate careful and rigorous work methods by giving close attention to detail in performing tasks.</td>");
            sb.Append("</tr>");
            sb.Append("<tr style='height:40px'>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Interpersonal Relationships</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:700'>Orientation for Results</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to network with others showing intuition and social insight</td>");
            sb.Append("<td style='width:30px'></td>");
            sb.Append("<td style='font-family:gilroy_semi_bold,sans-serif;font-weight:500;font-size:small'>Ability to reach and exceed expected results, setting demanding goals, managing resources and attending to quality, costs and benefits.</td>");
            sb.Append("</tr>");
            sb.Append("<tr style='height:40px'>");
            sb.Append("</tr>");
            sb.Append("</html>");

            return sb.ToString();
        }

        public void Dispose()
        {
            tabsInterview = null;
            dtpBornDate = null;
            ddlLocation = null;
            ddlMaritalStatus = null;
            ddlLiteraryAbilities = null;
            ddlLanguages = null;
            ddlJobRoles = null;
            ddlTechSkillCategory = null;
            ddlTechSkill = null;
            ddlContractType = null;
            ddlAvailability = null;
            ddlInterval = null;
            ddlMobility = null;
            dtpAvailableAt = null;
            ddlCandidateType = null;
            ddlCandidateExperience = null;
            upPersonalFiles = null;
            btnCompleteInterview = null;
            btnSaveInterviewAsDraft = null;
            candidateDialog = null;
            btnInterviewDialogOk = null;
            btnCandidateDialogYes = null;
            btnCandidateDialogNo = null;            
            clsLanguages = null;
            clsTechSkills = null;
            CompetenceGraph = null;
            Locations = null;
            MaritalStatuses = null;
            JobRoles = null;
            LiteraryAbilities = null;
            Languages = null;
            TechSkillsCategories = null;
            TechSkills = null;
            ContractTypes = null;
            CandidateAvailabilities = null;
            CandidateIntervals = null;
            CandidateMobility = null;
            CandidateTypes = null;
            CandidateExperience = null;
            LocationOptions = null;
            MaritalStatusesOptions = null;
            JobRolesOptions = null;
            LiteraryAbilitiesOptions = null;
            LanguagesOptions = null;
            TechSkillsCategoriesOptions = null;
            TechSkillsOptions = null;
            ContractTypesOptions = null;
            CandidateAvailabilitiesOptions = null;
            CandidateIntervalsOptions = null;
            CandidateMobilityOptions = null;
            CandidateTypesOptions = null;
            CandidateExperienceOptions = null;
            SelectedLocation = string.Empty;
            SelectedMaritalStatus = null;
            SelectedLiteraryAbility = null;
            SelectedJobRole = null;
            SelectedTechSkillCategory = null;
            SelectedTechSkill = null;
            SelectedContractType = null;
            SelectedAvailability = null;
            SelectedInterval = null;
            SelectedMobility = null;
            SelectedCandidateType = null;
            SelectedCandidateExperience = null;                 
            Interview = null;
            Candidate = null;
            CandidateReference = null;
            CandidateLanguages = null;
            CandidateTechSkills = null;
            CandidateUploads = null;
            AddLocationIcon = null;
            LocationToAdd = null;
            AddJobRoleIcon = null;
            JobRoleToAdd = null;
            AddLanguageIcon = null;
            LanguageToAdd = null;
            AddTechSkillIcon = null;
            TechSkillToAdd = null;
            InterviewDialogHeaderText = null;
            InterviewDialogContentText = null;          
            GraphDataSource = null;
            NaifasQuestions = null;
            CompetenceGraphPath = null;
            
            GC.Collect();
        }
        
    }    

}