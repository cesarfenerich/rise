﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Navigations;
using System;
using Microsoft.JSInterop;
using System.Collections.Generic;
using Domain.Models;
using System.Linq;
using WebApp.Services;
using Common.Extensions;
using Syncfusion.Blazor.Lists;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.Popups;
using Common.Exceptions;
using Common.Helpers;
using BlazorDownloadFile;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Http;
using WebApp.Services.Interfaces;
using Domain.Entities.Constants;
using Newtonsoft.Json;
using WebApp.Helpers;
using System.Globalization;
using WebApp.Shared;
using System.Threading.Tasks;

namespace WebApp.Pages.Candidate
{
    [Authorize]
    public partial class ViewModel : ComponentBase, IDisposable
    {
        [Inject] protected AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }

        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected IInterviewService InterviewService { get; set; }
        [Inject] protected ICandidateService CandidateService { get; set; }
        [Inject] protected IBlazorDownloadFileService DownloadFileService { get; set; }
        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }
        [Inject] protected ILogService LogService { get; set; }

        [Parameter]
        public string CandidateId { get; set; }

        [CascadingParameter] protected MainLayout _mainLayout { get; set; }

        #region Screen Components and Behaviour Components Reference Objects

        public SfListView<InterviewGridModel> ListInterviews;
        public SfListView<InterviewUpdateModel> ListUpdates = new SfListView<InterviewUpdateModel>();
        public SfListView<UploadModel> ListUploads = new SfListView<UploadModel>();
        public SfTab tabs = new SfTab();      

        protected SfButton btnFirstInterview = new SfButton();
        protected SfButton btnSecondInterview = new SfButton();
        protected SfButton btnThirdInterview = new SfButton();

        protected DialogButton btnInfoDialogOk = new DialogButton();

        protected SfContextMenu<MenuItem> cxtMenuInterviewUpdate = new SfContextMenu<MenuItem>();

        protected List<MenuItem> InterviewUpdateActions = new List<MenuItem> { new MenuItem { Text="Editar", IconCss="e-icons rise-icon rise-edit-icon",  },
                                                                               new MenuItem { Text="Remover", IconCss="e-icons rise-icon rise-remove-icon" }};

        public string CandidateTypeColor { get; set; }
        #endregion

        #region Page Models Setup 

        protected CandidateModel Candidate { get; set; } = new CandidateModel();

        protected List<InterviewGridModel> Interviews { get; set; } = new List<InterviewGridModel>();
        public InterviewInfoModel InterviewInfoModel { get; set; } = new InterviewInfoModel();
        protected List<InterviewUpdateModel> Updates { get; set; } = new List<InterviewUpdateModel>();

        protected InterviewUpdateModel SelectedInterviewUpdate { get; set; }
        protected List<UploadModel> Uploads { get; set; } = new List<UploadModel>();

        protected bool DisableSecondInterview { get; set; } = true;
        protected string VisibleSecondInterview { get; set; } = "hidden";
        protected bool DisableThirdInterview { get; set; } = true;
        protected string VisibleThirdInterview { get; set; } = "hidden";

        protected string FirstInterviewDate { get; set; } = "DD/MM/YYYY";
        protected string FirstInterviewDescription { get; set; } = "1a Entrevista";
        protected string SecondInterviewDate { get; set; } = "DD/MM/YYYY";
        protected string SecondInterviewDescription { get; set; } = "2a Entrevista";
        protected string ThirdInterviewDate { get; set; } = "DD/MM/YYYY";
        protected string ThirdInterviewDescription { get; set; } = "3a Entrevista";

        protected bool ShowInterviewUpdatesModal { get; set; } = false;
        protected bool IsAddingInterviewUpdate { get; set; } = false;
        protected string InterviewUpdateHeader { get; set; } = "Novo Update";
        protected string InterviewUpdateText { get; set; }

        protected bool ShowInfoDialog { get; set; } = false;

        protected bool IsErrorDialog { get; set; } = false;
        protected bool IsWarningDialog { get; set; } = false;
        protected bool IsSuccessDialog { get; set; } = false;

        protected string InfoDialogHeaderText { get; set; }
        protected string InfoDialogContentText { get; set; }

        protected Dictionary<string, object> minLinesToShow = new Dictionary<string, object>() { { "rows", "5" } };

        protected string CurrentPaymentFrequency { get; set; } = String.Empty;
        protected string ExpectationPaymentFrequency { get; set; } = String.Empty;

        #endregion

        #region Init and Data       

        protected override void OnInitialized()
        {
            try
            {       
                if (!CandidateId.IsNullOrWhitespaceOrEmpty())
                    LoadViewByCandidate();                
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }            
        }       

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
                StateHasChanged();
            else
            {
                _appState.EnableHeaderForCandidateView();
            }
        }

        private void LoadViewByCandidate()
        {
            Int64.TryParse(CandidateId, out long candidateId);
            Candidate = CandidateService.GetCandidateById(candidateId, _appState.AccessToken);
            Uploads = CandidateService.GetUploadsByCandidate(Candidate.Id.Value, _appState.AccessToken).ToList();

            Interviews = CandidateService.GetLastInterviewsOfCandidate(Candidate.Id.Value, _appState.AccessToken).ToList();
            InterviewInfoModel = InterviewService.GetInterviewInfoById(Interviews.FirstOrDefault().Id, _appState.AccessToken);
            Updates = InterviewService.GetInterviewUpdates(InterviewInfoModel.Id, _appState.AccessToken).ToList();
            InterviewInfoModel.YearsOfExperience = InterviewHelper.GetExperience(InterviewInfoModel.YearsOfExperience);

            if (InterviewInfoModel.CandidateAvailabilityInterval.IsNullOrWhitespaceOrEmpty())
                InterviewInfoModel.CandidateAvailableAt = "Nenhum";
            else if (!InterviewInfoModel.CandidateAvailabilityInterval.Equals("CINT_0005"))
                InterviewInfoModel.CandidateAvailableAt = string.Empty;

            InterviewInfoModel.CandidateAvailabilityInterval = InterviewHelper.GetAvailableInterval(InterviewInfoModel.CandidateAvailabilityInterval);

            CandidateTypeColor = InterviewInfoModel.CandidateType switch
            {
                CandidateTypes.CDT_0002 => "green",
                CandidateTypes.CDT_0003 => "blue",
                CandidateTypes.CDT_0004 => "yellow",
                CandidateTypes.CDT_0005 => "red",
                _ => string.Empty,
            };

            FirstInterviewDate = $"{Interviews.FirstOrDefault().Date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)} - 1a Entrevista";
            FirstInterviewDescription = $"Registada por {Interviews.FirstOrDefault().Owner} as {Interviews.FirstOrDefault().Date:hh:mm:sss}";

            if (Interviews.Count() > 1)
            {
                DisableSecondInterview = false;
                VisibleSecondInterview = "visible";

                SecondInterviewDate = $"{Interviews.ElementAt(1).Date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)} - 2a Entrevista";
                SecondInterviewDescription = $"Registada por {Interviews.ElementAt(1).Owner} as {Interviews.ElementAt(1).Date:hh:mm:sss}";

                if (Interviews.Count() > 2)
                {
                    DisableThirdInterview = false;
                    VisibleThirdInterview = "visible";

                    ThirdInterviewDate = $"{Interviews.ElementAt(2).Date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)} - Última Entrevista";
                    ThirdInterviewDescription = $"Registada por {Interviews.ElementAt(2).Owner} as {Interviews.ElementAt(2).Date:hh:mm:sss}"; 
                }
            }            

            if(InterviewInfoModel.ContractType == ContractTypes.CT_0001)
            {
                CurrentPaymentFrequency = (InterviewInfoModel?.IsCurrentMonthlyPayment ?? true) ? PaymentFrequency.PF_0002 : PaymentFrequency.PF_0001;
                ExpectationPaymentFrequency = (InterviewInfoModel?.IsExpectationMonthlyPayment ?? true) ? PaymentFrequency.PF_0002 : PaymentFrequency.PF_0001;
            }


        }

        #endregion

        #region Behaviours

        #region Spinner 
        public bool SpinnerVisible { get; set; } = false;
        public string SpinnerLabel { get; set; }

        public async Task SetSpinner(bool show, string label = "A Carregar...")
        {
            this.SpinnerVisible = show;
            this.SpinnerLabel = label;
            await Task.Delay(300);
        }

        public async Task ShowSpinner()
        {
            await SetSpinner(true);
        }

        public async Task HideSpinner()
        {
            await SetSpinner(false);
        }

        #endregion       

        public async void AbortView()
        {
            await ShowSpinner();

            _appState.EnableHeaderForHome();
            _navMan.NavigateTo("/home");

            await HideSpinner();
        }

        public void Focus(string elementId)
        {
            JsRuntime.InvokeVoidAsync("jsfunction.focusElement", elementId);
        }

        #endregion

        #region Tabs       

        public void Next()
        {
            var selectedTab = tabs.SelectedItem;

            if (selectedTab == 4)
            {
                tabs.Select(0);
                return;
            }

            tabs.Select(selectedTab + 1);
        }

        public void Previous()
        {
            var selectedTab = tabs.SelectedItem;

            if (selectedTab == 0)
            {
                tabs.Select(4);
                return;
            }

            tabs.Select(selectedTab - 1);
        }

        #endregion 

        public async Task LoadInterview(int number)
        {
            if (Interviews != null)
            {
                try
                {
                    await ShowSpinner();

                    await JsRuntime.InvokeVoidAsync("click", number);

                    var index = number - 1;
                    var interviewId = Interviews[index].Id;

                    InterviewInfoModel = InterviewService.GetInterviewInfoById(interviewId, _appState.AccessToken);
                    Updates = InterviewService.GetInterviewUpdates(InterviewInfoModel.Id, _appState.AccessToken).ToList();
                    InterviewInfoModel.YearsOfExperience = InterviewHelper.GetExperience(InterviewInfoModel.YearsOfExperience);

                    if (InterviewInfoModel.CandidateAvailabilityInterval.IsNullOrWhitespaceOrEmpty())
                        InterviewInfoModel.CandidateAvailableAt = "Nenhum";
                    else if (!InterviewInfoModel.CandidateAvailabilityInterval.Equals("CINT_0005"))
                        InterviewInfoModel.CandidateAvailableAt = string.Empty;

                    InterviewInfoModel.CandidateAvailabilityInterval = InterviewHelper.GetAvailableInterval(InterviewInfoModel.CandidateAvailabilityInterval);

                    switch (InterviewInfoModel.CandidateType)
                    {
                        case CandidateTypes.CDT_0002:
                            CandidateTypeColor = "green";
                            break;
                        case CandidateTypes.CDT_0003:
                            CandidateTypeColor = "blue";
                            break;
                        case CandidateTypes.CDT_0004:
                            CandidateTypeColor = "yellow";
                            break;
                        case CandidateTypes.CDT_0005:
                            CandidateTypeColor = "red";
                            break;
                        default:
                            break;
                    }

                    StateHasChanged();
                }
                catch (AppException ex)
                {
                    ShowErrorDialog(ex);
                }
                catch (Exception)
                {
                    ShowErrorDialog(ErrorHelper.Except("APP_040"));
                }
                finally
                {
                    await HideSpinner();
                }                               
            }
        }

        #region InterviewUpdates

        public void ModalInterviewUpdateOpen()
        {
            if (IsAddingInterviewUpdate)
                InterviewUpdateHeader = "Novo Update";
            else
                InterviewUpdateHeader = "Actualizar Update";
        }

        public void ModalInterviewUpdateClose()
        {
            ShowInterviewUpdatesModal = false;
            InterviewUpdateText = null;
        }

        public void AddInterviewUpdate()
        {
            IsAddingInterviewUpdate = true;
            ShowInterviewUpdatesModal = true;
        }

        public void AddInterviewNew()
        {
            _appState.EnableHeaderForCreateInterview();
            _navMan.NavigateTo($"/interview/new|{InterviewInfoModel.Email}");
        }

        public void EditInterview(int number)
        {
            try
            {
                var index = number - 1;
                var current = Interviews.Count;

                if (Interviews != null && current > index)
                {
                    var interviewId = Interviews[index].Id;

                    InterviewInfoModel = InterviewService.GetInterviewInfoById(interviewId, _appState.AccessToken);

                    _appState.EnableHeaderForCreateInterview();
                    _navMan.NavigateTo($"/interview/{InterviewInfoModel.Id}");
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }

        #region InterviewUpdates

        public void SelectInterviewUpdate(InterviewUpdateModel model)
        {
            this.SelectedInterviewUpdate = model;
        }       

        public void OpenInterviewUpdateModalInAddMode()
        {           
            IsAddingInterviewUpdate = true;
            ShowInterviewUpdatesModal = true;
        }
        public Task OpenInterviewUpdateModalInUpdateMode(long id)
        {
            SelectedInterviewUpdate = Updates.FirstOrDefault(x => x.Id == id);
            InterviewUpdateText = SelectedInterviewUpdate.Resume;
            IsAddingInterviewUpdate = false;
            ShowInterviewUpdatesModal = true;

            return Task.CompletedTask;
        }


        public async Task SaveInterviewUpdate()
        {
            try
            {
                await ShowSpinner();

                if (IsAddingInterviewUpdate)
                    CreateInterviewUpdate();
                else                
                    UpdateInterviewUpdate();                

                StateHasChanged();
                await HideSpinner();
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                await HideSpinner();
            }
        }

        public void CreateInterviewUpdate()
        {
            try
            {
                if (InterviewUpdateText.IsNullOrWhitespaceOrEmpty())
                    ShowErrorDialog(ErrorHelper.Except("APP_0027"));
                else
                {
                    var created = InterviewService.CreateInterviewUpdate(new InterviewUpdateModel(InterviewInfoModel.Id,                        
                                                                                                  _appState.OwnerLogged.Id.GetValueOrDefault(),
                                                                                                  InterviewUpdateText), _appState.AccessToken);
                    Updates.Add(created);

                    ShowSuccessDialog("Update Criado com Sucesso!");

                    LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(created), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                    ShowInterviewUpdatesModal = false;
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = InterviewUpdateText, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = InterviewUpdateText, Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
        }      

        public void UpdateInterviewUpdate()
        {
            try
            {              
                if (InterviewUpdateText.IsNullOrWhitespaceOrEmpty())
                    ShowErrorDialog(ErrorHelper.Except("APP_0027"));
                else
                {
                    SelectedInterviewUpdate.Resume = InterviewUpdateText;

                    var updated = InterviewService.UpdateInterviewUpdate(SelectedInterviewUpdate, _appState.AccessToken);

                    var currentUpdate = Updates.FirstOrDefault(x => x.Id.Equals(SelectedInterviewUpdate.Id));
                    Updates.Remove(currentUpdate);

                    Updates.Add(updated);

                    LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                    ShowSuccessDialog("Update Actualizado com Sucesso!");

                    ShowInterviewUpdatesModal = false;
                }
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }            
        }

        public async Task DeleteInterviewUpdate(long id)
        {  
            try
            {
                await ShowSpinner();
               
                SelectedInterviewUpdate = Updates.FirstOrDefault(x => x.Id == id);                   

                InterviewService.DeleteInterviewUpdate(SelectedInterviewUpdate, _appState.AccessToken);

                Updates.Remove(SelectedInterviewUpdate);

                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0011, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0002, Error = string.Empty }, _appState.AccessToken);

                ShowSuccessDialog("Update Removido com Sucesso!");

                StateHasChanged();                
               
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
            }
            finally
            {
                await HideSpinner();
            }
        }        

        #endregion

        public async Task DownloadFile(long? uploadId, string fileName)
        {
            try
            {
                await ShowSpinner();

                var download = CandidateService.DownloadFile(uploadId, _appState.AccessToken);

                var provider = new FileExtensionContentTypeProvider();

                if (!provider.TryGetContentType(fileName, out string contentType))
                {
                    contentType = "application/octet-stream";
                }

                await DownloadFileService.DownloadFile(fileName, download, contentType);
            }
            catch (AppException ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(ex);
            }
            catch (Exception ex)
            {
                LogService.CreateLog(new LogModel { IP = httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), Username = _appState.OwnerLogged.Name, CreatedAt = DateTime.Now, Operation = LogOperations.LO_0005, JsonRequest = JsonConvert.SerializeObject(SelectedInterviewUpdate), Status = LogStatus.LS_0001, Error = ex.Message }, _appState.AccessToken);
                ShowErrorDialog(new AppException("APP_0030", ex));
            }
            finally
            {
                await HideSpinner();
            }
        }

        #endregion

        #region InfoDialog 
        public void ShowSuccessDialog(string message)
        {
            ShowInfoDialog = true;
            IsSuccessDialog = true;
            InfoDialogHeaderText = "Sucesso!";
            InfoDialogContentText = message;
        }

        public void ShowWarningDialog(string message)
        {
            ShowInfoDialog = true;
            IsWarningDialog = true;
            InfoDialogHeaderText = "Atenção!";
            InfoDialogContentText = message;
        }

        public void ShowErrorDialog(AppException ex)
        {
            ShowInfoDialog = true;
            IsErrorDialog = true;
            InfoDialogHeaderText = "Erro!";
            InfoDialogContentText = $"{ex.Message} - ({ex.Code})";
        }


        public void HideInfoDialog()
        {
            ShowInfoDialog = false;
            InfoDialogHeaderText = null;
            InfoDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
        }

        #endregion

        public void Dispose()
        {
            CandidateId = null;
            ListInterviews = null;
            ListUpdates = null;
            ListUploads = null;
            tabs = null;
            btnFirstInterview = null;
            btnSecondInterview = null;
            btnThirdInterview = null;
            btnInfoDialogOk = null;
            cxtMenuInterviewUpdate = null;
            InterviewUpdateActions = null;
            CandidateTypeColor = null;
            Candidate = null;
            Interviews = null;
            InterviewInfoModel = null;
            Updates = null;
            SelectedInterviewUpdate = null;
            Uploads = null;
            VisibleSecondInterview = null;
            VisibleThirdInterview = null;
            FirstInterviewDate = null;
            FirstInterviewDescription = null;
            SecondInterviewDate = null;
            SecondInterviewDescription = null;
            ThirdInterviewDate = null;
            ThirdInterviewDescription = null;
            InterviewUpdateHeader = null;
            InterviewUpdateText = null;
            InfoDialogHeaderText = null;
            InfoDialogContentText = null;
            minLinesToShow = null;
            GC.Collect();
        }

    }
}
