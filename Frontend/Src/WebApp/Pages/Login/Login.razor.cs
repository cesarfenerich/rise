﻿using Common.Exceptions;
using Common.Models;
using Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System.Threading.Tasks;
using MatBlazor;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Http;
using System;
using Newtonsoft.Json;
using Domain.Entities.Constants;
using WebApp.Services.Interfaces;
using WebApp.Services;
using Domain.Models;
using Common.Helpers;
using System.Linq;
using WebApp.Shared;

namespace WebApp.Pages.Login
{
    [AllowAnonymous]
    public partial class LoginModel : ComponentBase, IDisposable
    {
        [Inject] private AppState _appState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }

        [Inject] protected IJSRuntime JsRuntime { get; set; }

        [Inject] protected IHttpContextAccessor httpContextAccessor { get; set; }

        [Inject] protected ILogService LogService { get; set; }
        [Inject] private IAuthService AuthService { get; set; }
        [Inject] private IOwnerService OwnerService { get; set; }

        [CascadingParameter] protected MainLayout _mainLayout { get; set; }


        protected string LoginDiv { get; set; }
        protected string Code { get; set; }
        protected string RecoverEmail { get; set; }
        protected string ConfirmPassword { get; set; }
        protected string UpdatePasswordMessage { get; set; }

        protected ErrorModel ErrorModel { get; set; } = new ErrorModel();
        protected AuthModel AuthModel { get; set; } = new AuthModel();
        protected AuthModel RecoverModel { get; set; } = new AuthModel();
        protected bool IsLogging { get; set; } = false;
        protected bool ShowLogin { get; set; } = true;
        protected bool ShowRecoverPassword { get; set; } = false;
        protected bool ShowRecoverPasswordCode { get; set; } = false; 
        protected bool ShowEmailSentMessage { get; set; } = false;
        protected bool ShowPasswordsDontMatch { get; set; } = false;
        protected bool ShowUpdatePassword { get; set; } = false; 
        protected bool ShowLoginFailed { get; set; }
        protected bool ShowVerifyCodeFailed { get; set; } = false;
        
        protected MatTextField<string> txtEmail = new MatTextField<string>();
        protected MatTextField<string> txtPassword = new MatTextField<string>();    
        protected MatTextField<string> txtCode = new MatTextField<string>();
        protected MatTextField<string> txtRecoverEmail = new MatTextField<string>();

        protected override async void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                StateHasChanged();
                await JsRuntime.InvokeVoidAsync("setAppHeightToFull");
               
            }
            else
                await HideSpinner();
        }

        public async Task ShowSpinner()
        {
            _mainLayout.SpinnerLabel = "A Carregar...";
            await _mainLayout.SetSpinner(true);            
        }

        public async Task HideSpinner()
        {
            await _mainLayout.SetSpinner(false);            
        }

        public void GoToRecoverPassword()
        {
            ShowLogin = false;
            ShowRecoverPassword = true;
        }

        public void GoToLogin()
        {
            ShowLogin = true;
            ShowRecoverPassword = false;
            ShowRecoverPasswordCode = false;
            ShowEmailSentMessage = false;
            ShowUpdatePassword = false;
            ShowVerifyCodeFailed = false;
            ShowLoginFailed = false;
            ShowPasswordsDontMatch = false;
            ConfirmPassword = string.Empty;
            RecoverEmail = string.Empty;
            Code = string.Empty;
            RecoverModel.Password = string.Empty;
            RecoverModel.Email = string.Empty;
        }
       

        protected async Task Login()
        {
            try
            {                
                IsLogging = true;       
                await ShowSpinner();
               
                ShowLoginFailed = false;

                if (AuthModel.Email.IsNullOrWhitespaceOrEmpty() ||
                    AuthModel.Password.IsNullOrWhitespaceOrEmpty())
                {
                    ShowLoginFailed = true;
                    IsLogging = false;
                    await HideSpinner();                    
                }
                else
                {
                    await JsRuntime.InvokeVoidAsync("clearContent");

                    var isLoggedIn = _appState.Login(AuthModel);                    

                    if (isLoggedIn)
                    {
                        _appState.IsLoggedIn = isLoggedIn;
                        
                        AuthModel.Password = SecurityHelper.Encript(AuthModel.Password);

                        LogService.CreateLog(new LogModel (httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), 
                                                           AuthModel.Email ?? "Desconhecido", 
                                                           DateTime.Now, 
                                                           LogOperations.LO_0001, 
                                                           JsonConvert.SerializeObject(AuthModel), 
                                                           LogStatus.LS_0002, string.Empty), _appState.AccessToken);
                        
                        await JsRuntime.InvokeAsync<string>("setContent", "token", _appState.RefreshToken);      

                        _navMan.NavigateTo("/home");
                    }                    
                }                
            }
            catch (AppException ex)
            {
                await HideSpinner();
                ShowLoginFailed = true;
                IsLogging = false;

                AuthModel.Password = string.Empty; 
                LogService.CreateLog(new LogModel (httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(), 
                                                   AuthModel.Email ?? "Desconhecido",
                                                   DateTime.Now, 
                                                   LogOperations.LO_0001,
                                                   JsonConvert.SerializeObject(AuthModel), 
                                                   LogStatus.LS_0001, 
                                                   ex.Message), _appState.AccessToken);                
            } 
            catch(Exception ex)
            {
                AuthModel.Password = string.Empty; 
                LogService.CreateLog(new LogModel(httpContextAccessor.HttpContext.Connection?.RemoteIpAddress.ToString(),
                                                  AuthModel.Email,
                                                  DateTime.Now,
                                                  LogOperations.LO_0001,
                                                  JsonConvert.SerializeObject(AuthModel),
                                                  LogStatus.LS_0001,
                                                  ex.Message), _appState.AccessToken);
            }
           
        }       

        public void OnFocusHandler(FocusEventArgs e)
        {
            ShowLoginFailed = false;            
        }

        public async Task Focus(string elementId)
        {
            await JsRuntime.InvokeVoidAsync("jsfunction.focusElement", elementId);
        }

        public async Task EnterLogin(KeyboardEventArgs e)
        {
            if (e?.Code?.Equals("Enter") ?? false)
                await Login();           
        }

        protected async Task SendRecoverEmail()
        {
            try
            {
                await ShowSpinner();
                if (!RecoverEmail.IsNullOrWhitespaceOrEmpty())
                {
                    var model = AuthService.ForgotPassword(RecoverEmail);

                    if(model.ForgotMailSent)
                    {
                        ShowRecoverPassword = false;
                        ShowRecoverPasswordCode = true;
                        //RecoverEmail = string.Empty;                        
                    }                    

                    await HideSpinner();
                }
                else
                {
                    //TODO: Warn user to fill the email
                    await HideSpinner();
                }
            }
            catch (AppException)
            {
                //TODO: Advise user that something went wrong
                await HideSpinner();
            }
        }

        protected async Task VerifyCode()
        {
            try
            {
                ShowVerifyCodeFailed = false;

                await ShowSpinner();
                if (!RecoverEmail.IsNullOrWhitespaceOrEmpty() && !Code.IsNullOrWhitespaceOrEmpty())
                {
                    var model = AuthService.VerifyCode(RecoverEmail, Code);

                    if (model.RecoveryCodeChecked)
                    {
                        ShowRecoverPasswordCode = false;
                        ShowUpdatePassword = true;
                    }
                    else
                    {
                        //TODO: Advise user that something went wrong
                        ShowVerifyCodeFailed = true;                        
                    }
                    await HideSpinner();
                }
                else
                {
                    await HideSpinner();
                }
            }
            catch (AppException)
            {
                ShowVerifyCodeFailed = true;
                await HideSpinner();
            }
        }

        protected async Task RecoverPassword()
        {
            try
            {
                RecoverModel.Email = RecoverEmail;
                ShowPasswordsDontMatch = false;

                await ShowSpinner();
                if (!RecoverModel.Password.IsNullOrWhitespaceOrEmpty() &&
                    !ConfirmPassword.IsNullOrWhitespaceOrEmpty())
                {
                    if (ValidatePasswordChange(RecoverModel.Password, ConfirmPassword, out string validationMessage))
                    {
                        //Call backend to update owner
                        var newOwner = OwnerService.UpdateOwnerPassword(new OwnerModel { Email = RecoverModel.Email, Password = RecoverModel.Password });
                        GoToLogin();
                    }
                    else
                    {
                        UpdatePasswordMessage = validationMessage;
                        ShowPasswordsDontMatch = true;
                    }
                    await HideSpinner();
                }
                else
                {
                    await HideSpinner();
                }
            }
            catch (AppException)
            {
               await HideSpinner();
            }
        }

        private bool ValidatePasswordChange(string password, string confirmPassword, out string validationMessage)
        {
            if (string.IsNullOrEmpty(password))
            {
                validationMessage = "Password inválida.";
                return false;
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                validationMessage = "Confirmação de password inválida.";
                return false;
            }

            if (!password.Equals(confirmPassword))
            {
                validationMessage = "As passwords não conferem.";
                return false;
            }

            if (password.Length < 10 || !password.Any(char.IsDigit) || !password.Any(char.IsUpper) || !CheckSpecialCharacters(password))
            {
                validationMessage = "A password não respeita os requisitos. Necessita ter pelo menos 1 maiúscula, 1 número 2 símbolos, e tem de ter um comprimento de 10 algarismos.";
                return false;
            }

            validationMessage = null;
            return true;
        }

        private bool CheckSpecialCharacters(string word)
        {
            char[] original = word.ToCharArray();
            char[] copy = new char[original.Length];
            int c = 0;
            for (int i = 0; i < original.Length; i++)
            {
                if (!char.IsLetterOrDigit(original[i]))
                {
                    copy[c] = original[i];
                    c++;
                }
            }
            Array.Resize(ref copy, c);
            if (copy.Length == 0) return false;
            return true;
        }

        public void Dispose()
        {
            txtEmail = null;
            txtPassword = null;
            ErrorModel = null;
            AuthModel = null;
            GC.Collect();
        }
    }
}
