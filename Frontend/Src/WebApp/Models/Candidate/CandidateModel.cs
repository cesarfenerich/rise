﻿using System;

namespace WebApp.Models
{
    public class CandidateModel
    {       
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Citizenship { get; set; }
        public string MaritalStatus { get; set; }
        public int DependentsUnderSevenYears { get; set; }       
        public int DependentsOlderSevenYears { get; set; }
        public string Location { get; set; } 
        
        public EducationModel Education{ get; set; }

        public CandidateModel()
        {
            this.Education = new EducationModel();

        }
    }
}
