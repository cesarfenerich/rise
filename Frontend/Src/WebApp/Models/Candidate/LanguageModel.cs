﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class LanguageModel
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Level { get; set; }

        public LanguageModel(int id, string language)
        {
            this.Id = id;
            this.Language = language;
        }
    }
}
