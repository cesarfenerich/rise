﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class LocationModel
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public LocationModel(int id, string description)
        {
            this.Id = id;
            this.Description = description;
        }
    }
}
