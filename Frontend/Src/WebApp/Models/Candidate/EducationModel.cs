﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class EducationModel
    {        
        public string LiteraryAbilities { get; set; }
        public string College { get; set; }
        public string Certifications { get; set; }      
        public IEnumerable<LanguageModel> Languages { get; set; }
        public string JobRole { get; set; }

        public EducationModel()
        {
            this.Languages = new List<LanguageModel>();
        }
    }
}
