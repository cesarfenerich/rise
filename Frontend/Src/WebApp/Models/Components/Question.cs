﻿namespace WebApp.Models.Components
{
    public class Question
    {
        public int Order { get; set; }
        public string Area { get; set; }
        public string Description { get; set; }
        public int MaxAnswerValue { get; set; }
        public string ValueChecked { get; set; }

        public Question(int order, string area, string description, int maxAnswerValue, string valueChecked = "0")
        {
            this.Order = order;
            this.Area = area;
            this.Description = description;
            this.MaxAnswerValue = maxAnswerValue;
            this.ValueChecked = valueChecked;
        }

        public void SetCheckedValue(string valueChecked)
        {
            this.ValueChecked = valueChecked;
        }
    }
}
