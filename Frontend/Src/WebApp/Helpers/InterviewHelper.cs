﻿using Domain.Entities.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Helpers
{
    public static class InterviewHelper
    { 
        public static string ConvertToExperience(int? candidateExperience)
        {
            if(candidateExperience.HasValue)
            {
                switch (candidateExperience.Value)
                {
                    case 0:
                        return "YOE_0001";
                    case 1:
                        return "YOE_0002";
                    case 2:
                    case 3:
                        return "YOE_0003";
                    case 4:
                    case 5:
                        return "YOE_0004";
                    default:
                        return "YOE_0005";                        
                }
            }
            return string.Empty;
        }

        public static int ConvertFromExperience(string candidateExperience)
        {
            switch (candidateExperience)
            {
                case "YOE_0001":
                    return 0;
                case "YOE_0002":
                    return 1;
                case "YOE_0003":
                    return 3;                
                case "YOE_0004":
                    return 5;
                default:
                    return 999;
            }
        }

        public static string GetExperience(string candidateExperience)
        {
            if (!string.IsNullOrEmpty(candidateExperience))
            {
                switch (candidateExperience)
                {
                    case "0":
                        return YearsOfExperienceRange.YOE_0001;
                    case "1":
                        return YearsOfExperienceRange.YOE_0002;
                    case "2":
                    case "3":                        
                        return YearsOfExperienceRange.YOE_0003;
                    case "4":
                    case "5":
                        return YearsOfExperienceRange.YOE_0004;
                    default:
                        return YearsOfExperienceRange.YOE_0005;
                }
            }
            return string.Empty;
        }

        public static string GetAvailableInterval(string candidateAvailableInterval)
        {
            if (!string.IsNullOrEmpty(candidateAvailableInterval))
            {
                return candidateAvailableInterval switch
                {
                    "CINT_0001" => "Imediata",
                    "CINT_0002" => "2 semanas",
                    "CINT_0003" => "4 semanas",
                    "CINT_0004" => "8 semanas",
                    _ => "Data Fixa",
                };
            }
            return string.Empty;
        }
    }
}
