﻿using Common.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using WebApp.Services;

namespace WebApp.Components
{
    public class RedirectToLoginModel : ComponentBase, IDisposable
    {
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] private AppState _appState { get; set; }      

        public void Dispose()
        {
            GC.Collect();
        }

        protected override async void OnInitialized()
        {
      
        }


        protected override async void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {                
                var token = await JsRuntime.InvokeAsync<string>("getContent", "token");  

                if (_appState.AuthenticateByToken(token))
                    NavigationManager.NavigateTo(NavigationManager.Uri);               
                else
                    NavigationManager.NavigateTo("/");
            }
        }
    }
}
