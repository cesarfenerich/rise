﻿using Common.Extensions;
using Domain.Entities.Constants;
using Domain.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Spinner;
using System;
using System.Threading.Tasks;
using WebApp.Helpers;
using WebApp.Services;

namespace WebApp.Components
{
    public class InterviewDialogModel : ComponentBase, IDisposable
    {
        [Inject] protected IJSRuntime JsRuntime { get; set; }
        [Inject] protected AppState AppState { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }       

        protected SfDialog ModalInterview = new SfDialog();
        protected SfTab TabsInfoInterview = new SfTab();
        protected SfSpinner Spinner = new SfSpinner();

        public InterviewInfoModel InterviewInfoModel { get; set; } = new InterviewInfoModel();
        public bool ShowModalInterview { get; set; } = false;
        public string CandidateTypeColor { get; set; } = string.Empty;

        public void ShowSpinner()
        {
            Spinner.Show();
        }

        public void HideSpinner()
        {
            Spinner.Hide();
        }

        public async void DisableModalInterview()
        {
            ShowModalInterview = false;
            await Focus("page");
        }

        public void RedirectToFirstTab()
        {
            TabsInfoInterview.Select(0);
        }

        public async void GoToInterview(MouseEventArgs e)
        {
            ShowSpinner();

            if (InterviewInfoModel.Status.Equals("IS_0001"))
            {
                AppState.EnableHeaderForCreateInterview();
                _navMan.NavigateTo($"/interview/{InterviewInfoModel.Id}");
            }
            else
            {
                AppState.EnableHeaderForCandidateView();
                _navMan.NavigateTo($"/candidate/{InterviewInfoModel.CandidateId}");
            }

            ShowModalInterview = false;
        }

        public async Task Focus(string elementId)
        {
            await JsRuntime.InvokeVoidAsync("jsfunction.focusElement", elementId);
        }

        public void Dispose()
        {
            ModalInterview = null;
            TabsInfoInterview = null;
            InterviewInfoModel = null;
            CandidateTypeColor = null;
            GC.Collect();
        }

        public void Hide()
        {
            ShowModalInterview = false;
        }

        public void Show(InterviewInfoModel model)
        {
            //InterviewInfoModel = InterviewService.GetInterviewInfoById(id);
            InterviewInfoModel = model;
            InterviewInfoModel.YearsOfExperience = InterviewHelper.GetExperience(InterviewInfoModel.YearsOfExperience);

            if (InterviewInfoModel.CandidateAvailabilityInterval.IsNullOrWhitespaceOrEmpty())
                InterviewInfoModel.CandidateAvailableAt = "Nenhum";  
            else if(!InterviewInfoModel.CandidateAvailabilityInterval.Equals("CINT_0005"))
                InterviewInfoModel.CandidateAvailableAt = string.Empty;

            InterviewInfoModel.CandidateAvailabilityInterval = InterviewHelper.GetAvailableInterval(InterviewInfoModel.CandidateAvailabilityInterval);

            CandidateTypeColor = InterviewInfoModel.CandidateType switch
            {
                CandidateTypes.CDT_0002 => "green",
                CandidateTypes.CDT_0003 => "blue",
                CandidateTypes.CDT_0004 => "yellow",
                CandidateTypes.CDT_0005 => "red",
                _ => string.Empty,
            };

            ShowModalInterview = true;

            StateHasChanged();
        }
    }
}
