﻿using Domain.Models;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Popups;
using System;
using System.Threading.Tasks;
using WebApp.Services;

namespace WebApp.Components
{
    public class ArchiveInterviewDialogModel : ComponentBase
    {
        [Inject] private IInterviewService InterviewService { get; set; }
        [Inject] private NavigationManager _navMan { get; set; }
        [Inject] private AppState _appState { get; set; }

        protected SfDialog ModalArchiveInterviewDialog = new SfDialog();
        protected DialogButton btnSave = new DialogButton();
        protected DialogButton btnCancel = new DialogButton();

        public Func<Task> CallBack { get; set; }

        protected bool ShowModal { get; set; } = false;
        protected InterviewModel interview { get; set; }
        protected string archiveReason { get; set; } = string.Empty;
        protected string warningError { get; set; } = string.Empty;
        private long interviewId { get; set; }      

        protected async Task SaveReason()
        {
            if (String.IsNullOrEmpty(archiveReason))
            {
                warningError = "É obrigatório informar o motivo da ação.";
                return;
            }

            InterviewService.ArchiveInterview(new InterviewArchiveModel(interview.Id.GetValueOrDefault(), archiveReason), _appState.AccessToken);

            CleanModalElements();
            CallBack.Invoke();
        }

        private void CleanModalElements()
        {
            interviewId = -1;
            archiveReason = String.Empty;
            warningError = String.Empty;
            ShowModal = false;          
            StateHasChanged();
        }

        protected void Cancel()
        {
            CleanModalElements();
        }

        public void Show(InterviewModel _interview, Func<Task> callBack)
        {
            interview = _interview;
            ShowModal = true;
            CallBack = callBack;

            StateHasChanged();
        }
    }
}
