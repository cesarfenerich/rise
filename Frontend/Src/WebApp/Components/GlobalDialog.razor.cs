﻿using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Popups;
using System;

namespace WebApp.Components
{
    public class GlobalDialogModel : ComponentBase, IDisposable
    {
        public SfDialog globalDialog = new SfDialog();
        public DialogButton btnGlobalDialogOk = new DialogButton();

        [Parameter]
        public Action? CloseCallback { get; set; }

        [Parameter]
        public bool ShowInterviewDialog { get; set; } = false;
        [Parameter]
        public string GlobalDialogHeaderText { get; set; } = string.Empty;
        [Parameter]
        public string GlobalDialogContentText { get; set; } = string.Empty;
        [Parameter]
        public bool IsErrorDialog { get; set; } = false;
        [Parameter]
        public bool IsWarningDialog { get; set; } = false;
        [Parameter]
        public bool IsSuccessDialog { get; set; } = false;

        public void Dispose()
        {
            globalDialog = null;
            btnGlobalDialogOk = null;
            ShowInterviewDialog = false;
            GlobalDialogHeaderText = null;
            GlobalDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
            GC.Collect();
        }

        public void HideDialog()
        {
            ShowInterviewDialog = false;
            GlobalDialogHeaderText = null;
            GlobalDialogContentText = null;
            IsSuccessDialog = false;
            IsWarningDialog = false;
            IsErrorDialog = false;
            if (CloseCallback != null)
            {
                CloseCallback.Invoke();
            }
        }
    }
}
