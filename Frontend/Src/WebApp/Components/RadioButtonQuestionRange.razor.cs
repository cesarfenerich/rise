﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using WebApp.Models.Components;

namespace WebApp.Components
{
    public class RadioButtonQuestionRangeModel : ComponentBase, IDisposable
    {
        public string LastQuestionArea = string.Empty;

        [Parameter]
        public List<Question> Questions { get;  set; }                  

        public void Dispose()
        {
            Questions = null;
        }
    }
}
