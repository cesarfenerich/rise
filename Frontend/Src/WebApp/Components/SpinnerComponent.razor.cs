﻿using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Spinner;
using System;
using System.Threading.Tasks;

namespace WebApp.Components
{
    public partial class SpinnerCode: ComponentBase, IDisposable
    {
        [Parameter] public string Label { get; set; }
        protected SfSpinner _spinner = new SfSpinner();

        protected override void OnAfterRender(bool firstRender)
        {  }

        public void Show()
        {
            _spinner.Show();
            Task.Delay(350).Wait();                      
        }

        public void Hide()
        {
            _spinner.Hide();
            Task.Delay(350).Wait();                     
        }

        public void Dispose()
        {
            _spinner = null;
            GC.Collect();
        }
    }
}
