using Common.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace WebApp
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            ConfigurationHelper.SetBasePath();
          
            CreateHostBuilder(args).Build().Run();
                 
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
             Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel(options => options.Configure());
                    webBuilder.UseStartup<Startup>();                   
                });

    }
}
