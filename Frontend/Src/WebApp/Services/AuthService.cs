using Common;
using Common.Models;
using Domain.Models;
using System;
using System.Threading.Tasks;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class AuthService : IAuthService
    {
        readonly IBaseService _base;       

        public AuthService(IBaseService baseService)
        {
            _base = baseService;
        }              

        public UserModel Authenticate(AuthModel model)
        {
            var response = _base.Post<AuthModel>(Urls.Login, model);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<UserModel>(content);            
        }

        public async Task<UserModel> GetUserById(long userId)
        {
            var url = string.Format(Urls.UserById, userId);
            var content = _base.HandleResponse(_base.Get(url));

            return _base.DeserializeResponse<UserModel>(content);
        }

        public RecoverPasswordModel ForgotPassword(string email)
        {
            var url = string.Format(Urls.ForgotPassword, email);

            var content = _base.HandleResponse(_base.Post(url));

            return _base.DeserializeResponse<RecoverPasswordModel>(content);
        }

        public RecoverPasswordModel VerifyCode(string email, string code)
        {
            var url = string.Format(Urls.VerifyCode, email, code);

            var content = _base.HandleResponse(_base.Post(url));

            return _base.DeserializeResponse<RecoverPasswordModel>(content);
        }

        public UserModel AuthenticateByToken(string refreshToken)
        {
            var response = _base.Get(String.Format(Urls.Token, refreshToken), null);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<UserModel>(content);
        }
    }
}
