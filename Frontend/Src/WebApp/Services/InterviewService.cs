using Common;
using Common.Extensions;
using Common.Models;
using Domain.Models;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class InterviewService : IInterviewService
    {
        readonly IBaseService _base;      

        public InterviewService(IBaseService baseService)
        {
            _base = baseService;
        }

        public InterviewModel GetInterviewById(long interviewId, string token)
        {
            var url = string.Format(Urls.InterviewById, interviewId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<InterviewModel>(content);
        }

        public InterviewGridModel[] GetLatestInterviews(string token)
        {           
            var content = _base.HandleResponse(_base.Get(Urls.RecentInterviews, token));

            return _base.DeserializeResponse<InterviewGridModel[]>(content);
        }

        public SelectionModel GetInterviewCount(string token)
        {
            var content = _base.HandleResponse(_base.Get(Urls.CountInterviews, token));

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public SelectionModel[] GetContractTypes(string token)
        {
            var response = _base.Get(Urls.ContractTypes, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetInterviewStatus(string token)
        {
            var response = _base.Get(Urls.InterviewStatus, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetSalaryExpectationRange(string token)
        {
            var response = _base.Get(Urls.ExpectedSalaries, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetYearsOfExperienceRange(string token)
        {
            var response = _base.Get(Urls.YearsOfExperience, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public InterviewModel CreateInterview(InterviewModel model, string token)
        {
            var response = _base.Post<InterviewModel>(Urls.InterviewCreation, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<InterviewModel>(content);
        }

        public InterviewModel UpdateInterview(InterviewModel model, string token)
        {
            var response = _base.Put(Urls.InterviewUpdate, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<InterviewModel>(content);
        }

        public BaseSearchModel[] GetQuickSearchSuggestions(string search, string token)
        {
            var url = string.Format(Urls.QuickSearchSuggestion, search.RemoveSpecialCharacters());
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<BaseSearchModel[]>(content);
        }

        public InterviewGridModel[] DoQuickSearch(string field, string search, string token)
        {
            var url = string.Format(Urls.QuickSearch, field.RemoveSpecialCharacters(), search, token);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<InterviewGridModel[]>(content);
        }

        public InterviewGridModel[] DoAdvancedSearch(AdvancedSearchModel model, string token)
        {
            var response = _base.Post<AdvancedSearchModel>(Urls.AdvancedSearch, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<InterviewGridModel[]>(content);
        }     

        public InterviewUpdateModel[] GetInterviewUpdates(long interviewId, string token)
        {
            var url = string.Format(Urls.UpdatesByInterview, interviewId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<InterviewUpdateModel[]>(content);
        }

        public InterviewUpdateModel CreateInterviewUpdate(InterviewUpdateModel model, string token)
        {
            var response = _base.Post(Urls.InterviewUpdateCreation, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<InterviewUpdateModel>(content);
        }

        public InterviewUpdateModel UpdateInterviewUpdate(InterviewUpdateModel model, string token)
        {
            var response = _base.Put(Urls.InterviewUpdateUpdate, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<InterviewUpdateModel>(content);
        }

        public bool DeleteInterviewUpdate(InterviewUpdateModel model, string token)
        {
            var url = string.Format(Urls.InterviewUpdateDeletion, model.Id);
            
            _base.HandleResponse(_base.Delete(url,token));

            return true;
        }

        public InterviewInfoModel GetInterviewInfoById(long interviewId, string token)
        {
            var url = string.Format(Urls.InterviewInfoById, interviewId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<InterviewInfoModel>(content);
        } 

        public bool ArchiveInterview(InterviewArchiveModel model, string token)
        {
            var response = _base.Post<InterviewArchiveModel>(Urls.InterviewArchive, model, token);
            _base.HandleResponse(response);           

            return true;
        }
    }
}
