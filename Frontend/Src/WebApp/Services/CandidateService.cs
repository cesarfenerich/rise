using Common;
using Common.Exceptions;
using Common.Models;
using Domain.Models;
using System;
using System.Net.Http;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class CandidateService : ICandidateService
    {
        readonly IBaseService _base;       

        public CandidateService(IBaseService baseService)
        {
            _base = baseService;
        }             

        public CandidateModel GetCandidateById(long candidateId, string token)
        {
            var url = string.Format(Urls.CandidateById, candidateId);           
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<CandidateModel>(content);            
        }

        public InterviewGridModel[] GetLastInterviewsOfCandidate(long candidateId, string token)
        {
            var url = string.Format(Urls.LastInterviewsOfCandidateById, candidateId);
            var content = _base.HandleResponse( _base.Get(url, token));

            return _base.DeserializeResponse<InterviewGridModel[]>(content);
        }
        
        public UploadModel UploadFile(string fileName, byte[] fileToUpload, string token)
        {
            using var content = new MultipartFormDataContent();            
            using var bytes = new ByteArrayContent(fileToUpload);

            content.Add(bytes, "File", fileName);   

            var response = _base.PostFile(Urls.FileUpload, content, token);
            var result = _base.HandleResponse(response);

            return _base.DeserializeResponse<UploadModel>(result);
        }

        public byte[] DownloadFile(long? uploadId, string token)
        {
            var url = string.Format(Urls.FileDownload, uploadId.Value);

            return _base.HandleResponseAsBinary(_base.Get(url, token));           
        }


        public SelectionModel[] GetCandidateTypes(string token)
        {
            var response = _base.Get(Urls.CandidateTypes, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetCandidateAvailabilities(string token)
        {
            var response = _base.Get(Urls.CandidateAvailability, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetCandidateIntervals(string token)
        {
            var response = _base.Get(Urls.CandidateInterval, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetCandidateMobility(string token)
        {
            var response = _base.Get(Urls.CandidateMobility, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetLiteraryAbilities(string token)
        {
            var response = _base.Get(Urls.LiteraryAbilities, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetMaritalStatuses(string token)
        {
            var response = _base.Get(Urls.MaritalStatus, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }       

        public SelectionModel[] GetLanguages(string token)
        {
            var response = _base.Get(Urls.Languages, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetLocations(string token)
        {
            var response = _base.Get(Urls.Locations, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetJobRoles(string token)
        {
            var response = _base.Get(Urls.JobRoles, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public TechSkillModel[] GetTechSkills(string token)
        {
            var response = _base.Get(Urls.TechSkills, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<TechSkillModel[]>(content);
        }

        public BaseSearchModel[] SearchByCandidateNames(string search, string token)
        {
            throw new System.NotImplementedException();
        }

        public BaseSearchModel[] SearchByCandidateJobRoles(string search, string token)
        {
            throw new System.NotImplementedException();
        }     

        public CandidateModel GetCandidateByReference(CandidateSearchModel requestModel, string token)
        {            
            var response = _base.Post<CandidateSearchModel>(Urls.CandidateReference, requestModel, token);

            if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                return null;

            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<CandidateModel>(content);            
        }

        public SelectionModel[] GetAllTechSkillCategories(string token)
        {
            var response = _base.Get(Urls.TechSkillsCategories, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }      

        public CandidateModel CreateCandidate(CandidateModel model, string token)
        {
            var response = _base.Post<CandidateModel>(Urls.CreateCandidate, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<CandidateModel>(content);
        }

        public CandidateModel UpdateCandidate(CandidateModel model, string token)
        {
            var response = _base.Put<CandidateModel>(Urls.UpdateCandidate, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<CandidateModel>(content);
        }

        public SelectionModel CreateJobRole(SelectionModel requestModel, string token)
        {
            var response = _base.Post<SelectionModel>(Urls.CreateJobRole, requestModel, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public SelectionModel UpdateJobRole(SelectionModel model, string token)
        {
            var response = _base.Put<SelectionModel>(Urls.UpdateJobRole, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public bool DeleteJobRole(long jobRoleId, string token)
        {
            var url = string.Format(Urls.DeleteJobRole, jobRoleId);
            var response = _base.Delete(url, token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public SelectionModel CreateLocation(SelectionModel requestModel, string token)
        {
            var response = _base.Post<SelectionModel>(Urls.CreateLocation, requestModel, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public SelectionModel CreateLanguage(SelectionModel requestModel, string token)
        {
            var response = _base.Post<SelectionModel>(Urls.CreateLanguage, requestModel, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public TechSkillModel CreateTechSkill(TechSkillModel requestModel, string token)
        {
            var response = _base.Post<TechSkillModel>(Urls.CreateTechSkill, requestModel, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<TechSkillModel>(content);
        }

        public TechSkillModel UpdateTechSkill(TechSkillModel model, string token)
        {
            var response = _base.Put<TechSkillModel>(Urls.UpdateTechSkill, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<TechSkillModel>(content);
        }

        public bool DeleteTechSkill(long techSkillId, string token)
        {
            var url = string.Format(Urls.DeleteTechSkill, techSkillId);
            var response = _base.Delete(url, token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public SelectionModel CreateCandidateLanguage(long candidateId, long idLanguage, string token)
        {
            var url = string.Format(Urls.CreateCandidateLanguage, candidateId, idLanguage);
            var response = _base.Post(url, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<SelectionModel>(content);
        }

        public TechSkillModel CreateCandidateTechSkill(long candidateId, long idTechSkill, string token)
        {
            var url = string.Format(Urls.CreateCandidateTechSkill, candidateId, idTechSkill);
            var response = _base.Post(url, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<TechSkillModel>(content);
        }

        public UploadModel CreateCandidateUpload(long candidateId, long idUpload, string token)
        {
            var url = string.Format(Urls.CreateCandidateUpload, candidateId, idUpload);
            var content = _base.HandleResponse(_base.Post(url, token));

            return _base.DeserializeResponse<UploadModel>(content);
        }

        public NaifasModel CreateNaifas(NaifasModel requestModel, string token)
        {
            var response = _base.Post<NaifasModel>(Urls.CreateNaifas, requestModel, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<NaifasModel>(content);
        }

        public NaifasModel UpdateNaifas(NaifasModel model, string token)
        {
            var response = _base.Put<NaifasModel>(Urls.UpdateNaifas, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<NaifasModel>(content);
        }

        public NaifasModel CreateCandidateNaifas(long candidateId, long idNaifas, string token)
        {
            var url = string.Format(Urls.CreateCandidateNaifas, candidateId, idNaifas);
            var response = _base.Post(url, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<NaifasModel>(content);
        }

        public bool DeleteCandidateLanguage(long candidateId, long idLanguage, string token)
        {
            var url = string.Format(Urls.DeleteCandidateLanguage, candidateId, idLanguage);
            var response = _base.Delete(url,token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public bool DeleteCandidateTechSkill(long candidateId, long idTechSkill, string token)
        {
            var url = string.Format(Urls.DeleteCandidateTechSkill, candidateId, idTechSkill);
            var response = _base.Delete(url,token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public bool DeleteCandidateNaifas(long candidateId, long idNaifas, string token)
        {
            var url = string.Format(Urls.DeleteCandidateNaifas, candidateId, idNaifas);
            var response = _base.Delete(url,token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public bool DeleteCandidateUpload(long candidateId, long idUpload, string token)
        {
            var url = string.Format(Urls.DeleteCandidateUpload, candidateId, idUpload);
            var response = _base.Delete(url, token);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public SelectionModel[] GetLanguagesByCandidate(long candidateId, string token)
        {
            var url = string.Format(Urls.GetCandidateLanguages, candidateId);
            var content = _base.HandleResponse(_base.Get(url,token));

            return _base.DeserializeResponse<SelectionModel[]>(content);
        }

        public TechSkillModel[] GetTechSkillsByCandidate(long candidateId, string token)
        {
            var url = string.Format(Urls.GetCandidateTechSkills, candidateId);
            var content = _base.HandleResponse(_base.Get(url,token));

            return _base.DeserializeResponse<TechSkillModel[]>(content);
        }

        public UploadModel[] GetUploadsByCandidate(long candidateId, string token)
        {
            var url = string.Format(Urls.GetCandidateUploads, candidateId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<UploadModel[]>(content);
        }

        public UploadModel GetFileUpload(string fileName, string token)
        {
            try
            {
                var url = string.Format(Urls.GetFileUpload, fileName);
                var content = _base.HandleResponse(_base.Get(url, token));

                return _base.DeserializeResponse<UploadModel>(content);
            }
            catch (AppException)
            {                
                return null;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public NaifasModel[] GetNaifasByCandidate(long candidateId, string token)
        {
            var url = string.Format(Urls.GetCandidateNaifas, candidateId);
            var content = _base.HandleResponse(_base.Get(url,token));

            return _base.DeserializeResponse<NaifasModel[]>(content);
        }        
    }
}
