﻿using Blazored.LocalStorage;
using System.Threading.Tasks;

namespace WebApp.Services
{
    public class StorageService
    {
        private readonly ILocalStorageService _localStorage;

        public StorageService(ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
        }

        public async Task<T> GetFromLocalStore<T>(string key)
            => await _localStorage.GetItemAsync<T>(key);

        public async void SaveAtLocalStore(string key, object value)
            => await _localStorage.SetItemAsync(key, value);

        public async void RemoveFromLocalStore(string key)
            => await _localStorage.RemoveItemAsync(key);

        public async void ClearLocalStore()
            => await _localStorage.ClearAsync();
    }
}
