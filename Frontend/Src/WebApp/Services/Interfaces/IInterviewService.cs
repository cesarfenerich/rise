using Common.Models;
using Domain.Models;

namespace WebApp.Services
{
    public interface IInterviewService
    {
        InterviewModel GetInterviewById(long interviewId, string token);
        InterviewInfoModel GetInterviewInfoById(long interviewId, string token);
        SelectionModel GetInterviewCount(string token);
        InterviewGridModel[] GetLatestInterviews(string token);
        SelectionModel[] GetContractTypes(string token);
        SelectionModel[] GetInterviewStatus(string token);
        SelectionModel[] GetSalaryExpectationRange(string token);
        InterviewModel CreateInterview(InterviewModel model, string token);
        InterviewModel UpdateInterview(InterviewModel model, string token);
        BaseSearchModel[] GetQuickSearchSuggestions(string search, string token);
        InterviewGridModel[] DoQuickSearch(string field, string search, string token);
        InterviewGridModel[] DoAdvancedSearch(AdvancedSearchModel model, string token);
        InterviewUpdateModel[] GetInterviewUpdates(long interviewId, string token);
        InterviewUpdateModel CreateInterviewUpdate(InterviewUpdateModel model, string token);
        InterviewUpdateModel UpdateInterviewUpdate(InterviewUpdateModel model, string token);
        bool DeleteInterviewUpdate(InterviewUpdateModel model, string token);
        SelectionModel[] GetYearsOfExperienceRange(string token);  
        bool ArchiveInterview(InterviewArchiveModel model, string token);
    }
}
