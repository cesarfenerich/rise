﻿using System.Net.Http;

namespace WebApp.Services.Interfaces
{
    public interface IBaseService
    {
        #region ApiClient

        HttpResponseMessage Get(string path, string token = null);

        HttpResponseMessage Post(string path, string token = null);

        HttpResponseMessage Post<T>(string path, T requestModel, string token = null);

        HttpResponseMessage PostFile(string path, MultipartFormDataContent content, string token = null);

        HttpResponseMessage Put<T>(string path, T requestModel, string token = null);

        HttpResponseMessage Delete(string url, string token = null);

        T DeserializeResponse<T>(string payload);

        string HandleResponse(HttpResponseMessage response);

        byte[] HandleResponseAsBinary(HttpResponseMessage response);

        #endregion

        #region LocalStorage

        //Task<T> GetFromLocalStore<T>(string key);

        //void SaveAtLocalStore(string key, object value);

        //void RemoveFromLocalStore(string key);

        //void ClearLocalStore();

        #endregion
    }
}
