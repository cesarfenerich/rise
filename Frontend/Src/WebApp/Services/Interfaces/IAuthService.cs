using Common.Models;
using Domain.Models;
using System.Threading.Tasks;

namespace WebApp.Services
{
    public interface IAuthService
    {
        UserModel Authenticate(AuthModel model);
        Task<UserModel> GetUserById(long userId);
        RecoverPasswordModel ForgotPassword(string email);
        RecoverPasswordModel VerifyCode(string email, string code);
        UserModel AuthenticateByToken(string refreshToken);
    }
}
