using Common.Models;
using Domain.Models;

namespace WebApp.Services
{
    public interface ICandidateService
    {
        CandidateModel GetCandidateById(long candidateId, string token);
        InterviewGridModel[] GetLastInterviewsOfCandidate(long candidateId, string token);
        UploadModel UploadFile(string fileName, byte[] fileToUpload, string token);
        SelectionModel[] GetCandidateTypes(string token);
        SelectionModel[] GetCandidateAvailabilities(string token);
        SelectionModel[] GetCandidateIntervals(string token);
        SelectionModel[] GetCandidateMobility(string token);
        SelectionModel[] GetLiteraryAbilities(string token);
        SelectionModel[] GetMaritalStatuses(string token);
        SelectionModel[] GetLanguages(string token);
        SelectionModel[] GetLocations(string token);
        SelectionModel[] GetJobRoles(string token);
        TechSkillModel[] GetTechSkills(string token);
        SelectionModel[] GetAllTechSkillCategories(string token);        
        BaseSearchModel[] SearchByCandidateNames(string search, string token);
        BaseSearchModel[] SearchByCandidateJobRoles(string search, string token);     
        CandidateModel CreateCandidate(CandidateModel model, string token);
        CandidateModel UpdateCandidate(CandidateModel model, string token);
        SelectionModel CreateJobRole(SelectionModel requestModel, string token);
        SelectionModel UpdateJobRole(SelectionModel requestModel, string token);
        bool DeleteJobRole(long jobRoleId, string token);
        SelectionModel CreateLocation(SelectionModel requestModel, string token);
        SelectionModel CreateLanguage(SelectionModel requestModel, string token);
        TechSkillModel CreateTechSkill(TechSkillModel requestModel, string token);
        TechSkillModel UpdateTechSkill(TechSkillModel requestModel, string token);
        bool DeleteTechSkill(long techSkillId, string token);
        CandidateModel GetCandidateByReference(CandidateSearchModel requestModel, string token);
        SelectionModel CreateCandidateLanguage(long candidateId, long idLanguage, string token);
        TechSkillModel CreateCandidateTechSkill(long candidateId, long idTechSkill, string token);
        UploadModel CreateCandidateUpload(long candidateId, long idUpload, string token);
        NaifasModel CreateNaifas(NaifasModel requestModel, string token);
        NaifasModel UpdateNaifas(NaifasModel model, string token);
        NaifasModel CreateCandidateNaifas(long candidateId, long idNaifas, string token);
        bool DeleteCandidateLanguage(long candidateId, long idLanguage, string token);
        bool DeleteCandidateTechSkill(long candidateId, long idTechSkill, string token);
        bool DeleteCandidateUpload(long candidateId, long idUpload, string token);
        bool DeleteCandidateNaifas(long candidateId, long idNaifas, string token);
        SelectionModel[] GetLanguagesByCandidate(long candidateId, string token);
        TechSkillModel[] GetTechSkillsByCandidate(long candidateId, string token);
        UploadModel[] GetUploadsByCandidate(long candidateId, string token);
        UploadModel GetFileUpload(string name, string token);
        byte[] DownloadFile(long? uploadId, string token);
        NaifasModel[] GetNaifasByCandidate(long candidateId, string token);      
    }
}
