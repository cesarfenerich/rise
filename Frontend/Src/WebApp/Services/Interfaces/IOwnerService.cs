using Common.Models;
using Domain.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApp.Services
{
    public interface IOwnerService
    {
        OwnerModel GetOwnerById(long ownerId, string token);
        OwnerModel[] GetAllOwners(string token);
        OwnerModel[] GetActiveOwners(string token);
        OwnerModel CreateOwner(OwnerModel model, string token);
        OwnerModel UpdateOwner(OwnerModel model, string token);
        OwnerModel UpdateOwnerPassword(OwnerModel model);        
        bool IsBlock(long ownerId, string token);
    }
}
