﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Services.Interfaces
{
    public interface ILogService
    {
        LogModel GetLogById(long logId, string token);
        LogModel[] GetAllLogs(string token);
        LogModel CreateLog(LogModel model, string token);
        LogModel CreateLog(string ip, string username, DateTime? createdAt, string operation, string request, string status, string error, string token);
    }
}
