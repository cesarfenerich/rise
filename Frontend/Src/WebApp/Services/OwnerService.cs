using Common;
using Domain.Models;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class OwnerService : IOwnerService
    {
        readonly IBaseService _base;       

        public OwnerService(IBaseService baseService)
        {
            _base = baseService;
        }                       

        public OwnerModel GetOwnerById(long ownerId, string token)
        {
            var url = string.Format(Urls.OwnerById, ownerId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<OwnerModel>(content);
        }        
        public OwnerModel[] GetAllOwners(string token)
        {
            var content = _base.HandleResponse(_base.Get(Urls.AllOwners, token));

            return _base.DeserializeResponse<OwnerModel[]>(content);
        }
        public OwnerModel[] GetActiveOwners(string token)
        { 
            var content = _base.HandleResponse(_base.Get(Urls.ActiveOwners, token));

            return _base.DeserializeResponse<OwnerModel[]>(content);
        }
        public OwnerModel CreateOwner(OwnerModel model, string token)
        {
            var response = _base.Post<OwnerModel>(Urls.OwnerCreation, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<OwnerModel>(content);
        }
        public OwnerModel UpdateOwner(OwnerModel model, string token)
        {
            var response = _base.Put<OwnerModel>(Urls.OwnerUpdate, model, token);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<OwnerModel>(content);
        }
        public OwnerModel UpdateOwnerPassword(OwnerModel model)
        {
            var response = _base.Put<OwnerModel>(Urls.UpdateOwnerPassword, model);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<OwnerModel>(content);
        }
        public bool IsBlock(long ownerId, string token)
        {
            var url = string.Format(Urls.IsBlock, ownerId);
            var content = _base.HandleResponse(_base.Get(url,token));

            return _base.DeserializeResponse<bool>(content);
        }
    }
}
