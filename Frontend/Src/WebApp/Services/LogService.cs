﻿using Common;
using Domain.Models;
using System;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class LogService : ILogService
    {
        readonly IBaseService _base;

        public LogService(IBaseService baseService)
        {
            _base = baseService;
        }

        public LogModel GetLogById(long logId, string token)
        {
            var url = string.Format(Urls.LogById, logId);
            var content = _base.HandleResponse(_base.Get(url, token));

            return _base.DeserializeResponse<LogModel>(content);
        }

        public LogModel[] GetAllLogs(string token)
        {
            var content = _base.HandleResponse(_base.Get(Urls.AllLogs, token));
            return _base.DeserializeResponse<LogModel[]>(content);
        }

        public LogModel CreateLog(LogModel model, string token)
        {
            var response = _base.Post<LogModel>(Urls.LogCreation, model, null);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<LogModel>(content);
        }

        public LogModel CreateLog(string ip, string username, DateTime? createdAt, string operation, string request, string status, string error, string token)
        {
            var model = new LogModel(ip, username, createdAt ?? DateTime.Now, operation, request, status, error);

            var response = _base.Post<LogModel>(Urls.LogCreation, model, null);
            var content = _base.HandleResponse(response);

            return _base.DeserializeResponse<LogModel>(content);
        }
    }
}
