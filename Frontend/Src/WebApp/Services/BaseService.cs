﻿using Blazored.LocalStorage;
using Common.Exceptions;
using Common.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class BaseService : IBaseService
    {                
        readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public string AccessToken { get; set; }


        public BaseService(HttpClient HttpClient, IConfiguration configuration)
        {            
            _httpClient = HttpClient;
            _configuration = configuration;            
        }

        #region ApiClient

        //TODO: Checar AuthHeader

        public HttpResponseMessage Get(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Get, path);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Post(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Post, path);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Post<T>(string path, T requestModel, string token = null)
        {
            HandleRequestHeaders(token);

            var content = HandleJsonRequest(requestModel);
            var request = RequestBuilder(HttpMethod.Post, path, content);

            return _httpClient.SendAsync(request).Result;            
        }

        public HttpResponseMessage PostFile(string path, MultipartFormDataContent content, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Post, path, content);

            return _httpClient.PostAsync(request.RequestUri, content).Result;
        }

        public HttpResponseMessage Put<T>(string path, T requestModel, string token = null)
        {
            HandleRequestHeaders(token);

            var content = HandleJsonRequest(requestModel);
            var request = RequestBuilder(HttpMethod.Put, path, content);
           
            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Delete(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Delete, path);

            return _httpClient.SendAsync(request).Result;
        }

        #region Handlers        

        private void HandleRequestHeaders(string token)
        {       
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (token != null)
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        private string GetAccessToken()
        {
            throw new NotImplementedException();
        }

        private static StreamContent HandleJsonRequest(object payload)
        {
            //Uncomment and breakpoint to get the string payload of request
            //string cont = JsonSerializer.Serialize(payload);
            var content = JsonSerializer.SerializeToUtf8Bytes(payload);

            var streamContent = new StreamContent(new MemoryStream(content));

            streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return streamContent;
        }

        public T DeserializeResponse<T>(string payload)
        {
            var options = new JsonSerializerOptions
            {
                IgnoreNullValues = true,
                PropertyNameCaseInsensitive = true   
            };

            return JsonSerializer.Deserialize<T>(payload, options);
        }        

        #endregion

        #region Utils

        private HttpRequestMessage RequestBuilder(HttpMethod method, string path, HttpContent content = null)
        {
            var url = _configuration.GetValue<string>("API:BaseUrl");
            var request = new HttpRequestMessage
            {
                Version = HttpVersion.Version20,
                Method = method,
                RequestUri = new Uri($"{url}/{path}"),

                Content = content
            };

            request.Headers.TryAddWithoutValidation("Accept", "application/json");

            return request;
        }

        public string HandleResponse(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                var error = DeserializeResponse<ErrorModel>(content);                
                throw new AppException(error.Code, error.Message);
            }

            return content;
        }

        public byte[] HandleResponseAsBinary(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsByteArrayAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                var payload = response.Content.ReadAsStringAsync().Result;
                var error = DeserializeResponse<ErrorModel>(payload);                
                throw new AppException(error.Code, error.Message);
            }

            return content;
        }

        #endregion

        #endregion

        #region LocalStorage

        //public async Task<T> GetFromLocalStore<T>(string key)
        //    => await LocalStore.GetItemAsync<T>(key);

        //public async void SaveAtLocalStore(string key, object value)
        //    => await LocalStore.SetItemAsync(key, value);       

        //public async void RemoveFromLocalStore(string key)
        //    => await LocalStore.RemoveItemAsync(key);

        //public async void ClearLocalStore()
        //    => await LocalStore.ClearAsync();

        #endregion        
    }
}
