﻿window.click = (index) => {    
    switch (index) {
        case 1:
            $("#btnFirstInterview").css("background", "#889AFD");
            $("#btnSecondInterview").css("background", "#F4F4F4");
            $("#btnThirdInterview").css("background", "#F4F4F4");
            break;
        case 2:
            $("#btnFirstInterview").css("background", "#F4F4F4");
            $("#btnSecondInterview").css("background", "#889AFD");
            $("#btnThirdInterview").css("background", "#F4F4F4");
            break;
        case 3:
            $("#btnFirstInterview").css("background", "#F4F4F4");
            $("#btnSecondInterview").css("background", "#F4F4F4");
            $("#btnThirdInterview").css("background", "#889AFD");
            break;        
    }
};

var getAppElement = function () {
    return document.getElementsByTagName("app")[0];
}

var setAppHeightToAuto = function () {   
    $(getAppElement()).attr('style', 'height:auto;')
}

var setAppHeightToFull = function () {        
    $(getAppElement()).attr('style', 'height:100%;')
}

var getContent = function (key) {
    return window.localStorage.getItem(key);
}

var setContent = function (key, value) {
    window.localStorage.setItem(key, value);
}

var clearContent = function () {
    window.localStorage.clear();
}