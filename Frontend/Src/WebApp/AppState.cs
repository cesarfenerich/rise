﻿using Common.Models;
using Domain.Models;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using WebApp.Services;

namespace WebApp
{
    public class AppState : AuthenticationStateProvider
    {
        private readonly IAuthService _authService;
        private readonly IOwnerService _ownerService;
        private readonly IConfiguration _configuration;

        public bool IsLoggedIn { get; set; }
        public long? OwnerId { get; private set; }
        public OwnerModel OwnerLogged { get; private set; } = new OwnerModel();
        public string AccessToken { get; private set; }
        public Guid RefreshToken { get; private set; }
        public DateTime? TokenExpiration { get; private set; }

        public bool ShowHomeHeader { get; set; }
        public bool ShowInterviewHeader { get; set; }
        public bool ShowQuickSearchHeader { get; set; }
        public bool ShowSearchHeader { get; set; }
        public bool ShowCandidateHeader { get; set; }
        public bool ShowUsersHeader { get; set; }
        public bool ShowLogsHeader { get; set; }
        public bool IsLoading { get; set; }

        public AppState(IAuthService authService, IOwnerService ownerService, IConfiguration configuration)
        {
            _authService = authService;
            _ownerService = ownerService;
            _configuration = configuration;
        }

        #region Auth

        public bool Login(AuthModel model)
        {
            var responseModel =  _authService.Authenticate(model);

            if (responseModel.AccessToken != null)
            {
                AccessToken = responseModel.AccessToken;
                RefreshToken = responseModel.RefreshToken;
                TokenExpiration = responseModel.ExpiresAt;               
                SetLoggedOwner(responseModel.OwnerId.Value);
                NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
            }

            return true;
        }

        public bool AuthenticateByToken(string refreshToken)
        {
            try
            {
                var responseModel = _authService.AuthenticateByToken(refreshToken);

                if (responseModel is null)
                    return false;
                {
                    AccessToken = responseModel.AccessToken;
                    RefreshToken = responseModel.RefreshToken;
                    TokenExpiration = responseModel.ExpiresAt;
                    IsLoggedIn = true;
                    SetLoggedOwner(responseModel.OwnerId);
                    NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }            
        }

        public void Logout()
        {
            AccessToken = null;
            TokenExpiration = null;
            IsLoggedIn = false;
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public override Task<AuthenticationState> GetAuthenticationStateAsync()
        {           
            var identity = string.IsNullOrEmpty(AccessToken)
                ? new ClaimsIdentity()
                : new ClaimsIdentity(ParseClaimsFromJwt(AccessToken), "jwt");
            return Task.FromResult(new AuthenticationState(new ClaimsPrincipal(identity)));
        }

        private static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
            return keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString()));
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }

        #endregion

        #region Handlers
        public string GetBaseUrlExternal()
        {
            return _configuration.GetValue<string>("API:BaseUrlExternal");
        }

        public void SetLoggedOwner(long? ownerId)
        {
            this.OwnerLogged = _ownerService.GetOwnerById(ownerId.GetValueOrDefault(), this.AccessToken);
        }    

        public void SetSpinner(bool show)
        {
            IsLoading = show;
        }

        #endregion

        #region Headers

        public void EnableHeaderForHome()
        {
            this.ShowHomeHeader = true;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForQuickSearch()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = true;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForSearch()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = true;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForCreateInterview()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = true;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForCandidateView()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = true;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForUsers()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = true;
            this.ShowLogsHeader = false;
        }

        public void EnableHeaderForLogs()
        {
            this.ShowHomeHeader = false;
            this.ShowInterviewHeader = false;
            this.ShowQuickSearchHeader = false;
            this.ShowSearchHeader = false;
            this.ShowCandidateHeader = false;
            this.ShowUsersHeader = false;
            this.ShowLogsHeader = true;
        }
        #endregion
    }
}
