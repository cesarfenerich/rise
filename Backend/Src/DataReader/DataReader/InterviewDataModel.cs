﻿using Domain.Models;
using System.Text;

namespace DataReader
{
    public class InterviewDataModel
    {
        public object Location { get; private set; } 
        public object CreatedAt { get; private set; } 
        public object Notes { get; private set; } 
        public object ExpectedSalary { get; private set; } 
        public object OwnerName { get; private set; }
        public object CandidateFirstName { get; private set; }
        public object CandidateLastName { get; private set; }
        public object JobRole { get; private set; }
        public object PhoneNumber { get; private set; }
        public object MobilePhoneNumber { get; private set; }
        public object Email { get; private set; }
        public object KeyTechSkills { get; private set; }
        public object LastActivity { get; private set; }
        public object LinkedinProfile { get; private set; }
        public object FrameworkTechSkills { get; private set; }
        public object DependentsTotal { get; private set; }
        public object Databases { get; private set; }
        public object DependentsAbove6 { get; private set; }
        public object SAPModules { get; private set; }
        public object CurrentSalary { get; private set; }
        public object Languages { get; private set; }
        public object Availability { get; private set; }
        public object MaritalStatus { get; private set; }
        public object Citizenship { get; private set; }
        public object College { get; private set; }
        public object BornDate { get; private set; }
        public object LiteraryAbility { get; private set; }
        public object ProgrammingTechSkills { get; private set; }
        public object YearsOfExperience { get; private set; }

        public InterviewDataModel()
        { }

        public InterviewDataModel(object[] row)
        {
            Location = row[0];
            CreatedAt = row[1]; 
            Notes = row[2];
            ExpectedSalary = row[3];
            OwnerName = row[4];
            CandidateFirstName = row[5];
            CandidateLastName = row[6];
            JobRole = row[7];
            PhoneNumber = row[8];
            MobilePhoneNumber = row[9];
            Email = row[10];
            KeyTechSkills = row[11];
            LastActivity = row[12];
            LinkedinProfile = row[13];
            FrameworkTechSkills = row[14];
            DependentsTotal = row[15];
            Databases = row[16];
            DependentsAbove6 = row[17];
            SAPModules = row[18];
            CurrentSalary = row[19];
            Languages = row[20];
            Availability = row[21];
            MaritalStatus = row[22];
            YearsOfExperience = row[23];
            Citizenship = row[24];
            College = row[25];
            BornDate = row[26];
            LiteraryAbility = row[27];
            ProgrammingTechSkills = row[28];
        }        

        public string AsString()
        {
            StringBuilder builder = new StringBuilder("Linha");

            builder.AppendLine(Location?.ToString() ?? string.Empty);
            builder.AppendLine(CreatedAt?.ToString() ?? string.Empty);
            builder.AppendLine(Notes?.ToString() ?? string.Empty);
            builder.AppendLine(ExpectedSalary?.ToString() ?? string.Empty);
            builder.AppendLine(OwnerName?.ToString() ?? string.Empty);
            builder.AppendLine(CandidateFirstName?.ToString() ?? string.Empty);
            builder.AppendLine(CandidateLastName?.ToString() ?? string.Empty);
            builder.AppendLine(JobRole?.ToString() ?? string.Empty);
            builder.AppendLine(PhoneNumber?.ToString() ?? string.Empty);
            builder.AppendLine(MobilePhoneNumber?.ToString() ?? string.Empty);
            builder.AppendLine(Email?.ToString() ?? string.Empty);
            builder.AppendLine(KeyTechSkills?.ToString() ?? string.Empty);
            builder.AppendLine(LastActivity?.ToString() ?? string.Empty);
            builder.AppendLine(LinkedinProfile?.ToString() ?? string.Empty);
            builder.AppendLine(FrameworkTechSkills?.ToString() ?? string.Empty);
            builder.AppendLine(DependentsTotal?.ToString() ?? string.Empty);
            builder.AppendLine(Databases?.ToString() ?? string.Empty);
            builder.AppendLine(DependentsAbove6?.ToString() ?? string.Empty);
            builder.AppendLine(SAPModules?.ToString() ?? string.Empty);
            builder.AppendLine(CurrentSalary?.ToString() ?? string.Empty);
            builder.AppendLine(Languages?.ToString() ?? string.Empty);
            builder.AppendLine(Availability?.ToString() ?? string.Empty);
            builder.AppendLine(MaritalStatus?.ToString() ?? string.Empty);
            builder.AppendLine(YearsOfExperience?.ToString() ?? string.Empty);
            builder.AppendLine(Citizenship?.ToString() ?? string.Empty);
            builder.AppendLine(College?.ToString() ?? string.Empty);
            builder.AppendLine(BornDate?.ToString() ?? string.Empty);
            builder.AppendLine(LiteraryAbility?.ToString() ?? string.Empty);
            builder.AppendLine(ProgrammingTechSkills?.ToString() ?? string.Empty);

            return builder.ToString();
        }       
    }
}
