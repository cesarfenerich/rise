﻿using Common;
using Common.Extensions;
using Common.Exceptions;
using Common.Models;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices.ComTypes;

namespace DataReader 
{ 
    public class DataService
    {
        readonly HttpClient _httpClient;

        public DataService()
        {
            _httpClient = new HttpClient();
        }

        #region DataTransform        

        public void ImportInterview(InterviewDataModel model)
        {
            var ownerId = GetOwnerIdByName(GetOwnerName(model.OwnerName?.ToString()));
            var candidateId = SaveCandidate(model);

            var interviewToImport = new InterviewModel(ownerId, 
                                                       model.OwnerName?.ToString(), 
                                                       candidateId, 
                                                       "IS_0001", 
                                                       null, 
                                                       null, 
                                                       null, 
                                                       HandleSalary(model.CurrentSalary), 
                                                       true, 
                                                       HandleSalary(model.ExpectedSalary), 
                                                       true, 
                                                       null, 
                                                       HandleNotes(model.Notes, model.LastActivity),
                                                       HandleCreatedDate(model.CreatedAt));

            CreateInterview(interviewToImport);           
        }

        private string GetOwnerName(string ownerName)
        {
            return ownerName switch
            {
                "Catarina P." => "Nádia Ferreira",
                "Inês B." => "Inês Reis Borges",
                "Inês V." => "Inês Castelo Branco Pulido Valente",
                "Joao Ruivo H." => "João Miguel Zarcos Ruivo Henriques",
                "Luisa M." => "Nádia Ferreira",
                "Madalena F." => "Madalena Albuquerque Ferreira",
                "Mariana B." => "Mariana Mello Breyner",
                "Nadia F." => "Nádia Ferreira",
                "Natália S." => "Natália dos Santos Silva",
                "Patricia M." => "Nádia Ferreira",
                "Pedro R." => "Pedro Rodrigues",
                "Sandra S." => "Sandra Santos",
                "Sara M." => "Sara Marques",
                "Viviane L." => "Viviane Almeida de Sousa Lino",
                _ => "Nádia Ferreira",
            };
        }

        private string HandleNotes(object notes, object lastActivity)
        {
            if (notes is null && lastActivity is null)
                return null;
            else
            {
                var strNotes = notes.ToString().Replace("➢", "");
                var strLast = notes.ToString().Replace("➢", "");
                var junction = $"{strNotes}\n{strLast}";

                Regex.Replace(junction, @"[^\w\s\:\,\/\(\)\&\""\-\.@]", "",
                              RegexOptions.None, TimeSpan.FromSeconds(1.5));

                return RemoveDiacritics(junction);
            }            
        }

        private string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        private long? SaveCandidate(InterviewDataModel model)
        {
            var candidateToImport = GetCandidateByReference(new CandidateSearchModel($"{model.CandidateFirstName} {model.CandidateLastName}", model.Email?.ToString() ?? string.Empty));

            if (candidateToImport is null)
            {
                var jobRoleId = CreateJobRole(new SelectionModel(0, model.JobRole?.ToString() ?? string.Empty)).Id;
                var locationId = CreateLocation(new SelectionModel(0, model.Location?.ToString() ?? string.Empty)).Id;
                HandleDependents(model.DependentsAbove6, model.DependentsTotal, out int? dependentsUnder7, out int? dependentsAbove7);

                candidateToImport = new CandidateModel(jobRoleId,
                                                       locationId,
                                                       null,
                                                       $"{model.CandidateFirstName} {model.CandidateLastName}",
                                                       HandleBornDate(model.BornDate),
                                                       HandlePhoneNumber(model.PhoneNumber, model.MobilePhoneNumber),
                                                       model.Email?.ToString() ?? null,
                                                       model.Citizenship?.ToString() ?? null,
                                                       HandleMaritalStatus(model.MaritalStatus),
                                                       dependentsUnder7,
                                                       dependentsAbove7,
                                                       HandleLiteraryAbility(model.LiteraryAbility),
                                                       model.College?.ToString(),
                                                       null,
                                                       HandleAvailability(model.Availability),
                                                       model.LinkedinProfile?.ToString() ?? null,
                                                       HandleYearsOfExperience(model.YearsOfExperience));

                candidateToImport = CreateCandidate(candidateToImport);

                if (candidateToImport.Id.HasValue)
                {
                    HandleLanguages(candidateToImport.Id.Value, model.Languages);
                    HandleTechSkills(candidateToImport.Id.Value, model.KeyTechSkills, model.FrameworkTechSkills, model.Databases, model.SAPModules, model.ProgrammingTechSkills);
                }
            }

            return candidateToImport.Id;
        }

        private int? HandleYearsOfExperience(object yearsOfExperience)
        {
            if (yearsOfExperience is null)
                return null;
            else
            {
                if (Int32.TryParse(yearsOfExperience.ToString().NumbersOnly(), out int result))
                    return result;
                else
                    return null;
            }
        }

        private string HandlePhoneNumber(object phoneNumber, object mobilePhoneNumber)
        {
            if (phoneNumber is null && mobilePhoneNumber is null)
                return string.Empty;
            else
            {
                StringBuilder builder = new StringBuilder();               
                   
                builder.AppendJoin(" ", phoneNumber?.ToString()?.NumbersOnly() ?? string.Empty, 
                                        mobilePhoneNumber?.ToString()?.NumbersOnly() ?? string.Empty);

                return builder.ToString();
            }           
        }

        private string HandleLiteraryAbility(object literaryAbility)
        {
            if (literaryAbility is null)
                return null;
            else
            {
                var ability = literaryAbility.ToString().Split(',');

                if (ability[0].InsensitiveContains("Outro"))
                    return null;
                else
                {
                    var abilit = ability[0].RemoveSpecialCharacters();

                    if (abilit.InsensitiveContains("Ensino Básico".RemoveSpecialCharacters()))
                        return "LA_0002";
                    if (abilit.InsensitiveContains("Ensino Secundário".RemoveSpecialCharacters()))
                        return "LA_0003";
                    if (abilit.InsensitiveContains("Licenciatura".RemoveSpecialCharacters()))
                        return "LA_0004";
                    if (abilit.InsensitiveContains("Mestrado".RemoveSpecialCharacters()))
                        return "LA_0005";
                    if (abilit.InsensitiveContains("Pós-graduação".RemoveSpecialCharacters()))
                        return "LA_0006";
                    if (abilit.InsensitiveContains("Doutoramento".RemoveSpecialCharacters()))
                        return "LA_0007";
                    if (abilit.InsensitiveContains("Bacharel".RemoveSpecialCharacters()))
                        return "LA_0008";
                    if (abilit.InsensitiveContains("Tecnólogo".RemoveSpecialCharacters()))
                        return "LA_0009";

                    return null;
                }
            }
        }

        #region Handlers

        private DateTime? HandleCreatedDate(object createdAt)
        {
            if (createdAt is null)
                return DateTime.Now;
            else
            {
                string[] date = createdAt.ToString().Split('-');

                var day = Convert.ToInt32(date[0]);
                var month = Convert.ToInt32(date[1]);
                var year = Convert.ToInt32(date[2]) + 2000;

                return new DateTime(year, month, day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }
        }

        private long? GetOwnerIdByName(string ownerName)
        {
            var owners = GetAllOwners();

            return owners.FirstOrDefault(x => x.Name.Equals(ownerName))?.Id ?? 16;
        }

        private string HandleAvailability(object availability)
        {
            if (availability is null)
                return "CAV_0001";
            else
            {
                if (availability.ToString().InsensitiveContains("Outros"))
                    return "CAV_0003";
                else
                    return "CAV_0002";                
            }
        }

        private string HandleMaritalStatus(object maritalStatus)
        {
            if (maritalStatus is null)
                return null;
            else
            {
                if (maritalStatus.ToString().InsensitiveContains("Outro"))
                    return null;
                else if (maritalStatus.ToString().InsensitiveContains("Não Casado"))
                    return "MS_0001";
                else
                    return "MS_0002";
            }
        }

        private DateTime? HandleBornDate(object bornDate)
        {
            if (bornDate is null)
                return null;
            else
            {
                if (DateTime.TryParse(bornDate.ToString(), out DateTime result))
                    return result;
                else
                    return null;                
            }
        }

        private void HandleDependents(object dependentsAbove6, object dependentsTotal, out int? dependentsUnder7, out int? dependentsAbove7)
        {
            if (dependentsTotal is null)
            {
                dependentsUnder7 = null;
                dependentsAbove7 = null;
            }
            else
            {
                if (dependentsAbove6 is null)
                {
                    dependentsUnder7 = null;
                    dependentsAbove7 = Convert.ToInt32(dependentsTotal.ToString());
                }
                else
                {
                    dependentsUnder7 = Convert.ToInt32(dependentsTotal.ToString()) - Convert.ToInt32(dependentsAbove6.ToString());
                    dependentsAbove7 = Convert.ToInt32(dependentsAbove6.ToString());
                }
            }
        }

        private decimal HandleSalary(object salary)
        {
            Decimal.TryParse(salary?.ToString().NumbersOnly() ?? "0", out decimal result);

            if (result >= 9999999)
                result = 0;

            return result;
        }       

        private void HandleLanguages(long candidateId, object languages)
        {
            if (languages != null)
            {
                string[] langs = languages.ToString().Split(',');

                foreach (string lang in langs)
                {
                    var l = lang.Trim();

                    if (l.InsensitiveContains("ingles"))
                        l = "Inglês";

                    var language = CreateLanguage(new SelectionModel(0, l));

                    if (language != null && language.Id.HasValue)
                        CreateCandidateLanguage(candidateId, language.Id.Value);
                }
                
            }
        }

        private void HandleTechSkills(long candidateId, object keyTechSkills, object frameworkTechSkills, object databases, object sAPModules, object programmingTechSkills)
        {
            if (keyTechSkills != null)
            {
                string[] skills = keyTechSkills.ToString().Split(',');

                foreach (string skill in skills)
                {
                    var skil = CreateTechSkill(new TechSkillModel(0, "TSC_0001", skill.Trim()));

                    if (skil != null)
                        CreateCandidateTechSkill(candidateId, skil.Id);
                }
            }

            if (frameworkTechSkills != null)
            {
                string[] skills = frameworkTechSkills.ToString().Split(',');

                foreach (string skill in skills)
                {
                    var skil = CreateTechSkill(new TechSkillModel(0, "TSC_0008", skill.Trim()));

                    if (skil != null)
                        CreateCandidateTechSkill(candidateId, skil.Id);
                }
            }

            if (databases != null)
            {
                string[] skills = databases.ToString().Split(',');

                foreach (string skill in skills)
                {
                    var skil = CreateTechSkill(new TechSkillModel(0, "TSC_0005", skill.Trim()));

                    if (skil != null)
                        CreateCandidateTechSkill(candidateId, skil.Id);
                }
            }

            if (sAPModules != null)
            {
                string[] skills = sAPModules.ToString().Split(',');

                foreach (string skill in skills)
                {
                    var skil = CreateTechSkill(new TechSkillModel(0, "TSC_0010", skill.Trim()));

                    if (skil != null)
                        CreateCandidateTechSkill(candidateId, skil.Id);
                }
            }              
        }

        #endregion

        #endregion

        #region Generics

        #region ApiClient    

        public HttpResponseMessage Get(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Get, path);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Post(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Post, path);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Post<T>(string path, T requestModel, string token = null)
        {
            HandleRequestHeaders(token);

            var content = HandleJsonRequest(requestModel);
            var request = RequestBuilder(HttpMethod.Post, path, content);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Put<T>(string path, T requestModel, string token = null)
        {
            HandleRequestHeaders(token);

            var content = HandleJsonRequest(requestModel);
            var request = RequestBuilder(HttpMethod.Put, path, content);

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Delete(string path, string token = null)
        {
            HandleRequestHeaders(token);

            var request = RequestBuilder(HttpMethod.Delete, path);

            return _httpClient.SendAsync(request).Result;
        }

        #endregion

        #region Handlers        

        private void HandleRequestHeaders(string token)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (token != null)
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        private static StreamContent HandleJsonRequest(object payload)
        {
            var content = JsonSerializer.SerializeToUtf8Bytes(payload);

            var streamContent = new StreamContent(new MemoryStream(content));

            streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return streamContent;
        }

        public T DeserializeResponse<T>(string payload)
        {
            var options = new JsonSerializerOptions
            {
                IgnoreNullValues = true,
                PropertyNameCaseInsensitive = true
            };

            return JsonSerializer.Deserialize<T>(payload, options);
        }
        
        #endregion

        #region Utils

        private HttpRequestMessage RequestBuilder(HttpMethod method, string path, HttpContent content = null)
        {
            var request = new HttpRequestMessage
            {
                Version = HttpVersion.Version20,
                Method = method,
                RequestUri = new Uri($"http://localhost:8181/1/{path}"), //TODO: Parameterize http://backend-628003632.us-east-1.elb.amazonaws.com/1/{path}
                Content = content
            };

            request.Headers.TryAddWithoutValidation("Accept", "application/json");

            return request;
        }

        public string HandleResponse(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                var error = DeserializeResponse<ErrorModel>(content);

                Console.WriteLine(error.Trace);

                throw new AppException(error.Code, error.Message);
            }

            return content;
        }

        #endregion

        #endregion

        #region Domain

        #region Candidate

        public CandidateModel GetCandidateById(long candidateId)
        {
            var url = string.Format(Urls.CandidateById, candidateId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<CandidateModel>(content);
        }

        public InterviewGridModel[] GetLastInterviewsOfCandidate(long candidateId)
        {
            var url = string.Format(Urls.LastInterviewsOfCandidateById, candidateId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<InterviewGridModel[]>(content);
        }

        public UploadModel UploadFile(IFormFile file)
        {
            var response = Post<IFormFile>(Urls.FileUpload, file);
            var content = HandleResponse(response);

            return DeserializeResponse<UploadModel>(content);
        }


        public SelectionModel[] GetCandidateTypes()
        {
            var response = Get(Urls.CandidateTypes);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetCandidateAvailabilities()
        {
            var response = Get(Urls.CandidateAvailability);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetLiteraryAbilities()
        {
            var response = Get(Urls.LiteraryAbilities);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetMaritalStatuses()
        {
            var response = Get(Urls.MaritalStatus);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetLanguages()
        {
            var response = Get(Urls.Languages);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetLocations()
        {
            var response = Get(Urls.Locations);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetJobRoles()
        {
            var response = Get(Urls.JobRoles);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public TechSkillModel[] GetTechSkills()
        {
            var response = Get(Urls.TechSkills);
            var content = HandleResponse(response);

            return DeserializeResponse<TechSkillModel[]>(content);
        }      

        public CandidateModel GetCandidateByReference(CandidateSearchModel requestModel)
        {
            var response = Post<CandidateSearchModel>(Urls.CandidateReference, requestModel);

            if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                return null;

            var content = HandleResponse(response);

            return DeserializeResponse<CandidateModel>(content);
        }

        public SelectionModel[] GetAllTechSkillCategories()
        {
            var response = Get(Urls.TechSkillsCategories);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public CandidateModel CreateCandidate(CandidateModel model)
        {
            var response = Post<CandidateModel>(Urls.CreateCandidate, model);
            var content = HandleResponse(response);

            return DeserializeResponse<CandidateModel>(content);
        }

        public CandidateModel UpdateCandidate(CandidateModel model)
        {
            var response = Put<CandidateModel>(Urls.UpdateCandidate, model);
            var content = HandleResponse(response);

            return DeserializeResponse<CandidateModel>(content);
        }

        public SelectionModel CreateJobRole(SelectionModel requestModel)
        {
            var response = Post<SelectionModel>(Urls.CreateJobRole, requestModel);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel>(content);
        }

        public SelectionModel CreateLocation(SelectionModel requestModel)
        {
            var response = Post<SelectionModel>(Urls.CreateLocation, requestModel);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel>(content);
        }

        public SelectionModel CreateLanguage(SelectionModel requestModel)
        {
            var response = Post<SelectionModel>(Urls.CreateLanguage, requestModel);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel>(content);
        }

        public TechSkillModel CreateTechSkill(TechSkillModel requestModel)
        {
            var response = Post<TechSkillModel>(Urls.CreateTechSkill, requestModel);
            var content = HandleResponse(response);

            return DeserializeResponse<TechSkillModel>(content);
        }

        public SelectionModel CreateCandidateLanguage(long candidateId, long idLanguage)
        {
            var url = string.Format(Urls.CreateCandidateLanguage, candidateId, idLanguage);
            var response = Post(url);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel>(content);
        }

        public TechSkillModel CreateCandidateTechSkill(long candidateId, long idTechSkill)
        {
            var url = string.Format(Urls.CreateCandidateTechSkill, candidateId, idTechSkill);
            var response = Post(url);
            var content = HandleResponse(response);

            return DeserializeResponse<TechSkillModel>(content);
        }

        public UploadModel CreateCandidateUpload(long candidateId, long idUpload)
        {
            var url = string.Format(Urls.CreateCandidateUpload, candidateId, idUpload);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<UploadModel>(content);
        }

        public NaifasModel CreateNaifas(NaifasModel requestModel)
        {
            var response = Post<NaifasModel>(Urls.CreateNaifas, requestModel);
            var content = HandleResponse(response);

            return DeserializeResponse<NaifasModel>(content);
        }

        public NaifasModel UpdateNaifas(NaifasModel model)
        {
            var response = Put<NaifasModel>(Urls.UpdateNaifas, model);
            var content = HandleResponse(response);

            return DeserializeResponse<NaifasModel>(content);
        }

        public NaifasModel CreateCandidateNaifas(long candidateId, long idNaifas)
        {
            var url = string.Format(Urls.CreateCandidateNaifas, candidateId, idNaifas);
            var response = Post(url);
            var content = HandleResponse(response);

            return DeserializeResponse<NaifasModel>(content);
        }

        public bool DeleteCandidateLanguage(long candidateId, long idLanguage)
        {
            var url = string.Format(Urls.DeleteCandidateLanguage, candidateId, idLanguage);
            var response = Delete(url);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public bool DeleteCandidateTechSkill(long candidateId, long idTechSkill)
        {
            var url = string.Format(Urls.DeleteCandidateTechSkill, candidateId, idTechSkill);
            var response = Delete(url);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public bool DeleteCandidateUpload(long candidateId, long idUpload)
        {
            var url = string.Format(Urls.DeleteCandidateUpload, candidateId, idUpload);
            var response = Delete(url);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public SelectionModel[] GetLanguagesByCandidate(long candidateId)
        {
            var url = string.Format(Urls.GetCandidateLanguages, candidateId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public TechSkillModel[] GetTechSkillsByCandidate(long candidateId)
        {
            var url = string.Format(Urls.GetCandidateTechSkills, candidateId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<TechSkillModel[]>(content);
        }

        public UploadModel[] GetUploadsByCandidate(long candidateId)
        {
            var url = string.Format(Urls.GetCandidateUploads, candidateId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<UploadModel[]>(content);
        }

        #endregion

        #region Owner

        public OwnerModel GetOwnerById(long ownerId)
        {
            var url = string.Format(Urls.OwnerById, ownerId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<OwnerModel>(content);
        }

        public OwnerModel[] GetAllOwners()
        {
            var content = HandleResponse(Get(Urls.AllOwners));

            return DeserializeResponse<OwnerModel[]>(content);
        }
        public OwnerModel[] GetActiveOwners()
        {
            var content = HandleResponse(Get(Urls.ActiveOwners));

            return DeserializeResponse<OwnerModel[]>(content);
        }

        public OwnerModel CreateOwner(OwnerModel model)
        {
            var response = Post<OwnerModel>(Urls.OwnerCreation, model);
            var content = HandleResponse(response);

            return DeserializeResponse<OwnerModel>(content);
        }

        public OwnerModel UpdateOwner(OwnerModel model)
        {
            var response = Put<OwnerModel>(Urls.OwnerUpdate, model);
            var content = HandleResponse(response);

            return DeserializeResponse<OwnerModel>(content);
        }

        #endregion

        #region Interview

        public InterviewModel GetInterviewById(long interviewId)
        {
            var url = string.Format(Urls.InterviewById, interviewId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<InterviewModel>(content);
        }

        public InterviewGridModel[] GetLatestInterviews()
        {
            var content = HandleResponse(Get(Urls.RecentInterviews));

            return DeserializeResponse<InterviewGridModel[]>(content);
        }

        public SelectionModel[] GetContractTypes()
        {
            var response = Get(Urls.ContractTypes);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetInterviewStatus()
        {
            var response = Get(Urls.InterviewStatus);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public SelectionModel[] GetSalaryExpectationRange()
        {
            var response = Get(Urls.ExpectedSalaries);
            var content = HandleResponse(response);

            return DeserializeResponse<SelectionModel[]>(content);
        }

        public InterviewModel CreateInterview(InterviewModel model)
        {
            var response = Post<InterviewModel>(Urls.InterviewCreation, model);
            var content = HandleResponse(response);

            return DeserializeResponse<InterviewModel>(content);
        }

        public InterviewModel UpdateInterview(InterviewModel model)
        {
            var response = Put<InterviewModel>(Urls.InterviewUpdate, model);
            var content = HandleResponse(response);

            return DeserializeResponse<InterviewModel>(content);
        }

        public BaseSearchModel[] GetQuickSearchSuggestions(string search)
        {
            var url = string.Format(Urls.QuickSearchSuggestion, search.RemoveSpecialCharacters());
            var content = HandleResponse(Get(url));

            return DeserializeResponse<BaseSearchModel[]>(content);
        }

        public InterviewGridModel[] DoQuickSearch(string field, string search)
        {
            var url = string.Format(Urls.QuickSearch, field.RemoveSpecialCharacters(), search.RemoveSpecialCharacters());
            var content = HandleResponse(Get(url));

            return DeserializeResponse<InterviewGridModel[]>(content);
        }

        public InterviewGridModel[] DoAdvancedSearch(AdvancedSearchModel model)
        {
            var response = Post<AdvancedSearchModel>(Urls.AdvancedSearch, model);
            var content = HandleResponse(response);

            return DeserializeResponse<InterviewGridModel[]>(content);
        }

        public InterviewUpdateModel[] GetInterviewUpdates(long interviewId)
        {
            var url = string.Format(Urls.UpdatesByInterview, interviewId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<InterviewUpdateModel[]>(content);
        }

        public InterviewUpdateModel CreateInterviewUpload(InterviewUpdateModel model)
        {
            var response = Post<InterviewUpdateModel>(Urls.InterviewUpdateCreation, model);
            var content = HandleResponse(response);

            return DeserializeResponse<InterviewUpdateModel>(content);
        }

        public InterviewUpdateModel UpdateInterviewUpload(InterviewUpdateModel model)
        {
            var response = Put<InterviewUpdateModel>(Urls.InterviewUpdateUpdate, model);
            var content = HandleResponse(response);

            return DeserializeResponse<InterviewUpdateModel>(content);
        }

        public bool DeleteInterviewUpload(InterviewUpdateModel model)
        {
            var url = string.Format(Urls.InterviewUpdateDeletion, model.Id);

            HandleResponse(Delete(url));

            return true;
        }

        public InterviewInfoModel GetInterviewInfoById(long interviewId)
        {
            var url = string.Format(Urls.InterviewInfoById, interviewId);
            var content = HandleResponse(Get(url));

            return DeserializeResponse<InterviewInfoModel>(content);
        }

        #endregion

        #endregion
    }
}
