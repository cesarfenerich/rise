﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataReader
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DataService dataService = new DataService();

                var startDate = DateTime.Now;
                List<InterviewDataModel> DataModels = new List<InterviewDataModel>();

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using var stream = File.Open("C:\\repos\\RISE\\Backend\\Src\\DataReader\\Data\\data.xls", FileMode.Open, FileAccess.Read);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                bool skipFirstLine = true;
                do
                {
                    while (reader.Read()) //EachRow
                    {
                        if (skipFirstLine)
                        {
                            skipFirstLine = false;
                            continue;
                        }
                        else
                        {
                            List<object> row = new List<object>();

                            for (int column = 0; column < reader.FieldCount; column++)
                            {
                                row.Add(reader.GetValue(column));
                            }

                            var model = new InterviewDataModel(row.ToArray());                            

                            DataModels.Add(model);
                        }
                    }

                } while (reader.NextResult()); //NextSheet

                foreach (var model in DataModels)
                {
                    dataService.ImportInterview(model);

                    //Console.WriteLine(model.Notes);
                    Console.WriteLine(DataModels.IndexOf(model));
                }

                Console.WriteLine($"Total de Linhas Importadas: { DataModels.Count } linhas");
                Console.WriteLine($"Tempo de conversão: { DateTime.Now.Subtract(startDate).TotalMinutes } minutos");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }   



}
