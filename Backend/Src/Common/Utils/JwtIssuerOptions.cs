﻿using Common.Extensions;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class JwtIssuerOptions
    {
        public string Issuer { get; private set; }
        public string Subject { get; private set; }
        public string Audience { get; private set; }
        public DateTime NotBefore { get; private set; }
        public DateTime IssuedAt { get; private set; }
        public TimeSpan ValidFor { get; private set; }
        public DateTime Expiration { get; private set; }
        public Func<Task<string>> JtiGenerator { get; private set; }
        public SymmetricSecurityKey SigningKey { get; private set; }
        public SigningCredentials SigningCredentials { get; private set; }

        public JwtIssuerOptions(string issuer,
                                string subject,
                                string audience,
                                int minutesToBeValid,
                                string jwtSecretKey)
        {
            if (!issuer.IsNullOrWhitespaceOrEmpty())
                this.Issuer = issuer;

            if (!subject.IsNullOrWhitespaceOrEmpty())
                this.Subject = subject;

            if (!audience.IsNullOrWhitespaceOrEmpty())
                this.Audience = audience;

            this.NotBefore = DateTime.UtcNow;
            this.IssuedAt = DateTime.UtcNow;

            if (minutesToBeValid > 0)
                this.ValidFor = TimeSpan.FromMinutes(minutesToBeValid);

            this.Expiration = IssuedAt.Add(ValidFor);
            this.JtiGenerator = () => Task.FromResult(Guid.NewGuid().ToString());

            if (!jwtSecretKey.IsNullOrWhitespaceOrEmpty())
                this.SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSecretKey));

            this.SigningCredentials = new SigningCredentials(this.SigningKey, SecurityAlgorithms.HmacSha256);
        }
    }
}
