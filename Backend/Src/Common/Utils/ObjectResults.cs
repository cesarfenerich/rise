﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Utils
{
    public static class ObjectResults
    {
        public partial class InternalServerErrorObjectResult : ObjectResult
        {
            public InternalServerErrorObjectResult(object value) : base(value)
            {
                StatusCode = 500;
            }
        }
        
        public partial class BadRequestObjectResult : ObjectResult
        {
            public BadRequestObjectResult(object value) : base(value)
            {
                StatusCode = 400;
            }
        }

    }
}
