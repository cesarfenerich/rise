﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Common
{
    public static class Urls
    {        
        public const string Login = "auth/login";
        public const string ForgotPassword = "auth/forgot?email={0}";
        public const string VerifyCode = "auth/verify?email={0}&code={1}";
        public const string UserById = "user/{0}";
        public const string Token = "auth/token/{0}";

        public const string CandidateById = "candidate/{0}";
        public const string LastInterviewsOfCandidateById = "candidate/{0}/last_interviews";
        public const string FileUpload = "candidate/file_upload";
        public const string FileDownload = "candidate/file_download/{0}";
        public const string CandidateTypes = "candidate/types";
        public const string CandidateAvailability = "candidate/availability";
        public const string CandidateInterval = "candidate/interval";
        public const string CandidateMobility = "candidate/mobility";
        public const string LiteraryAbilities = "candidate/literal_abilities";
        public const string MaritalStatus = "candidate/marital_status";
        public const string Languages = "candidate/languages";
        public const string Locations = "candidate/locations";
        public const string JobRoles = "candidate/job_roles";
        public const string TechSkills = "candidate/tech_skills";

        public const string CreateCandidate = "candidate";
        public const string UpdateCandidate = "candidate";        
        public const string CandidateReference = "candidate/reference";
        public const string CreateJobRole = "candidate/job_role";
        public const string UpdateJobRole = "candidate/job_role";
        public const string DeleteJobRole = "candidate/job_role/{0}";
        public const string CreateLocation = "candidate/location";        
        public const string CreateLanguage = "candidate/language";
        public const string GetCandidateLanguages = "candidate/{0}/languages";
        public const string CreateCandidateLanguage = "candidate/{0}/language/{1}";
        public const string DeleteCandidateLanguage = "candidate/{0}/language/{1}";
        public const string CreateTechSkill = "candidate/tech_skill";
        public const string UpdateTechSkill = "candidate/tech_skill";
        public const string DeleteTechSkill = "candidate/tech_skill/{0}";
        public const string GetCandidateTechSkills = "candidate/{0}/tech_skills";
        public const string CreateCandidateTechSkill = "candidate/{0}/tech_skill/{1}";
        public const string DeleteCandidateTechSkill = "candidate/{0}/tech_skill/{1}";
        public const string CreateUpload = "candidate/upload";
        public const string GetCandidateUploads = "candidate/{0}/uploads";
        public const string CreateCandidateUpload = "candidate/{0}/upload/{1}";
        public const string DeleteCandidateUpload = "candidate/{0}/upload/{1}";
        public const string TechSkillsCategories = "candidate/tech_skill_categories";
        public const string GetCandidateNaifas = "candidate/{0}/naifas";
        public const string CreateNaifas = "candidate/naifas";
        public const string UpdateNaifas = "candidate/naifas";        
        public const string CreateCandidateNaifas = "candidate/{0}/naifas/{1}";
        public const string DeleteCandidateNaifas = "candidate/{0}/naifas/{1}";

        public const string OwnerById = "owner/{0}";
        public const string AllOwners = "owner/all";
        public const string ActiveOwners = "owner/actives";
        public const string OwnerCreation = "owner";
        public const string OwnerUpdate = "owner";
        public const string OwnerRecover = "owner/recover";
        public const string UpdateOwnerPassword = "owner/reset";
        public const string IsBlock = "owner/{0}/status";

        public const string InterviewById = "interview/{0}";
        public const string InterviewInfoById = "interview/{0}/info";
        public const string RecentInterviews = "interview/recent";
        public const string CountInterviews = "interview/count";
        public const string ContractTypes = "interview/contract_types";
        public const string InterviewStatus = "interview/status";
        public const string ExpectedSalaries = "interview/expected_salaries";
        public const string InterviewCreation = "interview";
        public const string InterviewUpdate = "interview";
        public const string QuickSearchSuggestion = "interview/quick_search/suggestion?search={0}";
        public const string QuickSearch = "interview/quick_search?field={0}&search={1}";
        public const string AdvancedSearch = "interview/search";
        public const string UpdatesByInterview = "interview/{0}/updates";
        public const string InterviewUpdateCreation = "interview/update";
        public const string InterviewUpdateUpdate = "interview/update";
        public const string InterviewUpdateDeletion = "interview/update/{0}";    
        public const string InterviewArchive = "interview/archive";

        public const string YearsOfExperience = "interview/years_of_experience";
        public const string GetFileUpload = "candidate/upload/{0}";

        public const string LogById = "logs/{0}";
        public const string AllLogs = "logs/all";
        public const string LogCreation = "logs";
    }
}
