﻿using Common.Helpers;
using Common.Models;
using System;
using System.Text.Json;

namespace Common.Exceptions
{
    public class AppException : Exception
    {
        public string Code { get; set; } 
        public int? StatusCode { get; set; }

        public AppException()
        { }       

        public AppException(string code, string message)
            : base(message)
        {
            Code = code;
        }

        public AppException(string code, Exception inner)
            : base(inner.Message, inner)
        {
            Code = code;           
        }

        public AppException(string code, string message, Exception inner)
            : base(message, inner)
        {
            Code = code;            
        }

        public ErrorModel AsErrorModel()
            => new ErrorModel(this.Code, this.Message, String.Format("{0} - {1}", this.InnerException?.Message ?? string.Empty, 
                                                                                  this.StackTrace ?? this.InnerException.StackTrace));        
    }
}
