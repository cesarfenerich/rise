﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Common.Helpers
{    
    public static class ConfigurationHelper
    {
        public static string BasePath { get; private set; }     
        public static string AppUrl { get; private set; }
        public static string EnvUrl { get; private set; }
        public static string AppEnvironment { get; private set; }

        public static string DbConnectionString { get; private set; }       

        public static JwtIssuerOptions JwtIssuerOptions { get; private set; }       

        public static LogLevel LogLevel { get; private set; }

        public static string MailPath { get; private set; }
        public static string From { get; private set; }
        public static string SmtpServer { get; private set; }
        public static int? Port { get; private set; }
        public static string Username { get; private set; }
        public static string Password { get; private set; }

        public static string RegionEndpoint { get; private set; }
        public static string AccessKey { get; private set; }
        public static string SecretKey { get; private set; }
        public static string FileStorageBucketName { get; private set; }

        public static void SetupApplicationEnvironment(IConfigurationRoot configuration, string webRootPath)
        {
            BasePath = AppDomain.CurrentDomain.BaseDirectory;
            AppEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");            
           
            ConfigureDatabase(configuration);
            ConfigureAuthentication(configuration);
            ConfigureMailIntegration(configuration, webRootPath);
            ConfigureAWSIntegration(configuration);

            SetLogLevel();
        }        

        private static void ConfigureDatabase(IConfigurationRoot configuration)
        {
            var databases = configuration.GetSection("Databases");
            DbConnectionString = databases.GetValue<string>("MySQL");
        }

        private static void ConfigureAuthentication(IConfigurationRoot configuration)
        {
            var authentication = configuration.GetSection("Auth");

            AppUrl = authentication.GetValue<string>("AppUrl");
            EnvUrl = authentication.GetValue<string>("EnvUrl");

            JwtIssuerOptions = new JwtIssuerOptions(authentication.GetValue<string>("Issuer"),
                                                    authentication.GetValue<string>("Subject"),
                                                    authentication.GetValue<string>("Audience"),
                                                    authentication.GetValue<int>("ValidFor"),
                                                    authentication.GetValue<string>("SecretKey"));
        }

        private static void ConfigureMailIntegration(IConfigurationRoot configuration, string webRootPath)
        {
            var mailSettings = configuration.GetSection("Integration:Mail");

            MailPath = Path.Combine($"{webRootPath}", "mail");

            From = mailSettings.GetValue<string>("From");
            SmtpServer = mailSettings.GetValue<string>("SmtpServer");
            Port = mailSettings.GetValue<int?>("Port");
            Username = mailSettings.GetValue<string>("Username");
            Password = mailSettings.GetValue<string>("Password");
        }

        private static void ConfigureAWSIntegration(IConfigurationRoot configuration)
        {
            var awsSettings = configuration.GetSection("Integration:AWS");

            RegionEndpoint = awsSettings.GetValue<string>("RegionEndpoint");
            AccessKey = awsSettings.GetValue<string>("AccessKey");
            SecretKey = awsSettings.GetValue<string>("SecretKey");
            FileStorageBucketName = awsSettings.GetValue<string>("S3:FileStorageBucketName");
        }

        public static void SetBasePath()
        {
            BasePath = AppDomain.CurrentDomain.BaseDirectory;
        }

        private static LogLevel SetLogLevel()
        {
            return AppEnvironment switch
            {
                "Development" => LogLevel.Trace,
                "Staging" => LogLevel.Warning,
                "Production" => LogLevel.Warning,
                _ => LogLevel.Debug
            };
        }        
    }
}

