﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Helpers
{
    public static class DateTimeHelper
    {
        public static long ToUnixEpochDate(this DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}
