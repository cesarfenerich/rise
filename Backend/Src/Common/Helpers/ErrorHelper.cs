﻿using Common.Exceptions;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using System.Text;

namespace Common.Helpers
{
    public static class ErrorHelper
    {
        public static AppException Except(string code)
            => throw new AppException(code, GetErrorMessage(code));

        public static AppException Except(Exception catchedException, string code = null)
            => new AppException(code, GetErrorMessage(code), catchedException);

        public static ErrorModel ExceptAsModel(string code = null)
            => new AppException(code, GetErrorMessage(code)).AsErrorModel();

        public static string GetErrorMessage(string code)
        {
            var resourceManager = new ResourceManager("Common.Resources.ErrorMessages", Assembly.GetExecutingAssembly());

            if (code is null)
                code = "APP_0001";

            return resourceManager.GetString(code);
        }        
    }
}
