﻿using Common.Extensions;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Common.Helpers
{
    public static class ConstantsHelper
    {
        private static IEnumerable<FieldInfo> GetConstants(Type type)
        {
            var fieldInfos = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

            return fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly);
        }      

        public static IEnumerable<SelectionModel> GetOptionsOf(this Type type)
        {            
            var constants = GetConstants(type);

            return constants.Select(x => new SelectionModel(x.Name, x.GetRawConstantValue().ToString())).ToList();
        }

        public static string GetValueOf<T>(string option)
        {
            if (!option.IsNullOrWhitespaceOrEmpty())
            {
                var constants = GetConstants(typeof(T));

                return constants.FirstOrDefault(x => x.Name.Equals(option))
                                .GetRawConstantValue()
                                .ToString();
            }

            return null;
        }
    }
}
