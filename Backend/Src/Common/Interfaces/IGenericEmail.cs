﻿using Microsoft.AspNetCore.Http;
using MimeKit;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IGenericEmail
    {        
        public List<MailboxAddress> To { get; set; }
        public string Subject { get; }
        public string Content { get; set; }

        public IFormFileCollection Attachments { get; set; }
    }
}
