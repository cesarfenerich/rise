﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class, IGenericEntity
    {
        TEntity GetById(long id);

        Task<TEntity> Create(TEntity entity);        

        Task<TEntity> Update(TEntity entity);

        Task Delete(long id);

        Task<TEntity> UpdateAndDetach(TEntity entity);

        Task CreateMany(ICollection<TEntity> entities);

        Task UpdateMany(ICollection<TEntity> entities);

        Task DeleteMany(ICollection<TEntity> entities);        
    }
}
