﻿using Threenine.Map;

namespace Common.Interfaces
{
    public interface IGenericEntity : ICustomMap
    {
        long Id { get; set; }        
    }
}
