﻿using Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class NaifasUploadModel : IGenericModel
    {

        [JsonProperty(PropertyName = "candidateId")]
        public long CandidateId { get; set; }

        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "filePath")]
        public string FilePath { get; set; }

        public NaifasUploadModel() { }

        public NaifasUploadModel(long candidate, string fileName, string filePath)
        {
            CandidateId = candidate;
            FileName = fileName;
            FilePath = filePath;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
