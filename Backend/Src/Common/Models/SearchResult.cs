﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Models
{
    public class SearchModel
    {
        [JsonProperty(PropertyName = "results")]
        public List<BaseSearchModel> Results { get; set; }                 
    }
}
