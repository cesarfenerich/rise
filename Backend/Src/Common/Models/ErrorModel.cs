﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class ErrorModel
    {
        public string Code { get; set; }

        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Trace { get; set; }

        public ErrorModel()
        {

        }

        public ErrorModel(string code, string message)
        {
            this.Code = code;
            this.Message = message;            
        }

        public ErrorModel(string code, string message, string trace)
        {
            this.Code = code;
            this.Message = message;
            this.Trace = trace;
        }
    }
}
