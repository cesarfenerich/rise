﻿using Common.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Models
{
    public class SelectionModel : IGenericModel
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty(PropertyName = "constantId", NullValueHandling = NullValueHandling.Ignore )]
        public string ConstantId { get; set; }

        [JsonProperty(PropertyName = "value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        public SelectionModel()
        { }

        public SelectionModel(long id, string value)
        {
            this.Id = id;
            this.Value = value;
        }

        public SelectionModel(string constantId, string value)
        {
            this.ConstantId = constantId;
            this.Value = value;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
