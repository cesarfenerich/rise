﻿using Common.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Models
{
    public class ItemData
    {
        [JsonProperty(PropertyName = "text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }       

        [JsonProperty(PropertyName = "value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        public ItemData()
        { }

        public ItemData(string text, string value)
        {
            this.Text = text;
            this.Value = value;
        }      
    }
}
