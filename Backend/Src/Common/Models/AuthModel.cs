﻿using Common.Extensions;
using Common.Helpers;
using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Models
{
    public class AuthModel : IGenericModel
    {        
        public string Email { get; set; }        
        public string Password { get; set; }       

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> errors)
        { 
            if (this.Email.IsNullOrWhitespaceOrEmpty())
                errors.Add(ErrorHelper.ExceptAsModel("APP_0002"));

            if (this.Password.IsNullOrWhitespaceOrEmpty())
                errors.Add(ErrorHelper.ExceptAsModel("APP_0003"));

            return errors;
        }

        public AuthModel()
        { }

        public AuthModel(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }
    }
}
