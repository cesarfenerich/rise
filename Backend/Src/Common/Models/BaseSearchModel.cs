﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class BaseSearchModel
    {
        [JsonProperty("field")]
        public string Field { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }

        public BaseSearchModel()
        { }

        public BaseSearchModel(string field, string value)
        {
            Field = field;
            Value = value;
        }
    }
}
