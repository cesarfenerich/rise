﻿using Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Models
{
    public class TokenModel : IGenericModel
    {
        [JsonProperty(PropertyName = "accessToken", NullValueHandling = NullValueHandling.Ignore)]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "refreshToken", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? RefreshToken { get; set; }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> errors)
        { 
            return errors;
        }

        public TokenModel()
        { }

        public TokenModel(string accessToken, Guid refreshToken)
        {
            this.AccessToken = accessToken;
            this.RefreshToken = refreshToken;
        }
    }
}
