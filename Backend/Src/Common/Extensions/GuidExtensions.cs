﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Extensions
{
    public static class GuidExtensions
    {
        public static bool IsValid(this Guid guid)
        {
            if (guid == null ||
                guid.Equals(Guid.Empty) ||
                !Guid.TryParseExact(guid.ToString(), "D", out guid)) return false;

            return true;
        }
    }
}
