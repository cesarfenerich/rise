﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Common.Extensions
{
    public static class CollectionExtensions
    {
        public static ICollection<T> GetValueOrDefault<T>(this ICollection<T> value)
        {
            return value ?? new Collection<T>();
        }

        public static void RemoveAll<T>(this ICollection<T> source,
                                        Func<T, bool> predicate)
        {
            source.Where(predicate).ToList().ForEach(e => source.Remove(e));
        }
    }
}
