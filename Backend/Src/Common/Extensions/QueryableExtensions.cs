﻿using System;
using System.Linq;

namespace Common.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> DistinctBy<T>(this IQueryable<T> list, Func<T, object> propertySelector)
        {
            return list.GroupBy(propertySelector).Select(x => x.First()).AsQueryable();
        }
    }
}
