﻿using System;
using System.Text.RegularExpressions;

namespace Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrWhitespaceOrEmpty(this string str)
        {
            if (str == null ||
                str == " " ||
                str == string.Empty) return true;

            return false;
        }

        public static bool IsValidEmail(this string email)
        {
            if (email.IsNullOrWhitespaceOrEmpty())
                return false;

            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        public static bool InsensitiveContains(this string str, string value)
            => str.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) >= 0;

        public static string RemoveSpecialCharacters(this string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled);
        }

        public static string NumbersOnly (this string str)
        {
            return Regex.Replace(str, "[^\\d]", "", RegexOptions.Compiled);
        }
    }
}
