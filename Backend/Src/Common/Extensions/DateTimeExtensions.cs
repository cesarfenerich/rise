﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsNullOrEmpty(this DateTime date)
        {
            if (date == null ||
                date == DateTime.MinValue) return true;

            return false;
        }

        public static DateTime GetLastDateOfMonth(this DateTime date)
            => new DateTime(date.Year,
                             date.Month,
                             DateTime.DaysInMonth(date.Year, date.Month));

        public static bool HasValidYearDiffOf(this DateTime date, int years)
        {
            var diff = DateTime.UtcNow.Date.Subtract(date.Date);
            var diffDate = DateTime.MinValue + diff;

            return diffDate.Year - 1 < 100 &&
                   diffDate.Year - 1 >= years;
        }
    }
}
