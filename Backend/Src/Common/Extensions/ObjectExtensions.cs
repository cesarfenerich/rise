﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Extensions
{
    public static class ObjectExtensions
    {
        public static bool Exists(this object obj)
        {
            return obj is null;
        }
    }
}
