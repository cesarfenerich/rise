﻿using Common.Helpers;
using Common.Interfaces;
using Common.Models;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static Common.Utils.ObjectResults;

namespace API.ActionFilters
{
    public class RequestPayloadValidationFilter<T> : IActionFilter where T : class, IGenericModel
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var payload = JsonConvert.SerializeObject(context.ActionArguments["requestModel"]);
                var result = ValidateRequestPayload(payload);

                if (result.Count() > 0)
                    context.Result = new BadRequestObjectResult(result);
            }
            catch (Exception ex)
            {
                context.Result = new BadRequestObjectResult(ErrorHelper.Except(ex,"APP_0008").AsErrorModel());
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        /// <summary>
        /// APP_0008 or Domain Exception (Invalid payload data)
        /// </summary>      
        private IEnumerable<ErrorModel> ValidateRequestPayload(string payload)
        {
            var requestModel = JsonConvert.DeserializeObject<T>(payload, new JsonSerializerSettings
            {
                Error = HandleDeserializationError
            });

            List<ErrorModel> validationErrors = new List<ErrorModel>();

            if (requestModel is null)
                validationErrors.Add(ErrorHelper.ExceptAsModel("APP_0008"));
            else
                validationErrors.AddRange(requestModel.Validate(validationErrors));

            return validationErrors;
        }

        private void HandleDeserializationError(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs errorArgs)
        {
            var currentError = errorArgs.ErrorContext.Error.Message;
            errorArgs.ErrorContext.Handled = true;
        }       
    }
}
