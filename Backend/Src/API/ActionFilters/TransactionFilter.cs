﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System.Data;

namespace API.ActionFilters
{
    /// <summary>
    /// Action Filter to Control Database Transactions per Endpoint Roundtrips
    /// </summary>
    public class TransactionFilter : IActionFilter
    {
        private readonly AppDbContext _dbContext;

        public TransactionFilter(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                _dbContext.Database.CommitTransaction();
            }
            else
            {
                _dbContext.Database.RollbackTransaction();
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            _dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
    }
}
