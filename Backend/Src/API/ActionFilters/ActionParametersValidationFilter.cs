﻿using Common.Exceptions;
using Common.Extensions;
using Common.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using static Common.Utils.ObjectResults;

namespace API.ActionFilters
{
    /// <summary>
    /// Action Filter to validate endpoints mostly common params
    /// </summary>
    public class ActionParametersValidationFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                if (context.ActionArguments.ContainsKey("Id"))
                    ValidateIdParameter((long)context.ActionArguments["Id"]);

                if (context.ActionArguments.ContainsKey("Search"))
                    ValidateSearchTerm((string)context.ActionArguments["Search"]);

                if (context.ActionArguments.ContainsKey("File"))
                    ValidateFormFileParameter((IFormFile)context.ActionArguments["File"]);
            }
            catch (AppException ex)
            {
                context.Result = new BadRequestObjectResult(ex.AsErrorModel());
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        private void ValidateIdParameter(long id)
        {
            if (!id.IsValid())
                ErrorHelper.Except("APP_0005");
        }

        private void ValidateFormFileParameter(IFormFile file)
        {
            if (file is null ||
                file.Length <= 0)
                ErrorHelper.Except("APP_0006");
        }

        private void ValidateSearchTerm(string search)
        {
            if (search.IsNullOrWhitespaceOrEmpty())
                ErrorHelper.Except("APP_0007");
        }
    }
}
