#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Backend/Src/API/API.csproj", "Backend/Src/API/"]
COPY ["Backend/Src/Domain/Domain.csproj", "Backend/Src/Domain/"]
COPY ["Backend/Src/Common/Common.csproj", "Backend/Src/Common/"]
COPY ["Backend/Src/Persistence/Persistence.csproj", "Backend/Src/Persistence/"]
RUN dotnet restore "Backend/Src/API/API.csproj"
COPY . .
WORKDIR "/src/Backend/Src/API"
RUN dotnet build "API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "API.dll"]
