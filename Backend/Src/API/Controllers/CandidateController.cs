﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using API.ActionFilters;
using API.Attributes;
using Common.Helpers;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [ApiExplorerSettings(GroupName = @"Candidatos")]
    [Produces("application/json")]  
    [ApiController]
    [Route("1/candidate")]
    public class CandidateController : BaseController
    {
        private readonly ICandidateService _candidateService;

        /// <summary>
        /// Exposição de Endpoints de Dados Relacionados ao Candidato
        /// </summary>        
        public CandidateController(IHttpContextAccessor httpContextAccessor,
                                   IWebHostEnvironment hostingEnvironment,
                                   ICandidateService candidateService) : base(httpContextAccessor,
                                                                              hostingEnvironment)
        {
            _candidateService = candidateService;
        }

        #region CRUD

        /// <summary>
        /// Busca de Candidato pelo Id
        /// </summary>
        /// <param name="Id">Id do Candidato</param>
        /// <response code="200">Candidato Solicitado</response>
        /// <response code="400">APP_0011</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}")]
        [ProducesResponseType(200, Type = typeof(CandidateModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<CandidateModel>> GetById([FromRoute]long Id)
            => Ok(await _candidateService.GetById(Id));

        /// <summary>
        /// Criação de Novo Candidato
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Candidato Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(CandidateModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<CandidateModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<CandidateModel>> CreateCandidate([FromBody]CandidateModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateCandidate(requestModel));


        /// <summary>
        /// Edição de Candidato
        /// </summary>
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="200">Candidato Atualizado</response>
        /// <response code="400"></response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(CandidateModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<CandidateModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<CandidateModel>> UpdateCandidate([FromBody]CandidateModel requestModel)
            => Ok(await _candidateService.UpdateCandidate(requestModel));

        /// <summary>
        /// Busca de Entrevistas por Candidato
        /// </summary>
        /// <param name="Id">Id do Candidato</param>
        /// <response code="200">Lista com as Últimas Entrevistas do Candidato</response>
        /// <response code="400">APP_0011</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}/last_interviews")]
        [ProducesResponseType(200, Type = typeof(List<InterviewGridModel>))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<InterviewGridModel>>> GetLastInterviewsByCandidate([FromRoute]long Id)
            => Ok(await _candidateService.GetLastInterviewsByCandidate(Id));

        /// <summary>
        /// Busca de Candidato por Referência
        /// </summary>
        /// <param name="requestModel">Id do Candidato</param>
        /// <response code="200">Candidato Refenciado</response>
        /// <response code="400">APP_0011</response>         
        /// <response code="500">APP_0001</response>   
        [HttpPost("reference")]
        [ProducesResponseType(200, Type = typeof(CandidateModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<CandidateSearchModel>))]
        public async Task<ActionResult<IEnumerable<InterviewModel>>> GetCandidateByReference([FromBody]CandidateSearchModel requestModel)
            => Ok(await _candidateService.GetCandidateByReference(requestModel));

        #endregion

        #region JobRole      

        /// <summary>
        /// Busca de Cargos/Funções de Candidatos
        /// </summary>        
        /// <response code="200">Opções de Cargo/Função de Candidatos</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("job_roles")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllJobRoles()
            => Ok(await _candidateService.GetAllJobRoles());

        /// <summary>
        /// Criação de Novo JobRole
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">JobRole Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("job_role")]
        [ProducesResponseType(201, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<SelectionModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> CreateJobRole([FromBody]SelectionModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateJobRole(requestModel));

        /// <summary>
        /// Edição de JobRole
        /// </summary>
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="200">JobRole Atualizado</response>
        /// <response code="400">Erro ao Actualizar JobRole</response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut("job_role")]
        [ProducesResponseType(200, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<SelectionModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> UpdateJobRole([FromBody] SelectionModel requestModel)
            => Ok(await _candidateService.UpdateJobRole(requestModel));

        /// <summary>
        /// Exclusão de JobRole
        /// </summary>  
        /// <param name="Id">Id do JobRole</param>       
        /// <response code="204">Sem retorno (JobRole Removido com Sucesso)</response>
        /// <response code="400">Problema ao Remover JobRole</response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("job_role/{Id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> DeleteJobRole([FromRoute] long Id)
        {
            await _candidateService.DeleteJobRole(Id);

            return NoContent();
        }

        #endregion

        #region Location        

        /// <summary>
        /// Busca de Localizações de Candidatos
        /// </summary>        
        /// <response code="200">Opções de Localização de Candidatos</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("locations")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllLocations()
            => Ok(await _candidateService.GetAllLocations());

        /// <summary>
        /// Criação de Nova Location
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Loacation Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("location")]
        [ProducesResponseType(201, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<SelectionModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> CreateLocation([FromBody]SelectionModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateLocation(requestModel));

        #endregion

        #region Language

        /// <summary>
        /// Busca de Linguagens por Candidato
        /// </summary>        
        /// <param name="Id"> Id do Candidato</param>
        /// <response code="200">Linguagens do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("{Id}/languages")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetCandidadeLanguages([FromRoute] long Id)
            => Ok(await _candidateService.GetLanguagesByCandidate(Id));


        /// <summary>
        /// Busca de Linguagens de Candidato
        /// </summary>        
        /// <response code="200">Opções de Linguagem de Candidatos</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("languages")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllLanguages()
            => Ok(await _candidateService.GetAllLanguages());


        /// <summary>
        /// Criação de Nova Language
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Language Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("language")]
        [ProducesResponseType(201, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<SelectionModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> CreateLanguage([FromBody]SelectionModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateLanguage(requestModel));

        /// <summary>
        /// Criação de Nova Language por Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdLanguage">Id da Linguagem</param>
        /// <response code="201">Language Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("{Id}/language/{IdLanguage}")]
        [ProducesResponseType(201, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> CreateCandidateLanguage([FromRoute]long Id, [FromRoute]long IdLanguage)
            => new CreatedResult(string.Empty, await _candidateService.CreateCandidateLanguage(Id, IdLanguage));

        /// <summary>
        /// Exclusão de Language por Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdLanguage">Id da Linguagem</param>
        /// <response code="204">Sem retorno (Language Removida com Sucesso)</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("{Id}/language/{IdLanguage}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult> DeleteCandidateLanguage([FromRoute]long Id, [FromRoute]long IdLanguage)
        {
            await _candidateService.DeleteCandidateLanguage(Id, IdLanguage);

            return NoContent();
        }

        #endregion

        #region TechSkill

        #region TechSkillCandidate

        /// <summary>
        /// Busca de TechSkills por Candidato
        /// </summary>        
        /// <param name="Id"> Id do Candidato</param>
        /// <response code="200">TechSkills do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("{Id}/tech_skills")]
        [ProducesResponseType(200, Type = typeof(List<TechSkillModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<TechSkillModel>>> GetCandidadeTechSkills([FromRoute] long Id)
            => Ok(await _candidateService.GetTechSkillsByCandidate(Id));

        /// <summary>
        /// Criação de Nova TechSkill por Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdTechSkill">Id da TechSkill</param>
        /// <response code="201">TechSkill Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("{Id}/tech_skill/{IdTechSkill}")]
        [ProducesResponseType(201, Type = typeof(TechSkillModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> CreateCandidateTechSkill([FromRoute] long Id, [FromRoute] long IdTechSkill)
            => new CreatedResult(string.Empty, await _candidateService.CreateCandidateTechSkill(Id, IdTechSkill));

        /// <summary>
        /// Exclusão de TechSkill de Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdTechSkill">Id da TechSkill</param>
        /// <response code="204">Sem retorno (TechSkill Removido com Sucesso)</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("{Id}/tech_skill/{IdTechSkill}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> DeleteCandidateTechSkill([FromRoute] long Id, [FromRoute] long IdTechSkill)
        {
            await _candidateService.DeleteCandidateTechSkill(Id, IdTechSkill);

            return NoContent();
        }

        #endregion

        /// <summary>
        /// Busca de Skills de Candidatos
        /// </summary>        
        /// <response code="200">Opções de Skills de Candidatos</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("tech_skills")]
        [ProducesResponseType(200, Type = typeof(List<TechSkillModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<TechSkillModel>>> GetAllTechSkills()
            => Ok(await _candidateService.GetAllTechSkills());

        /// <summary>
        /// Criação de Nova TechSkill
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">TechSkill Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("tech_skill")]
        [ProducesResponseType(201, Type = typeof(TechSkillModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<TechSkillModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> CreateTechSkill([FromBody]TechSkillModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateTechSkill(requestModel));        

        /// <summary>
        /// Edição de TechSkill
        /// </summary>
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="200">TechSkill Actualizada</response>
        /// <response code="400">Erro ao Actualizar TechSkill</response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut("tech_skill")]
        [ProducesResponseType(200, Type = typeof(TechSkillModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<TechSkillModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> UpdateTechSkill([FromBody] TechSkillModel requestModel)
            => Ok(await _candidateService.UpdateTechSkill(requestModel));

        /// <summary>
        /// Exclusão de TechSkill
        /// </summary>  
        /// <param name="Id">Id da TechSkill</param>       
        /// <response code="204">Sem retorno (TechSKill Removida com Sucesso)</response>
        /// <response code="400">Problema ao Remover TechSKill</response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("tech_skill/{Id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> DeleteTechSkill([FromRoute] long Id)
        {
            await _candidateService.DeleteTechSkill(Id);

            return NoContent();
        }        

        #endregion

        #region Upload

        /// <summary>
        /// Busca de Uploads por Candidato
        /// </summary>        
        /// <param name="Id"> Id do Upload</param>
        /// <response code="200">Uploads do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("{Id}/uploads")]
        [ProducesResponseType(200, Type = typeof(List<UploadModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<UploadModel>>> GetCandidateUploads([FromRoute] long Id)
            => Ok(await _candidateService.GetUploadsByCandidate(Id));

        /// <summary>
        /// Busca de Uploads por Nome do Arquivo
        /// </summary>        
        /// <param name="FileName">Nome do Arquivo</param>
        /// <response code="200">Uploads do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("upload/{FileName}")]
        [ProducesResponseType(200, Type = typeof(List<UploadModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<UploadModel>>> GetCandidateUploads([FromRoute] string FileName)
            => Ok(await _candidateService.GetUploadsByFileName(HttpUtility.UrlDecode(FileName)));

        /// <summary>
        /// Upload de Arquivos
        /// </summary>               
        /// <param name="File">Arquivo a ser Carregado</param>
        /// <response code = "200"> Informações do Upload</response>        
        /// <response code = "400"> APP_0006 </response>        
        /// <response code = "500"> APP_0001 </response>           
        [HttpPost("file_upload"), DisableRequestSizeLimit]
        [ProducesResponseType(200, Type = typeof(UploadModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<UploadModel>> UploadFile(IFormFile File)
        {
            var file = HttpContext.Request.Form.Files.FirstOrDefault();

            return Ok(await _candidateService.UploadFile(file));
        }


        /// <summary>
        /// Download de Arquivos
        /// </summary>               
        /// <param name="Id">Id do Upload a ser Encontrado</param>
        /// <response code = "200"> Arquivo Armazenado</response>        
        /// <response code = "400"> APP_0006 </response>        
        /// <response code = "500"> APP_0001 </response>           
        [HttpGet("file_download/{Id}"), DisableRequestSizeLimit]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult> DownloadFile([FromRoute]long Id)
        {
            var file = await _candidateService.DownloadFile(Id);

            return File(file.Item2, "application/octet-stream", file.Item1);
        }      


        /// <summary>
        /// Criação de Novo Upload por Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdUpload">Id do Upload</param>
        /// <response code="201">Upload Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("{Id}/upload/{IdUpload}")]
        [ProducesResponseType(201, Type = typeof(SelectionModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]     
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<SelectionModel>> CreateCandidateUpload([FromRoute]long Id, [FromRoute]long IdUpload)
            => new CreatedResult(string.Empty, await _candidateService.CreateCandidateUpload(Id, IdUpload));

        /// <summary>
        /// Exclusão de Upload de Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdTechSkill">Id do Upload</param>
        /// <response code="204">Sem retorno (Upload Removido com Sucesso)</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("{Id}/upload/{IdUpload}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<TechSkillModel>> DeleteCandidateUpload([FromRoute]long Id, [FromRoute]long IdTechSkill)
        {
            await _candidateService.DeleteCandidateUpload(Id, IdTechSkill);

            return NoContent();
        }


        #endregion

        #region Options

        /// <summary>
        /// Busca de Tipos de Candidato
        /// </summary>        
        /// <response code="200">Opções de Tipo de Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("types")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllCandidateTypes()
            => Ok(await _candidateService.GetCandidateTypes());

        /// <summary>
        /// Busca de Disponibilidade de Candidato
        /// </summary>        
        /// <response code="200">Opções de Disponibilidade do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("availability")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllCandidateAvaliabilities()
            => Ok(await _candidateService.GetCandidateAvailability());

        /// <summary>
        /// Busca de Intervalo de Disponibilidade de Candidato
        /// </summary>        
        /// <response code="200">Opções de Intervalo de Disponibilidade do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("interval")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllCandidateIntervals()
            => Ok(await _candidateService.GetCandidateInterval());

        /// <summary>
        /// Busca de Mobilidadee de Candidato
        /// </summary>        
        /// <response code="200">Opções de Mobilidade do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("mobility")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetCandidateMobility()
            => Ok(await _candidateService.GetCandidateMobility());

        /// <summary>
        /// Busca de Habilitações Académicas de Candidato
        /// </summary>        
        /// <response code="200">Opções de Habilitação Literária de um Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("literal_abilities")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllLiteraryAbilities()
            => Ok(await _candidateService.GetLiteraryAbilities());

        /// <summary>
        /// Busca de Estado Civil de Candidato
        /// </summary>        
        /// <response code="200">Opções de Estado Civil de um Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("marital_status")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllMaritalStatuses()
            => Ok(await _candidateService.GetMaritalStatuses());

        /// <summary>
        /// Busca de Categorias de TechSkill
        /// </summary>        
        /// <response code="200">Opções de Categoria de TechSkill</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("tech_skill_categories")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllTechSkillCategories()
            => Ok(await _candidateService.GetAllTechSkillCategories());


        #endregion

        #region Naifas
        /// <summary>
        /// Pesquisa de Naifas por Candidato
        /// </summary>        
        /// <param name="Id"> Id do Candidato</param>
        /// <response code="200">Naifas do Candidato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("{Id}/naifas")]
        [ProducesResponseType(200, Type = typeof(List<NaifasModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<NaifasModel>>> GetCandidadeNaifas([FromRoute] long Id)
            => Ok(await _candidateService.GetNaifasByCandidate(Id));

        /// <summary>
        /// Criação de Nova Naifas
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Naifas Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("naifas")]
        [ProducesResponseType(201, Type = typeof(NaifasModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<NaifasModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<NaifasModel>> CreateNaifas([FromBody] NaifasModel requestModel)
            => new CreatedResult(string.Empty, await _candidateService.CreateNaifas(requestModel));

        /// <summary>
        /// Criação de Nova Naifas por Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdNaifas">Id da Naifas</param>
        /// <response code="201">Naifas Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("{Id}/naifas/{IdNaifas}")]
        [ProducesResponseType(201, Type = typeof(NaifasModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<NaifasModel>> CreateCandidateNaifas([FromRoute] long Id, [FromRoute] long IdNaifas)
            => new CreatedResult(string.Empty, await _candidateService.CreateCandidateNaifas(Id, IdNaifas));

        /// <summary>
        /// Edição de Naifas
        /// </summary>
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="200">Naifas Atualizado</response>
        /// <response code="400"></response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut("naifas")]
        [ProducesResponseType(200, Type = typeof(NaifasModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<NaifasModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<NaifasModel>> UpdateNaifas([FromBody] NaifasModel requestModel)
            => Ok(await _candidateService.UpdateNaifas(requestModel));

        /// <summary>
        /// Exclusão de Naifas de Candidato
        /// </summary>  
        /// <param name="Id">Id do Candidato</param>
        /// <param name="IdNaifas">Id da Naifas</param>
        /// <response code="204">Sem retorno (Naifas Removido com Sucesso)</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpDelete("{Id}/naifas/{IdNaifas}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<NaifasModel>> DeleteCandidateNaifas([FromRoute] long Id, [FromRoute] long IdNaifas)
        {
            await _candidateService.DeleteCandidateNaifas(Id, IdNaifas);

            return NoContent();
        }
     
        #endregion
    }
}
