﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.ActionFilters;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [ApiExplorerSettings(GroupName = @"Entrevistadores")]
    [Produces("application/json")]    
    [ApiController]
    [Route("1/owner")]
    public class OwnerController : BaseController
    {
        private readonly IOwnerService _ownerService;

        public OwnerController(IHttpContextAccessor httpContextAccessor,
                               IWebHostEnvironment hostingEnvironment,
                               IOwnerService ownerService) : base(httpContextAccessor,
                                                                  hostingEnvironment)
        {
            _ownerService = ownerService;
        }

        /// <summary>
        /// Busca de Entrevistador pelo Id
        /// </summary>
        /// <param name="Id">Id do Entrevistador</param>
        /// <response code="200">Entrevistador Solicitado</response>
        /// <response code="400">APP_0013</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}")]
        [ProducesResponseType(200, Type = typeof(OwnerModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<OwnerModel>> GetById([FromRoute]long Id)
            => Ok(await _ownerService.GetById(Id));

        /// <summary>
        /// Busca de Entrevistadores
        /// </summary>      
        /// <response code="200">Lista com todos os Entrevistadores registrados</response>     
        /// <response code="500">APP_0001</response>   
        [HttpGet("all")]
        [ProducesResponseType(200, Type = typeof(List<OwnerModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<OwnerModel>>> GetAllOwners()
            => Ok(await _ownerService.GetAllOwners());

        /// <summary>
        /// Busca de Entrevistadores Ativos
        /// </summary>      
        /// <response code="200">Lista com todos os Entrevistadores registrados e activos</response>     
        /// <response code="500">APP_0001</response>   
        [HttpGet("actives")]
        [ProducesResponseType(200, Type = typeof(List<OwnerModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<OwnerModel>>> GetAllActiveOwners()
            => Ok(await _ownerService.GetAllActiveOwners());

        /// <summary>
        /// Criação de Novo Entrevistador
        /// </summary>  
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="201">Entrevistador Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(OwnerModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<OwnerModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<OwnerModel>> CreateOwner([FromBody]OwnerModel RequestModel)
            => new CreatedResult(string.Empty, await _ownerService.CreateOwner(RequestModel));


        /// <summary>
        /// Edição de Entrevistador
        /// </summary>
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="200">Entrevistador Alterado</response>
        /// <response code="400">APP_0013</response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(OwnerModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<OwnerModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<OwnerModel>> UpdateOwner([FromBody]OwnerModel RequestModel)
            => Ok(await _ownerService.UpdateOwner(RequestModel));

        /// <summary>
        /// Retorna estado do utilizador por Id
        /// </summary>
        /// <param name="Id">Estado do utilizador</param>
        /// <response code="200">Entrevistador Solicitado</response>
        /// <response code="400">APP_0013</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}/status")]
        [ProducesResponseType(200, Type = typeof(OwnerModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<bool>> IsBlocked([FromRoute] long Id)
            => Ok(await _ownerService.IsBlock(Id)); 

        /// <summary>
        /// Update da password de Entrevistador
        /// </summary>
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="200">Entrevistador Alterado</response>
        /// <response code="400">APP_0013</response>    
        /// <response code="500">APP_0001</response>           
        [AllowAnonymous]
        [HttpPut("reset")]
        [ProducesResponseType(200, Type = typeof(OwnerModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<OwnerModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<OwnerModel>> UpdateOwnerPassword([FromBody]OwnerModel RequestModel)
            => Ok(await _ownerService.UpdateOwnerPassword(RequestModel));
    }
}
