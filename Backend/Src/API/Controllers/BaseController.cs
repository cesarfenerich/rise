﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        public readonly IWebHostEnvironment _hostingEnvironment;
        public readonly User _loggedUser;
        public readonly string _remoteIpAddress;

        protected BaseController(IHttpContextAccessor httpContextAccessor,
                                 IWebHostEnvironment hostEnvironment)
        {
            _hostingEnvironment = hostEnvironment;
            _remoteIpAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (_remoteIpAddress.Equals("::1"))
                _remoteIpAddress = "127.0.0.1";

            if (httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                var authenticatedUserId = Int64.Parse(httpContextAccessor.HttpContext.User.Claims
                                                                         .FirstOrDefault(c => c.Type.Equals("riseAppUser_")).Value);

                var userRepository = httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IUserRepository>();

                _loggedUser = userRepository.GetById(authenticatedUserId);
            }
        }
    }
}
