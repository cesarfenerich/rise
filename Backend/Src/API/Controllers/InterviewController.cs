﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using API.ActionFilters;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [ApiExplorerSettings(GroupName = @"Entrevistas")]
    [Produces("application/json")]    
    [ApiController]
    [Route("1/interview")]
    public class InterviewController : BaseController
    {
        private readonly IInterviewService _interviewService;

        public InterviewController(IHttpContextAccessor httpContextAccessor,
                                   IWebHostEnvironment hostingEnvironment,
                                   IInterviewService interviewService) : base(httpContextAccessor,
                                                                              hostingEnvironment)
        {
            _interviewService = interviewService;
        }

        /// <summary>
        /// Busca de Entrevista pelo Id
        /// </summary>
        /// <param name="Id">Id da Entrevista</param>
        /// <response code="200">Entrevista Solicitada</response>
        /// <response code="400">APP_0012</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}")]
        [ProducesResponseType(200, Type = typeof(InterviewModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<InterviewModel>> GetById([FromRoute]long Id)
            => Ok(await _interviewService.GetById(Id));


        /// <summary>
        /// Busca de Entrevistas Recentes
        /// </summary>      
        /// <response code="200">Lista com as 100 Entrevistas mais Recentes</response>     
        /// <response code="500">APP_0001</response>   
        [HttpGet("recent")]
        [ProducesResponseType(200, Type = typeof(List<InterviewGridModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]      
        public async Task<ActionResult<IEnumerable<InterviewGridModel>>> GetRecentInterviews()
            => Ok(await _interviewService.GetMostRecentInterviews());

        /// <summary>
        /// Busca de Informações de Entrevista
        /// </summary>      
        /// <response code="200">Informações de Entrevista</response>     
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}/info")]
        [ProducesResponseType(200, Type = typeof(InterviewInfoModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<InterviewInfoModel>>> GetInterviewInfo([FromRoute] long Id)
            => Ok(await _interviewService.GetInfosByInterview(Id));


        /// <summary>
        /// Listagem de Tipos de Contrato
        /// </summary>        
        /// <response code="200">Opções de Tipo de de Contrato</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("contract_types")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllContractTypes()
            => Ok(await _interviewService.GetContractTypes());

        
        /// <summary>
        /// Listagem de Status de Entrevista
        /// </summary>        
        /// <response code="200">Opções de Status de Entrevista Possíveis</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("status")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllInterviewStatuses()
            => Ok(await _interviewService.GetStatus());

        /// <summary>
        /// Listagem de Expectativas Salariais
        /// </summary>        
        /// <response code="200">Opções de Intervalos de Salários</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("expected_salaries")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetAllExpectedSalariesRange()
            => Ok(await _interviewService.GetExpectedSalariesRange());

        /// <summary>
        /// Listagem de Anos de Experiência
        /// </summary>        
        /// <response code="200">Opções de Intervalos de Anos de Experiência</response>             
        /// <response code="500">APP_0001</response>           
        [HttpGet("years_of_experience")]
        [ProducesResponseType(200, Type = typeof(List<SelectionModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> GetYearsOfExperienceRange()
            => Ok(await _interviewService.GetYearsOfExperienceRange());

        /// <summary>
        /// Busca de Total de Entrevistas Registadas
        /// </summary>      
        /// <response code="200">Número total de Entrevistas Registadas</response>     
        /// <response code="500">APP_0001</response>   
        [HttpGet("count")]
        [ProducesResponseType(200, Type = typeof(SelectionModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<SelectionModel>> GetInterviewsCount()
            => Ok(await _interviewService.GetInterviewsCount());


        /// <summary>
        /// Criação de Nova Entrevista
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Entrevista Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(InterviewModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<InterviewModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<InterviewModel>> CreateInterview([FromBody]InterviewModel requestModel)
            => new CreatedResult(string.Empty, await _interviewService.CreateInterview(requestModel));


        /// <summary>
        /// Edição de Entrevista
        /// </summary>
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="200">Entrevista Alterada</response>
        /// <response code="400"></response>    
        /// <response code="500">APP_0001</response>           
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(InterviewModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<InterviewModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<InterviewModel>> UpdateInterview([FromBody]InterviewModel RequestModel)
            => Ok(await _interviewService.UpdateInterview(RequestModel, _loggedUser));


        /// <summary>
        /// Suggestion de Pequisa Rápida de Entrevistas
        /// </summary>
        /// <param name="Search">Termo de pesquisa a ser pesquisada em Entrevistas</param>       
        /// <response code="200">Retorna lista de acordo com o termo pesquisado (campo | valor)</response>      
        /// <response code="400"></response>   
        /// <response code="500">APP_0001</response>   
        [HttpGet("quick_search/suggestion")]
        [ProducesResponseType(200, Type = typeof(List<BaseSearchModel>))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<BaseSearchModel>>> SearchInterviewsFields([FromQuery]string Search)
            => Ok(await _interviewService.SearchInterviewByFields(Search));


        /// <summary>
        /// Pequisa Rápida de Entrevistas
        /// </summary>
        /// <param name="Field">Campo Índice a ser pesquisada na Entrevista</param>      
        /// <param name="Search">Termo de pesquisa a ser pesquisada na Entrevista</param>       
        /// <response code="200">Retorna lista de acordo com o termo pesquisado (campo | valor)</response>      
        /// <response code="400"></response>   
        /// <response code="500">APP_0001</response>   
        [HttpGet("quick_search")]
        [ProducesResponseType(200, Type = typeof(List<InterviewModel>))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<InterviewModel>>> QuickSearchInterviews([FromQuery]string Field, [FromQuery]string Search)
            => Ok(await _interviewService.QuickSearchInterviews(HttpUtility.UrlDecode(Field), HttpUtility.UrlDecode(Search)));


        /// <summary>
        /// Pequisa Avançada de Entrevistas
        /// </summary>
        /// <param name="RequestModel">Payload de Requisição</param>  
        /// <response code="200">Retorna lista de acordo com o termo pesquisado (campo | valor)</response>      
        /// <response code="400"></response>   
        /// <response code="500">APP_0001</response>   
        [HttpPost("search")]
        [ProducesResponseType(200, Type = typeof(List<InterviewModel>))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<AdvancedSearchModel>))]
        public async Task<ActionResult<IEnumerable<InterviewModel>>> AdvancedSearchInterviews([FromBody]AdvancedSearchModel RequestModel)
            => Ok(await _interviewService.AdvancedSearchInterviews(RequestModel));      

        /// <summary>
        /// Archive Interview 
        /// </summary>
        /// <param name="RequestModel">Id da Entrevista</param>  
        /// <response code="204">Sem retorno</response>             
        /// <response code="500">APP_0001</response>
        [HttpPost("archive")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<InterviewArchiveModel>))]
        public async Task<ActionResult<IEnumerable<SelectionModel>>> ArchiveInterview([FromBody] InterviewArchiveModel RequestModel)
        {
            await _interviewService.ArchiveInterview(RequestModel);

            return NoContent();
        }       

        #region InterviewUpdates

        /// <summary>
        /// Busca de Atualizações de Entrevista
        /// </summary>
        /// <param name="Id">Id da Entrevista</param>
        /// <response code="200">Lista com as Últimas Atualizações da Entrevista</response>
        /// <response code="400">APP_0012</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}/updates")]
        [ProducesResponseType(200, Type = typeof(List<InterviewUpdateModel>))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<IEnumerable<InterviewUpdateModel>>> GetLastUpdatesByInterview([FromRoute]long Id)
            => Ok(await _interviewService.GetLastUpdatesByInterview(Id));


        /// <summary>
        /// Criação de Nova Atualização de Entrevista
        /// </summary>  
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="201">Atualização de Entrevista Criada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPost("update")]
        [ProducesResponseType(201, Type = typeof(InterviewUpdateModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<InterviewUpdateModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<InterviewUpdateModel>> CreateInterviewUpdate([FromBody]InterviewUpdateModel RequestModel)
            => new CreatedResult(string.Empty, await _interviewService.CreateInterviewUpdate(RequestModel));       


        /// <summary>
        /// Atualização de Atualização de Entrevista
        /// </summary>  
        /// <param name="RequestModel">Payload de Requisição</param>
        /// <response code="200">Atualização de Entrevista Alterada com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>           
        [HttpPut("update")]
        [ProducesResponseType(200, Type = typeof(InterviewUpdateModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<InterviewUpdateModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<InterviewUpdateModel>> UpdateInterviewUpdate([FromBody]InterviewUpdateModel RequestModel)
            => Ok(await _interviewService.UpdateInterviewUpdate(RequestModel));


        /// <summary>
        /// Exclusão de Atualização de Entrevista
        /// </summary>  
        /// <param name="Id">Id da Atualização de Entrevista</param>
        /// <response code="204">Atualização de Entrevista Excluída com Sucesso (sem retorno)</response>
        /// <response code="400">APP_0012</response>         
        /// <response code="500">APP_0001</response>    
        [HttpDelete("update/{Id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult> DeleteInterviewUpdate(long Id)
        {
            await _interviewService.DeleteInterviewUpdate(Id);

            return NoContent();
        }

        #endregion
    }
}
