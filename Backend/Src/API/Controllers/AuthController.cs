﻿using System;
using System.Threading.Tasks;
using API.ActionFilters;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [AllowAnonymous]
    [ApiExplorerSettings(GroupName = @"Gestão de Acessos")]
    [Produces("application/json")]    
    [ApiController]
    [Route("1/auth")]    
    public class AuthController : BaseController
    {        
        private readonly IUserService _userService;

        public AuthController(IHttpContextAccessor httpContextAccessor, 
                              IWebHostEnvironment hostingEnvironment,                              
                              IUserService userService) : base(httpContextAccessor,
                                                               hostingEnvironment)
        {            
            _userService = userService;
        }

        /// <summary>
        /// Endpoint para autenticação de acesso/utilização do app.
        /// </summary>
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="200">Retorna os Dados do Usuário Logado e Tokens de Acesso</response>
        /// <response code="400">APP_0003, APP_0009</response>       
        /// <response code="500">APP_0001</response>   
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(200, Type = typeof(UserModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel[]))]        
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<AuthModel>))]
        public async Task<ActionResult<UserModel>> LoginAsync([FromBody] AuthModel requestModel)
            => Ok(await _userService.Authenticate(requestModel));

        /// <summary>
        /// Endpoint para enviar email com codigo para recuperar password.
        /// </summary>
        /// <param name="email">Email do user que pretende mudar a password</param>
        /// <response code="200">Retorna os Dados do Usuário que pretende fazer reset à sua password</response>
        /// <response code="400">APP_0003, APP_0009</response>       
        /// <response code="500">APP_0001</response>         
        [HttpPost]
        [Route("forgot")]
        [ProducesResponseType(200, Type = typeof(RecoverPasswordModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel[]))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<RecoverPasswordModel>> ForgotPassword([FromQuery] string email)
            => Ok(await _userService.SendRecoverPasswordEmail(email));

        /// <summary>
        /// Endpoint para enviar verificar codigo de recuperação de password.
        /// </summary>
        /// <param name="email">Email do user que pretende mudar a password</param>
        /// <param name="code"></param>
        /// <response code="200">Retorna os Dados do Usuário que pretende fazer reset à sua password</response>
        /// <response code="400">APP_0003, APP_0009</response>       
        /// <response code="500">APP_0001</response>         
        [HttpPost]
        [Route("verify")]
        [ProducesResponseType(200, Type = typeof(RecoverPasswordModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel[]))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        public async Task<ActionResult<RecoverPasswordModel>> VerifyCode([FromQuery] string email, [FromQuery] string code)
            => Ok(await _userService.VerifyRecoverPasswordCode(email,code));       
        
        [HttpGet("env")]
        public IActionResult CheckAppEnvironment()
        {
            try
            {
                return Ok(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }        

        /// <summary>
        /// Endpoint para validação de token de acesso ao app.
        /// </summary>
        /// <param name="Token">Token de Autenticação</param>
        /// <response code="200">Retorna os Dados do Usuário Logado e Tokens de Acesso</response>
        /// <response code="400">APP_0003, APP_0009</response>       
        /// <response code="500">APP_0001</response>                   
        [HttpGet("token/{Token}")]
        [ProducesResponseType(200, Type = typeof(UserModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel[]))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]      
        public async Task<ActionResult<UserModel>> AuthenticateByToken([FromRoute] Guid? Token)
            => Ok(await _userService.AuthenticateByToken(Token));
    }
}
