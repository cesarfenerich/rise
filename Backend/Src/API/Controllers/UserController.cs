﻿using System.Threading.Tasks;
using API.ActionFilters;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [ApiExplorerSettings(GroupName = @"Gestão de Acessos")]
    [Produces("application/json")]   
    [ApiController]
    [Route("1/user")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IHttpContextAccessor httpContextAccessor,
                              IWebHostEnvironment hostingEnvironment,
                              IUserService userService) : base(httpContextAccessor,
                                                               hostingEnvironment)
        {
            _userService = userService;
        }

        /// <summary>
        /// Busca de Utilizador pelo Id
        /// </summary>
        /// <param name="Id">Id do Utilizador</param>
        /// <response code="200">Utilizador Solicitado</response>
        /// <response code="400">APP_0009</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}")]
        [ProducesResponseType(200, Type = typeof(UserModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<UserModel>> GetById([FromRoute]long Id)
            => Ok(await _userService.GetById(Id));       
    }
}
