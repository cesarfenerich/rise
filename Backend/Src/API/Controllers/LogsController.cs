﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.ActionFilters;
using Common.Models;
using Domain.Entities.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [ApiExplorerSettings(GroupName = @"Logs")]
    [Produces("application/json")]    
    [ApiController]
    [Route("1/logs")]
    public class LogsController : BaseController
    {
        private readonly ILogService _logService;

        /// <summary>
        /// Exposição de Endpoints de Logs
        /// </summary>        
        public LogsController(IHttpContextAccessor httpContextAccessor,
                              IWebHostEnvironment hostingEnvironment,
                              ILogService logService) : base(httpContextAccessor,
                                                             hostingEnvironment)
        {
            _logService = logService;
        }        

        /// <summary>
        /// Pesquisa de Log pelo Id
        /// </summary>
        /// <param name="Id">Id do Log</param>
        /// <response code="200">Log Solicitado</response>
        /// <response code="400">APP_0036</response>         
        /// <response code="500">APP_0001</response>   
        [HttpGet("{Id}")]
        [ProducesResponseType(200, Type = typeof(LogModel))]
        [ProducesResponseType(400, Type = typeof(ErrorModel))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<LogModel>> GetById([FromRoute] long Id)
            => Ok(await _logService.GetById(Id));

        /// <summary>
        /// Retorna todos os logs
        /// </summary>        
        /// <response code="200">Log Solicitado</response>                 
        /// <response code="500">APP_0001</response>   
        [HttpGet("all")]
        [ProducesResponseType(200, Type = typeof(LogModel))]        
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(ActionParametersValidationFilter))]
        public async Task<ActionResult<LogModel>> GetAllLogs()
            => Ok(await _logService.GetAllLogs());

        /// <summary>
        /// Criação de Novo Log
        /// </summary>  
        /// <param name="requestModel">Payload de Requisição</param>
        /// <response code="201">Log Criado com Sucesso</response>
        /// <response code="400"></response>         
        /// <response code="500">APP_0001</response>       
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(LogModel))]
        [ProducesResponseType(400, Type = typeof(List<ErrorModel>))]
        [ProducesResponseType(500, Type = typeof(ErrorModel))]
        [ServiceFilter(typeof(RequestPayloadValidationFilter<LogModel>))]
        [ServiceFilter(typeof(TransactionFilter))]
        public async Task<ActionResult<LogModel>> CreateLog([FromBody] LogModel requestModel)
            => new CreatedResult(string.Empty, await _logService.CreateLog(requestModel));       
    }
}
