﻿using Common.Exceptions;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace API.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly ILogService _logService;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger, ILogService logService)
        {
            _logger = logger;
            _next = next;
            _logService = logService;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {                
                httpContext.Request.EnableBuffering();
                await _next(httpContext);
            }
            catch (AppException ex)
            {
                _logger.LogWarning($"{ex.Message}\n{ex.StackTrace}");
                await _logService.CreateLog(new LogModel { IP = httpContext.Connection?.RemoteIpAddress.ToString(), Username = "System", CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = $"{ex.Message}\n{ex.StackTrace}\n{ex.InnerException}" });
                await HandleExceptionAsync(httpContext, ex);
            }
            catch (Exception ex)
            {                
                var error = ErrorHelper.Except(ex, "APP_0001");
                _logger.LogError($"{ex.Message}\n{ex.StackTrace}");
                await _logService.CreateLog(new LogModel { IP = httpContext.Connection?.RemoteIpAddress.ToString(), Username = "System", CreatedAt = DateTime.Now, Operation = LogOperations.LO_0013, JsonRequest = string.Empty, Status = LogStatus.LS_0001, Error = $"{ex.Message}\n{ex.StackTrace}\n{ex.InnerException}" });
                await HandleExceptionAsync(httpContext, error);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, AppException exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception.Code == "APP_0001" ? 500 : 400;

            return context.Response.WriteAsync(JsonSerializer.Serialize(exception.AsErrorModel()));
        }
    }
}
