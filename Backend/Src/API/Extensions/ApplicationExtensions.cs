﻿using API.ActionFilters;
using API.Middlewares;
using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Entities.Interfaces.Repositories;
using Domain.Entities.Interfaces.Services;
using Domain.Models;
using Domain.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Persistence;
using Persistence.Repositories;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Threenine.Map;

namespace API.Extensions
{
    /// <summary>
    /// Extension Methods for Startup Application
    /// </summary>
    public static class ApplicationExtensions
    {
        #region Services

        /// <summary>
        /// Setup Database Connection with Dependency Injection
        /// </summary>        
        public static void SetupDbContext(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseMySql(ConfigurationHelper.DbConnectionString));
        }

        /// <summary>
        /// Setup Memory Cache for Application
        /// </summary>
        /// <param name="services"></param>
        public static void SetupMemoryCache(this IServiceCollection services)
        {            
            services.AddMemoryCache();
        }

        /// <summary>
        /// Setup CORS policies between clients and API
        /// </summary>   
        public static void SetupCorsPolicy(this IServiceCollection services)
        {
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.WithMethods("OPTIONS");            

            services.AddCors(options => options.AddPolicy("AllowAll", corsBuilder.Build()));
        }

        /// <summary>
        /// Setup Authorization Policy to consume API Endpoints
        /// </summary>        
        public static void SetupAuthorization(this IServiceCollection services)
        {
            services.AddMvcCore(config =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                config.Filters.Add(new AuthorizeFilter(policy));

            }).AddJsonOptions(options =>
            {                
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                
            }).AddApiExplorer()
              .AddAuthorization(options => options.AddPolicy("riseAppUser", policy => policy.RequireClaim("riseAppUser_")));
        }

        public static void SetupJWTAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userRepository = context.HttpContext.RequestServices.GetRequiredService<IUserRepository>();
                        var claimType = "riseAppUser_";
                        var userId = Int64.Parse(((ClaimsIdentity)context.Principal.Identity)
                                                                         .Claims
                                                                         .FirstOrDefault(c => c.Type == claimType).Value);
                        if (!userRepository.ValidateUserToAuthenticate(userId))
                            context.Fail("Unauthorized");

                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidIssuer = ConfigurationHelper.JwtIssuerOptions.Issuer,

                    ValidateAudience = false,
                    ValidAudience = ConfigurationHelper.JwtIssuerOptions.Audience,

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = ConfigurationHelper.JwtIssuerOptions.SigningKey,

                    RequireExpirationTime = true,
                    ValidateLifetime = true,

                    ClockSkew = TimeSpan.Zero
                };
            });
        }

        [Obsolete]
        public static void SetupSwagger(this IServiceCollection services)
        {           
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Backend API of RISE App",
                    Description = "Functionality Endpoints to Frontend Clients"                   
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "Copy 'Bearer ' + token'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                
                        },
                        new List<string>()
                    }
                });

                c.DocInclusionPredicate((_, api) => !string.IsNullOrWhiteSpace(api.GroupName));
                c.TagActionsBy(api => api.GroupName);              

            });
        }

        public static void SetupDependencyInjection(this IServiceCollection services)
        {
            services.AddScoped<ICandidateRepository, CandidateRepository>();
            services.AddScoped<IInterviewRepository, InterviewRepository>();
            services.AddScoped<IInterviewHistoryRepository, InterviewHistoryRepository>();
            services.AddScoped<IInterviewUpdateRepository, InterviewUpdateRepository>();
            services.AddScoped<IJobRoleRepository, JobRoleRepository>();
            services.AddScoped<ICandidateLanguageRepository, CandidateLanguageRepository>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IOwnerRepository, OwnerRepository>();
            services.AddScoped<ICandidateTechSkillRepository, CandidateTechSkillRepository>();
            services.AddScoped<ICandidateNaifasRepository, CandidateNaifasRepository>();
            services.AddScoped<ICandidateUploadRepository, CandidateUploadRepository>();
            services.AddScoped<IUploadRepository, UploadRepository>();
            services.AddScoped<ITechSkillRepository, TechSkillRepository>();           
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INaifasRepository, NaifasRepository>();
            services.AddScoped<ILogRepository, LogRepository>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //DomainServices
            services.AddScoped<IBaseService, BaseService>();
            services.AddScoped<ICandidateService, CandidateService>();
            services.AddScoped<IInterviewService, InterviewService>();    
            services.AddScoped<IOwnerService, OwnerService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ILogService, LogService>();
        }

        public static void SetupActionFilters(this IServiceCollection services)
        {
            services.AddScoped<TransactionFilter>();
            services.AddScoped<ActionParametersValidationFilter>();

            services.AddScoped<RequestPayloadValidationFilter<AuthModel>>();
            services.AddScoped<RequestPayloadValidationFilter<CandidateModel>>();
            services.AddScoped<RequestPayloadValidationFilter<InterviewModel>>();
            services.AddScoped<RequestPayloadValidationFilter<InterviewUpdateModel>>();
            services.AddScoped<RequestPayloadValidationFilter<OwnerModel>>();
            services.AddScoped<RequestPayloadValidationFilter<SelectionModel>>();
            services.AddScoped<RequestPayloadValidationFilter<TechSkillModel>>();
            services.AddScoped<RequestPayloadValidationFilter<UploadModel>>();
            services.AddScoped<RequestPayloadValidationFilter<NaifasModel>>();
            services.AddScoped<RequestPayloadValidationFilter<CandidateSearchModel>>();
            services.AddScoped<RequestPayloadValidationFilter<AdvancedSearchModel>>();
            services.AddScoped<RequestPayloadValidationFilter<LogModel>>();
            services.AddScoped<RequestPayloadValidationFilter<NaifasUploadModel>>();
            services.AddScoped<RequestPayloadValidationFilter<InterviewArchiveModel>>();
            services.AddScoped<RequestPayloadValidationFilter<TokenModel>>();

            services.AddControllers().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        #endregion

        #region Middlewares       

        public static void SetupErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlingMiddleware>();
        }       

        /// <summary>
        /// Swagger Setup Exposed only for Development Environments
        /// </summary>
        /// <param name="app"></param>
        public static void SetupSwaggerMiddleware(this IApplicationBuilder app)
        {
            if (ConfigurationHelper.AppEnvironment.Equals("Local") ||
               ConfigurationHelper.AppEnvironment.Equals("Development"))
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.IndexStream = () => typeof(Startup).GetTypeInfo().Assembly
                                                         .GetManifestResourceStream("API.Swagger.CustomIndex.html");

                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "RISE App API");
                    c.RoutePrefix = "docs/api";

                    c.DocExpansion(DocExpansion.None);

                    c.InjectStylesheet("/ext/custom-stylesheet.css");
                });
            }
        }

        #endregion

        #region Mapping
        public static void SetupMapConfiguration(this IApplicationBuilder app)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var assemblyWithMaps = assemblies.FirstOrDefault(x => x.GetName().Name.InsensitiveContains("Domain"));

            MapConfigurationFactory.LoadAllMappings(assemblyWithMaps.GetTypes());
        }
    #endregion
}
}
