using API.Extensions;
using AutoMapper;
using Common.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Persistence;

namespace API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
               .AddEnvironmentVariables();

            Configuration = builder.Build();

            ConfigurationHelper.SetupApplicationEnvironment(Configuration, env.WebRootPath);
        }       
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.Configure<MvcOptions>(options =>
            {
                options.EnableEndpointRouting = false;
            });

            services.SetupDbContext();
            services.SetupMemoryCache();
            services.SetupCorsPolicy();
            services.SetupAuthorization();
            services.SetupJWTAuthentication();
            services.SetupSwagger();
            services.SetupDependencyInjection();
            services.SetupActionFilters();
            services.AddControllers();           
        }        
   
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory, AppDbContext db)
        {
            db.Database.Migrate();

            loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

            app.Use(async (context, next) => { context.Request.EnableBuffering();
                await next();
            });
           
            app.SetupErrorHandlingMiddleware();           
            app.SetupSwaggerMiddleware();
            app.SetupMapConfiguration();

            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();           
            app.UseAuthorization();
            app.UseMvc(); 
        }
    }
}
