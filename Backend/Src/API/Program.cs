using System;
using System.IO;
using Common.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConfigurationHelper.SetBasePath();           
            var host = new WebHostBuilder()
                .UseKestrel(options =>
                {
                    options.Configure();
                    options.Limits.MaxRequestBodySize = 1000000;
                })
                .UseContentRoot(Directory.GetCurrentDirectory())                
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }        
    }
}
