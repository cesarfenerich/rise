﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class UploadModel : IGenericModel, IMapFrom<Upload>
    {      
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "filePath")]
        public string FilePath { get; set; }


        [JsonProperty(PropertyName = "uploadedAt")]
        public DateTime? UploadedAt { get; set; }

        public UploadModel()
        { }

        public UploadModel(long id, string fileName, string filePath)
        {
            this.Id = id;
            this.FileName = fileName;
            this.FilePath = filePath;
            this.UploadedAt = DateTime.UtcNow;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
