﻿using Common.Extensions;
using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Models
{
    public class AdvancedSearchModel : IGenericModel
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "matchFields")]
        public bool MatchFields { get; set; }

        [JsonProperty(PropertyName = "availability")]
        public string Availability { get; set; }

        [JsonProperty(PropertyName = "expectedSalary")]
        public string ExpectedSalary { get; set; }

        [JsonProperty(PropertyName = "yearsOfExperience")]
        public string YearsOfExperience { get; set; }

        [JsonProperty(PropertyName = "contractType")]
        public string ContractType { get; set; }

        [JsonProperty(PropertyName = "ownerId")]
        public long? OwnerId { get; set; }

        [JsonProperty(PropertyName = "techSkills")]
        public IEnumerable<long> TechSkills { get; set; }

        [JsonProperty(PropertyName = "linkdin")]
        public string Linkdin { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "languages")]
        public IEnumerable<long> Languages { get; set; }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }

        public AdvancedSearchModel()
        {

        }

        public AdvancedSearchModel(bool matchFields,
                                   string name,
                                   string availability,
                                   string expectedSalary,
                                   string yearsOfExperience,
                                   string contractType,
                                   long? ownerId,
                                   IEnumerable<long> techSkills,
                                   string linkdin,
                                   string location,
                                   IEnumerable<long> languages)
        {
            this.MatchFields = matchFields;
            this.Name = name.IsNullOrWhitespaceOrEmpty() ? null : name;
            this.Availability = availability.IsNullOrWhitespaceOrEmpty() ? null : availability;
            this.ExpectedSalary = expectedSalary.IsNullOrWhitespaceOrEmpty() ? null : expectedSalary;
            this.YearsOfExperience = yearsOfExperience.IsNullOrWhitespaceOrEmpty() ? null : yearsOfExperience;
            this.ContractType = contractType.IsNullOrWhitespaceOrEmpty() ? null : contractType;
            this.OwnerId = ownerId;
            this.TechSkills = techSkills;
            this.Linkdin = linkdin.IsNullOrWhitespaceOrEmpty() ? null : linkdin;
            this.Location = location.IsNullOrWhitespaceOrEmpty() ? null : location;
            this.Languages = languages.Any() ? languages : null;
        }
    }
}