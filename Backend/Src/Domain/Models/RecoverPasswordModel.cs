﻿using Newtonsoft.Json;
using System;

namespace Domain.Models
{
    public class RecoverPasswordModel
    {
        [JsonProperty(PropertyName = "forgotMailSent", NullValueHandling = NullValueHandling.Ignore)]
        public bool ForgotMailSent { get; set; }

        [JsonProperty(PropertyName = "recoveryCodeChecked", NullValueHandling = NullValueHandling.Ignore)]
        public bool RecoveryCodeChecked { get; set; }

        public RecoverPasswordModel()
        { }

        public RecoverPasswordModel(bool mailSent)
        {
            this.ForgotMailSent = mailSent;           
        }

        public RecoverPasswordModel(bool mailSent, bool recoveryCodeChecked)
        {
            this.ForgotMailSent = mailSent;
            this.RecoveryCodeChecked = recoveryCodeChecked;
        }
    }
}
