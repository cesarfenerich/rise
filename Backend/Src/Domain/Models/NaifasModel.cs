﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Threenine.Map;

namespace Domain.Models
{
    public class NaifasModel : IGenericModel, IMapFrom<Naifas>
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "question")]
        public string Question { get; set; }

        [JsonProperty(PropertyName = "answer")]
        public string Answer { get; set; }

        public NaifasModel() { }

        public NaifasModel(long id, string question, string answer)
        {
            this.Id = id;
            this.Question = question;
            this.Answer = answer;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
