﻿using Common.Extensions;
using Common.Models;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Threenine.Map;

namespace Domain.Models
{
    public class InterviewInfoModel : IMapFrom<Interview>
    {       
        public long Id { get; set; }
        public long OwnerId { get; set; }
        public string Status { get; set; }
        public long CandidateId { get; set; }
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string CandidateAvailability { get; set; }
        public string CandidateAvailabilityInterval { get; set; }
        public string CandidateAvailableAt { get; set; }
        public string CandidateType { get; set; }
        public string YearsOfExperience { get; set; }
        public string LinkedinProfile { get; set; }

        public string Email { get; set; }
        public string BornDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Citizenship { get; set; }
        public string MaritalStatus { get; set; }
        public string DependentsUnder7 { get; set; }
        public string DependentsAbove7 { get; set; }
        public string Location { get; set; }
        public string LiteraryAbility { get; set; }
        public string College { get; set; }
        public string Certifications { get; set; }
        public string Languages { get; set; }

        public List<TechSkillModel> TechSkills { get; set; }

        public string ContractType { get; set; }
        public string OngoingProcesses { get; set; }        
        public string ReasonForLeaving { get; set; }
        public string CurrentSalary { get; set; }
        public string IsCurrentSalary { get; set; }
        public string ExpectedSalary { get; set; }
        public string IsExpectedSalary { get; set; }
        public bool? IsExpectationMonthlyPayment { get; set; }
        public bool? IsCurrentMonthlyPayment { get; set; }



        public List<NoteModel> Experiences { get; set; }
        public List<NoteModel> Notes { get; set; }

        public InterviewInfoModel()
        { }

        public InterviewInfoModel(long id, 
                                  long ownerId,
                                  string status,
                                  long candidateId,
                                  string name, 
                                  string jobTitle, 
                                  string availabilityDescription,
                                  string availabilityInterval,
                                  DateTime? availableAt,
                                  string typeDescription, 
                                  string email, 
                                  DateTime? bornDate, 
                                  string phoneNumber,
                                  string citizenship, 
                                  string maritalStatus, 
                                  string city, 
                                  string literaryAbilityDescription, 
                                  string college, 
                                  string certifications, 
                                  string contractTypeDescription, 
                                  string ongoingProcesses, 
                                  decimal? currentSalary,
                                  bool? isCurrentSalaryNet,
                                  decimal? expectationSalary,
                                  bool? isExpectedSalaryNet,
                                  string reasonForLeaving,
                                  int dependentsUnder7,
                                  int dependentsAbove7,
                                  int yearsOfExperience,
                                  IEnumerable<SelectionModel> candidateLanguages,
                                  IEnumerable<TechSkillModel> candidateTechSkills,
                                  IEnumerable<NoteModel> experiences, 
                                  IEnumerable<NoteModel> notes,
                                  string linkedinProfileLink,
                                  bool? isCurrentMonthlyPayment,
                                  bool? isExpectationMonthlyPayment) 
        {
            Id = id;
            OwnerId = ownerId;
            Status = status;
            CandidateId = candidateId;
            Name = name;
            JobTitle = jobTitle;
            YearsOfExperience = yearsOfExperience.ToString();
            CandidateAvailability = availabilityDescription;
            CandidateAvailabilityInterval = availabilityInterval;
            CandidateAvailableAt = availableAt.GetValueOrDefault().ToString("dd/MM/yyyy");
            CandidateType = typeDescription;
            Email = email;
            BornDate = bornDate.GetValueOrDefault().ToString("dd/MM/yyyy");
            PhoneNumber = phoneNumber;
            Citizenship = citizenship;
            MaritalStatus = maritalStatus;
            DependentsUnder7 = dependentsUnder7.ToString();
            DependentsAbove7 = dependentsAbove7.ToString();
            Location = city;
            LiteraryAbility = literaryAbilityDescription;
            College = college;
            Certifications = certifications;
            ContractType = contractTypeDescription;
            OngoingProcesses = ongoingProcesses;
            ReasonForLeaving = reasonForLeaving;
            SetSalaries(currentSalary, isCurrentSalaryNet, expectationSalary, isExpectedSalaryNet);           
            SetLanguages(candidateLanguages);
            SetTechSkills(candidateTechSkills);
            SetExperiences(experiences);
            SetNotes(notes);
            LinkedinProfile = linkedinProfileLink;
            IsCurrentMonthlyPayment = isCurrentMonthlyPayment;
            IsExpectationMonthlyPayment = isExpectationMonthlyPayment;
        }

        private void SetSalaries(decimal? currentSalary, bool? isCurrentSalaryNet, decimal? expectationSalary, bool? isExpectedSalaryNet)
        {
            CurrentSalary = currentSalary.GetValueOrDefault(0).ToString();

            if (isCurrentSalaryNet.HasValue)
                IsCurrentSalary = isCurrentSalaryNet.Value ? "Líquido" : "Bruto";            

            ExpectedSalary = expectationSalary.GetValueOrDefault(0).ToString();

            if (isExpectedSalaryNet.HasValue)
                IsExpectedSalary = isExpectedSalaryNet.Value ? "Líquido" : "Bruto";
        }

        private void SetLanguages(IEnumerable<SelectionModel> candidateLanguages)
        {
            foreach(var language in candidateLanguages)
            {
                if (Languages.IsNullOrWhitespaceOrEmpty())
                    Languages = language.Value;
                else
                    Languages += $",{language.Value}";
            }
        }

        private void SetTechSkills(IEnumerable<TechSkillModel> candidateTechSkills)
        {
            TechSkills = candidateTechSkills.ToList();
        }

        private void SetExperiences(IEnumerable<NoteModel> experiences)
        {
            Experiences = experiences.ToList();
        }

        private void SetNotes(IEnumerable<NoteModel> notes)
        {
            Notes = notes.ToList();
        }
    }    
}
