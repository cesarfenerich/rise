﻿using Newtonsoft.Json;
using System;

namespace Domain.Models
{
    public class UserModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "ownerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? OwnerId { get; set; }

        [JsonProperty(PropertyName = "email", NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "authType", NullValueHandling = NullValueHandling.Ignore)]
        public string AuthType { get; set; }

        [JsonProperty(PropertyName = "accessToken", NullValueHandling = NullValueHandling.Ignore)]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "refreshToken", NullValueHandling = NullValueHandling.Ignore)]
        public Guid RefreshToken { get; set; }

        [JsonProperty(PropertyName = "expiresAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime ExpiresAt { get; set; }

        [JsonProperty(PropertyName = "failedAttempts", NullValueHandling = NullValueHandling.Ignore)]
        public int FailedAttempts { get; set; }

        [JsonProperty(PropertyName = "blocked", NullValueHandling = NullValueHandling.Ignore)]
        public bool Blocked { get; set; }

        [JsonProperty(PropertyName = "resetCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ResetCode { get; set; }

        [JsonProperty(PropertyName = "lastPasswordUpdate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime LastPasswordUpdate { get; set; }

        public UserModel()
        { }

        public UserModel(long userId, string email, string password)
        {
            this.Id = userId;
            this.Email = email;
            this.Password = password;
        }

        public UserModel(string email, string password)
        {            
            this.Email = email;
            this.Password = password;
        }
    }
}
