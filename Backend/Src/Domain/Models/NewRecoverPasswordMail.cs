﻿using Common.Helpers;
using Common.Interfaces;
using Microsoft.AspNetCore.Http;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Domain.Models
{
    public class NewRecoverPasswordMail : IGenericEmail
    {
        public string Subject { get { return "Reset Password RISE"; } }
        public string Receiver { get; set; }
        public string Body { get; set; }
        public List<MailboxAddress> To { get; set; }
       
        public string Content { get ; set; }
        public IFormFileCollection Attachments { get; set; }

        public NewRecoverPasswordMail(string email, string name, string code)
        {
            this.Receiver = email;
            this.To = new List<MailboxAddress> { new MailboxAddress(email) };
            BuildMailBody(name, code);
        }

        private void BuildMailBody(string name, string code)
        {
           
            var layoutPath = Path.Combine($"{ConfigurationHelper.MailPath}", "RecoverPasswordEmail.html");

            using (StreamReader SourceReader = System.IO.File.OpenText(layoutPath))
            {
                Body = SourceReader.ReadToEnd();
            }

            Body = Body.Replace("{name}", name);
            Body = Body.Replace("{code}", code);

            Content = Body;
        }
    }
}
