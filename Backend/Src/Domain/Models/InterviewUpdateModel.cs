﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class InterviewUpdateModel : IGenericModel, IMapFrom<InterviewUpdate>
    {
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }       

        [JsonProperty(PropertyName = "interviewId")]
        public long InterviewId { get; set; }

        [JsonProperty(PropertyName = "ownerId")]
        public long OwnerId { get; set; }

        [JsonProperty(PropertyName = "owner")]
        public string Owner { get; set; }

        [JsonProperty(PropertyName = "resume")]
        public string Resume { get; set; }

        [JsonProperty(PropertyName = "createdAt")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty(PropertyName = "updatedAt")]
        public DateTime? UpdatedAt { get; set; }

        public InterviewUpdateModel()
        { }

        public InterviewUpdateModel(long interviewId, long ownerId, string resume)
        {
            this.InterviewId = interviewId;
            this.OwnerId = ownerId;
            this.Resume = resume;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
