﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class NoteModel
    {       
        public string Note { get; set; }
        public string Footer { get; set; }

        public NoteModel()
        {

        }

        public NoteModel(string note, long id, string owner, DateTime interviewDate)
        {
            this.Note = note;
            this.Footer = $"Entrevista {id} - {owner} - {interviewDate.ToString("dd-MM-yyyy")}"; 
        }
    }
}
