﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class CandidateModel : IGenericModel, IMapFrom<Candidate>
    {
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        [JsonProperty(PropertyName = "jobRole")]
        public long? JobRoleId { get; set; }

        [JsonProperty(PropertyName = "location")]
        public long? LocationId { get; set; }       

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "bornDate")]
        public DateTime? BornDate { get; set; }

        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "citizenship")]
        public string Citizenship { get; set; }

        [JsonProperty(PropertyName = "maritalStatus")]
        public string MaritalStatus { get; set; }

        [JsonProperty(PropertyName = "dependentsUnder7")]
        public int? DependentsUnder7 { get; set; }

        [JsonProperty(PropertyName = "dependentsAbove7")]
        public int? DependentsAbove7 { get; set; }       

        [JsonProperty(PropertyName = "literaryAbility")]
        public string LiteraryAbility { get; set; }

        [JsonProperty(PropertyName = "college")]
        public string College { get; set; }

        [JsonProperty(PropertyName = "certifications")]
        public string Certifications { get; set; }

        [JsonProperty(PropertyName = "available")]
        public string Available { get; set; }
        [JsonProperty(PropertyName = "interval")]
        public string Interval { get; set; }

        [JsonProperty(PropertyName = "linkedinProfileLink")]
        public string LinkedInProfileLink { get; set; }

        [JsonProperty(PropertyName = "yearsOfExperience")]
        public int? YearsOfExperience { get; set; }

        [JsonProperty(PropertyName = "availableAt")]
        public DateTime? AvailableAt { get; set; }

        [JsonProperty(PropertyName = "mobility")]
        public string Mobility { get; set; }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }

        public CandidateModel()
        { }

        public CandidateModel(long? jobRoleId, 
                              long? locationId, 
                              string type, 
                              string name, 
                              DateTime? bornDate, 
                              string phoneNumber, 
                              string email, 
                              string citizenship, 
                              string maritalStatus,
                              int? dependentsUnder7,
                              int? dependentsAbove7,
                              string literaryAbility,
                              string college,
                              string certifications,
                              string available,
                              string linkedinProfile,
                              int? yearsOfExperience)
        {
            JobRoleId = jobRoleId;
            LocationId = locationId;
            Type = type;
            Name = name;
            BornDate = bornDate;
            PhoneNumber = phoneNumber;
            Email = email;
            Citizenship = citizenship;
            MaritalStatus = maritalStatus;
            DependentsUnder7 = dependentsUnder7;
            DependentsAbove7 = dependentsAbove7;
            LiteraryAbility = literaryAbility;
            College = college;
            Certifications = certifications;
            Available = available;
            LinkedInProfileLink = linkedinProfile;
            YearsOfExperience = yearsOfExperience;
        }
    }
}
