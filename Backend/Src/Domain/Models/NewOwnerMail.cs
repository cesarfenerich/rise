﻿using Common.Helpers;
using Common.Interfaces;
using Microsoft.AspNetCore.Http;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Domain.Models
{
    public class NewOwnerMail : IGenericEmail
    {
        public string Subject { get { return "Acesso ao RISE"; } }
        public string Receiver { get; set; }
        public string Body { get; set; }
        public List<MailboxAddress> To { get; set; }
       
        public string Content { get ; set; }
        public IFormFileCollection Attachments { get; set; }

        public NewOwnerMail(string ownerEmail, string ownerName, string password)
        {
            this.Receiver = ownerEmail;
            this.To = new List<MailboxAddress> { new MailboxAddress(ownerEmail) };
            BuildMailBody(ownerName, ownerEmail, password);
        }

        private void BuildMailBody(string name, string login, string password)
        {
           
            var layoutPath = Path.Combine($"{ConfigurationHelper.MailPath}", "NewOwnerMail.html");

            using (StreamReader SourceReader = System.IO.File.OpenText(layoutPath))
            {
                Body = SourceReader.ReadToEnd();
            }

            Body = Body.Replace("{name}", name);
            Body = Body.Replace("{userLogin}", login);
            Body = Body.Replace("{userPass}", password);

            Content = Body;
        }
    }
}
