﻿using Domain.Entities;
using System;
using Threenine.Map;

namespace Domain.Models
{
    public class InterviewGridModel
    {        
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string State { get; set; }
        public long CandidateId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string JobRole { get; set; }
        public string Technologies { get; set; }
        public decimal SalaryExpected { get; set; }
        public string Owner { get; set; }
        public string Avaliability { get; set; }

        public InterviewGridModel()
        {

        }

        public InterviewGridModel(DateTime date, string state, string name, string email, string jobRole, string technologies, decimal salaryExpected, string owner, string avaliability)
        {
            this.Date = date;
            this.State = state;
            this.Name = name;
            this.Email = email;
            this.JobRole = jobRole;
            this.Technologies = technologies;
            this.SalaryExpected = salaryExpected;
            this.Owner = owner;
            this.Avaliability = avaliability;
        }
    }
}
