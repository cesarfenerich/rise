﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class TechSkillModel : IGenericModel, IMapFrom<TechSkill>
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public TechSkillModel()
        {

        }

        public TechSkillModel(long Id, string category, string description)
        {
            this.Id = Id;
            this.Category = category;
            this.Description = description;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
