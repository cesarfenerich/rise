﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class CandidateSearchModel : IGenericModel, IMapFrom<Candidate>
    {       

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }    

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }

        public CandidateSearchModel()
        {
                
        }

        public CandidateSearchModel(string name, string email)
        {
            this.Name = name;
            this.Email = email;
        }
    }
}
