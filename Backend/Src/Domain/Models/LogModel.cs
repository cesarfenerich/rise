﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Threenine.Map;

namespace Domain.Models
{
    public class LogModel : IGenericModel, IMapFrom<Log>
    {
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }
        [JsonProperty(PropertyName = "ip")]
        public string IP { get; set; }
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(PropertyName = "operation")]
        public string Operation { get; set; }
        [JsonProperty(PropertyName = "json_request")]
        public string JsonRequest { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }

        public LogModel()
        { }

        public LogModel(string ip, string username, DateTime createdAt, string operation, string jsonRequest, string status, string error)
        {
            IP = ip;
            Username = username;
            CreatedAt = createdAt;
            Operation = operation;
            JsonRequest = jsonRequest;
            Status = status;
            Error = error;
        }
    }
}
