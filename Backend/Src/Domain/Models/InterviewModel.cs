﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Threenine.Map;

namespace Domain.Models
{
    public class InterviewModel : IGenericModel, IMapFrom<Interview>
    {
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        [JsonProperty(PropertyName = "ownerId")]
        public long? OwnerId { get; set; }

        [JsonProperty(PropertyName = "owner")]
        public string Owner { get; set; }

        [JsonProperty(PropertyName = "candidateId")]
        public long? CandidateId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "contractType")]
        public string ContractType { get; set; }

        [JsonProperty(PropertyName = "reasonForLeaving")]
        public string ReasonForLeaving { get; set; }

        [JsonProperty(PropertyName = "ongoingProcesses")]
        public string OngoingProcesses { get; set; }

        [JsonProperty(PropertyName = "currentSalary")]
        public decimal? CurrentSalary { get; set; }

        [JsonProperty(PropertyName = "isCurrentSalaryNet")]
        public bool? IsCurrentSalaryNet { get; set; }

        [JsonProperty(PropertyName = "expectationSalary")]
        public decimal? ExpectationSalary { get; set; }

        [JsonProperty(PropertyName = "isExpectationSalaryNet")]
        public bool? IsExpectationSalaryNet { get; set; }

        [JsonProperty(PropertyName = "professionalExperience")]
        public string ProfessionalExperience { get; set; }

        [JsonProperty(PropertyName = "notes")]
        public string Notes { get; set; }

        [JsonProperty(PropertyName = "createdAt")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty(PropertyName = "archived")]
        public bool Archived { get; set; }

        [JsonProperty(PropertyName = "archivedReason")]
        public string ArchivedReason { get; set; }
        
        [JsonProperty(PropertyName = "isCurrentMonthlyPayment")]
        public bool? IsCurrentMonthlyPayment { get; set; }
        
        [JsonProperty(PropertyName = "isExpectationMonthlyPayment")]
        public bool? IsExpectationMonthlyPayment { get; set; }
     

        public InterviewModel()
        { }

        public InterviewModel(long? ownerId,
                              string owner,
                              long? candidateId,
                              string status,
                              string contractType,
                              string reasonForLeaving,
                              string ongoingProcesses,
                              decimal currentSalary,
                              bool isCurrentSalaryNet,
                              decimal expectationSalary,
                              bool isExpectationSalaryNet,
                              string professionalExperience,
                              string notes,
                              bool archived,
                              string archivedReason,
                              DateTime? createdAt = null,                              
                              bool? isCurrentMonthlyPayment = null,
                              bool? isExpectationMonthlyPayment = null)
        {
            OwnerId = ownerId;
            Owner = owner;
            CandidateId = candidateId;
            Status = status;
            ContractType = contractType;
            ReasonForLeaving = reasonForLeaving;
            OngoingProcesses = ongoingProcesses;
            CurrentSalary = currentSalary;
            IsCurrentSalaryNet = isCurrentSalaryNet;
            ExpectationSalary = expectationSalary;
            IsExpectationSalaryNet = isExpectationSalaryNet;
            ProfessionalExperience = professionalExperience;
            Notes = notes;
            CreatedAt = createdAt;           
            IsCurrentMonthlyPayment = isCurrentMonthlyPayment;
            IsExpectationMonthlyPayment = isExpectationMonthlyPayment;
            Archived = archived;
            ArchivedReason = archivedReason;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }

    }
}

