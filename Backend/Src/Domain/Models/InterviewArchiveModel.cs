﻿using Common.Interfaces;
using Common.Models;
using System.Collections.Generic;

namespace Domain.Models
{
    public class InterviewArchiveModel : IGenericModel

    {
        public long InterviewId { get; set; }    
        public string ArchivedReason { get; set; }

        public InterviewArchiveModel()
        {

        }

        public InterviewArchiveModel(long interviewArchived, string archivedReason)
        {
            this.InterviewId = interviewArchived;        
            this.ArchivedReason = archivedReason;
        }

        public IEnumerable<ErrorModel> Validate(IList<ErrorModel> validations)
        {
            return validations;
        }
    }
}
