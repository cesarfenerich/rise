﻿using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Domain.Entities;
using Domain.Entities.Interfaces;
using Domain.Entities.ValueObjects;
using Domain.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IMemoryCache _cache;

        private readonly IBaseService _base;
        private readonly IUserRepository _userRepository;

        public UserService(IMemoryCache memoryCache, IBaseService baseService, IUserRepository userRepository)
        {
            _cache = memoryCache;
            _base = baseService;
            _userRepository = userRepository;
        }

        public Task<UserModel> GetById(long id)
        {
            var user = _userRepository.GetUserById(id);

            _base.NullCheck(user, "APP_0009");

            return Task.FromResult(_base.Map<User, UserModel>(user));
        }

        #region Auth

        public async Task<UserModel> Authenticate(AuthModel requestModel)
        {
            var userToAuthenticate = _userRepository.GetByEmail(requestModel.Email);             

            _base.NullCheck(userToAuthenticate, "APP_0009");           
            if(userToAuthenticate != null)
            {
                string errorMessage;
                userToAuthenticate.ValidatePassword(requestModel.Password, true, out errorMessage, true);
                userToAuthenticate.SetAuthToken(await GetAuthTokenByLogin(userToAuthenticate.Id));

                userToAuthenticate.Update(userToAuthenticate.Email, userToAuthenticate.Password, userToAuthenticate.FailedAttempts, userToAuthenticate.Blocked, true);
                await _userRepository.Update(userToAuthenticate);

                userToAuthenticate.HandleErrorMessage(errorMessage);
            }
            return _base.Map<User, UserModel>(userToAuthenticate);
        }

        public async Task<UserModel> AuthenticateByToken(Guid? token)
        {          
            try
            {
                if (token is null)
                    ErrorHelper.Except("APP_0043");
                else
                {                                       
                    var _cachedAccessToken = _cache.Get<string>(token.Value.ToString("D"));

                    if (_cachedAccessToken is null)
                        ErrorHelper.Except("APP_0043");
                    else
                    {
                        var t = new JwtSecurityTokenHandler().ReadToken(_cachedAccessToken) as JwtSecurityToken;
                        var userToAuthenticate = _userRepository.GetUserById(GetUserOwnerOfAuthToken(t).Value);

                        _base.NullCheck(userToAuthenticate, "APP_0009");

                        userToAuthenticate.SetAuthToken(await GetAuthTokenByRefresh(userToAuthenticate.Id, t, token.GetValueOrDefault()));

                        return _base.Map<User, UserModel>(userToAuthenticate);                        
                    }
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }           
        }

        #region Utils

        private async Task<JwtToken> GetAuthTokenByLogin(long userId)
             => await ConstructAuthToken(userId, null, null);

        private async Task<JwtToken> GetAuthTokenByRefresh(long userId, JwtSecurityToken accessToken, Guid refreshToken)
           => await ConstructAuthToken(userId, accessToken, refreshToken);

        private async Task<JwtToken> ConstructAuthToken(long userId, JwtSecurityToken accessToken, Guid? refreshToken)
        {
            var _claimJti = await ConfigurationHelper.JwtIssuerOptions.JtiGenerator();
            var _claimIat = ConfigurationHelper.JwtIssuerOptions.IssuedAt.ToUnixEpochDate().ToString();
            JwtToken authToken = new JwtToken();

            if (refreshToken is null)
            {
                authToken = new JwtToken(userId,
                                         ConfigurationHelper.JwtIssuerOptions.Issuer,
                                         ConfigurationHelper.JwtIssuerOptions.Audience,
                                         ConfigurationHelper.JwtIssuerOptions.SigningCredentials,
                                         ConfigurationHelper.JwtIssuerOptions.NotBefore,
                                         CurrentExpiration(ConfigurationHelper.JwtIssuerOptions.ValidFor),
                                         _claimJti,
                                         _claimIat);                
            }
            else
            {
                string _cachedAccessToken = _cache.Get<string>(refreshToken.Value.ToString("D"));

                if (_cachedAccessToken.IsNullOrWhitespaceOrEmpty())
                    ErrorHelper.Except("APP_0010");
                else
                {
                    authToken = new JwtToken(userId,
                                             refreshToken.Value,
                                             accessToken,
                                             _cachedAccessToken,
                                             ConfigurationHelper.JwtIssuerOptions.Issuer,
                                             ConfigurationHelper.JwtIssuerOptions.Audience,
                                             ConfigurationHelper.JwtIssuerOptions.SigningCredentials,
                                             ConfigurationHelper.JwtIssuerOptions.NotBefore,
                                             CurrentExpiration(ConfigurationHelper.JwtIssuerOptions.ValidFor),
                                             _claimJti,
                                             _claimIat);
                }
            }

            CacheRefreshToken(authToken);            

            return authToken;
        }

        private void CacheRefreshToken(JwtToken token)
        {
            if (token.CacheRefreshToken)
                _cache.Remove(token.RefreshToken.Value.ToString("D"));
           
            MemoryCacheEntryOptions opcoesCache = new MemoryCacheEntryOptions();

            opcoesCache.SetAbsoluteExpiration(TimeSpan.FromMinutes(15));

            _cache.Set(token.RefreshToken.Value.ToString("D"),
                        token.AccessToken,
                        opcoesCache);                
        }

        private DateTime CurrentExpiration(TimeSpan validFor)
        {
            return DateTime.UtcNow.Add(validFor);
        }

        private long? GetUserOwnerOfAuthToken(JwtSecurityToken authToken)
        {
            if (authToken is null)
                return null;
            else
            {
                var userId = authToken.Claims.FirstOrDefault(t =>
                t.Type == "riseAppUser_");

                return Int64.Parse(userId.Value);
            }
        }


        #endregion

        #endregion      

        public async Task<UserModel> CreateUser(UserModel model)
        {
            ValidateUserEmail(model.Email);

            var userToCreate = new User(model.Email, SecurityHelper.Encript(model.Password), 0, false, false);

            var userCreated = await _userRepository.Create(userToCreate);

            return _base.Map<User, UserModel>(userCreated);
        }

        public async Task<UserModel> UpdateUser(UserModel model)
        {
            var userToUpdate = _userRepository.GetUserById(model.Id);

            _base.NullCheck(userToUpdate, "APP_0009");

            if (model.Email is null)
                model.Email = userToUpdate.Email;

            if (userToUpdate.Email != model.Email)
                ValidateUserEmail(model.Email);

            if (!userToUpdate.Password.Equals(model.Password))
                userToUpdate.Update(model.Email, SecurityHelper.Encript(model.Password), 0, false);
            else
                userToUpdate.Update(model.Email, userToUpdate.Password, 0, false);

            var userUpdated = await _userRepository.Update(userToUpdate);

            return _base.Map<User, UserModel>(userUpdated);
        }

        public async Task<RecoverPasswordModel> SendRecoverPasswordEmail(string email)
        {
            var userToReset = _userRepository.GetByEmail(email);   
            _base.NullCheck(userToReset, "APP_0009");           
           
            String code = SecurityHelper.GetRandomSixDigit();

            userToReset.SetResetCode(code);
            var mailSent = SendRecoverPasswordMail(userToReset.Email, userToReset.Owner.Name, code);
            await _userRepository.Update(userToReset);
            
            return new RecoverPasswordModel(mailSent);
        }

        public async Task<UserModel> UpdateUserPassword(UserModel userModel)
        {
            var user = _userRepository.GetByEmail(userModel.Email);
            _base.NullCheck(user, "APP_0009");

            var userUpdated = await UpdateUser(new UserModel(user.Id, userModel.Email, userModel.Password));

            _base.NullCheck(userUpdated, "APP_0009");

            return userUpdated;
        }

        public Task<RecoverPasswordModel> VerifyRecoverPasswordCode(string email, string code)
        {
            var user = _userRepository.GetByEmail(email);

            _base.NullCheck(user, "APP_0009");           

            return Task.FromResult(new RecoverPasswordModel(user.ResetCode.IsNullOrWhitespaceOrEmpty(), 
                                                            user.ResetCode.Equals(code)));
        }

        private void ValidateUserEmail(string email)
        {
            var user = _userRepository.GetByEmail(email);

            if (user != null)
                ErrorHelper.Except("APP_0031");
        }

        private bool SendRecoverPasswordMail(string email, string name, string code)
        {
            var emailToSend = new NewRecoverPasswordMail(email, name, code);
            return _base.SendMail(emailToSend);
        }
    }
}
