﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using Domain.Entities.Interfaces.Repositories;
using Domain.Entities.Interfaces.Services;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class LogService : ILogService
    {
        private readonly IBaseService _base;
        private readonly ILogRepository _logRepository;

        public LogService(IBaseService baseService,
                            ILogRepository logRepository)
        {
            _base = baseService;
            _logRepository = logRepository;
        }

        public Task<LogModel> GetById(long id)
        {
            var log = _logRepository.GetLogById(id);

            _base.NullCheck(log, "APP_0036");

            return Task.FromResult(_base.Map<Log, LogModel>(log));
        }

        public Task<IEnumerable<LogModel>> GetAllLogs()
        {
            var logs = _logRepository.GetAllLogs();

            return Task.FromResult(_base.MapList<Log, LogModel>(logs));
        }

        public async Task<LogModel> CreateLog(LogModel requestModel)
        {           
            var logCreated = await _logRepository.Create(new Log(requestModel));
            //SendNewOwnerMail(ownerCreated);
            return _base.Map<Log, LogModel>(logCreated);
        }
    }
}
