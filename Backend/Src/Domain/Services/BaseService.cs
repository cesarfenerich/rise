﻿using AutoMapper;
using Common.Helpers;
using Common.Interfaces;
using Common.Models;
using Domain.Entities.Interfaces;
using Integrations.Email;
using Integrations.S3;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class BaseService : IBaseService
    {
        public BaseService()
        { }        

        #region FileStorageClient

        public void UploadFile(IFormFile fileToUpload, string fileName)
        {
            try
            {
                if (fileToUpload is null)
                    throw ErrorHelper.Except("APP_0028");
                else
                {                                   
                    var filePath = Path.GetTempFileName();

                    using var fileStream = new FileStream(filePath, FileMode.Create);
                    fileToUpload.CopyTo(fileStream);

                    using var fileStorageClient = new FileStorageClient();
                    fileStorageClient.UploadFileAsync(fileStream, fileName).Wait();
                }
            }
            catch (Exception ex)
            {
                throw ErrorHelper.Except(ex, "APP_0029");
            }                   
        }


        public async Task<byte[]> DownloadFile(string key)
        {
            try
            {
                using var fileStorageClient = new FileStorageClient();
                var filePath = await fileStorageClient.DownloadFileAsync(key);

                if (System.IO.File.Exists(filePath))
                    return await File.ReadAllBytesAsync(filePath);               
                else
                    throw ErrorHelper.Except("APP_0030");
            }
            catch (Exception ex)
            {
                throw ErrorHelper.Except(ex, "APP_0030");
            }
        }

        #endregion      

        #region MailClient

        public bool SendMail(IGenericEmail mailRequest)
        {
            try
            {
                using var mailClient = new MailClient(new MailSettings(ConfigurationHelper.From,
                                                                        ConfigurationHelper.SmtpServer,
                                                                        ConfigurationHelper.Port,
                                                                        ConfigurationHelper.Username,
                                                                        ConfigurationHelper.Password));                                                   
                mailClient.SendEmailAsync(mailRequest).Wait();   

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }      

        #endregion

        #region Common Validations

        public void NullCheck(object obj, string errorCode)
        {
            if (obj is null)
                ErrorHelper.Except(errorCode);
        }

        #endregion

        #region Response Mapping Helpers        

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return (TDestination)Mapper.Map(source, (TDestination)Activator.CreateInstance(typeof(TDestination)),
                                typeof(TSource), typeof(TDestination));
        }

        public IEnumerable<TDestination> MapList<TSource, TDestination>(IEnumerable<TSource> sourceList)
        {
            var mappedList = new List<TDestination>();

            foreach (var source in sourceList)            
                mappedList.Add(Map<TSource, TDestination>(source));           

            return mappedList;
        }

        #endregion

        #region Utils

        public Task<IEnumerable<SelectionModel>> GetConstantType<T>()
            => Task.FromResult(typeof(T).GetOptionsOf());   

        #endregion
    }
}
