﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class OwnerService : IOwnerService
    {
        private readonly IBaseService _base;
        private readonly IUserService _userService;

        private readonly IOwnerRepository _ownerRepository;       

        public OwnerService(IBaseService baseService,
                            IUserService userService,
                            IOwnerRepository ownerRepository)
        {
            _base = baseService;
            _ownerRepository = ownerRepository;
            _userService = userService;
        }

        public Task<OwnerModel> GetById(long id)
        {
            var owner = _ownerRepository.GetOwnerById(id);

            _base.NullCheck(owner, "APP_0013");

            return Task.FromResult(_base.Map<Owner, OwnerModel>(owner));
        }

        public Task<IEnumerable<OwnerModel>> GetAllActiveOwners()
        {
            var owners = _ownerRepository.GetAllActiveOwners();            

            return Task.FromResult(_base.MapList<Owner, OwnerModel>(owners));
        }

        public Task<IEnumerable<OwnerModel>> GetAllOwners()
        {
            var owners = _ownerRepository.GetAllOwners();

            return Task.FromResult(_base.MapList<Owner, OwnerModel>(owners));
        }        
        
        public async Task<OwnerModel> CreateOwner(OwnerModel requestModel)
        {
            var user = await _userService.CreateUser(new UserModel(requestModel.Email, requestModel.Password));

            _base.NullCheck(user, "APP_0009");

            var ownerCreated = await _ownerRepository.Create(new Owner(_base.Map<UserModel, User>(user),
                                                                       requestModel.Profile,
                                                                       requestModel.Name));
            SendNewOwnerMail(requestModel);
            return _base.Map<Owner, OwnerModel>(ownerCreated);
        }

        public async Task<OwnerModel> UpdateOwner(OwnerModel requestModel)
        {
            var ownerToUpdate = _ownerRepository.GetOwnerById(requestModel.Id.GetValueOrDefault());

            _base.NullCheck(ownerToUpdate, "APP_0013");

            var user = await _userService.UpdateUser(new UserModel(requestModel.UserId.Value, requestModel.Email, requestModel.Password));

            _base.NullCheck(user, "APP_0009");

            ownerToUpdate.Update(requestModel.Name, requestModel.Profile, requestModel.Active);            

            var ownerUpdated = await _ownerRepository.Update(ownerToUpdate);

            return _base.Map<Owner, OwnerModel>(ownerUpdated);
        }

        public async Task<OwnerModel> UpdateOwnerPassword(OwnerModel requestModel)
        {
            var user = await _userService.UpdateUserPassword(new UserModel { Email = requestModel.Email, Password = requestModel.Password});
            _base.NullCheck(user, "APP_0009");

            return requestModel;
        }

        public async Task<bool> IsBlock(long id)
        {
            var owner = _ownerRepository.GetOwnerById(id);

            _base.NullCheck(owner, "APP_0013");

            return owner.User.Blocked;
        }

        private void SendNewOwnerMail(OwnerModel owner)
        {
            var email = owner.Email ?? owner.Email ?? string.Empty;
            _base.SendMail(new NewOwnerMail(email, owner.Name, owner.Password));
        }
    }
}
