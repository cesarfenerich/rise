﻿using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Domain.Entities;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces;
using Domain.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class InterviewService : IInterviewService
    {
        private readonly IBaseService _base;
        private readonly ICandidateService _candidateService;
        private readonly IOwnerService _ownerService;
        private readonly IInterviewRepository _interviewRepository;      
        private readonly IInterviewHistoryRepository _interviewHistoryRepository;
        private readonly IInterviewUpdateRepository _interviewUpdateRepository;       

        public InterviewService(IBaseService baseService, 
                                ICandidateService candidateService,
                                IOwnerService ownerService,
                                IInterviewRepository interviewRepository,                                                          
                                IInterviewHistoryRepository interviewHistoryRepository,
                                IInterviewUpdateRepository interviewUpdateRepository)
        {
            _base = baseService;
            _candidateService = candidateService;            
            _ownerService = ownerService;
            _interviewRepository = interviewRepository;
            _interviewHistoryRepository = interviewHistoryRepository;
            _interviewUpdateRepository = interviewUpdateRepository;            
        }

        public Task<InterviewModel> GetById(long interviewId)
        {
            var interview = _interviewRepository.GetInterviewById(interviewId);

            _base.NullCheck(interview, "APP_0012");

            return Task.FromResult(_base.Map<Interview, InterviewModel>(interview));
        }

        public async Task<IEnumerable<InterviewGridModel>> GetMostRecentInterviews()
        {
            var lastInterviews = _interviewRepository.GetLastNotArchivedInterview();

            var candidateIds = lastInterviews.Select(x => x.CandidateId).Distinct();
            var selectedSkills = await _candidateService.GetTechSkillsByCandidates(candidateIds);

            foreach (var interview in lastInterviews)
            {
                var skills = selectedSkills.Where(x => x.CandidateId.Equals(interview.CandidateId));
                interview.Candidate.SetCandidateTechSkills(skills.Select(x => new TechSkillModel
                {
                    Id = x.TechSkill.Id,
                    Category = x.TechSkill.Category,
                    Description = x.TechSkill.Description
                }));
            }

            return _base.MapList<Interview, InterviewGridModel>(lastInterviews);
        }

        public Task<SelectionModel> GetInterviewsCount()
        {
            var count = _interviewRepository.GetInterviewsCount();

            return Task.FromResult(new SelectionModel(null, count.ToString()));
        }

        public Task<IEnumerable<SelectionModel>> GetContractTypes()
            => Task.FromResult(typeof(ContractTypes).GetOptionsOf());

        public Task<IEnumerable<SelectionModel>> GetStatus()
            => Task.FromResult(typeof(InterviewStatus).GetOptionsOf());

        public Task<IEnumerable<SelectionModel>> GetExpectedSalariesRange()
            => Task.FromResult(typeof(SalaryExpectations).GetOptionsOf());        

        public async Task<InterviewModel> CreateInterview(InterviewModel model)
        {
            var candidate = await _candidateService.GetById(model.CandidateId.GetValueOrDefault(0));
            var owner = await _ownerService.GetById(model.OwnerId.GetValueOrDefault(0));

            _base.NullCheck(owner, "APP_0020");

            var interviewCreated = await _interviewRepository.Create(new Interview(model,
                                                                                   _base.Map<CandidateModel, Candidate>(candidate),
                                                                                   _base.Map<OwnerModel, Owner>(owner)));
            return _base.Map<Interview, InterviewModel>(interviewCreated);
        }    

        public async Task<InterviewModel> UpdateInterview(InterviewModel model, User loggedUser)
        {           
            var interviewToUpdate = _interviewRepository.GetById(model.Id.GetValueOrDefault());

            _base.NullCheck(interviewToUpdate, "APP_0012");            

            interviewToUpdate.CreateInterviewHistory(model, 
                                                     out ICollection<InterviewHistory> interviewHistory);
            interviewToUpdate.Update(model);

            await _interviewRepository.Update(interviewToUpdate);
            await _interviewHistoryRepository.CreateMany(interviewHistory);

            return _base.Map<Interview, InterviewModel>(interviewToUpdate);
        }

        public async Task<List<BaseSearchModel>> SearchInterviewByFields(string search)
        {
            //TODO: Criar methods no repositorio de entrevistas para cada field a ser incluido na pesquisa
            //e conforme encontrar criar um novo item de BaseSearchModel e o associa a lista de retorno

            List<BaseSearchModel> result = new List<BaseSearchModel>();
            var candidateNames = await _candidateService.SearchByCandidateNames(search);
            var candidateEmails = await _candidateService.SearchByCandidateEmails(search);
            var candidateJobRoles = await _candidateService.SearchByCandidateJobRoles(search);

            result.AddRange(candidateNames);
            result.AddRange(candidateJobRoles);
            result.AddRange(candidateEmails);

            return result;
        }        

        public async Task<IEnumerable<InterviewGridModel>> QuickSearchInterviews(string field, string search)
        {
            List<Interview> result = new List<Interview>();

            if (field.Contains("Nome"))
                result.AddRange(_interviewRepository.SearchByCandidateName(search));
            else if (field.Contains("Email"))
                result.AddRange(_interviewRepository.SearchByCandidateEmail(search));
            else if (field.Contains("JobRole"))
                result.AddRange(_interviewRepository.SearchByCandidateJobRole(search));

            result = RemoveArchivedItems(result.AsEnumerable<Interview>()).ToList();

            var candidateIds = result
                                .Select(x => x.CandidateId).Distinct();
            var selectedSkills = await _candidateService.GetTechSkillsByCandidates(candidateIds);

            foreach (var interview in result)
            {
                var skills = selectedSkills.Where(x => x.CandidateId.Equals(interview.CandidateId));                
                interview.Candidate.SetCandidateTechSkills(skills.Select(x => new TechSkillModel { Id = x.TechSkill.Id, 
                                                                                                   Category = x.TechSkill.Category,
                                                                                                   Description = x.TechSkill.Description}));
            }

            result = result.AsQueryable().DistinctBy(x => x.Id).ToList();
            result = result.OrderByDescending(x => x.CreatedAt).ToList();

            return _base.MapList<Interview,InterviewGridModel>(result.Distinct());
        }

        //Sobrescrita de referência.
        private IEnumerable<Interview> RemoveArchivedItems(IEnumerable<Interview> result)
        {
            return result.Where(x => !x.Archived);
        }

        public async Task<IEnumerable<InterviewGridModel>> AdvancedSearchInterviews(AdvancedSearchModel model)
        {
            DoSearchByFieldMatchingCondition(model, out IEnumerable<Interview> result);

            result = RemoveArchivedItems(result);

            var candidateIds = result.Select(x => x.CandidateId).Distinct();
            var selectedSkills = await _candidateService.GetTechSkillsByCandidates(candidateIds);

            var candidateWithAllTechSkills = new List<long>();
            foreach (var interview in result)
            {
                var skills = selectedSkills.Where(x => x.CandidateId.Equals(interview.CandidateId));
                interview.Candidate.SetCandidateTechSkills(skills.Select(x => new TechSkillModel
                {
                    Id = x.TechSkill.Id,
                    Category = x.TechSkill.Category,
                    Description = x.TechSkill.Description
                }));

                if (interview.Candidate.MatchAllTechSkills(model.TechSkills))
                    candidateWithAllTechSkills.Add(interview.CandidateId);
            }

            if (model.MatchFields)
                result = result.Where(x => candidateWithAllTechSkills.Contains(x.CandidateId));

            result = result.AsQueryable().DistinctBy(x => x.Id).ToList();
            result = result.OrderByDescending(x => x.CreatedAt).ToList();

            return _base.MapList<Interview, InterviewGridModel>(result.Distinct());
        }

        private void DoSearchByFieldMatchingCondition(AdvancedSearchModel model, out IEnumerable<Interview> result)
        {
            result = new List<Interview>();

            if (!model.Name.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByCandidateName(model.Name), ref result);           
                         
            if (!model.Availability.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByCandidateAvaliability(model.Availability), ref result);    

            if (!model.ExpectedSalary.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByExpectedSalary(model.ExpectedSalary), ref result);              

            if (!model.YearsOfExperience.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByYearOfExperience(model.YearsOfExperience), ref result);              

            if (!model.ContractType.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByContractType(model.ContractType), ref result);               

            if (model.OwnerId.HasValue && model.OwnerId.Value > 0)
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByOwner(model.OwnerId.Value), ref result);                

            if (!model.Linkdin.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByCandidateLinkdin(model.Linkdin), ref result);          

            if (!model.Location.IsNullOrWhitespaceOrEmpty())
                AggregateSearchResult(model.MatchFields, _interviewRepository.SearchByCandidateLocation(model.Location), ref result);        
            
            if (model.TechSkills?.Count() > 0)
                AggregateSearchResult(model.MatchFields, SearchByCandidateTechSkills(model.TechSkills).Result, ref result);
            
            if (model.Languages?.Count() > 0)
                AggregateSearchResult(model.MatchFields, SearchByCandidatesLanguages(model.Languages).Result, ref result);
            
        }

        private void AggregateSearchResult(bool matchFields, IEnumerable<Interview> search, ref IEnumerable<Interview> result)
        {
            if (result.Count() == 0 || !matchFields)
                result = result.Concat(search);
            else
                result = result.Where(x => search.Select(y => y.Id).Contains(x.Id)).ToList();
        }

        private async Task<IEnumerable<Interview>> SearchByCandidateTechSkills(IEnumerable<long> techSkillIds)
        {
            var candidateIdsWithSkills = await _candidateService.GetCandidatesBySkills(techSkillIds);

            return _interviewRepository.GetInterviewsByCandidates(candidateIdsWithSkills);
        }      

        public async Task<InterviewInfoModel> GetInfosByInterview(long interviewId)
        {
            var interview = _interviewRepository.GetInterviewById(interviewId);

            _base.NullCheck(interview, "APP_0012");

            var candidateLanguages = await _candidateService.GetLanguagesByCandidate(interview.CandidateId);
            var candidateTechSkills = await _candidateService.GetTechSkillsByCandidate(interview.CandidateId);
            var candidateLastInterviews = _interviewRepository.GetLastInterviewsByCandidate(interview.CandidateId)
                                                              .OrderBy(x => x.CreatedAt)
                                                              .ToList();

            IList<NoteModel> experiences = new List<NoteModel>(), 
                             notes = new List<NoteModel>();            

            foreach (var inter in candidateLastInterviews)
            {
                var experienceDescription = inter.ProfessionalExperience.IsNullOrWhitespaceOrEmpty() ? "Nenhuma informação registada." : inter.ProfessionalExperience;
                var notesDescription = inter.Notes.IsNullOrWhitespaceOrEmpty() ? "Nenhuma informação registada." : inter.Notes;
                var number = candidateLastInterviews.FindIndex(x => x.Id.Equals(inter.Id)) + 1;

                experiences.Add(new NoteModel(experienceDescription, number, inter.Owner.Name, inter.CreatedAt));
                notes.Add(new NoteModel(notesDescription, number, inter.Owner.Name, inter.CreatedAt));                
            }          

            return new InterviewInfoModel(interview.Id,
                                          interview.OwnerId,
                                          interview.Status,
                                          interview.CandidateId,
                                          interview.Candidate.Name,
                                          interview.Candidate.JobRole?.JobTitle ?? "Nenhum",
                                          interview.Candidate.AvailabilityDescription,
                                          interview.Candidate.Interval,
                                          interview.Candidate.AvailableAt,
                                          interview.Candidate.TypeDescription,
                                          interview.Candidate.Email,
                                          interview.Candidate.BornDate,
                                          interview.Candidate.PhoneNumber,
                                          interview.Candidate.Citizenship,
                                          interview.Candidate.MaritalStatusDescription,
                                          interview.Candidate.Location?.City ?? "Nenhuma",
                                          interview.Candidate.LiteraryAbilityDescription,
                                          interview.Candidate.College,
                                          interview.Candidate.Certifications,
                                          interview.ContractTypeDescription,
                                          interview.OngoingProcesses,
                                          interview.CurrentSalary,
                                          interview.IsCurrentSalaryNet,
                                          interview.ExpectationSalary,   
                                          interview.IsExpectationSalaryNet,
                                          interview.ReasonForLeaving,
                                          interview.Candidate.DependentsUnder7.GetValueOrDefault(0),
                                          interview.Candidate.DependentsAbove7.GetValueOrDefault(0),
                                          interview.Candidate.YearsOfExperience.GetValueOrDefault(0),
                                          candidateLanguages,
                                          candidateTechSkills,
                                          experiences,
                                          notes,
                                          interview.Candidate.LinkedInProfileLink ?? "Nenhum",
                                          interview.IsCurrentMonthlyPayment,
                                          interview.IsExpectationMonthlyPayment);
        }

        public Task<IEnumerable<InterviewUpdateModel>> GetLastUpdatesByInterview(long interviewId)
        {
            var interview = _interviewRepository.GetById(interviewId);

            _base.NullCheck(interview, "APP_0012");

            var updates = _interviewUpdateRepository.GetUpdatesByInterview(interview.Id);

            return Task.FromResult(_base.MapList<InterviewUpdate, InterviewUpdateModel>(updates));
        }

        public async Task<InterviewUpdateModel> CreateInterviewUpdate(InterviewUpdateModel model)
        {
            var interview = _interviewRepository.GetById(model.InterviewId);            

            _base.NullCheck(interview, "APP_0012");

            var owner = await _ownerService.GetById(model.OwnerId);

            _base.NullCheck(owner, "APP_0013");

            var updateCreated = await _interviewUpdateRepository.Create(new InterviewUpdate(interview, 
                                                                                            _base.Map<OwnerModel, Owner>(owner), 
                                                                                            model.Resume));                
            return _base.Map<InterviewUpdate, InterviewUpdateModel>(updateCreated);
        }

        public async Task<InterviewUpdateModel> UpdateInterviewUpdate(InterviewUpdateModel model)
        {
            var updateToUpdate = _interviewUpdateRepository.GetById(model.Id.GetValueOrDefault());

            _base.NullCheck(updateToUpdate, "APP_0027");

            var owner = await _ownerService.GetById(model.OwnerId);

            _base.NullCheck(owner, "APP_0013");

            updateToUpdate.Update(_base.Map<OwnerModel, Owner>(owner), model.Resume);

            //TODO: Pensar em criar histórico de alteração entrevista aqui            

            await _interviewUpdateRepository.Update(updateToUpdate);            

            return _base.Map<InterviewUpdate, InterviewUpdateModel>(updateToUpdate);
        }

        public async Task DeleteInterviewUpdate(long updateId)
        {
            var update = _interviewUpdateRepository.GetById(updateId);

            _base.NullCheck(update, "APP_0027");

            //TODO: Pensar em criar histórico de alteração entrevista aqui            

            await _interviewUpdateRepository.Delete(updateId);            
        }

        public Task<IEnumerable<SelectionModel>> GetYearsOfExperienceRange()
         => Task.FromResult(typeof(YearsOfExperienceRange).GetOptionsOf());

        private async Task<IEnumerable<Interview>> SearchByCandidatesLanguages(IEnumerable<long> languages)
        {
            var candidatesId = await _candidateService.GetCandidateByLanguages(languages);
            return _interviewRepository.GetInterviewsByCandidates(candidatesId);
        }     

        public async Task ArchiveInterview(InterviewArchiveModel model)
        {           
            var interview = _interviewRepository.GetInterviewById(model.InterviewId);

            _base.NullCheck(interview, "APP_0041");

            interview.Archive(model.ArchivedReason);

            await _interviewRepository.Update(interview);    
        }
    }
}
