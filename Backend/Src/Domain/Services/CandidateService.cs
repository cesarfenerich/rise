﻿using Common.Helpers;
using Common.Models;
using Domain.Entities;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces;
using Domain.Entities.Interfaces.Repositories;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class CandidateService : ICandidateService
    {
        readonly IBaseService _base;
        readonly IOwnerService _ownerService;

        readonly ICandidateRepository _candidateRepository;
        readonly ICandidateLanguageRepository _candidateLanguageRepository;
        readonly ICandidateTechSkillRepository _candidateTechSkillRepository;        
        readonly ICandidateUploadRepository _candidateUploadRepository;
        readonly ICandidateNaifasRepository _candidateNaifasRepository;
        readonly ILanguageRepository _languageRepository;       
        readonly ILocationRepository _locationRepository;
        readonly IJobRoleRepository _jobRoleRepository;
        readonly ITechSkillRepository _techSkillRepository;
        readonly IUploadRepository _uploadRepository;
        readonly INaifasRepository _naifasRepository;

        public CandidateService(IBaseService baseService, IOwnerService ownerService,
                                                          ICandidateRepository candidateRepository,
                                                          ICandidateLanguageRepository candidateLanguageRepository,
                                                          ICandidateTechSkillRepository candidateTechSkillRepository,
                                                          ICandidateUploadRepository candidateUploadRepository,
                                                          ICandidateNaifasRepository candidateNaifasRepository,
                                                          ILanguageRepository languageRepository,
                                                          ILocationRepository locationRepository,
                                                          IJobRoleRepository jobRoleRepository,
                                                          ITechSkillRepository techSkillRepository,
                                                          IUploadRepository uploadRepository,
                                                          INaifasRepository naifasRepository)
        {
            _base = baseService;
            _ownerService = ownerService;
            _candidateRepository = candidateRepository;
            _candidateLanguageRepository = candidateLanguageRepository;
            _candidateTechSkillRepository = candidateTechSkillRepository;
            _candidateUploadRepository = candidateUploadRepository;
            _candidateNaifasRepository = candidateNaifasRepository;
            _languageRepository = languageRepository;
            _locationRepository = locationRepository;
            _jobRoleRepository = jobRoleRepository;
            _techSkillRepository = techSkillRepository;
            _uploadRepository = uploadRepository;
            _naifasRepository = naifasRepository;
        }      

        #region CRUD
        public Task<CandidateModel> GetById(long candidateId)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            return Task.FromResult(_base.Map<Candidate, CandidateModel>(candidate));
        }
        public async Task<CandidateModel> CreateCandidate(CandidateModel model)
        {
            JobRole jobRole = null;
            if (model.JobRoleId.HasValue)
            {
                jobRole = _jobRoleRepository.GetById(model.JobRoleId.GetValueOrDefault(0));

                _base.NullCheck(jobRole, "APP_0016");
            }

            Location location = null;
            if (model.LocationId.HasValue)
            {
                location = _locationRepository.GetById(model.LocationId.GetValueOrDefault(0));

                _base.NullCheck(location, "APP_0015");
            }

            var candidateCreated = await _candidateRepository.Create(new Candidate(model,
                                                                                   jobRole,
                                                                                   location));
            return _base.Map<Candidate, CandidateModel>(candidateCreated);
        }
        public async Task<CandidateModel> UpdateCandidate(CandidateModel model)
        {
            var candidateToUpdate = _candidateRepository.GetById(model.Id.GetValueOrDefault());

            _base.NullCheck(candidateToUpdate, "APP_0011");

            JobRole jobRole = null;
            if (model.JobRoleId.HasValue)
            {
                jobRole = _jobRoleRepository.GetById(model.JobRoleId.GetValueOrDefault(0));

                _base.NullCheck(jobRole, "APP_0016");
            }

            Location location = null;
            if (model.LocationId.HasValue)
            {
                location = _locationRepository.GetById(model.LocationId.GetValueOrDefault(0));

                _base.NullCheck(location, "APP_0015");
            }

            candidateToUpdate.Update(model,
                                     jobRole,
                                     location);

            var candidateUpdated = await _candidateRepository.Update(candidateToUpdate);

            return _base.Map<Candidate, CandidateModel>(candidateUpdated);
        }
        #endregion 

        #region Files       

        public async Task<UploadModel> UploadFile(IFormFile file)
        {        
            string ext = Path.GetExtension(file.FileName);
            string key = $"{Guid.NewGuid()}{ext}";

            try
            {
                _base.UploadFile(file, key);                
                return _base.Map<Upload, UploadModel>(await _uploadRepository.Create(new Upload(file.FileName, key)));              
            }
            catch (Exception ex)
            {
                throw ErrorHelper.Except(ex, "APP_0029");               
            }            
        }

        public async Task<UploadModel> CreateNaifas(NaifasUploadModel naifasUpload)
        {
            var createdUpload = await _uploadRepository.Create(new Upload(naifasUpload.FileName, naifasUpload.FilePath));

            return _base.Map<Upload, UploadModel>(createdUpload);
        }

        public async Task<Tuple<string, byte[]>> DownloadFile(long uploadId)
        {
            var uploadToDownload = _uploadRepository.GetById(uploadId);

            _base.NullCheck(uploadToDownload, "APP_0028");

            return new Tuple<string, byte[]>(uploadToDownload.FileName, await _base.DownloadFile(uploadToDownload.FilePath));           
        }        

        #endregion

        #region CandidateRelations     
        public Task<IEnumerable<SelectionModel>> GetCandidateTypes()
            => _base.GetConstantType<CandidateTypes>();

        public Task<IEnumerable<SelectionModel>> GetAllLanguages()
        {
            var languages = _languageRepository.GetAllDistinctLanguages();

            return Task.FromResult(_base.MapList<Language, SelectionModel>(languages));
        }

        public Task<IEnumerable<SelectionModel>> GetCandidateAvailability()
             => _base.GetConstantType<CandidateAvailability>();

        public Task<IEnumerable<SelectionModel>> GetCandidateInterval()
             => _base.GetConstantType<CandidateInterval>();

        public Task<IEnumerable<SelectionModel>> GetCandidateMobility()
             => _base.GetConstantType<CandidateMobility>();

        public Task<IEnumerable<SelectionModel>> GetLiteraryAbilities()
            => _base.GetConstantType<LiteraryAbilities>();

        public Task<IEnumerable<SelectionModel>> GetMaritalStatuses()
            => _base.GetConstantType<MaritalStatus>();

        public Task<IEnumerable<SelectionModel>> GetAllTechSkillCategories()
            => _base.GetConstantType<TechSkillCategories>();

        public Task<IEnumerable<SelectionModel>> GetAllLocations()
        {
            var locations = _locationRepository.GetAllDistinctLocations();

            return Task.FromResult(_base.MapList<Location, SelectionModel>(locations));
        }

        public Task<IEnumerable<SelectionModel>> GetAllJobRoles()
        {
            var jobRoles = _jobRoleRepository.GetAllDistinctJobRoles();

            return Task.FromResult(_base.MapList<JobRole, SelectionModel>(jobRoles));
        }

        public Task<IEnumerable<TechSkillModel>> GetAllTechSkills()
        {
            var techSkills = _techSkillRepository.GetAllDistinctTechSkills();

            return Task.FromResult(_base.MapList<TechSkill, TechSkillModel>(techSkills));
        }   
        
        public async Task<IEnumerable<InterviewGridModel>> GetLastInterviewsByCandidate(long candidateId)
        {
            var candidate = _candidateRepository.GetById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var lastInterviews = _candidateRepository.GetInterviewsByCandidate(candidateId)
                                                     .OrderBy(x => x.CreatedAt)
                                                     .AsEnumerable();
            foreach (var inter in lastInterviews)
            {
                var owner = await _ownerService.GetById(inter.OwnerId);
                inter.SetOwner(_base.Map<OwnerModel, Owner>(owner));

                var candidateSkills = await GetTechSkillsByCandidate(inter.CandidateId);
                inter.Candidate.SetCandidateTechSkills(candidateSkills);
            }

            return _base.MapList<Interview, InterviewGridModel>(lastInterviews);
        }       

        public Task<IList<BaseSearchModel>> SearchByCandidateNames(string search)
        {
            var candidateNames = _candidateRepository.SearchByName(search);

            IList<BaseSearchModel> results = new List<BaseSearchModel>();

            foreach (var name in candidateNames)
            {
                results.Add(new BaseSearchModel("Name", name));
            }

            return Task.FromResult(results);
        }

        public Task<IList<BaseSearchModel>> SearchByCandidateEmails(string search)
        {
            var candidateNames = _candidateRepository.SearchByEmail(search);

            IList<BaseSearchModel> results = new List<BaseSearchModel>();

            foreach (var name in candidateNames)
            {
                results.Add(new BaseSearchModel("Email", name));
            }

            return Task.FromResult(results);
        }

        public Task<IList<BaseSearchModel>> SearchByCandidateJobRoles(string search)
        {
            var jobRoles = _jobRoleRepository.SearchByJobTitle(search);

            IList<BaseSearchModel> results = new List<BaseSearchModel>();
            foreach (var jobRole in jobRoles)
            {
                results.Add(new BaseSearchModel("JobRole", jobRole));
            }

            return Task.FromResult(results);
        }      

        public Task<IEnumerable<long>> GetCandidatesBySkills(IEnumerable<long> techSkills)
        {
            var candidatesTechSkills = _candidateTechSkillRepository.GetByTechSkillIds(techSkills);

            return Task.FromResult(candidatesTechSkills.Select(x => x.CandidateId).Distinct());
        }

        public Task<CandidateModel> GetCandidateByReference(CandidateSearchModel requestModel)
        {
            var candidate = _candidateRepository.GetByNameAndEmail(requestModel.Name, requestModel.Email);

            //_base.NullCheck(candidate, "APP_0011");

            return Task.FromResult(_base.Map<Candidate, CandidateModel>(candidate));
        }

        public async Task<SelectionModel> CreateJobRole(SelectionModel requestModel)
        {
            JobRole jobRole;
            if (requestModel.Id == 0)
            {
                jobRole = _jobRoleRepository.GetByJobTitle(requestModel.Value);

                if (jobRole is null)
                    jobRole = await _jobRoleRepository.Create(new JobRole(requestModel.Value));
            }
            else
            {
                jobRole = _jobRoleRepository.GetById(requestModel.Id.Value);

                _base.NullCheck(jobRole, "APP_0016");
            }

            return _base.Map<JobRole, SelectionModel>(jobRole);
        }

        public async Task<SelectionModel> CreateLocation(SelectionModel requestModel)
        {
            Location location;
            if (requestModel.Id == 0)
            {
                location = _locationRepository.GetByCity(requestModel.Value);

                if (location is null)
                    location = await _locationRepository.Create(new Location(requestModel.Value));
            }
            else
            {
                location = _locationRepository.GetById(requestModel.Id.Value);

                _base.NullCheck(location, "APP_0015");
            }

            return _base.Map<Location, SelectionModel>(location);
        }

        public async Task<SelectionModel> CreateLanguage(SelectionModel requestModel)
        {
            Language language;
            if (requestModel.Id == 0)
            {
                language = _languageRepository.GetByName(requestModel.Value);

                if (language is null)
                    language = await _languageRepository.Create(new Language(requestModel.Value));
            }
            else
            {
                language = _languageRepository.GetById(requestModel.Id.Value);

                _base.NullCheck(language, "APP_0014");
            }

            return _base.Map<Language, SelectionModel>(language);
        }       

        public async Task<TechSkillModel> CreateTechSkill(TechSkillModel requestModel)
        {            
            TechSkill techSkill;
            if (requestModel.Id == 0)
            {
                techSkill = _techSkillRepository.GetByCategoryAndDescription(requestModel.Category, requestModel.Description);

                if (techSkill is null)
                    techSkill = await _techSkillRepository.Create(new TechSkill(0, requestModel.Category, requestModel.Description));
            }
            else
            {
                techSkill = _techSkillRepository.GetById(requestModel.Id);

                _base.NullCheck(techSkill, "APP_0017");
            }

            return _base.Map<TechSkill, TechSkillModel>(techSkill);
        }      

        public async Task<SelectionModel> CreateCandidateLanguage(long candidateId, long idLanguage)
        {            
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var language = _languageRepository.GetById(idLanguage);

            _base.NullCheck(language, "APP_0014");

            CandidateLanguage candidateLanguage = _candidateLanguageRepository.GetByCandidateAndLanguage(candidateId, idLanguage);

            if (candidateLanguage is null)
               await _candidateLanguageRepository.Create(new CandidateLanguage(candidate, language));

            return _base.Map<Language, SelectionModel>(language);
        }

        public async Task<TechSkillModel> CreateCandidateTechSkill(long candidateId, long idTechSkill)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var techSkill = _techSkillRepository.GetById(idTechSkill);

            _base.NullCheck(techSkill, "APP_0017");

            CandidateTechSkill candidateTechSkill = _candidateTechSkillRepository.GetByCandidateAndTechSkill(candidateId, idTechSkill);

            if (candidateTechSkill is null)
                await _candidateTechSkillRepository.Create(new CandidateTechSkill(candidate, techSkill));

            return _base.Map<TechSkill, TechSkillModel>(techSkill);
        }             

        public async Task<UploadModel> CreateCandidateUpload(long candidateId, long idUpload)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var upload = _uploadRepository.GetById(idUpload);

            _base.NullCheck(upload, "APP_0019");

            CandidateUpload candidateUpload = _candidateUploadRepository.GetByCandidateAndUpload(candidateId, idUpload);

            if (candidateUpload is null)
                await _candidateUploadRepository.Create(new CandidateUpload(candidate, upload));

            return _base.Map<Upload, UploadModel>(upload);
        }

        public async Task<NaifasModel> CreateNaifas(NaifasModel requestModel)
        {
            Naifas naifas;
            if (requestModel.Id == 0)
            {
                naifas = await _naifasRepository.Create(new Naifas(0, requestModel.Question, requestModel.Answer));
            }
            else
            {
                naifas = _naifasRepository.GetById(requestModel.Id);

                _base.NullCheck(naifas, "APP_0035");
            }

            return _base.Map<Naifas, NaifasModel>(naifas);
        }

        public async Task<NaifasModel> UpdateNaifas(NaifasModel model)
        {
            var naifasToUpdate = _naifasRepository.GetById(model.Id);

            _base.NullCheck(naifasToUpdate, "APP_0035");

            naifasToUpdate.Update(model);

            var naifasUpdated = await _naifasRepository.Update(naifasToUpdate);

            return _base.Map<Naifas, NaifasModel>(naifasUpdated);
        }

        public async Task<NaifasModel> CreateCandidateNaifas(long candidateId, long idNaifas)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var naifas = _naifasRepository.GetById(idNaifas);

            _base.NullCheck(naifas, "APP_0035");

            CandidateNaifas candidateNaifas = _candidateNaifasRepository.GetByCandidateAndNaifas(candidateId, idNaifas);

            if (candidateNaifas is null)
                await _candidateNaifasRepository.Create(new CandidateNaifas(candidate, naifas));

            return _base.Map<Naifas, NaifasModel>(naifas);
        }

        public async Task DeleteCandidateLanguage(long candidateId, long idLanguage)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var language = _languageRepository.GetById(idLanguage);

            _base.NullCheck(language, "APP_0014");

            CandidateLanguage candidateLanguage = _candidateLanguageRepository.GetByCandidateAndLanguage(candidateId, idLanguage);

            if (candidateLanguage != null)
                await _candidateLanguageRepository.Delete(candidateLanguage.Id);
        }

        public async Task DeleteCandidateTechSkill(long candidateId, long idTechSkill)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var techSkill = _techSkillRepository.GetById(idTechSkill);

            _base.NullCheck(techSkill, "APP_0017");

            CandidateTechSkill candidateTechSkill = _candidateTechSkillRepository.GetByCandidateAndTechSkill(candidateId, idTechSkill);

            if (candidateTechSkill != null)
                await _candidateTechSkillRepository.Delete(candidateTechSkill.Id);           
        }

        public async Task DeleteCandidateNaifas(long candidateId, long idNaifas)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var naifas = _naifasRepository.GetById(idNaifas);

            _base.NullCheck(idNaifas, "APP_0035");

            CandidateNaifas candidateNaifas = _candidateNaifasRepository.GetByCandidateAndNaifas(candidateId, idNaifas);

            if (candidateNaifas != null)
                await _candidateNaifasRepository.Delete(candidateNaifas.Id);
        }

        public async Task DeleteCandidateUpload(long candidateId, long idUpload)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var upload = _uploadRepository.GetById(idUpload);

            _base.NullCheck(upload, "APP_0019");

            CandidateUpload candidateUpload = _candidateUploadRepository.GetByCandidateAndUpload(candidateId, idUpload);

            if (candidateUpload != null)
                await _candidateUploadRepository.Delete(candidateUpload.Id);            
        }

        public Task<IEnumerable<SelectionModel>> GetLanguagesByCandidate(long candidateId)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var languages = _candidateLanguageRepository.GetLanguagesByCandidate(candidate.Id);

            return Task.FromResult(_base.MapList<Language, SelectionModel>(languages.Select(x => x.Language)));
        }

        public Task<IEnumerable<TechSkillModel>> GetTechSkillsByCandidate(long candidateId, bool bypassCandidateValidation = false)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            if(!bypassCandidateValidation)
                _base.NullCheck(candidate, "APP_0011");

            var techSkills = _candidateTechSkillRepository.GetTechSkillsByCandidate(candidateId);

            return Task.FromResult(_base.MapList<TechSkill, TechSkillModel>(techSkills.Select(x => x.TechSkill)));
        }

        public Task<IEnumerable<CandidateTechSkill>> GetTechSkillsByCandidates(IEnumerable<long> candidateIds)
            => Task.FromResult(_candidateTechSkillRepository.GetTechSkillsByCandidates(candidateIds));

        public Task<IEnumerable<NaifasModel>> GetNaifasByCandidate(long candidateId, bool bypassCandidateValidation = false)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            if (!bypassCandidateValidation)
                _base.NullCheck(candidate, "APP_0011");

            var naifas = _candidateNaifasRepository.GetNaifasByCandidate(candidateId);

            return Task.FromResult(_base.MapList<Naifas, NaifasModel>(naifas.Select(x => x.Naifas)));
        }

        public Task<IEnumerable<UploadModel>> GetUploadsByCandidate(long candidateId)
        {
            var candidate = _candidateRepository.GetCandidateById(candidateId);

            _base.NullCheck(candidate, "APP_0011");

            var uploads = _candidateUploadRepository.GetUploadsByCandidate(candidate.Id);

            return Task.FromResult(_base.MapList<Upload, UploadModel>(uploads.Select(x => x.Upload)));
        }

        public Task<UploadModel> GetUploadsByFileName(string fileName)
        {
            var uploads = _uploadRepository.GetByFileName(fileName);
            var uploadIds = uploads.Select(x => x.Id);
            var uploadsWithCandidate = _candidateUploadRepository.GetByUploads(uploadIds).Select(x => x.Upload);
            var upload = uploads.Except(uploadsWithCandidate).FirstOrDefault();

            _base.NullCheck(upload, "APP_0019");         

            return Task.FromResult(_base.Map<Upload, UploadModel>(upload));
        }

        public async Task<SelectionModel> UpdateJobRole(SelectionModel requestModel)
        {
            var jobRoleToUpdate = _jobRoleRepository.GetById(requestModel.Id.GetValueOrDefault(0));

            _base.NullCheck(jobRoleToUpdate, "APP_0016");            

            jobRoleToUpdate.Update(requestModel.Value);

            var jobRoleUpdated = await _jobRoleRepository.Update(jobRoleToUpdate);

            return _base.Map<JobRole, SelectionModel>(jobRoleUpdated);
        }

        public async Task DeleteJobRole(long jobRoleId)
        {
            var jobRole = _jobRoleRepository.GetById(jobRoleId);

            _base.NullCheck(jobRole, "APP_0016");

            var candidatesWithJobRole = _candidateRepository.GetByJobRole(jobRoleId);
            
            //Remove o JobRole de todos os candidatos que o utilizam antes de excluí-lo
            if(candidatesWithJobRole.Count() > 0)
            {
                foreach (var candidate in candidatesWithJobRole)                
                    candidate.SetJobRole(null);

                await _candidateRepository.UpdateMany(candidatesWithJobRole);
            }
            
            await _jobRoleRepository.Delete(jobRoleId);            
        }

        public async Task<TechSkillModel> UpdateTechSkill(TechSkillModel requestModel)
        {
            var techSkillToUpdate = _techSkillRepository.GetById(requestModel.Id);

            _base.NullCheck(techSkillToUpdate, "APP_0017");

            techSkillToUpdate.Update(requestModel.Category, requestModel.Description);

            var techSkillUpdated = await _techSkillRepository.Update(techSkillToUpdate);

            return _base.Map<TechSkill, TechSkillModel>(techSkillUpdated);
        }

        public async Task DeleteTechSkill(long techSkillId)
        {
            var techSkill = _techSkillRepository.GetById(techSkillId);

            _base.NullCheck(techSkill, "APP_0017");

            var candidateTechSkills = _candidateTechSkillRepository.GetByTechSkill(techSkillId);

            if(candidateTechSkills.Count > 0)
                await _candidateTechSkillRepository.DeleteMany(candidateTechSkills);           

            await _techSkillRepository.Delete(techSkillId);
        }

        public Task<IEnumerable<long>> GetCandidateByLanguages(IEnumerable<long> languages)
        {
            var candidatesLanguages = _candidateLanguageRepository.GetCandidatesByLanguages(languages);
            return Task.FromResult(candidatesLanguages.Select(x => x.CandidateId).Distinct());
        }      

        #endregion
    }
}
