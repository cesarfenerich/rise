﻿using AutoMapper;
using Common.Models;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("location")]
    public class Location : ILocation
    {        
        #region Columns        

        [Required]
        [Column("id")]
        public long Id { get; set; }     

        [Required]
        [Column("city"), MaxLength(50)]
        public string City { get; private set; }

        #endregion

        #region DependencyProperties

        public virtual ICollection<Candidate> Candidates { get; private set; }

        #endregion

        #region Constructors

        public Location()
        { }

        public Location(string city)
        {
            this.City = city;
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Location, SelectionModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(ent => ent.Id))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(ent => ent.City));
        }

        #endregion
    }
}
