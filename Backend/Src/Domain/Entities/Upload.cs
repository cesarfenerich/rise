﻿using AutoMapper;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("upload")]
    public class Upload : IUpload
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }   

        [Required]
        [Column("file_name"), MaxLength(100)]
        public string FileName { get; private set; }

        [Required]
        [Column("file_path"), MaxLength(300)]
        public string FilePath { get; private set; }

        [Required]
        [Column("uploaded_at")]
        public DateTime UploadedAt { get; private set; }

        #endregion   

        #region Constructors

        public Upload()
        {  }

        public Upload(string fileName, string filePath)
        {            
            this.FileName = fileName;
            this.FilePath = filePath;
            this.UploadedAt = DateTime.UtcNow;
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
           
        }

        #endregion  
    }
}
