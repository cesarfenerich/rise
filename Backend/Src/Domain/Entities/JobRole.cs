﻿using AutoMapper;
using Common.Models;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("job_role")]
    public class JobRole : IJobRole
    {
        #region Columns 
        [Required]
        [Column("id")]
        public long Id { get; set; }       

        [Required]
        [Column("job_title"), MaxLength(150)]
        public string JobTitle { get; private set; }        

        #endregion

        #region DependencyProperties

        public virtual ICollection<Candidate> Candidates { get; private set; }

        #endregion

        #region Constructors

        public JobRole()
        { }

        public JobRole(string jobTitle)
        {
            this.JobTitle = jobTitle;
        }

        #endregion

        public void Update (string jobTitle)
        {
            this.JobTitle = jobTitle;
        }

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<JobRole, SelectionModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(ent => ent.Id))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(ent => ent.JobTitle));
        }        

        #endregion
    }
}
