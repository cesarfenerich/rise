﻿using AutoMapper;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("interview_update")]
    public class InterviewUpdate : IInterviewUpdate
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }       

        [Required]
        [Column("interview_id")]
        public long InterviewId { get; private set; }

        [Required]
        [Column("owner_id")]
        public long OwnerId { get; private set; }

        [Required]
        [Column("resume"),MaxLength(2000)]
        public string Resume { get; private set; }  

        [Required]
        [Column("created_at")]
        public DateTime CreatedAt { get; private set; }
        
        [Column("updated_at")]
        public DateTime? UpdatedAt { get; private set; }

        #endregion

        #region DependencyProperties

        public virtual Interview Interview { get; private set; }

        public virtual Owner Owner { get; private set; }

        #endregion

        #region Constructors

        public InterviewUpdate()
        { }

        public InterviewUpdate(Interview interview, Owner owner, string resume)
        {
            SetInterview(interview);
            SetOwner(owner);
            this.Resume = resume;
            this.CreatedAt = DateTime.UtcNow;
        }

        #endregion

        #region Context

        #region Dependency

        public static Expression<Func<InterviewUpdate, object>>[] Dependences = { x => x.Owner,
                                                                                  x => x.Interview,
                                                                                  x => x.Interview.Candidate};

        #endregion

        internal void Update(Owner owner, string resume)
        {
            SetOwner(owner);
            this.Resume = resume;
            this.UpdatedAt = DateTime.UtcNow;
        }
        
        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<InterviewUpdate, InterviewUpdateModel>()
            .ForMember(dest => dest.Owner, opt => opt.MapFrom(ent => ent.Owner.Name))
            .ReverseMap();
        }

        #endregion

        #region Setters        

        private void SetInterview(Interview interview)
        {
            this.Interview = interview;
            this.InterviewId = interview.Id;
        }

        private void SetOwner(Owner owner)
        {
            this.Owner = owner;
            this.OwnerId = owner.Id;
        }

        #endregion
    }
}
