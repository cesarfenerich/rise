﻿using Common.Extensions;
using Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Domain.Entities.ValueObjects
{
    public class JwtToken
    {
        public string AuthType = "bearer";
        readonly static JwtSecurityTokenHandler _tokenHandler = new JwtSecurityTokenHandler();

        private string _issuer;
        private string _audience;
        private ClaimsIdentity _identity;
        private SigningCredentials _signingCredentials;
        private DateTime _notBefore;
        private DateTime _expiration;
        private string _claimJti;
        private string _claimIat;
        private Claim[] _claims;

        private JwtSecurityToken _accessToken;

        public long UserId { get; private set; }
        public string AccessToken { get { return _tokenHandler.WriteToken(_accessToken); } }
        public Guid? RefreshToken { get; private set; }
        public DateTime Generated { get { return _accessToken.ValidFrom; } }
        public DateTime Expires { get { return _accessToken.ValidTo; } }

        public bool CacheRefreshToken { get; private set; }

        public JwtToken()
        { }

        public JwtToken(long userId,
                        string issuer,
                        string audience,
                        SigningCredentials signingCredentials,
                        DateTime notBefore,
                        DateTime expiration,
                        string claimJti,
                        string claimIat)
        {
            this.UserId = userId;
            SetRefreshToken(null);

            _issuer = issuer;
            _audience = audience;
            _signingCredentials = signingCredentials;
            _notBefore = notBefore;
            _expiration = expiration;
            _claimJti = claimJti;
            _claimIat = claimIat;

            GenerateAccessToken();
        }

        public JwtToken(long userId,
                        Guid refreshToken,
                        JwtSecurityToken accessToken,
                        string cachedAccessToken,
                        string issuer,
                        string audience,
                        SigningCredentials signingCredentials,
                        DateTime notBefore,
                        DateTime expiration,
                        string claimJti,
                        string claimIat)
        {
            this.UserId = userId;
            SetRefreshToken(refreshToken);
            _accessToken = accessToken;

            if (!CachedAccessTokenIsValid(cachedAccessToken, refreshToken))
                ErrorHelper.Except("APP_0010");                
            else
            {
                this.UserId = GetUserOwnerOfAuthToken(_accessToken).GetValueOrDefault();

                _issuer = issuer;
                _audience = audience;
                _signingCredentials = signingCredentials;
                _notBefore = notBefore;
                _expiration = expiration;
                _claimJti = claimJti;
                _claimIat = claimIat;

                GenerateAccessToken();
            }
        }

        private bool CachedAccessTokenIsValid(string cachedAccessToken,
                                              Guid refreshToken)
        {
            var _cachedAccessToken = _tokenHandler.ReadToken(cachedAccessToken) as JwtSecurityToken;

            if (!this.RefreshToken.Equals(refreshToken))
                return false;

            if (GetUserOwnerOfAuthToken(_accessToken) !=
                GetUserOwnerOfAuthToken(_cachedAccessToken))
                return false;

            return true;
        }
        private void GenerateAccessToken()
        {
            GenerateClaims();
            _accessToken = new JwtSecurityToken(_issuer,
                                                _audience,
                                                _claims,
                                                _notBefore,
                                                _expiration,
                                                _signingCredentials);
        }

        private void SetRefreshToken(Guid? refreshToken)
        {
            if (refreshToken.HasValue &&
                refreshToken.Value.IsValid())
                this.RefreshToken = refreshToken;
            else
            {
                this.RefreshToken = Guid.NewGuid();
                CacheRefreshToken = true;
            }
        }

        private void GenerateClaims()
        {
            _identity = new ClaimsIdentity(new GenericIdentity(this.UserId.ToString(), "Token"),
                                           new[] { new Claim("riseAppUser_", this.UserId.ToString()) });
            _claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, this.UserId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, _claimJti),
                new Claim(JwtRegisteredClaimNames.Iat, _claimIat, ClaimValueTypes.Integer64), _identity.FindFirst("riseAppUser_")
            };
        }

        private long? GetUserOwnerOfAuthToken(JwtSecurityToken _token)
        {
            if (_token is null)
                return null;
            else
            {
                var userId = _token.Claims.FirstOrDefault(t =>
                t.Type == "riseAppUser_");

                return Int64.Parse(userId.Value);
            }
        }


        
        private static SecurityToken ValidateToken(string authToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            
            var validationParameters = GetValidationParameters("2f6f311f-f64f-499b-9927-6dea981c953a"); //FIXME: COPIED PARAMETER FROM APPSETTINGS.

            SecurityToken validatedToken;
            IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
            return validatedToken;
        }

        private static TokenValidationParameters GetValidationParameters(string key)
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = true, 
                ValidateAudience = true, 
                ValidateIssuer = true,   
                ValidIssuer = "Sample", //TO BE DEFINED
                ValidAudience = "Sample", //TO BE DEFINED
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)) 
            };
        }
    }
}
