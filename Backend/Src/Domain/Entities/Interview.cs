﻿using AutoMapper;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("interview")]
    public class Interview :IInterview
    {
        #region Columns 

        [Required]

        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("candidate_id")]
        public long CandidateId { get; private set; }

        [Required]
        [Column("owner_id")]
        public long OwnerId { get; private set; }

        [Required]
        [Column("status"), MaxLength(50)]
        public string Status { get; private set; } //TODO: Persist only values of enum InterviewStatus
       
        [Column("contract_type"), MaxLength(50)]
        public string ContractType { get; private set; }

        [Column("reason_leaving"),MaxLength(500)]
        public string ReasonForLeaving { get; private set; }

        [Column("ongoing_processes"), MaxLength(10000)]
        public string OngoingProcesses { get; private set; }

        [Column("current_salary")]
        public decimal? CurrentSalary { get; private set; }

        [Column("current_salary_net")]
        public bool? IsCurrentSalaryNet { get; private set; }

        [Column("expectation_salary")]
        public decimal? ExpectationSalary { get; private set; }

        [Column("expectation_salary_net")]
        public bool? IsExpectationSalaryNet { get; private set; }

        [Column("professional_experience"), MaxLength(20000)]
        public string ProfessionalExperience { get; private set; }

        [Column("notes"), MaxLength(20000)]
        public string Notes { get; private set; }

        [Required]
        [Column("created_at")]
        public DateTime CreatedAt { get; private set; }

        [Required]
        [Column("active")]
        public bool Active { get; private set; }       

        [Column("expectation_payment_monthly", TypeName = "BOOLEAN")]
        public bool? IsExpectationMonthlyPayment { get; set; }

        [Column("current_payment_monthly", TypeName = "BOOLEAN")]
        public bool? IsCurrentMonthlyPayment { get; set; }

        [Column("archived", TypeName = "BOOLEAN")]
        public bool Archived { get; private set; }

        [Column("archived_reason"),MaxLength(2000)]
        public string ArchivedReason { get; private set; }

        #endregion

        #region Properties

        [NotMapped]
        public string StatusDescription => GetStatusDescription();

        [NotMapped]
        public string ContractTypeDescription => GetContractTypeDescription();

        private string GetStatusDescription()
        {
            return ConstantsHelper.GetValueOf<InterviewStatus>(this.Status);
        }

        private string GetContractTypeDescription()
        {
            return ConstantsHelper.GetValueOf<ContractTypes>(this.ContractType);
        }        

        #region DependencyProperties
        public virtual Candidate Candidate { get; private set; }
        public virtual Owner Owner { get; private set; }

        public virtual ICollection<InterviewHistory> InterviewHistories { get; private set; }
        public virtual ICollection<InterviewUpdate> InterviewUpdates { get; private set; }

        #endregion

        #endregion

        #region Dependency

        public static Expression<Func<Interview, object>>[] Dependences = { x => x.Candidate,
                                                                            x => x.Candidate.JobRole,
                                                                            x => x.Candidate.Location,
                                                                            x => x.Candidate.CandidateLanguages,
                                                                            x => x.Candidate.CandidateTechSkills,
                                                                            x => x.Candidate.CandidateUploads,
                                                                            x => x.Owner };

        public static Expression<Func<Interview, object>>[] DependenceCollections = { x => x.InterviewHistories,
                                                                                      x => x.InterviewUpdates };

        #endregion

        #region Constructors

        public Interview()
        { }

        public Interview(InterviewModel model, Candidate candidate, Owner owner)
        {
            SetCandidate(candidate);
            SetOwner(owner);
            MappingContext(model, "IS_0001");

            this.CreatedAt = model.CreatedAt ?? DateTime.UtcNow;
            this.Active = true;                 
            this.IsCurrentMonthlyPayment = model.IsCurrentMonthlyPayment;
            this.IsExpectationMonthlyPayment = model.IsExpectationMonthlyPayment;            
        }

        #endregion

        #region Context

        public void Update(InterviewModel model)
        {           
            MappingContext(model, model.Status);
        }             

        public void Finish(InterviewModel model)
        {
            MappingContext(model, "IS_0002");
        }

        public void Archive(string reason)
        {
            this.Archived = true;
            this.ArchivedReason = reason;
        }

        internal void CreateInterviewHistory(InterviewModel model, out ICollection<InterviewHistory> interviewHistory)
        {
            interviewHistory = new Collection<InterviewHistory>();
            //TODO: Implementar histórico que valida se cada campo foi alterado e registra na tabela
            //Somente se o estado atual do campo for diferente de null
        }

        private void MappingContext(InterviewModel model, string status)
        {
            this.Status = status;
            this.ContractType = model.ContractType;
            this.OngoingProcesses = model.OngoingProcesses;
            this.ReasonForLeaving = model.ReasonForLeaving;
            this.CurrentSalary = model.CurrentSalary.GetValueOrDefault(0);
            this.IsCurrentSalaryNet = model.IsCurrentSalaryNet;
            this.ExpectationSalary = model.ExpectationSalary.GetValueOrDefault(0);
            this.IsExpectationSalaryNet = model.IsExpectationSalaryNet;
            this.ProfessionalExperience = model.ProfessionalExperience;
            this.Notes = model.Notes;           
            this.IsCurrentMonthlyPayment = model.IsCurrentMonthlyPayment;
            this.IsExpectationMonthlyPayment = model.IsExpectationMonthlyPayment;
        }


        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Interview, InterviewGridModel>()           
           .ForMember(dest => dest.Date, opt => opt.MapFrom(ent => ent.CreatedAt))
           .ForMember(dest => dest.State, opt => opt.MapFrom(ent => ent.StatusDescription))
           .ForMember(dest => dest.CandidateId, opt => opt.MapFrom(ent => ent.Candidate.Id))
           .ForMember(dest => dest.Name, opt => opt.MapFrom(ent => ent.Candidate.Name))
           .ForMember(dest => dest.Email, opt => opt.MapFrom(ent => ent.Candidate.Email))
           .ForMember(dest => dest.JobRole, opt => opt.MapFrom(ent => ent.Candidate.JobRole == null ? "Nenhum" : ent.Candidate.JobRole.JobTitle))
           .ForMember(dest => dest.Technologies, opt => opt.MapFrom(ent => ent.Candidate.TechSkills))
           .ForMember(dest => dest.SalaryExpected, opt => opt.MapFrom(ent => ent.ExpectationSalary))
           .ForMember(dest => dest.Owner, opt => opt.MapFrom(ent => ent.Owner.Name))
           .ForMember(dest => dest.Avaliability, opt => opt.MapFrom(ent => ent.Candidate.AvailabilityDescription ?? "Nenhum"));   
        }

        #endregion

        #region Setters

        public void SetOwner(Owner owner)
        {
            this.Owner = owner;
            this.OwnerId = owner?.Id ?? 0;
        }

        public void SetCandidate(Candidate candidate)
        {
            this.Candidate = candidate;
            this.CandidateId = candidate?.Id ?? 0;
        }       

        #endregion
    }
}
