﻿using AutoMapper;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Log : ILog
    {
        #region Columns
        [Required]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("ip")]
        public string IP { get; private set; }
        [Required]
        [Column("username")]
        public string Username { get; private set; }
        [Required]
        [Column("create_at")]
        public DateTime CreatedAt { get; private set; }
        [Required]
        [Column("operation")]
        public string Operation { get; private set; }
        [Required]
        [Column("json_request")]
        public string JsonRequest { get; private set; }
        [Required]
        [Column("status")]
        public string Status { get; private set; }
        [Required]
        [Column("error")]
        public string Error { get; private set; }
        #endregion

        #region Constructors
        public Log() { }

        public Log(LogModel model)
        {
            IP = model.IP;
            Username = model.Username;
            CreatedAt = model.CreatedAt;
            Operation = model.Operation;
            JsonRequest = model.JsonRequest;
            Status = model.Status;
            Error = model.Error;
        }
        #endregion

        #region Mapping
        public void CustomMap(IMapperConfigurationExpression configuration)
        {            
        }
        #endregion
    }
}
