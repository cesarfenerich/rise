﻿using AutoMapper;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using System.Text;

namespace Domain.Entities
{
    public class CandidateNaifas : ICandidateNaifas
    {

        #region Columns
        [Required]
        [Column("id")]
        public long Id { get; set; }
        [Column("candidate_id")]
        public long CandidateId { get; set; }

        [Required]
        [Column("naifas_id")]
        public long NaifasId { get; private set; }
        #endregion

        #region DependencyProperties

        public virtual Candidate Candidate { get; private set; }
        public virtual Naifas Naifas { get; private set; }

        #endregion

        #region Dependency

        public static Expression<Func<CandidateNaifas, object>>[] Dependences = { x => x.Candidate,
                                                                                  x => x.Naifas};

        #endregion

        #region Constructors

        public CandidateNaifas()
        { }

        public CandidateNaifas(Candidate candidate, Naifas naifas)
        {
            SetCandidate(candidate);
            SetNaifas(naifas);
        }

        #endregion

        #region Mapping
        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            
        }
        #endregion

        #region Setters

        public void SetCandidate(Candidate candidate)
        {
            this.Candidate = candidate;
            this.CandidateId = candidate?.Id ?? 0;
        }

        public void SetNaifas(Naifas naifas)
        {
            this.Naifas = naifas;
            this.NaifasId = naifas?.Id ?? 0;
        }

        #endregion
    }
}
