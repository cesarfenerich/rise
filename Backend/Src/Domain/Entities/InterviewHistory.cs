﻿using AutoMapper;
using Domain.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("interview_history")]
    public class InterviewHistory : IInterviewHistory
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("interview_id")]
        public long InterviewId { get; private set; }

        [Column("updated_field"), MaxLength(50)]
        public string UpdatedField { get; private set; }

        [Column("old_value"), MaxLength(2000)]
        public string OldValue { get; private set; }

        [Required]
        [Column("created_at")]
        public DateTime CreatedAt { get; private set; }

        [Required]
        [Column("active")]
        public bool Active { get; private set; }

        
        #endregion

        #region DependencyProperties

        public virtual Interview Interview { get; private set; }

        #endregion

        #region Constructors

        public InterviewHistory()
        { }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            
        }

        #endregion
    }
}
