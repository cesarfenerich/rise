﻿using AutoMapper;
using Common.Extensions;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces;
using Domain.Entities.ValueObjects;
using Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("user")]
    public class User : IUser
    {
        #region Columns 
        
        [Required]
        [Column("id")]
        public long Id { get; set; }        

        [Required]
        [Column("email"), MaxLength(50)]
        public string Email { get; private set; }

        [Required]
        [Column("password"), MaxLength(500)]
        public string Password { get; private set; }
                
        [Column("failedAttempts")]
        public int FailedAttempts { get; private set; }

        [Column("blocked")]
        public bool Blocked { get; private set; }

        [Column("ResetCode")]
        public string ResetCode { get; private set; }

        [Column("LastPasswordUpdate")]
        public DateTime LastPasswordUpdate { get; private set; }

        #endregion

        #region Properties

        [NotMapped]
        public virtual JwtToken AuthToken { get; private set; }


        #region DependencyProperties

        [NotMapped]
        public virtual Owner Owner { get; private set; }

        #endregion

        #endregion

        #region Constructors
        public User()
        { }

        public User(string email, string password, int failedAttempts, bool blocked, bool validateBlock)
        {
            ValidateEmail(email);
            string errorMessage;
            ValidatePassword(password, validateBlock, out errorMessage);
            HandleErrorMessage(errorMessage);
            this.Email = email;
            this.Password = password;
            this.FailedAttempts = failedAttempts;
            this.Blocked = blocked;
        }

        #endregion

        #region Dependency

        public static Expression<Func<User, object>>[] Dependences = { x => x.Owner};

        //public static Expression<Func<User, object>>[] DependenceCollections = { x => x.Permissions };

        #endregion

        #region Handlers

        public void ValidateEmail(string email)
        {
            if (email.IsNullOrWhitespaceOrEmpty())
                ErrorHelper.Except("APP_0032");

            if (!email.Contains("@onrising.com"))
                ErrorHelper.Except("APP_0033");
        }

        public void ValidatePassword(string password, bool validateBlock, out string errorMessage, bool validateCurrent = false)
        {
            errorMessage = string.Empty;
            if (validateBlock && this.Blocked)
            {
                errorMessage = "APP_0034";                
                return;
            }

            if (password.IsNullOrWhitespaceOrEmpty())
            {
                this.FailedAttempts++;
                if (this.FailedAttempts == 10)
                {
                    this.Blocked = true;
                    errorMessage = "APP_0034";                    
                    return;
                }
                errorMessage = "APP_0003";                
            }

            if (validateCurrent && !this.Password.IsNullOrWhitespaceOrEmpty() && !SecurityHelper.ValidateString(password, this.Password))
            {
                this.FailedAttempts++;
                if (this.FailedAttempts == 10)
                {
                    this.Blocked = true;
                    errorMessage = "APP_0034";                    
                    return;
                }
                errorMessage = "APP_0003";                
            }
        }

        public void HandleErrorMessage(string errorMessage)
        {
            if(!string.IsNullOrEmpty(errorMessage)) ErrorHelper.Except(errorMessage);
        }

        public void Update(string email, string password, int failedAttempts, bool blocked, bool passwordValidated = false)
        {
            ValidateEmail(email);
            string errorMessage;
            if (!passwordValidated)
            {
                ValidatePassword(password, false, out errorMessage);
                HandleErrorMessage(errorMessage);
            }
            this.Email = email;
            this.Password = password;
            this.FailedAttempts = failedAttempts;
            this.Blocked = blocked;
            this.LastPasswordUpdate = DateTime.Now;
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<User, UserModel>()           
            .ForMember(dest => dest.Email, opt => opt.MapFrom(ent => ent.Email))
            .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(ent => ent.Owner.Id))
            .ForMember(dest => dest.AuthType, opt => opt.MapFrom(ent => ent.AuthToken.AuthType))
            .ForMember(dest => dest.AccessToken, opt => opt.MapFrom(ent => ent.AuthToken.AccessToken))
            .ForMember(dest => dest.RefreshToken, opt => opt.MapFrom(ent => ent.AuthToken.RefreshToken))
            .ForMember(dest => dest.ExpiresAt, opt => opt.MapFrom(ent => ent.AuthToken.Expires))
            .ForMember(dest => dest.FailedAttempts, opt => opt.MapFrom(ent => ent.FailedAttempts))
            .ForMember(dest => dest.Blocked, opt => opt.MapFrom(ent => ent.Blocked))
            .ForMember(dest => dest.ResetCode, opt => opt.MapFrom(ent => ent.ResetCode))
            .ForMember(dest => dest.LastPasswordUpdate, opt => opt.MapFrom(ent => ent.LastPasswordUpdate))
            .ReverseMap();
        }

        #endregion

        #region Setters        

        public void SetAuthToken(JwtToken authToken)
        {
            this.AuthToken = authToken;
        }
        public void SetResetCode(string resetCode)
        {
            this.ResetCode = resetCode;
        }

        #endregion
    }
}
