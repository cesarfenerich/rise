﻿using AutoMapper;
using Domain.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("candidate_upload")]
    public class CandidateUpload : ICandidateUpload
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }
        
        [Column("candidate_id")]
        public long CandidateId { get; set; }       

        [Required]
        [Column("upload_id")]
        public long UploadId { get; private set; }

        #endregion

        #region DependencyProperties

        public virtual Candidate Candidate { get; private set; }
        public virtual Upload Upload { get; private set; }

        #endregion

        #region Dependency

        public static Expression<Func<CandidateUpload, object>>[] Dependences = { x => x.Candidate,
                                                                                  x => x.Upload};

        #endregion

        #region Constructors

        public CandidateUpload()
        {  }

        public CandidateUpload(Candidate candidate, Upload upload)
        {
            SetCandidate(candidate);
            SetUpload(upload);
        }


        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {


            
        }

        #endregion

        #region Setters

        public void SetCandidate(Candidate candidate)
        {
            this.Candidate = candidate;
            this.CandidateId = candidate?.Id ?? 0;
        }

        public void SetUpload(Upload upload)
        {
            this.Upload = upload;
            this.UploadId = upload?.Id ?? 0;
        }

        #endregion


    }
}
