﻿using AutoMapper;
using Domain.Entities.Interfaces;
using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("naifas")]
    public class Naifas : INaifas
    {
        #region Columns
        [Required]
        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("question")]
        public string Question { get; set; }

        [Column("answer")]
        public string Answer { get; set; }
        #endregion

        #region Constructors
        public Naifas() { }

        public Naifas(long id, string question, string answer) 
        {
            this.Id = id;
            this.Question = question;
            this.Answer = answer;
        }
        #endregion

        #region Context
        public void Update(NaifasModel model)
        {
            this.Question = model.Question;
            this.Answer = model.Answer;
        }
        #endregion

        #region Mapping
        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            
        }
        #endregion
    }

}
