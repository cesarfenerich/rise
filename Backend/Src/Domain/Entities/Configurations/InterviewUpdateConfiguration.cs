﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class InterviewUpdateConfiguration : IEntityTypeConfiguration<InterviewUpdate>
    {
        public void Configure(EntityTypeBuilder<InterviewUpdate> builder)
        {
            builder.ToTable("interview_update");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");           

            builder.HasOne(a => a.Interview)
           .WithMany(b => b.InterviewUpdates)
           .HasForeignKey(x => x.InterviewId);

            builder.HasOne(a => a.Owner)
           .WithMany(b => b.InterviewUpdates)
           .HasForeignKey(x => x.OwnerId);
        }
    }
}
