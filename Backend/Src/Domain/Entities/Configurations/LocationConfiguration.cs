﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class LocationConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder.ToTable("location");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");            
        }
    }
}
