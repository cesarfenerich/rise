﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class CandidateUploadConfiguration : IEntityTypeConfiguration<CandidateUpload>
    {
        public void Configure(EntityTypeBuilder<CandidateUpload> builder)
        {
            builder.ToTable("candidate_upload");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");           

            builder.HasOne(a => a.Candidate)
           .WithMany(b => b.CandidateUploads)
           .HasForeignKey(x => x.CandidateId);            
        }
    }
}
