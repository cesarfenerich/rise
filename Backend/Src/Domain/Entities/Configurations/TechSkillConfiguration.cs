﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class TechSkillConfiguration : IEntityTypeConfiguration<TechSkill>
    {
        public void Configure(EntityTypeBuilder<TechSkill> builder)
        {
            builder.ToTable("tech_skill");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");                  
        }
    }
}
