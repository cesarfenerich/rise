﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Configurations
{
    public class NaifasConfiguration : IEntityTypeConfiguration<Naifas>
    {
        public void Configure(EntityTypeBuilder<Naifas> builder)
        {
            builder.ToTable("naifas");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");
        }
    }
}
