﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class CandidateConfiguration : IEntityTypeConfiguration<Candidate>
    {
        public void Configure(EntityTypeBuilder<Candidate> builder)
        {
            builder.ToTable("candidate");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");

            builder.HasOne(a => a.JobRole)
            .WithMany(b => b.Candidates)
            .HasForeignKey(x => x.JobRoleId);

            builder.HasOne(a => a.Location)
           .WithMany(b => b.Candidates)
           .HasForeignKey(x => x.LocationId);

        }
    }
}
