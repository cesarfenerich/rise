﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Configurations
{
    public class LogConfiguration : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.ToTable("log");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");
        }
    }
}
