﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Configurations
{
    public class CandidateNaifasConfiguration : IEntityTypeConfiguration<CandidateNaifas>
    {
        public void Configure(EntityTypeBuilder<CandidateNaifas> builder)
        {
            builder.ToTable("candidate_naifas");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");

            builder.HasOne(a => a.Candidate)
           .WithMany(b => b.CandidateNaifas)
           .HasForeignKey(x => x.CandidateId);
        }
    }
}
