﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class CandidateTechSkillConfiguration : IEntityTypeConfiguration<CandidateTechSkill>
    {
        public void Configure(EntityTypeBuilder<CandidateTechSkill> builder)
        {
            builder.ToTable("candidate_tech_skill");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");

            builder.HasOne(a => a.Candidate)
            .WithMany(b => b.CandidateTechSkills)
            .HasForeignKey(x => x.CandidateId);
        }
    }
}
