﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class InterviewHistoryConfiguration : IEntityTypeConfiguration<InterviewHistory>
    {
        public void Configure(EntityTypeBuilder<InterviewHistory> builder)
        {
            builder.ToTable("interview_history");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");           

            builder.HasOne(a => a.Interview)
           .WithMany(b => b.InterviewHistories)
           .HasForeignKey(x => x.InterviewId);
           
        }
    }
}
