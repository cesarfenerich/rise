﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class CandidateLanguageConfiguration : IEntityTypeConfiguration<CandidateLanguage>
    {
        public void Configure(EntityTypeBuilder<CandidateLanguage> builder)
        {
            builder.ToTable("candidate_language");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");           

            builder.HasOne(a => a.Candidate)
           .WithMany(b => b.CandidateLanguages)
           .HasForeignKey(x => x.CandidateId);            
        }
    }
}
