﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities.Configurations
{
    public class InterviewConfiguration : IEntityTypeConfiguration<Interview>
    {
        public void Configure(EntityTypeBuilder<Interview> builder)
        {
            builder.ToTable("interview");

            builder
                .HasKey(m => m.Id);

            builder
             .Property(x => x.Id)
             .ValueGeneratedOnAdd()
             .HasColumnType("bigint");            

            builder.HasOne(a => a.Candidate)
           .WithMany(b => b.Interviews)
           .HasForeignKey(x => x.CandidateId);

            builder.HasOne(a => a.Owner)
           .WithMany(b => b.Interviews)
           .HasForeignKey(x => x.OwnerId);
        }
    }
}
