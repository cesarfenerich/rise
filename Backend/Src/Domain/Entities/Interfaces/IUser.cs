﻿using Common.Interfaces;

namespace Domain.Entities.Interfaces
{
    public interface IUser : IGenericEntity
    { }
}
