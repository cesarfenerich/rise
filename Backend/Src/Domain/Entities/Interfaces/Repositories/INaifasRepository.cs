﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Interfaces.Repositories
{
    public interface INaifasRepository : IGenericRepository<Naifas>
    {
        IEnumerable<Naifas> GetNaifasByIds(IEnumerable<long> naifaIds);
    }
}
