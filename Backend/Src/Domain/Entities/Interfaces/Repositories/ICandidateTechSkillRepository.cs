﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateTechSkillRepository : IGenericRepository<CandidateTechSkill>
    {
        IEnumerable<CandidateTechSkill> GetByTechSkillIds(IEnumerable<long> techSkillIds);
        CandidateTechSkill GetByCandidateAndTechSkill(long candidateId, long idTechSkill);
        IEnumerable<CandidateTechSkill> GetTechSkillsByCandidate(long id);
        IEnumerable<CandidateTechSkill> GetTechSkillsByCandidates(IEnumerable<long> candidateIds);
        ICollection<CandidateTechSkill> GetByTechSkill(long techSkillId);
    }
}
