﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Interfaces.Repositories
{
    public interface ICandidateNaifasRepository : IGenericRepository<CandidateNaifas>
    {
        CandidateNaifas GetByCandidateAndNaifas(long candidateId, long idNaifas);

        IEnumerable<CandidateNaifas> GetNaifasByCandidate(long candidateId);
    }
}
