﻿using Common.Interfaces;

namespace Domain.Entities.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        User GetUserById(long userId);
        bool ValidateUserToAuthenticate(long id);
        User GetByEmail(string email);
    }
}
