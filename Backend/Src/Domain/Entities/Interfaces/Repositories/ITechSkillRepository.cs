﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ITechSkillRepository : IGenericRepository<TechSkill>
    {
        IEnumerable<TechSkill> GetAllDistinctTechSkills();
        TechSkill GetByCategoryAndDescription(string categoryId, string description);
        IEnumerable<TechSkill> GetTechSkillsByCategory(string categoryId);
        IEnumerable<TechSkill> GetTechSkillsByIds(IEnumerable<long> techSkillIds);
    }
}
