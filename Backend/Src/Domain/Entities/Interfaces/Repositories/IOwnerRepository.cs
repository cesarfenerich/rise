﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IOwnerRepository : IGenericRepository<Owner>
    {
        Owner GetOwnerById(long id);        
        IEnumerable<Owner> GetAllOwners();
        IEnumerable<Owner> GetAllActiveOwners(bool active = true);
    }
}
