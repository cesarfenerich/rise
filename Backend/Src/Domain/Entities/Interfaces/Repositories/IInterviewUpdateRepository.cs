﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewUpdateRepository : IGenericRepository<InterviewUpdate>
    {
        IEnumerable<InterviewUpdate> GetUpdatesByInterview(long interviewId);
    }
}
