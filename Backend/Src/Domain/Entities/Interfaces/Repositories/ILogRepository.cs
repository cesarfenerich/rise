﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Interfaces.Repositories
{
    public interface ILogRepository : IGenericRepository<Log>
    {
        IEnumerable<Log> GetAllLogs();
        Log GetLogById(long id);
        IEnumerable<Log> GetLogsByStatus(string status);
        IEnumerable<Log> GetLogsByOperation(string operation);
        IEnumerable<Log> GetLogsByDate(DateTime date);
        IEnumerable<Log> GetLogsByUsername(string username);
    }
}
