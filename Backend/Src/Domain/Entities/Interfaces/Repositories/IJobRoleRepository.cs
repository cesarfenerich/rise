﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IJobRoleRepository : IGenericRepository<JobRole>
    {
        IEnumerable<JobRole> GetAllDistinctJobRoles();
        JobRole GetByJobTitle(string value);
        IEnumerable<string> SearchByJobTitle(string search);
    }
}
