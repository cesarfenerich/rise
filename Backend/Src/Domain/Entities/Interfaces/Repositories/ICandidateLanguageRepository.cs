﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateLanguageRepository : IGenericRepository<CandidateLanguage>
    {
        public IEnumerable<CandidateLanguage> GetLanguagesByCandidate(long id);
        CandidateLanguage GetByCandidateAndLanguage(long candidateId, long idLanguage);
        IEnumerable<CandidateLanguage> GetCandidatesByLanguages(IEnumerable<long> idLanguages);
    }
}
