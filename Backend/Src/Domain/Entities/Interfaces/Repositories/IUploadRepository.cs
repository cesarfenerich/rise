﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IUploadRepository : IGenericRepository<Upload>
    {
        IEnumerable<Upload> GetUploadByIds(IEnumerable<long> uploadIds);
        IEnumerable<Upload> GetByFileName(string fileName);
    }
}
