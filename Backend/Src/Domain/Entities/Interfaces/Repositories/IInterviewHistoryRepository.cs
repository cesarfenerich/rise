﻿using Common.Interfaces;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewHistoryRepository : IGenericRepository<InterviewHistory>
    {
    }
}
