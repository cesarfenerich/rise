﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ILocationRepository : IGenericRepository<Location>
    {
        IEnumerable<Location> GetAllDistinctLocations();
        Location GetByCity(string value);
    }
}
