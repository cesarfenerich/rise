﻿using Common.Interfaces;
using Domain.Models;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateRepository : IGenericRepository<Candidate>
    {
        Candidate GetCandidateById(long candidateId);
        IEnumerable<Interview> GetInterviewsByCandidate(long candidateId);
        Candidate GetByNameAndEmail(string name, string email);
        IEnumerable<string> SearchByName(string search);
        IEnumerable<string> SearchByEmail(string search);
        ICollection<Candidate> GetByJobRole(long jobRoleId);
    }
}
