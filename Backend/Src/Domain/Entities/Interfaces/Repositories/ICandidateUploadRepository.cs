﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateUploadRepository : IGenericRepository<CandidateUpload>
    {
        CandidateUpload GetByCandidateAndUpload(long candidateId, long idUpload);
        IEnumerable<CandidateUpload> GetUploadsByCandidate(long id);
        IEnumerable<CandidateUpload> GetByUploads(IEnumerable<long> uploadIds);
        IEnumerable<CandidateUpload> GetAllUploads();
    }
}
