﻿using Common.Interfaces;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ILanguageRepository : IGenericRepository<Language>
    {
        IEnumerable<Language> GetAllDistinctLanguages();
        Language GetByName(string value);
        IEnumerable<Language> GetLanguagesByIds(IEnumerable<long> ids);
    }
}
