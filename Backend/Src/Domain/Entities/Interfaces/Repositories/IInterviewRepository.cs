﻿using Common.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewRepository : IGenericRepository<Interview>
    {
        Interview GetInterviewById(long interviewId);
        IEnumerable<Interview> GetLastInterviews();
        IEnumerable<Interview> GetLastNotArchivedInterview();
        IEnumerable<Interview> SearchByCandidateName(string search);
        IEnumerable<Interview> SearchByCandidateEmail(string search);
        IEnumerable<Interview> SearchByCandidateJobRole(string search);
        IEnumerable<Interview> SearchByCandidateAvaliability(string availability);
        IEnumerable<Interview> SearchByExpectedSalary(string expectedSalary);
        IEnumerable<Interview> SearchByContractType(string contractType);
        IEnumerable<Interview> SearchByOwner(long ownerId);
        IEnumerable<Interview> GetInterviewsByCandidates(IEnumerable<long> candidateIdsWithSkills);
        IEnumerable<Interview> GetLastInterviewsByCandidate(long candidateId);
        int GetInterviewsCount();
        IEnumerable<Interview> SearchByYearOfExperience(string yearsOfExperience);
        IEnumerable<Interview> SearchByCandidateLinkdin(string search);
        IEnumerable<Interview> SearchByCandidateLocation(string search);
    }
}
