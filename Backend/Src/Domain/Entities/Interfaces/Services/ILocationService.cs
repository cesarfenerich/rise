﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ILocationService
    {
        Location Add(Location location);
        Location Update(Location location);
        Location GetById(long id);
        IEnumerable<Location> GetAll();
        IEnumerable<Location> GetByLocationId(Int64 Id);
    }
}
