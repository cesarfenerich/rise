﻿using Common.Interfaces;
using Common.Models;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface IBaseService
    {
        void UploadFile(IFormFile fileToUpload, string fileName);
        Task<byte[]> DownloadFile(string filePath);

        //TODO: maybe use report structure for naifas
        //string GenerateReport(string reportName, Dictionary<string, object> parameters = null, DataSet dataSet = null);

        void NullCheck(object obj, string errorCode);
        TDestination Map<TSource, TDestination>(TSource source);
        IEnumerable<TDestination> MapList<TSource, TDestination>(IEnumerable<TSource> source);

        Task<IEnumerable<SelectionModel>> GetConstantType<T>();
        
        bool SendMail(IGenericEmail mailRequest);
    }
}
