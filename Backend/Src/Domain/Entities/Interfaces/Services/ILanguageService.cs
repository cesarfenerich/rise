﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ILanguageService
    {
        Language Add(Language candidateLanguage);
        Language Update(Language candidateLanguage);
        Language GetById(long id);
        IEnumerable<Language> GetAll();
        IEnumerable<Language> GetByCandidateId(Int64 candidateId);
    }
}
