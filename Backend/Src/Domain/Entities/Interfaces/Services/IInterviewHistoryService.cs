﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewHistoryService
    {
        InterviewHistory Add(InterviewHistory interviewHistory);
        InterviewHistory Update(InterviewHistory interviewHistory);
        InterviewHistory GetById(long id);
        IEnumerable<InterviewHistory> GetAll();
    }
}
