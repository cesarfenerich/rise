﻿using Common.Models;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewService
    {
        Task<InterviewModel> GetById(long id);
        Task<IEnumerable<InterviewGridModel>> GetMostRecentInterviews();
        Task<SelectionModel> GetInterviewsCount();
        Task<IEnumerable<SelectionModel>> GetContractTypes();
        Task<IEnumerable<SelectionModel>> GetStatus();
        Task<IEnumerable<SelectionModel>> GetExpectedSalariesRange();
        Task<InterviewModel> CreateInterview(InterviewModel requestModel);
        Task<InterviewModel> UpdateInterview(InterviewModel requestModel, User loggedUser);
        Task<List<BaseSearchModel>> SearchInterviewByFields(string search);
        Task<IEnumerable<InterviewGridModel>> QuickSearchInterviews(string field, string search);
        Task<IEnumerable<InterviewGridModel>> AdvancedSearchInterviews(AdvancedSearchModel requestModel);
        Task<IEnumerable<InterviewUpdateModel>> GetLastUpdatesByInterview(long id);
        Task<InterviewUpdateModel> CreateInterviewUpdate(InterviewUpdateModel requestModel);
        Task<InterviewUpdateModel> UpdateInterviewUpdate(InterviewUpdateModel requestModel);
        Task DeleteInterviewUpdate(long id);
        Task<InterviewInfoModel> GetInfosByInterview(long id);
        Task<IEnumerable<SelectionModel>> GetYearsOfExperienceRange();         
        Task ArchiveInterview(InterviewArchiveModel requestModel);
    }
}
