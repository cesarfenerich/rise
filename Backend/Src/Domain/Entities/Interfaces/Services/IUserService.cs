﻿using Common.Models;
using Domain.Models;
using System;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface IUserService
    {       
        Task<UserModel> Authenticate(AuthModel requestModel);
        Task<UserModel> AuthenticateByToken(Guid? Token);
        Task<UserModel> GetById(long id);
        Task<UserModel> CreateUser(UserModel userModel);
        Task<UserModel> UpdateUser(UserModel userModel);
        Task<RecoverPasswordModel> SendRecoverPasswordEmail(string email);
        Task<RecoverPasswordModel> VerifyRecoverPasswordCode(string email, string code);
        Task<UserModel> UpdateUserPassword(UserModel userModel);     
    }
}
