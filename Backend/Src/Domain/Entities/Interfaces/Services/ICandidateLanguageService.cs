﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateLanguageService
    {        
        Task<CandidateLanguage> Create(CandidateLanguage candidateLanguage);
        CandidateLanguage Update(CandidateLanguage candidateLanguage);
        CandidateLanguage GetById(long id);
        IEnumerable<CandidateLanguage> GetAll();
    }
}
