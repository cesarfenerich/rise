﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IJobRoleService
    {
        JobRole Add(JobRole jobRole);
        JobRole Update(JobRole jobRole);
        JobRole GetById(long id);
        IEnumerable<JobRole> GetAll();
    }
}
