﻿using Common.Models;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateService
    {
        Task<CandidateModel> GetById(long id);
        Task<IEnumerable<InterviewGridModel>> GetLastInterviewsByCandidate(long id);
        Task<IEnumerable<SelectionModel>> GetCandidateTypes();
        Task<IEnumerable<SelectionModel>> GetCandidateAvailability();
        Task<IEnumerable<SelectionModel>> GetCandidateInterval();
        Task<IEnumerable<SelectionModel>> GetCandidateMobility();
        Task<IEnumerable<SelectionModel>> GetLiteraryAbilities();
        Task<IEnumerable<SelectionModel>> GetMaritalStatuses();
        Task<IEnumerable<SelectionModel>> GetAllLanguages();
        Task<IEnumerable<SelectionModel>> GetAllLocations();
        Task<IEnumerable<SelectionModel>> GetAllJobRoles();
        Task<IEnumerable<TechSkillModel>> GetAllTechSkills();
        Task<IEnumerable<SelectionModel>> GetAllTechSkillCategories();
        Task<IEnumerable<long>> GetCandidateByLanguages(IEnumerable<long> languages);
        Task<UploadModel> UploadFile(IFormFile file);
        Task<Tuple<string, byte[]>> DownloadFile(long uploadId);
        Task<IList<BaseSearchModel>> SearchByCandidateNames(string search);
        Task<IList<BaseSearchModel>> SearchByCandidateEmails(string search);
        Task<IList<BaseSearchModel>> SearchByCandidateJobRoles(string search);
        Task<IEnumerable<long>> GetCandidatesBySkills(IEnumerable<long> techSkills);
        Task<CandidateModel> CreateCandidate(CandidateModel model);
        Task<CandidateModel> UpdateCandidate(CandidateModel model);
        Task<SelectionModel> CreateJobRole(SelectionModel requestModel);
        Task<SelectionModel> CreateLocation(SelectionModel requestModel);
        Task<SelectionModel> CreateLanguage(SelectionModel requestModel);
        Task<TechSkillModel> CreateTechSkill(TechSkillModel requestModel);
        Task<CandidateModel> GetCandidateByReference(CandidateSearchModel requestModel);
        Task<SelectionModel> CreateCandidateLanguage(long candidateId, long idLanguage);
        Task<TechSkillModel> CreateCandidateTechSkill(long candidateId, long idTechSkill);
        Task<UploadModel> CreateCandidateUpload(long candidateId, long idUpload);
        Task<NaifasModel> CreateNaifas(NaifasModel requestModel);
        Task<NaifasModel> CreateCandidateNaifas(long candidateId, long idNaifas);
        Task<NaifasModel> UpdateNaifas(NaifasModel model);
        Task<SelectionModel> UpdateJobRole(SelectionModel requestModel);
        Task DeleteCandidateLanguage(long candidateId, long idLanguage);
        Task DeleteCandidateTechSkill(long candidateId, long idTechSkill);
        Task DeleteCandidateUpload(long candidateId, long idUpload);
        Task DeleteCandidateNaifas(long candidateId, long idNaifas);
        Task<IEnumerable<SelectionModel>> GetLanguagesByCandidate(long candidateId);
        Task<IEnumerable<TechSkillModel>> GetTechSkillsByCandidate(long candidateId, bool bypassCandidateValidation = false);
        Task<IEnumerable<UploadModel>> GetUploadsByCandidate(long candidateId);
        Task<IEnumerable<CandidateTechSkill>> GetTechSkillsByCandidates(IEnumerable<long> candidateIds);
        Task<UploadModel> GetUploadsByFileName(string fileName);
        Task<IEnumerable<NaifasModel>> GetNaifasByCandidate(long candidateId, bool bypassCandidateValidation = false);
        Task DeleteJobRole(long id);
        Task<TechSkillModel> UpdateTechSkill(TechSkillModel requestModel);
        Task DeleteTechSkill(long id);     

    }
}
