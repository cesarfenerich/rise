﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface IInterviewUpdateService
    {
        InterviewUpdate Add(InterviewUpdate interviewContact);
        InterviewUpdate Update(InterviewUpdate interviewContact);
        InterviewUpdate GetById(long id);
        IEnumerable<InterviewUpdate> GetAll();
    }
}
