﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ICandidateTechSkillService
    {
        CandidateTechSkill Add(CandidateTechSkill candidateTechSkill);
        CandidateTechSkill Update(CandidateTechSkill candidateTechSkill);
        CandidateTechSkill GetById(long id);
        IEnumerable<CandidateTechSkill> GetAll();
    }
}
