﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Domain.Entities.Interfaces
{
    public interface ITechSkillService
    {
        TechSkill Add(TechSkill technology);
        TechSkill Update(TechSkill technology);
        TechSkill GetById(long id);
        IEnumerable<TechSkill> GetAll();
        IEnumerable<TechSkill> GetByCategoryId(long categoryId);
    }
}
