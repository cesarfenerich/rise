﻿using Domain.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces
{
    public interface IOwnerService
    {
        Task<OwnerModel> GetById(long id);
        Task<IEnumerable<OwnerModel>> GetAllOwners();
        Task<IEnumerable<OwnerModel>> GetAllActiveOwners();
        Task<OwnerModel> CreateOwner(OwnerModel requestModel);
        Task<OwnerModel> UpdateOwner(OwnerModel requestModel);
        Task<OwnerModel> UpdateOwnerPassword(OwnerModel requestModel);        
        Task<bool> IsBlock(long id);
    }
}
