﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Interfaces.Services
{
    public interface ILogService
    {
        Task<LogModel> GetById(long id);
        Task<IEnumerable<LogModel>> GetAllLogs();
        Task<LogModel> CreateLog(LogModel requestModel);
    }
}
