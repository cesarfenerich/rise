﻿using AutoMapper;
using Domain.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("candidate_tech_skill")]
    public class CandidateTechSkill : ICandidateLanguage
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("candidate_id")]
        public long CandidateId { get; set; }

        [Required]
        [Column("tech_skill_id")]
        public long TechSkillId { get; set; }           

        #endregion

        #region DependencyProperties

        public virtual Candidate Candidate { get; private set; }

        public virtual TechSkill TechSkill { get; private set; }

        #endregion

        #region Dependency

        public static Expression<Func<CandidateTechSkill, object>>[] Dependences = { x => x.Candidate,
                                                                                     x => x.TechSkill };

        #endregion

        #region Constructors

        public CandidateTechSkill()
        {  }

        public CandidateTechSkill(Candidate candidate, TechSkill techSkill)
        {
            SetCandidate(candidate);
            SetTechSkill(techSkill);
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            
        }

        #endregion

        #region Setters

        public void SetCandidate(Candidate candidate)
        {
            this.Candidate = candidate;
            this.CandidateId = candidate?.Id ?? 0;
        }

        public void SetTechSkill(TechSkill techSkill)
        {
            this.TechSkill = techSkill;
            this.TechSkillId = techSkill?.Id ?? 0;
        }

        #endregion
    }
}
