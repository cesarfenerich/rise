﻿using AutoMapper;
using Common.Models;
using Domain.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("language")]
    public class Language : ILanguage
    {        
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }       

        [Required]
        [Column("name"), MaxLength(50)]
        public string Name { get; private set; }

        #endregion

        #region Constructors

        public Language()
        {  }

        public Language(string name)
        {
            this.Name = name;
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Language, SelectionModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(ent => ent.Id))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(ent => ent.Name));            
        }

        #endregion
    }
}
