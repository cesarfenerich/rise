﻿using AutoMapper;
using Common.Helpers;
using Domain.Entities.Constants;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Domain.Entities
{
    [Table("candidate")]
    public class Candidate : ICandidate
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }
       
        [Column("job_role_id")]
        public long? JobRoleId { get; set; }

        [Column("location_id")]
        public long? LocationId { get; set; }

        [Column("type"), MaxLength(50)]
        public string Type { get; private set; } //TODO: Persist only values of CandidateTypes ConstantKeys    

        [Required]
        [Column("name"),MaxLength(100)]
        public string Name { get; private set; }     

        [Column("born_date")]
        public DateTime? BornDate { get; private set; }        

        [Column("phone_number"), MaxLength(100)]
        public string PhoneNumber { get; private set; }
       
        [Column("email"),MaxLength(300)]
        public string Email { get; private set; }
       
        [Column("citizenship"), MaxLength(50)]
        public string Citizenship { get; private set; }
        
        [Column("marital_status"), MaxLength(50)]
        public string MaritalStatus { get; private set; }

        [Column("dependents_under7")]
        public int? DependentsUnder7 { get; private set; }

        [Column("dependents_above7")]
        public int? DependentsAbove7 { get; private set; }

        [Column("linkein_profile_link"), MaxLength(300)]
        public string LinkedInProfileLink { get; private set; }
        
        [Column("cvfile_path"), MaxLength(150)]
        public string CvFilePath { get; private set; }

        [Column("cvonrising_file_path"), MaxLength(150)]
        public string CvOnrisingFilePath { get; private set; }

        [Column("naifas_file_path"), MaxLength(150)]
        public string NaifasFilePath { get; private set; }

        [Column("literary_ability"), MaxLength(50)]
        public string LiteraryAbility { get; private set; }

        [Column("college"), MaxLength(300)]
        public string College { get; private set; }

        [Column("certifications"), MaxLength(200)]
        public string Certifications { get; private set; }       
        
        [Column("available")]
        public string Available { get; private set; }

        [Column("interval")]
        public string Interval { get; private set; }

        [Column("available_at")]
        public DateTime AvailableAt { get; private set; }

        [Required]
        [Column("create_at")]
        public DateTime CreatedAt { get; private set; }

        [Required]
        [Column("active")]
        public bool Active { get; private set; }
        
        [Column("years_of_experience")]
        public int? YearsOfExperience { get; private set; }

        [Column("mobility")]
        public string Mobility { get; private set; }

        #endregion

        #region Properties     

        [NotMapped]
        public string AvailabilityDescription => GetAvailabilityDescription();

        [NotMapped]
        public string TypeDescription => GetTypeDescription();

        [NotMapped]
        public string MaritalStatusDescription => GetMaritalStatusDescription();

        [NotMapped]
        public string LiteraryAbilityDescription => GetLiteraryAbilityDescription();

        [NotMapped]
        public string TechSkills => GetTechSkills();

        //TODO: Pensar em forma de como Carregar os TechSkills a partir da entrevista
        private string GetTechSkills()
        {
            StringBuilder builder = new StringBuilder();

            if (this.CandidateTechSkills != null)
            {
                if (this.CandidateTechSkills.Count <= 0)
                    return "Nenhum";

                foreach (var techSkill in this.CandidateTechSkills.Select(x => x.TechSkill).AsEnumerable())
                {
                    builder.Append($"{techSkill.Description},");
                }

                builder.Remove(builder.Length - 1, 1);
            }

            return builder.ToString();
        }

        private string GetAvailabilityDescription()
        {
            return ConstantsHelper.GetValueOf<CandidateAvailability>(this.Available);
        }

        private string GetTypeDescription()
        {
            return ConstantsHelper.GetValueOf<CandidateTypes>(this.Type);
        }

        private string GetMaritalStatusDescription()
        {
            return ConstantsHelper.GetValueOf<MaritalStatus>(this.MaritalStatus);
        }

        private string GetLiteraryAbilityDescription()
        {
            return ConstantsHelper.GetValueOf<LiteraryAbilities>(this.LiteraryAbility);
        }

        #region DependencyProperties
        public virtual JobRole JobRole { get; private set; }
        public virtual Location Location { get; private set; }

        public virtual ICollection<Interview> Interviews { get; private set; }

        public virtual ICollection<CandidateLanguage> CandidateLanguages { get; private set; }
        public virtual ICollection<CandidateTechSkill> CandidateTechSkills { get; private set; }
        public virtual ICollection<CandidateUpload> CandidateUploads { get; private set; }
        public virtual ICollection<CandidateNaifas> CandidateNaifas { get; private set; }

        #endregion

        #endregion

        #region Dependency

        public static Expression<Func<Candidate, object>>[] Dependences = { x => x.JobRole,
                                                                            x => x.Location };

        public static Expression<Func<Candidate, object>>[] DependenceCollections = { x => x.Interviews, 
                                                                                      x => x.CandidateLanguages,  
                                                                                      x => x.CandidateTechSkills,
                                                                                      x => x.CandidateUploads};        

        #endregion

        #region Constructors

        public Candidate()
        { }

        public Candidate(CandidateModel model, 
                         JobRole jobRole, 
                         Location location)
        {
            this.Name = model.Name;
            SetJobRole(jobRole);
            SetLocation(location);

            this.Type = model.Type;
            this.Name = model.Name;
            this.BornDate = model.BornDate;
            this.PhoneNumber = model.PhoneNumber;
            this.Email = model.Email;
            this.Citizenship = model.Citizenship;
            this.MaritalStatus = model.MaritalStatus;
            this.DependentsUnder7 = model.DependentsUnder7;
            this.DependentsAbove7 = model.DependentsAbove7;
            this.LiteraryAbility = model.LiteraryAbility;
            this.College = model.College;
            this.Certifications = model.Certifications;
            this.Available = model.Available;
            this.LinkedInProfileLink = model.LinkedInProfileLink;
            this.YearsOfExperience = model.YearsOfExperience;
            this.Interval = model.Interval;
            this.AvailableAt = model.AvailableAt.HasValue ? model.AvailableAt.Value : DateTime.MinValue;
            this.CreatedAt = DateTime.UtcNow;
            this.Active = true;
            this.Mobility = model.Mobility;
        }


        #endregion

        #region Context

        public bool MatchAllTechSkills(IEnumerable<long> techSkillsIds)
        {
            var ret = true;
            foreach (var id in techSkillsIds)
            {
                if (!this.CandidateTechSkills.Select(x => x.TechSkillId).Contains(id))
                    ret = false;
            }

            return ret;
        }

        public void Update(CandidateModel model,
                           JobRole jobRole,
                           Location location)
        {
            this.Name = model.Name;
            SetJobRole(jobRole);
            SetLocation(location);

            this.Type = model.Type;
            this.Name = model.Name;
            this.BornDate = model.BornDate;
            this.PhoneNumber = model.PhoneNumber;
            this.Email = model.Email;
            this.Citizenship = model.Citizenship;
            this.MaritalStatus = model.MaritalStatus;
            this.DependentsUnder7 = model.DependentsUnder7;
            this.DependentsAbove7 = model.DependentsAbove7;
            this.LiteraryAbility = model.LiteraryAbility;
            this.College = model.College;
            this.Certifications = model.Certifications;
            this.Available = model.Available;
            this.Interval = model.Interval;
            this.AvailableAt = model.AvailableAt.HasValue ? model.AvailableAt.Value : DateTime.MinValue;
            this.LinkedInProfileLink = model.LinkedInProfileLink;
            this.YearsOfExperience = model.YearsOfExperience;
            this.Mobility = model.Mobility;
        }

        internal void UpdateLanguages(IEnumerable<Language> currentLanguages,
                                      IEnumerable<Language> updatedLanguages, 
                                      out ICollection<CandidateLanguage> languagesToRemove, 
                                      out ICollection<CandidateLanguage> languagesToAdd)
        {
            languagesToRemove = new Collection<CandidateLanguage>();
            languagesToAdd = new Collection<CandidateLanguage>();

            foreach (var curLanguage in currentLanguages)
            {
                var updatedLanguage = updatedLanguages.FirstOrDefault(x => x.Id.Equals(curLanguage.Id));

                if (updatedLanguage is null)
                    languagesToRemove.Add(this.CandidateLanguages.FirstOrDefault(x => x.LanguageId.Equals(curLanguage.Id)));
            }

            foreach (var modLanguage in updatedLanguages)
            {
                var currentLanguage = currentLanguages.FirstOrDefault(x => x.Id.Equals(modLanguage.Id));

                if (currentLanguage is null)
                    languagesToAdd.Add(new CandidateLanguage(this, modLanguage));
            }
        }

        internal void UpdateTechSkills(IEnumerable<TechSkill> currentTechSkills,         
                                       IEnumerable<TechSkill> updatedTechSkills,
                                       out ICollection<CandidateTechSkill> techSkillsToRemove, 
                                       out ICollection<CandidateTechSkill> techSkillsToAdd)
        { 
            techSkillsToRemove = new Collection<CandidateTechSkill>();
            techSkillsToAdd = new Collection<CandidateTechSkill>();

            foreach (var curTechSkill in currentTechSkills)
            {
                var updatedTechSkill = updatedTechSkills.FirstOrDefault(x => x.Id.Equals(curTechSkill.Id));

                if (updatedTechSkill is null)
                    techSkillsToRemove.Add(this.CandidateTechSkills.FirstOrDefault(x => x.TechSkillId.Equals(curTechSkill.Id)));
            }

            foreach (var modTechSkill in updatedTechSkills)
            {
                var currentTechSkill = currentTechSkills.FirstOrDefault(x => x.Id.Equals(modTechSkill.Id));

                if (currentTechSkill is null)
                    techSkillsToAdd.Add(new CandidateTechSkill(this, modTechSkill));
            }
        }

        internal void UpdateUploads(IEnumerable<Upload> currentUploads, 
                                    IEnumerable<Upload> updatedUploads, 
                                    out ICollection<CandidateUpload> uploadsToRemove, 
                                    out ICollection<CandidateUpload> uploadsToAdd)
        {
            uploadsToRemove = new Collection<CandidateUpload>();
            uploadsToAdd = new Collection<CandidateUpload>();

            foreach (var curUpload in currentUploads)
            {
                var updatedUpload = updatedUploads.FirstOrDefault(x => x.Id.Equals(curUpload.Id));

                if (updatedUpload is null)
                    uploadsToRemove.Add(this.CandidateUploads.FirstOrDefault(x => x.UploadId.Equals(curUpload.Id)));
            }

            foreach (var modUpload in updatedUploads)
            {
                var currentUpload = currentUploads.FirstOrDefault(x => x.Id.Equals(modUpload.Id));

                if (currentUpload is null)
                    uploadsToAdd.Add(new CandidateUpload(this, modUpload));
            }
        }

        internal void UpdateNaifas(IEnumerable<Naifas> currentNaifas,
                                   IEnumerable<Naifas> updateNaifas,
                                   out ICollection<CandidateNaifas> naifasToRemove,
                                   out ICollection<CandidateNaifas> naifasToAdd)
        {
            naifasToRemove = new Collection<CandidateNaifas>();
            naifasToAdd = new Collection<CandidateNaifas>();

            foreach (var curNaifas in currentNaifas)
            {
                var updatedNaifas = updateNaifas.FirstOrDefault(x => x.Id.Equals(curNaifas.Id));

                if (updatedNaifas is null)
                    naifasToRemove.Add(this.CandidateNaifas.FirstOrDefault(x => x.NaifasId.Equals(curNaifas.Id)));
            }

            foreach (var modNaifas in updateNaifas)
            {
                var currentNaifa = currentNaifas.FirstOrDefault(x => x.Id.Equals(modNaifas.Id));

                if (currentNaifa is null)
                    naifasToAdd.Add(new CandidateNaifas(this, modNaifas));
            }
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
           configuration.CreateMap<Candidate, CandidateModel>().ReverseMap();
        }

        #endregion

        #region Setters
       
        public void SetJobRole(JobRole jobRole)
        {
            this.JobRole = jobRole;
            this.JobRoleId = jobRole?.Id ?? null;
        }

        private void SetLocation(Location location)
        {
            this.Location = location;
            this.LocationId = location?.Id ?? null;
        }

        public void SetCandidateTechSkills(IEnumerable<TechSkillModel> techSkills)
        {
            foreach (var candidateTechSkill in this.CandidateTechSkills)
            {
                var techSkill = techSkills.FirstOrDefault(x => candidateTechSkill.TechSkillId.Equals(x.Id));
                candidateTechSkill.SetTechSkill(new TechSkill(techSkill.Id, techSkill.Category, techSkill.Description));
            }           
        }

        public void SetCandidateNaifas(IEnumerable<NaifasModel> naifas)
        {
            foreach (var candidateNaifas in this.CandidateNaifas)
            {
                var naifa = naifas.FirstOrDefault(x => candidateNaifas.NaifasId.Equals(x.Id));
                candidateNaifas.SetNaifas(new Naifas(naifa.Id, naifa.Question, naifa.Answer));
            }
        }

        #endregion
    }
}
