﻿using AutoMapper;
using Common.Models;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("tech_skill")]
    public class TechSkill : ITechSkill
    {       
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }        

        [Required]
        [Column("category")]
        public string Category { get; set; }

        [Required]
        [Column("description"),MaxLength(500)]
        public string Description { get; private set; }

        #endregion

        #region Constructors
        public TechSkill()
        { }

        public TechSkill(long id, string category, string description)
        {
            this.Id = id;
            this.Category = category;
            this.Description = description;
        }

        #endregion

        internal void Update(string category, string description)
        {
            this.Category = category;
            this.Description = description;
        }

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TechSkill, SelectionModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(ent => ent.Id))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(ent => ent.Description)).ReverseMap();

            configuration.CreateMap<TechSkill, TechSkillModel>().ReverseMap();
        }       

        #endregion
    }
}
