﻿namespace Domain.Entities.Constants
{
    public class InterviewStatus
    {       
        public const string IS_0001 = "Rascunho";        
        public const string IS_0002 = "Concluída";
        public const string IS_0003 = "Arquivada";
    }
}
