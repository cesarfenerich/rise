﻿namespace Domain.Entities.Constants
{
    public class CandidateTypes
    {
        public const string CDT_0001 = "Nenhum";
        public const string CDT_0002 = "Consultor";
        public const string CDT_0003 = "Ex-Consultor";
        public const string CDT_0004 = "Contratação Directa";
        public const string CDT_0005 = "Não Contratar";        
    }
}