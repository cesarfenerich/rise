﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Constants
{
    public class CandidateInterval
    {
        public const string CINT_0001 = "Imediata";
        public const string CINT_0002 = "2 semanas";
        public const string CINT_0003 = "4 semanas";
        public const string CINT_0004 = "8 semanas";
        public const string CINT_0005 = "Data Fixa";
    }
}
