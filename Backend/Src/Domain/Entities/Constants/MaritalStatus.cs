﻿namespace Domain.Entities.Constants
{
    public class MaritalStatus
    {
        public const string MS_0001 = "Solteiro";
        public const string MS_0002 = "Casado 1 Titular";
        public const string MS_0003 = "Casado 2 Titulares";
        public const string MS_0004 = "Divorciado";
        public const string MS_0005 = "Viúvo";
    }
}