﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Constants
{
    public sealed class PaymentFrequency
    {
        public const string PF_0001 = "Diário";
        public const string PF_0002 = "Mensal";
    }
}
