﻿namespace Domain.Entities.Constants
{    
    public class ContractTypes
    {        
        public const string CT_0001 = "Recibos Verdes";
        public const string CT_0002 = "Faturas";
        public const string CT_0003 = "Contrato a Termo Incerto (CTI)";
        public const string CT_0004 = "Contrato a Termo Certo (CTC)";
    }
}
