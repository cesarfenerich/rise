﻿namespace Domain.Entities.Constants
{
    public class CandidateAvailability
    {
        public const string CAV_0001 = "Nenhum";
        public const string CAV_0002 = "Disponível";
        public const string CAV_0003 = "Indisponível";               
    }
}