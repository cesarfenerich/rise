﻿namespace Domain.Entities.Constants
{
    public class LiteraryAbilities
    {
        public const string LA_0001 = "Nenhum";
        public const string LA_0002 = "Ensino Básico";
        public const string LA_0003 = "Ensino Secundário";
        public const string LA_0004 = "Licenciatura";
        public const string LA_0005 = "Mestrado";
        public const string LA_0006 = "Pós-graduação";
        public const string LA_0007 = "Doutoramento";
        public const string LA_0008 = "Bacharel";
        public const string LA_0009 = "Tecnólogo";
    }
}
