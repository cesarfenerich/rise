﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Constants
{
    public class LogOperations
    {
        public const string LO_0001 = "Login";
        public const string LO_0002 = "New Interview";
        public const string LO_0003 = "New Candidate";
        public const string LO_0004 = "New User";        
        public const string LO_0005 = "Updated Interview";
        public const string LO_0006 = "Updated Candidate";
        public const string LO_0007 = "Updated Interview";
        public const string LO_0008 = "Updated User";
        public const string LO_0009 = "Logout";
        public const string LO_0010 = "Interview Completed";
        public const string LO_0011 = "Interview update removed";
        public const string LO_0012 = "File update";
        public const string LO_0013 = "General";
        public const string LO_0014 = "Load Interview";
        public const string LO_0015 = "TechSkills Page Action";
    }
}
