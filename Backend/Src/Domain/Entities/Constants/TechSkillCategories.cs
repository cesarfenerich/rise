﻿namespace Domain.Entities.Constants
{
    public class TechSkillCategories
    {
        public const string TSC_0001 = "Perfil";
        public const string TSC_0002 = "Negócios";
        public const string TSC_0003 = "Programação";
        public const string TSC_0004 = "Sistemas";
        public const string TSC_0005 = "Bases de Dados";
        public const string TSC_0006 = "Testes";
        public const string TSC_0007 = "Business Intelligence";
        public const string TSC_0008 = "Frameworks";
        public const string TSC_0009 = "CMS";
        public const string TSC_0010 = "SAP";
        public const string TSC_0011 = "Metodologias";       
    }
}
