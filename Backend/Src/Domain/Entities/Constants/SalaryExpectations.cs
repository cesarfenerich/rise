﻿namespace Domain.Entities.Constants
{
    public class SalaryExpectations
    {
        public const string SEC_0001 = "Menor que 1000";
        public const string SEC_0002 = "Entre 1000 e 2000";
        public const string SEC_0003 = "Entre 2001 e 3000";
        public const string SEC_0004 = "Entre 3001 e 4000";
        public const string SEC_0005 = "Entre 4001 e 5000";
        public const string SEC_0006 = "Entre 5001 e 6000";
        public const string SEC_0007 = "Entre 6001 e 7000";
        public const string SEC_0008 = "Entre 7001 e 8000";
        public const string SEC_0009 = "Entre 8001 e 9000";
        public const string SEC_0010 = "Entre 9001 e 10000";
        public const string SEC_0011 = "Maior que 10000";         
    }
}
