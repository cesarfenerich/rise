﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Constants
{
    public class LogStatus
    {
        public const string LS_0001 = "Error";
        public const string LS_0002 = "Information";        
    }
}
