﻿namespace Domain.Entities.Constants
{
    public class NaifasCategories
    {
        public const string NCT_0001 = "Liderança";
        public const string NCT_0002 = "Trabalho em Equipa";
        public const string NCT_0003 = "Relações Interpessoais";
        public const string NCT_0004 = "Resiliência";
        public const string NCT_0005 = "Meticulosidade";
        public const string NCT_0006 = "Orientação para Resultados";
    }
}
