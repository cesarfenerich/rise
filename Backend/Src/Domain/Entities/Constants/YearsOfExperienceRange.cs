﻿namespace Domain.Entities.Constants
{
    public class YearsOfExperienceRange
    {
        public const string YOE_0001 = "0";
        public const string YOE_0002 = "Menos de 1 ano";
        public const string YOE_0003 = "1 a 3 anos";
        public const string YOE_0004 = "3 a 5 anos";
        public const string YOE_0005 = "Mais de 5 anos";        
    }
}
