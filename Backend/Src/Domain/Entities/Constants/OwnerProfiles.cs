﻿namespace Domain.Entities.Constants
{
    public class OwnerProfiles
    {
        public const string OP_0001 = "Administrator";
        public const string OP_0002 = "Owner";       
    }
}
