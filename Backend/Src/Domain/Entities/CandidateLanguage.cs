﻿using AutoMapper;
using Domain.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("candidate_language")]
    public class CandidateLanguage : ICandidateLanguage
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("candidate_id")]
        public long CandidateId { get; set; }

        [Required]
        [Column("language_id"), MaxLength(50)]
        public long LanguageId { get; private set; }        

        #endregion

        #region DependencyProperties

        public virtual Candidate Candidate { get; private set; }

        public virtual Language Language { get; private set; }

        #endregion

        #region Dependency

        public static Expression<Func<CandidateLanguage, object>>[] Dependences = { x => x.Candidate,
                                                                                    x => x.Language };

        #endregion

        #region Constructors

        public CandidateLanguage()
        { }

        public CandidateLanguage(Candidate candidate, Language language)
        {
            SetCandidate(candidate);
            SetLanguage(language);            
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            
        }

        #endregion

        #region Setters

        public void SetCandidate(Candidate candidate)
        {
            this.Candidate = candidate;
            this.CandidateId = candidate?.Id ?? 0;
        }

        public void SetLanguage(Language language)
        {
            this.Language = language;
            this.LanguageId = language?.Id ?? 0;
        }

        #endregion
    }
}
