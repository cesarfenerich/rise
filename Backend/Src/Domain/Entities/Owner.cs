﻿using AutoMapper;
using Domain.Entities.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Domain.Entities
{
    [Table("owner")]
    public class Owner : IOwner
    {
        #region Columns 

        [Required]
        [Column("id")]
        public long Id { get; set; }

        [Required]
        [Column("user_id")]
        public long UserId { get; set; }

        [Required]
        [Column("profile")]
        public string Profile { get; private set; } //TODO: GetOptions for OwnerProfile Constants    

        [Required]
        [Column("name"), MaxLength(50)]
        public string Name { get; private set; }        
        
        [Required]
        [Column("created_at")]
        public DateTime CreatedAt { get; private set; }

        [Required]
        [Column("active")]
        public bool Active { get; private set; }

        #endregion

        #region Properties

        #region DependencyProperties

        public virtual User User { get; private set; }
        public virtual ICollection<Interview> Interviews { get; private set; }
        public virtual ICollection<InterviewUpdate> InterviewUpdates { get; private set; }

        #endregion

        #endregion

        #region Dependency

        public static Expression<Func<Owner, object>>[] Dependences = { x => x.User};

        public static Expression<Func<Owner, object>>[] DependenceCollections = { x => x.Interviews};

        #endregion


        #region Constructors

        public Owner()
        { }

        public Owner(User user, string profile, string name)
        {
            this.User = user;
            this.Profile = profile;
            this.Name = name;
            this.CreatedAt = DateTime.UtcNow;
            this.Active = true;
        }

        internal void Update(string name, string profile, bool? active)
        {
            if(name != null)
                this.Name = name;

            if(profile != null)
                this.Profile = profile;
            
            if(active.HasValue)
                this.Active = active.Value;                
        }

        #endregion

        #region Mapping

        public void CustomMap(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Owner, OwnerModel>()
           .ForMember(dest => dest.Id, opt => opt.MapFrom(ent => ent.Id))
           .ForMember(dest => dest.Name, opt => opt.MapFrom(ent => ent.Name))
           .ForMember(dest => dest.UserId, opt => opt.MapFrom(ent => ent.User.Id))
           .ForMember(dest => dest.Email, opt => opt.MapFrom(ent => ent.User.Email))
           .ForMember(dest => dest.Password, opt => opt.MapFrom(ent => ent.User.Password))
           .ForMember(dest => dest.Profile, opt => opt.MapFrom(ent => ent.Profile)).ReverseMap(); 
        }

        #endregion
    }
}
