﻿SET FOREIGN_KEY_CHECKS=0;

DROP TABLE candidate;
DROP TABLE candidate_language;
DROP TABLE candidate_tech_skill;
DROP TABLE interview;
DROP TABLE interview_history;
DROP TABLE interview_update;
DROP TABLE job_role;
DROP TABLE language;
DROP TABLE location;
DROP TABLE owner;
DROP TABLE tech_skill;
DROP TABLE user;
DROP TABLE __EFMigrationsHistory;

SET FOREIGN_KEY_CHECKS=1;
