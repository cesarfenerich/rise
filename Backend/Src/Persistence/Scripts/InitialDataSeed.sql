﻿START TRANSACTION;

--Languages
INSERT INTO language (name) VALUES ('Português'),
                                   ('Inglês'),
                                   ('Francês'),
                                   ('Espanhol'),
                                   ('Alemão'),
                                   ('Italiano'),
                                   ('Russo'),
                                   ('Árabe');

--Locations
INSERT INTO location (city) VALUES ('Lisboa'),
                                   ('Porto'),
                                   ('Brasil'),
                                   ('UE'),
                                   ('Fora UE');

--JobRoles
INSERT INTO job_role (job_title) VALUES ('Fullstack Developer'),
                                        ('Backend Developer'),
                                        ('Front Developer'),
                                        ('Mobile Developer'),
                                        ('Project Manager'),
                                        ('Team Leader'),
                                        ('BI'),
                                        ('Analyst'),
                                        ('DevOps'),
                                        ('SAP'),
                                        ('ABAP'),
                                        ('SAP Funcional'),
                                        ('SYS Admin'),
                                        ('Data Scientist'),
                                        ('Suporte');


--TechSkills
  --TSC_0001 = "Perfil";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0001', 'Desenvolvimento Web'),
                                                        ('TSC_0001', 'SAP'),
                                                        ('TSC_0001', 'Desenvolvimento Mobile'),
                                                        ('TSC_0001', 'Análise'),
                                                        ('TSC_0001', 'Administração de Sistemas'),
                                                        ('TSC_0001', 'DevOps'),
                                                        ('TSC_0001', 'Gestão de Projetos'),
                                                        ('TSC_0001', 'Automação de Testes'),
                                                        ('TSC_0001', 'Testes Funcionais'),
                                                        ('TSC_0001', 'Análise BI'),
                                                        ('TSC_0001', 'Big Data'),
                                                        ('TSC_0001', 'Data Science');
  
  --TSC_0002 = "Negócios";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0002', 'Banca'),
                                                        ('TSC_0002', 'Seguros'),
                                                        ('TSC_0002', 'Telecomunicações'),
                                                        ('TSC_0002', 'Fintech'),
                                                        ('TSC_0002', 'Administração Pública'),
                                                        ('TSC_0002', 'Saúde'),
                                                        ('TSC_0002', 'Transportes'),
                                                        ('TSC_0002', 'Aviação'),
                                                        ('TSC_0002', 'Jogos'),
                                                        ('TSC_0002', 'Energias');
  
  --TSC_0003 = "Programação";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0003', 'C# .NET'),
                                                        ('TSC_0003', '.NET Core'),
                                                        ('TSC_0003', 'SAP ABAP'),
                                                        ('TSC_0003', 'ASP.NET'),
                                                        ('TSC_0003', 'VBA'),
                                                        ('TSC_0003', 'VB6'),
                                                        ('TSC_0003', 'VB.NET'),
                                                        ('TSC_0003', 'T-SQL'),
                                                        ('TSC_0003', 'PL-SQL'),
                                                        ('TSC_0003', 'Java'),
                                                        ('TSC_0003', 'PERL'),
                                                        ('TSC_0003', 'Python'),
                                                        ('TSC_0003', 'PHP'),
                                                        ('TSC_0003', 'Ruby On Rails'),
                                                        ('TSC_0003', 'Swift'),
                                                        ('TSC_0003', 'Objective-C'),
                                                        ('TSC_0003', 'DART'),
                                                        ('TSC_0003', 'Kotlin'),
                                                        ('TSC_0003', 'COBOL'),
                                                        ('TSC_0003', 'Javascript'),
                                                        ('TSC_0003', 'HTML'),
                                                        ('TSC_0003', 'CSS'),
                                                        ('TSC_0003', 'RPA'),
                                                        ('TSC_0003', 'C++');
  
  --TSC_0004 = "Sistemas";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0004', 'Linux'),
                                                        ('TSC_0004', 'Unix'),
                                                        ('TSC_0004', 'Microsoft'),
                                                        ('TSC_0004', 'Mainframe'),
                                                        ('TSC_0004', 'AS400'),
                                                        ('TSC_0004', 'Azure'),
                                                        ('TSC_0004', 'AWS');
  
  --TSC_0005 = "Base de Dados";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0005', 'MySQL'),
                                                        ('TSC_0005', 'SQL Server'),
                                                        ('TSC_0005', 'MariaDB'),
                                                        ('TSC_0005', 'Oracle'),
                                                        ('TSC_0005', 'MongoDB'),
                                                        ('TSC_0005', 'PostgreSQL'),
                                                        ('TSC_0005', 'SQL Azure');
  
  --TSC_0006 = "Testes";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0006', 'ISTQB'),
                                                        ('TSC_0006', 'Selenium'),
                                                        ('TSC_0006', 'Cucumber'),
                                                        ('TSC_0006', 'Swagger'),
                                                        ('TSC_0006', 'SOAP UI');
  
  --TSC_0007 = "Business Intelligence";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0007', 'SSAS'),
                                                        ('TSC_0007', 'SSIS'),
                                                        ('TSC_0007', 'SSRS'),
                                                        ('TSC_0007', 'ODI'),
                                                        ('TSC_0007', 'OBIEE'),
                                                        ('TSC_0007', 'Qlik View'),
                                                        ('TSC_0007', 'Qlik Sense'),
                                                        ('TSC_0007', 'PowerBI'),
                                                        ('TSC_0007', 'Power Center'),
                                                        ('TSC_0007', 'Talend'),
                                                        ('TSC_0007', 'Tableau'),
                                                        ('TSC_0007', 'MicroStrategy'),
                                                        ('TSC_0007', 'SAS Guide'),
                                                        ('TSC_0007', 'SAS DIS'),
                                                        ('TSC_0007', 'Data Stage'),
                                                        ('TSC_0007', 'Hyperion');

  --TSC_0008 = "Frameworks";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0008', 'Hibernate'),
                                                        ('TSC_0008', 'NHibernate'),
                                                        ('TSC_0008', 'JDBC'),
                                                        ('TSC_0008', 'Spring'),                                                       
                                                        ('TSC_0008', 'EntityFramework'),
                                                        ('TSC_0008', 'LINQ'),
                                                        ('TSC_0008', 'Bootstrap'),
                                                        ('TSC_0008', 'React'),
                                                        ('TSC_0008', 'EmberJS'),
                                                        ('TSC_0008', 'MeteorJS'),
                                                        ('TSC_0008', 'Angular'),
                                                        ('TSC_0008', 'ExtJS'),
                                                        ('TSC_0008', 'VueJS'),
                                                        ('TSC_0008', 'MVC'),
                                                        ('TSC_0008', 'GIT'),
                                                        ('TSC_0008', 'SVP'),
                                                        ('TSC_0008', 'Hadoop'),
                                                        ('TSC_0008', 'Spark'),
                                                        ('TSC_0008', 'Apache Tomcat'),
                                                        ('TSC_0008', 'Sharepoint'),
                                                        ('TSC_0008', 'Kubernetes'),
                                                        ('TSC_0008', 'Docker'),
                                                        ('TSC_0008', 'Xamarin'),
                                                        ('TSC_0008', 'Apache Cordova'),
                                                        ('TSC_0008', 'Ionic');
  
  --TSC_0009 = "CMS";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0009', 'Umbraco'),
                                                        ('TSC_0009', 'Laravel'),
                                                        ('TSC_0009', 'Symfony'),
                                                        ('TSC_0009', 'Magento'),
                                                        ('TSC_0009', 'Zend'),
                                                        ('TSC_0009', 'CakePHP'),
                                                        ('TSC_0009', 'CodeIgniter'),
                                                        ('TSC_0009', 'Drupal'),
                                                        ('TSC_0009', '.NET Nuke');
  
  --TSC_0010 = "SAP";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0010', 'FI/CO'),
                                                        ('TSC_0010', 'MM'),
                                                        ('TSC_0010', 'LO'),
                                                        ('TSC_0010', 'HANA'),
                                                        ('TSC_0010', 'S/4'),
                                                        ('TSC_0010', 'HR'),
                                                        ('TSC_0010', 'SD'),
                                                        ('TSC_0010', 'BW'),
                                                        ('TSC_0010', 'FIORI'),
                                                        ('TSC_0010', 'BO'),
                                                        ('TSC_0010', 'BI');
  
  --TSC_0011 = "Metodologias";
  INSERT INTO tech_skill (category, description) VALUES ('TSC_0011', 'Scrum'),
                                                        ('TSC_0011', 'Kanban'),
                                                        ('TSC_0011', 'Agile'),
                                                        ('TSC_0011', 'Waterfall');

--"User"
INSERT INTO user (id, email, password) VALUES (1,'admin@onrising.com', 'admin'),
                                              (2,'owner@onrising.com', 'owner'),
                                              (3,'ines.borges@onrising.com','rise2020'),
                                              (4,'ines.valente@onrising.com','rise2020'),	
                                              (5,'joao.henriques@onrising.com','rise2020'),
                                              (6,'madalena.ferreira@onrising.com','rise2020'),	
                                              (7,'mariana.breyner@onrising.com','rise2020'),
                                              (8,'natalia.silva@onrising.com','rise2020'),
                                              (9,'nuno.godinho@onrising.com','rise2020'),
                                              (10,'pedro.rodrigues@onrising.com','rise2020'),
                                              (11,'sandra.santos@onrising.com','rise2020'),
                                              (12,'sara.marques@onrising.com','rise2020'),	
                                              (13,'viviane.lino@onrising.com','rise2020'),
                                              (14,'rodrigo.paiva@onrising.com','rise2020'),
                                              (15,'rui.madruga@onrising.com','rise2020'),
                                              (16,'nadia.ferreira@onrising.com','rise2020'),
                                              (17,'manuela.peixoto@onrising.com','rise2020');

--"Owner"
INSERT INTO owner (user_id, profile, name, created_at, active) VALUES (1,'OP_0001', 'Owner Admin', NOW(), true),
                                                                      (2,'OP_0002', 'Owner Teste', NOW(), true),
                                                                      (3,'OP_0002', 'Inês Reis Borges',NOW(),1),
                                                                      (4,'OP_0002', 'Inês Castelo Branco Pulido Valente',NOW(),1),
                                                                      (5,'OP_0002', 'João Miguel Zarcos Ruivo Henriques',NOW(),1),
                                                                      (6,'OP_0002', 'Madalena Albuquerque Ferreira',NOW(),1),
                                                                      (7,'OP_0002', 'Mariana Mello Breyner',NOW(),1),
                                                                      (8,'OP_0002', 'Natália dos Santos Silva',NOW(),1),
                                                                      (9,'OP_0002', 'Nuno Alexandre Afonso Godinho',NOW(),1),
                                                                      (10,'OP_0002','Pedro Rodrigues',NOW(),1),
                                                                      (11,'OP_0002','Sandra Santos',NOW(),1),
                                                                      (12,'OP_0002','Sara Marques',NOW(),1),
                                                                      (13,'OP_0002','Viviane Almeida de Sousa Lino',NOW(),1),
                                                                      (14,'OP_0002','Rodrigo Paiva',NOW(),1),
                                                                      (15,'OP_0002','Rui Madruga',NOW(),1),
                                                                      (16,'OP_0001','Nádia Ferreira',NOW(),1),
                                                                      (17,'OP_0001','Manuela Peixoto',NOW(),1);
COMMIT;
