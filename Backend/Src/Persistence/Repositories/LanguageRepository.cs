﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class LanguageRepository : GenericRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(AppDbContext context) : base(context)
        {  }

        public IEnumerable<Language> GetAllDistinctLanguages()
            => GetAll().Distinct().AsEnumerable();

        public Language GetByName(string name)
            => GetAll().FirstOrDefault(x => x.Name.Contains(name));        

        public IEnumerable<Language> GetLanguagesByIds(IEnumerable<long> ids)
        {
            return FilterBy(x => ids.Contains(x.Id)).ToList();
        }
    }
}
