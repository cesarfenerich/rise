﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class TechSkillRepository : GenericRepository<TechSkill>, ITechSkillRepository
    {
        public TechSkillRepository(AppDbContext context) : base(context)
        {  }

        public IEnumerable<TechSkill> GetAllDistinctTechSkills()
            => GetAll().Distinct().ToList();
       
        public TechSkill GetByCategoryAndDescription(string categoryId, string description)
           => FilterBy(x => x.Category.Equals(categoryId) && x.Description.Contains(description)).FirstOrDefault();

        public IEnumerable<TechSkill> GetTechSkillsByCategory(string categoryId)
            => FilterBy(x => x.Category.Contains(categoryId, StringComparison.InvariantCultureIgnoreCase)).ToList();

        public IEnumerable<TechSkill> GetTechSkillsByIds(IEnumerable<long> techSkillIds)
            => FilterBy(x => techSkillIds.Contains(x.Id)).ToList();        
    }
}
