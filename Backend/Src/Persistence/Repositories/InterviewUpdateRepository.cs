﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class InterviewUpdateRepository : GenericRepository<InterviewUpdate>, IInterviewUpdateRepository
    {
        public InterviewUpdateRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(InterviewUpdate.Dependences);
        }

        public IEnumerable<InterviewUpdate> GetUpdatesByInterview(long interviewId)
        {
            return FilterBy(x => x.InterviewId.Equals(interviewId)).ToList();
        }
    }
}
