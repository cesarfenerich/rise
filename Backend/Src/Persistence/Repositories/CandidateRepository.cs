﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class CandidateRepository : GenericRepository<Candidate>, ICandidateRepository
    {
        public CandidateRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(Candidate.Dependences);
        }

        public Candidate GetByNameAndEmail(string name, string email)
        {
            IncludeDependency(Candidate.DependenceCollections);

            return FilterBy(x => x.Name.Equals(name) || x.Email.Contains(email)).FirstOrDefault();            
        }      

        public Candidate GetCandidateById(long candidateId)
        {
            IncludeDependency(Candidate.DependenceCollections);

            return FilterBy(x => x.Id.Equals(candidateId)).FirstOrDefault();
        }

        public IEnumerable<Interview> GetInterviewsByCandidate(long candidateId)
        {
            IncludeDependency(Candidate.DependenceCollections);

            return FilterBy(x => x.Id.Equals(candidateId)).FirstOrDefault().Interviews.ToList();
        }

        public IEnumerable<string> SearchByName(string search)
            => FilterBy(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Name).Distinct().Take(3).ToList();

        public IEnumerable<string> SearchByEmail(string search)
           => FilterBy(x => x.Email.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Email).Distinct().Take(3).ToList();

        public ICollection<Candidate> GetByJobRole(long jobRoleId)
        {
            IncludeDependency(Candidate.DependenceCollections);

            return FilterBy(x => jobRoleId.Equals(x.JobRoleId)).ToList();
        }
    }
}
