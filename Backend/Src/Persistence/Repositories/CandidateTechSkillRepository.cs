﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class CandidateTechSkillRepository : GenericRepository<CandidateTechSkill>, ICandidateTechSkillRepository
    {
        public CandidateTechSkillRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(CandidateTechSkill.Dependences);
        }

        public CandidateTechSkill GetByCandidateAndTechSkill(long candidateId, long idTechSkill)
         => FilterBy(x => candidateId.Equals(x.CandidateId) &&
                          idTechSkill.Equals(x.TechSkillId)).FirstOrDefault();       

        public IEnumerable<CandidateTechSkill> GetByTechSkillIds(IEnumerable<long> techSkillIds)
            => FilterBy(x => techSkillIds.Contains(x.TechSkillId)).ToList();

        public IEnumerable<CandidateTechSkill> GetTechSkillsByCandidate(long candidateId)
            => FilterBy(x => candidateId.Equals(x.CandidateId)).ToList();

        public IEnumerable<CandidateTechSkill> GetTechSkillsByCandidates(IEnumerable<long> candidateIds)
            => FilterBy(x => candidateIds.Contains(x.CandidateId)).ToList();

        public ICollection<CandidateTechSkill> GetByTechSkill(long techSkillId)
            => FilterBy(x => techSkillId.Equals(x.TechSkillId)).ToList();

    }
}
