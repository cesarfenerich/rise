﻿using Common.Extensions;
using Common.Interfaces;
using Common.Models;
using Common.Paging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IGenericEntity
    {
        private readonly AppDbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public List<Expression<Func<TEntity, object>>> IncludeStatements { get; private set; }

        public GenericRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll() => Includes(_dbSet.AsNoTracking());

        public IQueryable<TEntity> FilterBy(Expression<Func<TEntity, bool>> filterCriteria)
           => Includes(_dbSet.Where(filterCriteria)
                             .AsNoTracking());

        public PagedResult<TEntity> GetAllPaged(int page, int pageSize)
            => GetAll().GetPaged<TEntity>(page, pageSize);

        public PagedResult<TEntity> GetPagedByFilter(Expression<Func<TEntity, bool>> filterCriteria,
                                                     int page, int pageSize)
            => FilterBy(filterCriteria).GetPaged<TEntity>(page, pageSize);

        public TEntity GetById(long id)
            => FilterBy(e => e.Id.Equals(id))
                                 .FirstOrDefault();

        public Task<TEntity> Create(TEntity entity)
        {            
            _dbContext.Entry(entity).State = EntityState.Added;
            _dbContext.SaveChanges();
            return Task.FromResult(entity);
        }        

        public Task<TEntity> Update(TEntity entity)
        {            
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return Task.FromResult(entity);
        }

        public Task Delete(long id)
        {
            var entity = _dbContext.Set<TEntity>().Find(id);
            _dbContext.Entry(entity).State = EntityState.Deleted;
            _dbContext.SaveChanges();
            return Task.CompletedTask;
        }

        public Task<TEntity> UpdateAndDetach(TEntity entity)
        {
            Update(entity).Wait();
            _dbContext.Entry(entity).State = EntityState.Detached;
            return Task.FromResult(entity);
        }      

        #region Range Handlers

        public Task CreateMany(ICollection<TEntity> entities)
        {
            foreach (var entity in entities.GetValueOrDefault<TEntity>())
            {                
                _dbContext.Entry(entity).State = EntityState.Added;
            }

            _dbContext.SaveChanges();

            return Task.CompletedTask;
        }

        public Task UpdateMany(ICollection<TEntity> entities)
        {
            foreach (var entity in entities.GetValueOrDefault<TEntity>())
            {                
                _dbContext.Entry(entity).State = EntityState.Modified;
            }

            _dbContext.SaveChanges();

            return Task.CompletedTask;
        }

        public Task DeleteMany(ICollection<TEntity> entities)
        {
            foreach (var entity in entities.GetValueOrDefault<TEntity>())
            {                
                _dbContext.Entry(entity).State = EntityState.Deleted;
            }

            _dbContext.SaveChanges();

            return Task.CompletedTask;
        }

        #endregion 

        #region Utils       

        private IQueryable<TEntity> Includes(IQueryable<TEntity> query)
        {
            foreach (var include in IncludeStatements.GetValueOrDefault())
            {
                query = query.Include(include);
            }

            return query;
        }

        public void IncludeDependency(Expression<Func<TEntity, object>>[] dependencyIncludes)
        {
            if (IncludeStatements is null)
                IncludeStatements = dependencyIncludes.ToList();
            else
            {
                foreach (var dependency in dependencyIncludes)
                {
                    IncludeStatements.Add(dependency);
                }
            }
        }       

        #endregion
    }

}
