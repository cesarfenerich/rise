﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class CandidateUploadRepository : GenericRepository<CandidateUpload>, ICandidateUploadRepository
    {
        public CandidateUploadRepository(AppDbContext context) : base(context)
        {

            IncludeDependency(CandidateUpload.Dependences);
        }

        public CandidateUpload GetByCandidateAndUpload(long candidateId, long idUpload)
            => FilterBy(x => candidateId.Equals(x.CandidateId) &&
                             idUpload.Equals(x.UploadId)).FirstOrDefault();

        public IEnumerable<CandidateUpload> GetUploadsByCandidate(long candidateId)
            => FilterBy(x => candidateId.Equals(x.Candidate.Id)).ToList();

        public IEnumerable<CandidateUpload> GetByUploads(IEnumerable<long> uploadIds)
            => FilterBy(x => uploadIds.Contains(x.Upload.Id)).ToList();

        public IEnumerable<CandidateUpload> GetAllUploads()
            => GetAll().ToList();
    }
}
