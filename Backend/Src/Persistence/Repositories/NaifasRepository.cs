﻿using Domain.Entities;
using Domain.Entities.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Repositories
{
    public class NaifasRepository : GenericRepository<Naifas>, INaifasRepository
    {
        public NaifasRepository(AppDbContext context) : base(context)
        { }

        public IEnumerable<Naifas> GetNaifasByIds(IEnumerable<long> naifaIds)
           => FilterBy(x => naifaIds.Contains(x.Id)).ToList();
    }
}
