﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class JobRoleRepository : GenericRepository<JobRole>, IJobRoleRepository
    {
        public JobRoleRepository(AppDbContext context) : base(context)
        { }       

        public IEnumerable<JobRole> GetAllDistinctJobRoles()
            => GetAll().Distinct().ToList();

        public JobRole GetByJobTitle(string jobTitle)
            => FilterBy(x => x.JobTitle.Contains(jobTitle, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

        public IEnumerable<string> SearchByJobTitle(string search)
            => FilterBy(x => x.JobTitle.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Select(y => y.JobTitle).Distinct().Take(3).ToList();        
    }
}
