﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Persistence.Repositories
{
    public class UploadRepository : GenericRepository<Upload>, IUploadRepository
    {
        public UploadRepository(AppDbContext context) : base(context)
        {  }        

        public IEnumerable<Upload> GetUploadByIds(IEnumerable<long> uploadIds)
            => FilterBy(x => uploadIds.Contains(x.Id)).ToList();

        public IEnumerable<Upload> GetByFileName(string fileName)
            => FilterBy(x => x.FileName.Equals(fileName)).OrderByDescending(y => y.UploadedAt).ToList();
    }
}
