﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class OwnerRepository : GenericRepository<Owner>, IOwnerRepository
    {
        public OwnerRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(Owner.Dependences);
        }

        public IEnumerable<Owner> GetAllActiveOwners(bool active = true)
            => FilterBy(x => x.Active == active).ToList();

        public IEnumerable<Owner> GetAllOwners()
            => GetAll().ToList();

        public Owner GetOwnerById(long id)
        {
            IncludeDependency(Owner.DependenceCollections);

            return FilterBy(x => x.Id.Equals(id)).FirstOrDefault();
        }
    }
}
