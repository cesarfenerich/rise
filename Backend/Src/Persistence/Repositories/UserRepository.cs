﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Linq;

namespace Persistence.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext readContext) : base(readContext)
        {
            IncludeDependency(User.Dependences);
        }    

        public User GetUserById(long userId)
        {       
            return FilterBy(x => x.Id.Equals(userId)).FirstOrDefault();
        }

        public bool ValidateUserToAuthenticate(long id)
        {
            var user = GetById(id);

            if (user != null)
                return true;

            return false;
        }

        public User GetByEmail(string email)
        {
            var filteredUsers = FilterBy(x => x.Email.Equals(email));

            return filteredUsers.FirstOrDefault();
        }
    }
}
