﻿using Domain.Entities;
using Domain.Entities.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Repositories
{
    public class CandidateNaifasRepository : GenericRepository<CandidateNaifas>, ICandidateNaifasRepository
    {
        public CandidateNaifasRepository(AppDbContext context) : base(context)
        {

            IncludeDependency(CandidateNaifas.Dependences);
        }

        public CandidateNaifas GetByCandidateAndNaifas(long candidateId, long idNaifas)
           => FilterBy(x => candidateId.Equals(x.CandidateId) &&
                            idNaifas.Equals(x.NaifasId)).FirstOrDefault();

        public IEnumerable<CandidateNaifas> GetNaifasByCandidate(long candidateId)
            => FilterBy(x => candidateId.Equals(x.Candidate.Id)).ToList();
    }
}
