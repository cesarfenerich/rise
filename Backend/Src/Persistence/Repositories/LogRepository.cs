﻿using Domain.Entities;
using Domain.Entities.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Repositories
{
    public class LogRepository : GenericRepository<Log>, ILogRepository
    {
        public LogRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Log> GetAllLogs()
           => GetAll();

        public Log GetLogById(long id) 
            => FilterBy(x => x.Id.Equals(id)).FirstOrDefault();

        public IEnumerable<Log> GetLogsByStatus(string status)
            => FilterBy(x => x.Status.Equals(status)).ToList();

        public IEnumerable<Log> GetLogsByOperation(string operation)
            => FilterBy(x => x.Operation.Equals(operation)).ToList();

        public IEnumerable<Log> GetLogsByDate(DateTime date)
            => FilterBy(x => x.CreatedAt.Equals(date)).ToList();

        public IEnumerable<Log> GetLogsByUsername(string username)
            => FilterBy(x => x.Username.Equals(username)).ToList();
    }
}
