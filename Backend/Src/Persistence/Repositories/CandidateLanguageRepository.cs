﻿using Domain.Entities;
using Domain.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class CandidateLanguageRepository : GenericRepository<CandidateLanguage>, ICandidateLanguageRepository
    {
        public CandidateLanguageRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(CandidateLanguage.Dependences);
        }

        public CandidateLanguage GetByCandidateAndLanguage(long candidateId, long idLanguage)
            => FilterBy(x => candidateId.Equals(x.CandidateId) && 
                             idLanguage.Equals(x.LanguageId)).FirstOrDefault();

        public IEnumerable<CandidateLanguage> GetCandidatesByLanguages(IEnumerable<long> idLanguages)
            => FilterBy(x => idLanguages.Contains(x.LanguageId));

        public IEnumerable<CandidateLanguage> GetLanguagesByCandidate(long id)
            => FilterBy(x => id.Equals(x.CandidateId)).ToList();
    }
}
