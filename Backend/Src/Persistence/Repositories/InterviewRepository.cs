﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class InterviewRepository : GenericRepository<Interview>, IInterviewRepository
    {
        public InterviewRepository(AppDbContext context) : base(context)
        {
            IncludeDependency(Interview.Dependences);
        }

        public Interview GetInterviewById(long interviewId)
        {
            IncludeDependency(Interview.DependenceCollections);

            return FilterBy(x => x.Id.Equals(interviewId)).FirstOrDefault();
        }

        public IEnumerable<Interview> GetLastInterviews()
        {
            IncludeDependency(Interview.DependenceCollections);

            return GetAll().DistinctBy(x => x.CandidateId).OrderByDescending(x => x.CreatedAt).ToList();
        }

        public IEnumerable<Interview> GetLastNotArchivedInterview()
        {
            IncludeDependency(Interview.DependenceCollections);
            return FilterBy(x=>x.Archived == false).DistinctBy(x => x.CandidateId).OrderByDescending(x => x.CreatedAt).ToList();
        }


        public IEnumerable<Interview> SearchByCandidateName(string search)
        {
            IncludeDependency(Interview.DependenceCollections);  
            
            var res = FilterBy(x => x.Candidate.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToList();

            return res;      
        }

        public IEnumerable<Interview> SearchByCandidateEmail(string search)
        {
            IncludeDependency(Interview.DependenceCollections);

            var all = GetAll();
            var result = new List<Interview>();

            foreach (var interview in all)
            {
                if (interview.Candidate.Email.InsensitiveContains(search))
                    result.Add(interview);
            }

            return result;
        }


        public IEnumerable<Interview> SearchByCandidateJobRole(string search)
        {
            IncludeDependency(Interview.DependenceCollections);

            var all = GetAll();
            var result = new List<Interview>();

            foreach (var interview in all)
            {
                if (!interview.Candidate.JobRole?.JobTitle.IsNullOrWhitespaceOrEmpty() ?? false)
                {
                    if (interview.Candidate.JobRole.JobTitle.Contains(search))
                        result.Add(interview);
                }
            }

            return result;
        }

        public IEnumerable<Interview> SearchByCandidateAvaliability(string availability)
        {
            IncludeDependency(Interview.DependenceCollections);

            return FilterBy(x => x.Candidate.Available.Equals(availability)).ToList();
        }

        public IEnumerable<Interview> SearchByExpectedSalary(string expectedSalary)
        {
            decimal minValue = 0;
            decimal maxValue = 0;
            
            switch (expectedSalary)
            {               
                case "SEC_0001": maxValue = 999; break;
                case "SEC_0002": minValue = 1000; maxValue = 2000; break;
                case "SEC_0003": minValue = 2001; maxValue = 3000; break;
                case "SEC_0004": minValue = 3001; maxValue = 4000; break;
                case "SEC_0005": minValue = 4001; maxValue = 5000; break;
                case "SEC_0006": minValue = 5001; maxValue = 6000; break;
                case "SEC_0007": minValue = 6001; maxValue = 7000; break;
                case "SEC_0008": minValue = 7001; maxValue = 8000; break;
                case "SEC_0009": minValue = 8001; maxValue = 9000; break;
                case "SEC_0010": minValue = 9001; maxValue = 10000; break;
                case "SEC_0011": minValue = 10001; maxValue = 999999999; break;
            }

            return FilterBy(x => x.ExpectationSalary >= minValue && x.ExpectationSalary <= maxValue).ToList();
        }

        public IEnumerable<Interview> SearchByYearOfExperience(string yearOfExperience)
        {
            decimal minValue = -1;
            decimal maxValue = 0;

            switch (yearOfExperience)
            {
                case "YOE_0001": maxValue = 0; break;
                case "YOE_0002": maxValue = 1; break;
                case "YOE_0003": minValue = 1; maxValue = 3; break;
                case "YOE_0004": minValue = 3; maxValue = 5; break;
                case "YOE_0005": minValue = 5; maxValue = 999; break;
                default: maxValue = 0; break;
            }

            return FilterBy(x => x.Candidate.YearsOfExperience > minValue && 
                                 x.Candidate.YearsOfExperience <= maxValue).ToList();
        }

        public IEnumerable<Interview> SearchByContractType(string contractType)
            => FilterBy(x => x.ContractType.Equals(contractType)).ToList();

        public IEnumerable<Interview> SearchByOwner(long ownerId)
            => FilterBy(x => x.OwnerId.Equals(ownerId)).ToList();

        public IEnumerable<Interview> SearchByCandidateLinkdin(string search)
        {
            IncludeDependency(Interview.DependenceCollections);

            return FilterBy(x => x.Candidate.LinkedInProfileLink.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public IEnumerable<Interview> SearchByCandidateLocation(string search)
        {
            IncludeDependency(Interview.DependenceCollections);

            return FilterBy(x => x.Candidate.Location.City.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public IEnumerable<Interview> GetInterviewsByCandidates(IEnumerable<long> candidateIds)
            => FilterBy(x => candidateIds.Contains(x.CandidateId)).ToList();

        public IEnumerable<Interview> GetLastInterviewsByCandidate(long candidateId)
            => FilterBy(x => x.CandidateId.Equals(candidateId)).OrderByDescending(x => x.CreatedAt).Take(3).ToList();

        public int GetInterviewsCount()
            => GetAll().Count();       
    }
}
