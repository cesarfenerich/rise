﻿using Domain.Entities;
using Domain.Entities.Interfaces;

namespace Persistence.Repositories
{
    public class InterviewHistoryRepository : GenericRepository<InterviewHistory>, IInterviewHistoryRepository
    {
        public InterviewHistoryRepository(AppDbContext context) : base(context)
        {  }
    }
}
