﻿using Common.Extensions;
using Domain.Entities;
using Domain.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Persistence.Repositories
{
    public class LocationRepository : GenericRepository<Location>, ILocationRepository
    {
        public LocationRepository(AppDbContext context) : base(context)
        {  }

        public IEnumerable<Location> GetAllDistinctLocations()
            => GetAll().Distinct().ToList();

        public Location GetByCity(string city)
            => FilterBy(x => x.City.Contains(city, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
    }
}
