﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "job_role",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    job_title = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_job_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "language",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_language", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "location",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    city = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "tech_skill",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    category = table.Column<string>(nullable: false),
                    description = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tech_skill", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    email = table.Column<string>(maxLength: 50, nullable: false),
                    password = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "candidate",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    job_role_id = table.Column<long>(nullable: false),
                    location_id = table.Column<long>(nullable: false),
                    type = table.Column<string>(maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    born_date = table.Column<DateTime>(nullable: false),
                    phone_number = table.Column<string>(maxLength: 15, nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    citizenship = table.Column<string>(maxLength: 50, nullable: true),
                    marital_status = table.Column<string>(maxLength: 50, nullable: true),
                    dependents_under7 = table.Column<int>(nullable: false),
                    dependents_above7 = table.Column<int>(nullable: false),
                    linkein_profile_link = table.Column<string>(maxLength: 150, nullable: true),
                    cvfile_path = table.Column<string>(maxLength: 150, nullable: true),
                    cvonrising_file_path = table.Column<string>(maxLength: 150, nullable: true),
                    naifas_file_path = table.Column<string>(maxLength: 150, nullable: true),
                    college = table.Column<string>(maxLength: 100, nullable: true),
                    certifications = table.Column<string>(maxLength: 200, nullable: true),
                    available = table.Column<bool>(nullable: false),
                    available_at = table.Column<DateTime>(nullable: false),
                    create_at = table.Column<DateTime>(nullable: false),
                    active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_candidate", x => x.id);
                    table.ForeignKey(
                        name: "FK_candidate_job_role_job_role_id",
                        column: x => x.job_role_id,
                        principalTable: "job_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_candidate_location_location_id",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "owner",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(nullable: false),
                    profile = table.Column<string>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_owner", x => x.id);
                    table.ForeignKey(
                        name: "FK_owner_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "candidate_language",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    candidate_id = table.Column<long>(nullable: false),
                    language_id = table.Column<long>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_candidate_language", x => x.id);
                    table.ForeignKey(
                        name: "FK_candidate_language_candidate_candidate_id",
                        column: x => x.candidate_id,
                        principalTable: "candidate",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_candidate_language_language_language_id",
                        column: x => x.language_id,
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "candidate_tech_skill",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    candidate_id = table.Column<long>(nullable: false),
                    tech_skill_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_candidate_tech_skill", x => x.id);
                    table.ForeignKey(
                        name: "FK_candidate_tech_skill_candidate_candidate_id",
                        column: x => x.candidate_id,
                        principalTable: "candidate",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_candidate_tech_skill_tech_skill_tech_skill_id",
                        column: x => x.tech_skill_id,
                        principalTable: "tech_skill",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "interview",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    candidate_id = table.Column<long>(nullable: false),
                    owner_id = table.Column<long>(nullable: false),
                    status = table.Column<string>(maxLength: 50, nullable: false),
                    contract_type = table.Column<string>(maxLength: 50, nullable: false),
                    reason_leaving = table.Column<string>(maxLength: 500, nullable: true),
                    current_salary = table.Column<decimal>(nullable: false),
                    current_salary_net = table.Column<bool>(nullable: false),
                    expectation_salary = table.Column<decimal>(nullable: false),
                    expectation_salary_net = table.Column<bool>(nullable: false),
                    professional_experience = table.Column<string>(maxLength: 2000, nullable: true),
                    created_at = table.Column<DateTime>(nullable: false),
                    active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interview", x => x.id);
                    table.ForeignKey(
                        name: "FK_interview_candidate_candidate_id",
                        column: x => x.candidate_id,
                        principalTable: "candidate",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_interview_owner_owner_id",
                        column: x => x.owner_id,
                        principalTable: "owner",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "interview_history",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    interview_id = table.Column<long>(nullable: false),
                    updated_field = table.Column<string>(maxLength: 50, nullable: true),
                    old_value = table.Column<string>(maxLength: 2000, nullable: true),
                    created_at = table.Column<DateTime>(nullable: false),
                    active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interview_history", x => x.id);
                    table.ForeignKey(
                        name: "FK_interview_history_interview_interview_id",
                        column: x => x.interview_id,
                        principalTable: "interview",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "interview_update",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    interview_id = table.Column<long>(nullable: false),
                    resume = table.Column<string>(maxLength: 1000, nullable: false),
                    created_at = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interview_update", x => x.id);
                    table.ForeignKey(
                        name: "FK_interview_update_interview_interview_id",
                        column: x => x.interview_id,
                        principalTable: "interview",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_candidate_job_role_id",
                table: "candidate",
                column: "job_role_id");

            migrationBuilder.CreateIndex(
                name: "IX_candidate_location_id",
                table: "candidate",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "IX_candidate_language_candidate_id",
                table: "candidate_language",
                column: "candidate_id");

            migrationBuilder.CreateIndex(
                name: "IX_candidate_language_language_id",
                table: "candidate_language",
                column: "language_id");

            migrationBuilder.CreateIndex(
                name: "IX_candidate_tech_skill_candidate_id",
                table: "candidate_tech_skill",
                column: "candidate_id");

            migrationBuilder.CreateIndex(
                name: "IX_candidate_tech_skill_tech_skill_id",
                table: "candidate_tech_skill",
                column: "tech_skill_id");

            migrationBuilder.CreateIndex(
                name: "IX_interview_candidate_id",
                table: "interview",
                column: "candidate_id");

            migrationBuilder.CreateIndex(
                name: "IX_interview_owner_id",
                table: "interview",
                column: "owner_id");

            migrationBuilder.CreateIndex(
                name: "IX_interview_history_interview_id",
                table: "interview_history",
                column: "interview_id");

            migrationBuilder.CreateIndex(
                name: "IX_interview_update_interview_id",
                table: "interview_update",
                column: "interview_id");

            migrationBuilder.CreateIndex(
                name: "IX_owner_user_id",
                table: "owner",
                column: "user_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "candidate_language");

            migrationBuilder.DropTable(
                name: "candidate_tech_skill");

            migrationBuilder.DropTable(
                name: "interview_history");

            migrationBuilder.DropTable(
                name: "interview_update");

            migrationBuilder.DropTable(
                name: "language");

            migrationBuilder.DropTable(
                name: "tech_skill");

            migrationBuilder.DropTable(
                name: "interview");

            migrationBuilder.DropTable(
                name: "candidate");

            migrationBuilder.DropTable(
                name: "owner");

            migrationBuilder.DropTable(
                name: "job_role");

            migrationBuilder.DropTable(
                name: "location");

            migrationBuilder.DropTable(
                name: "user");
        }
    }
}
