﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class AddingUploadEntityAndAdjustments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_candidate_job_role_job_role_id",
                table: "candidate");

            migrationBuilder.DropForeignKey(
                name: "FK_candidate_location_location_id",
                table: "candidate");

            migrationBuilder.DropColumn(
                name: "file_name",
                table: "candidate_upload");

            migrationBuilder.DropColumn(
                name: "file_path",
                table: "candidate_upload");

            migrationBuilder.DropColumn(
                name: "uploaded_at",
                table: "candidate_upload");

            migrationBuilder.AddColumn<long>(
                name: "upload_id",
                table: "candidate_upload",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<long>(
                name: "location_id",
                table: "candidate",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "job_role_id",
                table: "candidate",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "dependents_under7",
                table: "candidate",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "dependents_above7",
                table: "candidate",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "born_date",
                table: "candidate",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AlterColumn<string>(
                name: "available",
                table: "candidate",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)");

            migrationBuilder.CreateTable(
                name: "upload",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    file_name = table.Column<string>(maxLength: 100, nullable: false),
                    file_path = table.Column<string>(maxLength: 300, nullable: false),
                    uploaded_at = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_upload", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_candidate_upload_upload_id",
                table: "candidate_upload",
                column: "upload_id");

            migrationBuilder.AddForeignKey(
                name: "FK_candidate_job_role_job_role_id",
                table: "candidate",
                column: "job_role_id",
                principalTable: "job_role",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_candidate_location_location_id",
                table: "candidate",
                column: "location_id",
                principalTable: "location",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_candidate_upload_upload_upload_id",
                table: "candidate_upload",
                column: "upload_id",
                principalTable: "upload",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_candidate_job_role_job_role_id",
                table: "candidate");

            migrationBuilder.DropForeignKey(
                name: "FK_candidate_location_location_id",
                table: "candidate");

            migrationBuilder.DropForeignKey(
                name: "FK_candidate_upload_upload_upload_id",
                table: "candidate_upload");

            migrationBuilder.DropTable(
                name: "upload");

            migrationBuilder.DropIndex(
                name: "IX_candidate_upload_upload_id",
                table: "candidate_upload");

            migrationBuilder.DropColumn(
                name: "upload_id",
                table: "candidate_upload");

            migrationBuilder.AddColumn<string>(
                name: "file_name",
                table: "candidate_upload",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "file_path",
                table: "candidate_upload",
                type: "bigint",
                maxLength: 300,
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "uploaded_at",
                table: "candidate_upload",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<long>(
                name: "location_id",
                table: "candidate",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "job_role_id",
                table: "candidate",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "dependents_under7",
                table: "candidate",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "dependents_above7",
                table: "candidate",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "born_date",
                table: "candidate",
                type: "datetime(6)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "available",
                table: "candidate",
                type: "tinyint(1)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_candidate_job_role_job_role_id",
                table: "candidate",
                column: "job_role_id",
                principalTable: "job_role",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_candidate_location_location_id",
                table: "candidate",
                column: "location_id",
                principalTable: "location",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
