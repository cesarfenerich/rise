﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class Adjustments1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "literary_ability",
                table: "candidate",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "candidate_upload",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    candidate_id = table.Column<long>(nullable: false),
                    file_name = table.Column<string>(maxLength: 100, nullable: false),
                    file_path = table.Column<long>(maxLength: 300, nullable: false),
                    uploaded_at = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_candidate_upload", x => x.id);
                    table.ForeignKey(
                        name: "FK_candidate_upload_candidate_candidate_id",
                        column: x => x.candidate_id,
                        principalTable: "candidate",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_candidate_upload_candidate_id",
                table: "candidate_upload",
                column: "candidate_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "candidate_upload");

            migrationBuilder.DropColumn(
                name: "literary_ability",
                table: "candidate");
        }
    }
}
