﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class UserFailedAttempsBlocked : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "blocked",
                table: "user",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "failedAttempts",
                table: "user",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "blocked",
                table: "user");

            migrationBuilder.DropColumn(
                name: "failedAttempts",
                table: "user");
        }
    }
}
