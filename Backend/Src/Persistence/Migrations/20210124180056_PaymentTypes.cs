﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class PaymentTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "current_payment_monthly",
                table: "interview",
                type: "BOOLEAN",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "expectation_payment_monthly",
                table: "interview",
                type: "BOOLEAN",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "current_payment_monthly",
                table: "interview");

            migrationBuilder.DropColumn(
                name: "expectation_payment_monthly",
                table: "interview");
        }
    }
}
