﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class AddingOwnerRelationAtInterviewUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "resume",
                table: "interview_update",
                maxLength: 2000,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(1000) CHARACTER SET utf8mb4",
                oldMaxLength: 1000);

            migrationBuilder.AddColumn<long>(
                name: "owner_id",
                table: "interview_update",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_interview_update_owner_id",
                table: "interview_update",
                column: "owner_id");

            migrationBuilder.AddForeignKey(
                name: "FK_interview_update_owner_owner_id",
                table: "interview_update",
                column: "owner_id",
                principalTable: "owner",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_interview_update_owner_owner_id",
                table: "interview_update");

            migrationBuilder.DropIndex(
                name: "IX_interview_update_owner_id",
                table: "interview_update");

            migrationBuilder.DropColumn(
                name: "owner_id",
                table: "interview_update");

            migrationBuilder.AlterColumn<string>(
                name: "resume",
                table: "interview_update",
                type: "varchar(1000) CHARACTER SET utf8mb4",
                maxLength: 1000,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 2000);
        }
    }
}
