﻿using Domain.Entities;
using Domain.Entities.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
           : base(options)
        { }

        public AppDbContext()
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly);            

            base.OnModelCreating(modelBuilder);
        }      

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #region ConnectionStrings HardCoded (Only for Manual Db Migration Apply)  

            //string connectionString = ConfigurationHelper.DbConnectionString;  

            //LOCAL
            //string connectionString = "Server=localhost;Port=3306;Database=risedb;Uid=root;Pwd=root;";

            //DEV         
            //string connectionString = "Server=risedb.c4z9vgssvt4o.us-east-1.rds.amazonaws.com;Port=3306;Database=risedb;Uid=master;Pwd=E7BRnDATD5ssQ6szFBoi;";


            //PROD
            //string connectionString = "Server=risedb.c4z9vgssvt4o.us-east-1.rds.amazonaws.com;Port=3306;Database=risedb;Uid=master;Pwd=E7BRnDATD5ssQ6szFBoi;";

            //optionsBuilder.UseMySql(connectionString);

            #endregion
        }

        public DbSet<User> User { get; set; }
        public DbSet<Interview> Interview { get; set; }
        public DbSet<InterviewUpdate> InterviewUpdate { get; set; }
        public DbSet<InterviewHistory> InterviewHistory { get; set; }        
        public DbSet<Candidate> Candidate { get; set; }
        public DbSet<Location> Location{ get; set; }        
        public DbSet<CandidateLanguage> CandidateLanguage { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<CandidateTechSkill> CandidateTechSkill { get; set; }
        public DbSet<TechSkill> TechSkill { get; set; }
        public DbSet<CandidateUpload> CandidateUpload { get; set; }
        public DbSet<Upload> Upload { get; set; }
        public DbSet<Log> Log { get; set; }

    }
}
