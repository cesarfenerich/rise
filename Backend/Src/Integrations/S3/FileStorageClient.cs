﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Integrations.S3
{    public class FileStorageClient : IDisposable
    {
        readonly IAmazonS3 s3Client;
        bool disposed = false;

        public FileStorageClient()
        {
            s3Client = new AmazonS3Client(ConfigurationHelper.AccessKey,
                                          ConfigurationHelper.SecretKey,
                                          RegionEndpoint.GetBySystemName(ConfigurationHelper.RegionEndpoint));
        }

        public async Task UploadFileAsync(Stream fileStream, string fileId)
        {
            try
            {
                using var fileTransfer = new TransferUtility(s3Client);
                await fileTransfer.UploadAsync(fileStream,
                                               ConfigurationHelper.FileStorageBucketName,
                                               fileId);
            }
            catch
            {
                throw;
            }
        }

        public async Task<string> DownloadFileAsync(string key)
        {
            try
            {
                var filePath = Path.GetTempFileName();

                using var fileTransfer = new TransferUtility(s3Client);
                await fileTransfer.DownloadAsync(filePath,
                                                 ConfigurationHelper.FileStorageBucketName,
                                                 key);
                return filePath;
            }
            catch
            {
                throw;
            }            
        }

        public Task<string> GenerateFileUrl(string fileName)
        {
            try
            {
                return Task.FromResult($"https://{ConfigurationHelper.FileStorageBucketName}.s3.amazonaws.com/{fileName}");
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                s3Client.Dispose();
            }

            disposed = true;
        }

        ~FileStorageClient()
        {
            Dispose(false);
        }
    }
}
