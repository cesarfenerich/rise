﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integrations.Email
{
    public class MailSettings
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public int? Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public MailSettings()
        {

        }

        public MailSettings(string from, string smtpServer, int? port, string username, string password)
        {
            this.From = from;
            this.SmtpServer = smtpServer;
            this.Port = port;
            this.UserName = username;
            this.Password = password;
        }
    }
}
